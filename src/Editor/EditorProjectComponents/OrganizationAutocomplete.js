import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
//import { ReactComponent as CreateIcon } from "./../../assets/elg-icons/editor/navigation-arrows-down-1.svg";
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/task-checklist-add.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/task-checklist-remove.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
//import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import GenericOrganization from "../EditorGenericComponents/GenericOrganization";
import { generic_organization_obj } from "../Models/GenericModels";
import messages from "./../../config/messages"; 

export default class OrganizationAutocomplete extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValueArray: props.initialValueArray || [] };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValueArray: nextProps.initialValueArray || []
        };
    }

    setValues = (action, index) => {
        const { initialValueArray } = this.state;
        switch (action) {
            case "addArrayItem":
                //initialValueArray.push({ actor_type: "actor_type" });
                const filteredArray = initialValueArray.filter(item => {
                    if (item && item.organization_name) {
                        return item.organization_name["en"];
                    }
                    return false;
                });
                if (filteredArray.length !== initialValueArray.length) {
                    return;//do not add element if there are empty values
                }
                initialValueArray.push(JSON.parse(JSON.stringify(generic_organization_obj)));
                this.props.updateModel_Array(this.props.field, initialValueArray);
                break;
            case "removeArrayItem":
                initialValueArray.splice(index, 1);
                if (initialValueArray.length === 0) {
                    this.props.updateModel_Array(this.props.field, []);
                    return;
                } else {
                    this.props.updateModel_Array(this.props.field, initialValueArray);
                }
                break;
            default:
                break;
        }
        this.setState({ initialValueArray });
        //this.props.updateModel_Array(this.props.field, initialValueArray);
    }

    updateModel = (index, value) => {
        const { initialValueArray } = this.state;
        initialValueArray[index] = value;
        this.setState({ initialValueArray }, this.onBlur);
        //this.props.updateModel_Array(this.props.field, this.state.initialValueArray);
    }

    onBlur = () => {
        if (this.state.initialValueArray.filter(item => item.organization_name && item.organization_name["en"]).length === 0) {
            this.props.updateModel_Array(this.props.field, []);
            return;
        }
        this.props.updateModel_Array(this.props.field, this.state.initialValueArray);
    }

    render() {
        const { initialValueArray } = this.state;
        initialValueArray.length === 0 && initialValueArray.push(JSON.parse(JSON.stringify(generic_organization_obj)));
        //console.log(initialValueArray)
        return <div onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {/*initialValueArray.length === 0 && <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addArrayItem")}><CreateIcon /></Button>*/}
                    {/*initialValueArray.length !== 0 && <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addArrayItem")}><AddCircleOutlineIcon /></Button>*/}
                </Grid>
            </Grid>
            {
                initialValueArray.map((item, index) => {
                    //if (item.actor_type === "actor_type") {
                    //    return <div key={index}><ActorChoice {...this.props.formElements.actor_type} index={index} setActor={this.setActor} /></div>
                    //} else if (item.actor_type === "Organization") {
                    return <div key={index}>
                        <Grid container className="mb2" direction="row" justifyContent="space-between" alignItems="baseline" >
                            {!(item.organization_name && item.organization_name["en"] === "") && <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", index)}>{messages.array_elements_remove}</Button></Grid>
                            </Grid>}

                            <Grid item xs={12}><GenericOrganization {...this.props} index={index} initialValue={item} updateModel={this.updateModel} /></Grid>

                            {(!(item.organization_name && item.organization_name["en"] === "") && ((this.state.initialValueArray.length - 1) === index)) ?
                                <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                    <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", index)}>{messages.array_elements_add}</Button></Grid>
                                </Grid>
                                : <div style={{ paddingBottom: "1em" }}></div>
                            }
                        </Grid>

                    </div>
                    //}
                    //return <div key={index}></div>
                })
            }
        </div>
    }
}