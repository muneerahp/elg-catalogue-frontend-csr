import React from "react";
import axios from "axios";
import validator from 'validator';

import { LOOK_UP_PROJECT_BY_NAME } from "../../config/editorConstants";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import WebsiteList from "../editorCommonComponents/WebsiteList";
import LRIdentifier from "../EditorServiceToolComponents/LRIdentifier";
import { genericProjectObj } from "../Models/ProjectModel";
import { project_identifier_obj } from "../Models/ProjectModel";
import { TextField } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
//import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import ActorTypeAutocomplete from "../editorCommonComponents/ActorTypeAutocomplete"

//const filter = createFilterOptions();

const checkElg = obj => obj.project_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg";

export default class RelatedProject extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: props.initialValue, options: [], loading: false, grantNumberError: false, source: null };
        //this.isUnmound = false;
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            if (this.state.source) {
                this.state.source.cancel("");
            }
        }
    }


    getProjectChoices = (field, value) => {
        if (value.trim().length <= 2) {
            //this.setState({ options: []});
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ loading: true, source: source });
        axios.get(LOOK_UP_PROJECT_BY_NAME(value), { cancelToken: source.token }).then(res => {
            this.setState({ options: res.data, loading: false, source: null })
        }).catch((err) => {
            console.log(err);
            this.setState({ loading: false, options: [], source: null })
        });
    }

    setValues = (action, value) => {
        let { initialValue } = this.state;
        switch (action) {
            case "autoCompleteSelected":
                if (value.similarity) {
                    delete value["similarity"];
                }
                if (value.display_name) {
                    delete value["display_name"];
                }
                if (value.hasOwnProperty('editor-placeholder')) {
                    delete value['editor-placeholder'];
                }
                initialValue = value;
                this.setState({ initialValue, options: [] });
                this.props.updateModel(this.props.index, initialValue);
                break;
            default: break;
        }
        this.setState({ initialValue, options: [] });
    }

    autocompleteTextField = (field) => {
        const activeLanguage = this.props.activeLanguage || "en";
        const scheme_choices = this.props.formStuff.project_identifier.formElements.project_identifier_scheme.choices;
        return <span>
            <Autocomplete className={"wd-100"}
                freeSolo
                clearOnBlur
                autoHighlight
                value={this.state.initialValue}
                loading={this.state.loading}
                onChange={(event, newValue) => {
                    if (!newValue) {
                        return;
                    }
                    if (typeof newValue === "object") {
                        this.setValues("autoCompleteSelected", newValue);
                    } else {
                        this.setObjectGeneral(field, { "en": newValue });
                    }
                }}
                options={this.state.loading ? [] : this.state.options}
                filterOptions={(options, params) => {
                    //const filtered = filter(options, params);
                    const filtered = options;
                    if (!this.state.loading) {
                        if (params.inputValue !== '') {
                            /*filtered.push({
                                ...JSON.parse(JSON.stringify(genericProjectObj)),
                                [field]: { 'en': params.inputValue },
                                display_name: `Missing project? Add "${params.inputValue}"`,
                            });*/
                            filtered.splice(0, 0, {
                                ...JSON.parse(JSON.stringify(genericProjectObj)),
                                [field]: { 'en': params.inputValue },
                                display_name: `Missing ${params.inputValue}? Add "${params.inputValue}"`,
                            });
                        }
                    }
                    if (filtered && filtered.length === 0 && this.state.loading) {
                        return [{ display_name: "Loading please wait...", }]
                    }
                    return filtered;
                }}
                //getOptionLabel={(option) => option.project_name ? Object.values(option[field])[0] : ""}
                getOptionLabel={(option) => option.project_name ? Object.keys(option.project_name).includes("en") ? option.project_name["en"] : Object.values(option.project_name)[0] : ""}
                renderInput={(params) => (
                    <TextField {...params} label={this.props.formStuff[field].label} helperText={this.props.formStuff[field].help_text} variant="outlined"
                        placeholder={this.props.formStuff[field].placeholder ? this.props.formStuff[field].placeholder : ""}
                        //onBlur={(e) => { this.setObjectGeneral(field, { "en": e.target.value }); }}
                        onChange={(e) => { this.getProjectChoices(field, e.target.value) }}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {this.state.loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            )
                        }}
                    />)}
                renderOption={(option, { inputValue }) => {
                    if (option.display_name) {
                        const matches1 = match(option.display_name, inputValue);
                        const parts1 = parse(option.display_name, matches1);
                        return parts1.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                {part.highlight ? '\u00A0' : ''}{part.text}
                            </span>
                        ))
                    }
                    //const matches = match(Object.values(option[field])[0], inputValue);
                    //const parts = parse(Object.values(option[field])[0], matches);
                    const matches = match(option[field][activeLanguage] || Object.values(option[field])[0], inputValue);
                    const parts = parse(option[field][activeLanguage] || Object.values(option[field])[0], matches);
                    return (
                        <div>
                            {
                                option[field] && <div>
                                    {parts.map((part, index) => (
                                        <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                            {part.text}
                                        </span>
                                    ))}

                                </div>
                            }
                            {
                                option.project_identifier && option.project_identifier.map((item, index) => {
                                    return <div key={index}>
                                        {item.project_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg" ? "" :
                                            (`${item.value} - ${scheme_choices.filter(schemeChoice => schemeChoice.value === item.project_identifier_scheme)[0].display_name}`)
                                        }
                                    </div>
                                })
                            }
                            {
                                option.website && option.website.map((website, websiteIndex) => {
                                    return <div key={websiteIndex}>
                                        <span className="info_url">
                                            {<span>{website}</span>}
                                        </span>
                                    </div>
                                })

                            }

                        </div>
                    );
                }
                }
            />
        </span>
    }

    setObjectGeneral = (field, valueObj) => {
        const { initialValue } = this.state;
        initialValue[field] = valueObj;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] });
    }

    updateModel_generic_array = (obj2update, valueArray) => {
        const { initialValue } = this.state;
        if (!initialValue[obj2update]) {
            initialValue[obj2update] = [];
        }
        initialValue[obj2update] = valueArray;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] }, this.onBlur);
    }

    onBlur = () => {
        this.setState({ options: [], loading: false });
        const { initialValue } = this.state;
        this.props.updateModel(this.props.index, initialValue);
    }

    ///TODO validation: string with max_length
    setGrantnumber = (event, index) => {
        const { initialValue } = this.state;
        if (!initialValue.grant_number) {
            initialValue.grant_number = "";
        }
        initialValue.grant_number = event.target.value;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] }, this.onBlur);
    }
    //////  
    blur = (event, max_length) => {
        const valid = validator.isLength(event.target.value, { max: max_length });
        this.setState({ grantNumberError: !valid });
    }



    render() {
        const { grantNumberError, initialValue } = this.state;
        const { help_text: grant_number_help_text, label: grant_number_label, required: grant_number_required, type: grant_number_type, max_length: grant_number_max_length, placeholder: grant_number_placeholder } = GenericSchemaParser.getFormElement("grant_number", this.props.formStuff.grant_number);
        const grant_number_default_value = initialValue.grant_number || "";
        const grantNumberValidationError = "Value should be " + grant_number_max_length + " characters length";

        const hide_help_text = initialValue.hasOwnProperty("pk") ? true : false;

        return <div onBlur={this.onBlur}>
            {/*<TextField
                helperText={GenericSchemaParser.getFormElement("actor_type", this.props.formStuff.actor_type).help_text}
                required={GenericSchemaParser.getFormElement("actor_type", this.props.formStuff.actor_type).required}
                label={GenericSchemaParser.getFormElement("actor_type", this.props.formStuff.actor_type).label}
                disabled={true}
                variant="outlined"
                value={initialValue.actor_type}
            />*/}
            {initialValue.project_name && !initialValue.project_name["en"] ?
                <div>
                    <div>{this.autocompleteTextField("project_name")}</div>
                </div>
                :
                <div className="pb-3 inner--group nested--group">
                    <div onBlur={this.onBlur}>
                        <LanguageSpecificText
                            {...GenericSchemaParser.getFormElement("project_name", this.props.formStuff.project_name)}
                            help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("project_name", this.props.formStuff.project_name).help_text}
                            defaultValueObj={initialValue.project_name || { "en": "" }}
                            field="project_name"
                            disable={initialValue.pk >= 0 ? true : false}
                            setLanguageSpecificText={this.setObjectGeneral}
                        />
                    </div>

                    <div onBlur={this.onBlur}>
                        <LanguageSpecificText
                            {...GenericSchemaParser.getFormElement("project_short_name", this.props.formStuff.project_short_name)}
                            help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("project_short_name", this.props.formStuff.project_short_name).help_text}
                            defaultValueObj={initialValue.project_short_name || { "en": "" }}
                            field="project_short_name"
                            disable={initialValue.pk >= 0 ? true : false}
                            setLanguageSpecificText={this.setObjectGeneral}
                        />
                    </div>

                    {initialValue.pk && ((initialValue.project_identifier.length === 1 && initialValue.project_identifier.some(checkElg)))
                        ?
                        (<></>)
                        :
                        (
                            <div className="inner--group nested--group">
                                <LRIdentifier
                                    key={JSON.stringify(initialValue) + 'generic_project' + this.props.index}
                                    {...GenericSchemaParser.getFormElement("project_identifier", this.props.formStuff.project_identifier)}
                                    identifier_scheme="project_identifier_scheme"
                                    scheme_choices={this.props.formStuff.project_identifier.formElements.project_identifier_scheme.choices}
                                    identifier_scheme_required={this.props.formStuff.project_identifier.formElements.project_identifier_scheme.required}
                                    identifier_scheme_label={this.props.formStuff.project_identifier.formElements.project_identifier_scheme.label}
                                    identifier_scheme_help_text={hide_help_text ? '' : this.props.formStuff.project_identifier.formElements.project_identifier_scheme.help_text}
                                    identifier_value_required={this.props.formStuff.project_identifier.formElements.value.required}
                                    identifier_value_label={this.props.formStuff.project_identifier.formElements.value.label}
                                    identifier_value_placeholder={hide_help_text ? '' : this.props.formStuff.project_identifier.formElements.value.placeholder}
                                    identifier_value_help_text={hide_help_text ? '' : this.props.formStuff.project_identifier.formElements.value.help_text}
                                    identifier_obj={JSON.parse(JSON.stringify(project_identifier_obj))}
                                    className="wd-100"
                                    default_valueArray={initialValue.project_identifier}
                                    field="project_identifier"
                                    disable={initialValue.pk >= 0 ? true : false}
                                    updateModel_Identifier={this.updateModel_generic_array}
                                /> </div>
                        )

                    }

                    <div className="pt-3 pb-3">
                        <WebsiteList
                            label={this.props.formStuff.website.label}
                            help_text={hide_help_text ? '' : this.props.formStuff.website.help_text}
                            placeholder={hide_help_text ? '' : this.props.formStuff.website.placeholder}
                            required={this.props.formStuff.website.required}
                            className="wd-100"
                            default_value_Array={initialValue.website || []}
                            field="website"
                            disable={initialValue.pk >= 0 ? true : false}
                            updateModel_website={this.updateModel_generic_array}
                        />
                    </div>

                    <div className="pt-3 pb-3">
                        <TextField className="wd-100" type={grant_number_type} required={grant_number_required} label={grant_number_label}
                            helperText={hide_help_text ? '' : grantNumberError ? `${grantNumberValidationError}` : grant_number_help_text} variant="outlined" value={grant_number_default_value}
                            placeholder={hide_help_text ? '' : grant_number_placeholder}
                            inputProps={{ name: 'grant_number', maxLength: grant_number_max_length }} onChange={(e) => this.setGrantnumber(e)}
                            onBlur={(e) => this.blur(e, grant_number_max_length)}
                            error={grantNumberError}
                            disabled={initialValue.pk >= 0 ? true : false} />
                    </div>


                    <div className="pt-3 pb-3">
                        <AutocompleteChoicesChips className="wd-100"
                            required={this.props.formStuff.funding_type.required}
                            help_text={hide_help_text ? '' : this.props.formStuff.funding_type.help_text}
                            label={this.props.formStuff.funding_type.label}
                            choices={this.props.formStuff.funding_type.choices}
                            initialValuesArray={initialValue.funding_type || []}
                            field="funding_type"
                            lr_subclass={false}
                            disabled={initialValue.pk >= 0 ? true : false}
                            updateModel_array={this.updateModel_generic_array} />
                    </div>


                    <div>
                        <ActorTypeAutocomplete
                            required={this.props.formStuff.funder.required}
                            help_text={hide_help_text ? '' : this.props.formStuff.funder.formElements.help_text}
                            label={this.props.formStuff.funder.label}
                            formElements={this.props.formStuff.funder.formElements}
                            initialValueArray={initialValue.funder || []}
                            field="funder"
                            disabled={initialValue.pk >= 0 ? true : false}
                            updateModel_Array={this.setObjectGeneral} />
                    </div>

                </div>
            }
        </div>
    }
}