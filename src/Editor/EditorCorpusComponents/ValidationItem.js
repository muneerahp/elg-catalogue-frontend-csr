import React from "react";

import LRAutocomplete from "./LRAutocomplete";
import IsDescribedByAutocomplete from "../EditorServiceToolComponents/IsDescribedByAutocomplete";
import ActorTypeAutocomplete from "../editorCommonComponents/ActorTypeAutocomplete";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";

export default class ValidationItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { validationItem: props.validationItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            validationItem: nextProps.validationItem
        };
    }

    setObjectGeneral = (field, valueObj) => {
        const { validationItem } = this.state;
        validationItem[field] = valueObj;
        this.setState({ validationItem }, this.onBlur);
    }

    updateModel__array = (obj2update, valueArray) => {
        const { validationItem } = this.state;
        if (!validationItem[obj2update]) {
            validationItem[obj2update] = [];
        }
        validationItem[obj2update] = valueArray;
        this.setState({ validationItem }, this.onBlur);
    }

    setSingleSelectListGeneric = (event, selectedObjArray, field) => {
        const { validationItem } = this.state;
        if (selectedObjArray.length === 0) {
            selectedObjArray[0] = { value: null };
        }
        validationItem[field] = selectedObjArray[0].value;
        this.setState({ validationItem }, this.onBlur);
    }

    onBlur = () => {
        const { validationItem } = this.state;
        const exist_validation_type = validationItem.validation_type ? true : false;
        const exist_validation_mode = validationItem.validation_mode ? true : false;
        const exist_validation_extent = validationItem.validation_extent ? true : false;
        const exist_is_validated_by = (validationItem.is_validated_by && validationItem.is_validated_by.filter(item => !item.hasOwnProperty("editor-placeholder")).length) ? true : false;
        const exist_validation_report = (validationItem.validation_report && validationItem.validation_report.filter(item => !item.hasOwnProperty("editor-placeholder")).length) ? true : false;
        const exist_validator = (validationItem.validator && validationItem.validator.filter(item => item.actor_type !== "actor_type" && !item.hasOwnProperty("editor-placeholder")).length) ? true : false;
        const exist_validation_details = validationItem && validationItem.validation_details && validationItem.validation_details.hasOwnProperty("en") && validationItem.validation_details["en"] ? true : false;
        //console.log(exist_validation_type, exist_validation_mode, exist_validation_extent, exist_is_validated_by, exist_validation_report, exist_validator, exist_validation_details);
        if (exist_validation_type === false && exist_validation_mode === false && exist_validation_extent === false && exist_is_validated_by === false && exist_validation_report === false && exist_validator === false && exist_validation_details === false) {
            validationItem["editor-placeholder"] = true;
        } else {
            delete validationItem["editor-placeholder"];
        }
        this.props.updateModel("setvalidationItem", this.props.validationItemIndex, validationItem);
    }

    render() {
        const { validationItem } = this.state;
        //for tool service
        if (this.props.model && this.props.model.described_entity.lr_subclass && this.props.model.described_entity.lr_subclass.lr_type === "ToolService") {
            return <>
                {false && <div className="pt-3 pb-3"> <IsDescribedByAutocomplete model={this.props.model} className="wd-100" {...this.props.formElements.validation_report} initialValueArray={validationItem.validation_report || []} field="validation_report" updateModel_Array={this.updateModel__array} /></div>}
                {false && <div className="pt-3 pb-3"><RecordSelectList className="wd-100"{...this.props.formElements.validation_type} choices={this.props.formElements.validation_type.choices} default_value={validationItem.validation_type} field="validation_type" lr_subclass={false} setSelectedvalue={this.setSingleSelectListGeneric} /></div>}
                {false && <div className="pt-3 pb-3"><RecordSelectList className="wd-100"{...this.props.formElements.validation_extent} choices={this.props.formElements.validation_extent.choices} default_value={validationItem.validation_extent} field="validation_extent" lr_subclass={false} setSelectedvalue={this.setSingleSelectListGeneric} /></div>}
                {false && <div className="pt-3 pb-3"><RecordSelectList className="wd-100"{...this.props.formElements.validation_mode} choices={this.props.formElements.validation_mode.choices} default_value={validationItem.validation_mode} field="validation_mode" lr_subclass={false} setSelectedvalue={this.setSingleSelectListGeneric} /></div>}
                {false && <div className="pt-3 pb-3"><LRAutocomplete {...this.props.formElements.is_validated_by} initialValueArray={validationItem.is_validated_by || []} field="is_validated_by" updateModel_Array={this.updateModel__array} /> </div>}
                {false && <div className="pt-3 pb-3"><ActorTypeAutocomplete  {...this.props.formElements.validator} initialValueArray={validationItem.validator || []} field="validator" updateModel_Array={this.updateModel__array} /></div>}
                {false && <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.validation_details} defaultValueObj={validationItem.validation_details || { "en": "" }} field="validation_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>}
            </>
        }
        //for corpus
        return <>
            <div className="pt-3 pb-3"><LRAutocomplete {...this.props.formElements.is_validated_by} initialValueArray={validationItem.is_validated_by || []} field="is_validated_by" updateModel_Array={this.updateModel__array} /> </div>
            <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.validation_details} defaultValueObj={validationItem.validation_details || { "en": "" }} field="validation_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pt-3 pb-3"><RecordSelectList className="wd-100"{...this.props.formElements.validation_type} choices={this.props.formElements.validation_type.choices} default_value={validationItem.validation_type} field="validation_type" lr_subclass={false} setSelectedvalue={this.setSingleSelectListGeneric} /></div>
            <div className="pt-3 pb-3"><RecordSelectList className="wd-100"{...this.props.formElements.validation_mode} choices={this.props.formElements.validation_mode.choices} default_value={validationItem.validation_mode} field="validation_mode" lr_subclass={false} setSelectedvalue={this.setSingleSelectListGeneric} /></div>
            <div className="pt-3 pb-3"><RecordSelectList className="wd-100"{...this.props.formElements.validation_extent} choices={this.props.formElements.validation_extent.choices} default_value={validationItem.validation_extent} field="validation_extent" lr_subclass={false} setSelectedvalue={this.setSingleSelectListGeneric} /></div>
            <div className="pt-3 pb-3"> <IsDescribedByAutocomplete model={this.props.model} className="wd-100" {...this.props.formElements.validation_report} initialValueArray={validationItem.validation_report || []} field="validation_report" updateModel_Array={this.updateModel__array} /></div>
            <div className="pt-3 pb-3"><ActorTypeAutocomplete  {...this.props.formElements.validator} initialValueArray={validationItem.validator || []} field="validator" updateModel_Array={this.updateModel__array} /></div>
        </>
    }



}