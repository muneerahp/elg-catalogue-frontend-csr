import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { distribution_text_featureObj } from "../Models/CorpusModel";
import DistributionTextItem from "./DistributionTextItem";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";

export default class DistributionTextArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { distribution_text_featureArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            distribution_text_featureArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, distribution_text_featureItemIndex, value) => {
        const { distribution_text_featureArray } = this.state;
        switch (action) {
            case "adddistribution_text_featureItem":
                const filteredArray = distribution_text_featureArray.filter(item => {
                    if (item.data_format && item.data_format.length) {
                    } else {
                        return false;
                    }
                    if (item.size) {
                        if (item.size.filter(size => {
                            if (!(size.amount !== null && Number(size.amount) >= 0)) {
                                return false;
                            } if (!size.size_unit) {
                                return false;
                            }
                            return true;
                        }).length !== item.size.length) {
                            return false;
                        }
                    }
                    return true;
                })
                if (filteredArray.length !== distribution_text_featureArray.length) {
                    return;//do not add element if required fields are blank
                }
                distribution_text_featureArray.push(JSON.parse(JSON.stringify(distribution_text_featureObj)));
                break;
            case "removedistribution_text_featureItem":
                distribution_text_featureArray.splice(distribution_text_featureItemIndex, 1);
                break;
            case "setdistribution_text_featureItem":
                distribution_text_featureArray[distribution_text_featureItemIndex] = value;
                this.props.updateModel_array(this.props.field, distribution_text_featureArray);
                break;
            default: break;
        }
        this.setState({ distribution_text_featureArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.distribution_text_featureArray);
    }

    render() {
        const { distribution_text_featureArray } = this.state;
        distribution_text_featureArray.length === 0 && distribution_text_featureArray.push(JSON.parse(JSON.stringify(distribution_text_featureObj)));
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                {/*<Grid item sm={1}>
                    {distribution_text_featureArray.length === 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setValues("adddistribution_text_featureItem")}>{messages.group_elements_create}</Button>}
    </Grid>*/}
            </Grid>
            {
                distribution_text_featureArray.map((distribution_text_featureItem, distribution_text_featureItemIndex) => {
                    return <div key={distribution_text_featureItemIndex} onBlur={() => this.onBlur()}>
                        {(distribution_text_featureItem.size && distribution_text_featureItem.size.length > 0) &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removedistribution_text_featureItem", distribution_text_featureItemIndex)}>{messages.array_elements_remove}</Button></Tooltip></Grid>
                            </Grid>
                        }
                        {((!distribution_text_featureItem.size && distribution_text_featureItem.size.length === 0) && distribution_text_featureItemIndex !== 0) &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removedistribution_text_featureItem", distribution_text_featureItemIndex)}>{messages.array_elements_remove}</Button></Tooltip></Grid>
                            </Grid>
                        }
                        <DistributionTextItem  {...this.props} distribution_text_featureItem={distribution_text_featureItem} distribution_text_featureItemIndex={distribution_text_featureItemIndex} updateModel={this.setValues} />
                        <div className="mb2">{((distribution_text_featureItem.size && distribution_text_featureItem.size.length > 0) && ((distribution_text_featureArray.length - 1) === distribution_text_featureItemIndex)) ?
                            <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("adddistribution_text_featureItem")}>{messages.array_elements_add}</Button></Tooltip>
                                </Grid>
                            </Grid>
                            : <span></span>
                        }
                        </div>
                        <div style={{ marginTop: "1em" }} />
                    </div>
                })
            }
        </div>
    }
}