import React from "react";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import LanguageModel from "../EditorServiceToolComponents/LanguageModel";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import MediaTypeAutocomplete from "../editorCommonComponents/MediaTypeAutocomplete";
import LanguageSpecificTextList from "../editorCommonComponents/LanguageSpecificTextList";
import AnnotationArray from "./AnnotationArray";
//import LinkToOtherMediaArray from "./LinkToOtherMediaArray";

import { TextField } from "@material-ui/core";
import { audioGenreObj, audio_genre_identifierObj, speechGenreObj, speech_genre_identifierObj } from "../Models/GenericModels";

export default class CorpusAudioPartItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: props.initialValue }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue
        };
    }


    setNumber = (field, value) => {
        if (value.length === 0) {
            value = null;
        } else {
            value = Number(value);
        }
        const { initialValue } = this.state;
        initialValue[field] = value;
        this.setState({ initialValue });
    }

    updateModel__array = (obj2update, valueArray) => {
        const { initialValue } = this.state;
        if (!initialValue[obj2update]) {
            initialValue[obj2update] = [];
        }
        initialValue[obj2update] = valueArray;
        this.setState({ initialValue }, this.onBlur());
    }

    /*setSelectListLingualityType = (event, selectedObjArray) => {
        const { initialValue } = this.state;
        initialValue.linguality_type = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ initialValue });
    }*/

    setSelectListMultiLingualityType = (event, selectedObjArray) => {
        const { initialValue } = this.state;
        initialValue.multilinguality_type = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ initialValue });
    }

    onBlur = () => {
        const { initialValue } = this.state;
        const exists_language = initialValue.language && initialValue.language.filter(l => l.language_tag || l.language_id || l.script_id || l.region_id || (l.variant_id && l.variant_id.length) || (l.language_variety_name && l.language_variety_name.hasOwnProperty("pk") && l.language_variety_name["en"])).length ? true : false;
        const exists_audio_genre = initialValue.audio_genre && initialValue.audio_genre.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        const exists_speech_genre = initialValue.speech_genre && initialValue.speech_genre.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        const exists_annotation = initialValue.annotation && initialValue.annotation.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        //const exists_link_to_other_media = initialValue && initialValue.link_to_other_media.filter(item => !item.hasOwnProperty('editor-placeholder')).length ? true : false;
        const exists_number_of_participants = initialValue.number_of_participants !== null && initialValue.number_of_participants >= 0 ? true : false;
        const exists_multilinguality_type = initialValue.multilinguality_type ? true : false;
        const exist_multilinguality_type_details = initialValue && initialValue.multilinguality_type_details && initialValue.multilinguality_type_details.hasOwnProperty("en") && initialValue.multilinguality_type_details["en"] ? true : false;
        const exist_dialect_accent_of_participants = initialValue.dialect_accent_of_participants && initialValue.dialect_accent_of_participants.length ? true : false;
        const exist_geographic_distribution_of_participants = initialValue.geographic_distribution_of_participants && initialValue.geographic_distribution_of_participants.length ? true : false;
        if (exists_language === false && exists_audio_genre === false && exists_annotation === false && exists_multilinguality_type === false &&
            exist_multilinguality_type_details === false && exists_speech_genre === false && exists_number_of_participants === false && exist_dialect_accent_of_participants === false && exist_geographic_distribution_of_participants === false) {
            initialValue["editor-placeholder"] = true;
        } else {
            delete initialValue["editor-placeholder"];
        }
        this.props.setValues("setPart", this.props.index, initialValue);
    }

    render() {
        const { initialValue } = this.state;
        const { corpus_subclass_default_value } = this.props;

        const showMultilinguality = (initialValue.language && initialValue.language.length >= 2) ? true : false;
        const showAnnotation = ["http://w3id.org/meta-share/meta-share/annotatedCorpus", "http://w3id.org/meta-share/meta-share/annotationsCorpus"].includes(corpus_subclass_default_value);

        if (!showMultilinguality) {
            initialValue.multilinguality_type = null;
            initialValue.multilinguality_type_details = null;
        }

        return <div onBlur={this.onBlur}>
            <div><LanguageModel className="wd-100" group_className={"nested--group"} {...GenericSchemaParser.getFormElement("language", this.props.language)} initialValuesArray={initialValue.language || []} field="language" updateModel_array={this.updateModel__array} /></div>

            {showMultilinguality && <>
                <div className="pt-3 pb-3"><RecordSelectList className="wd-100"{...this.props.multilinguality_type} choices={this.props.multilinguality_type.choices} default_value={initialValue.multilinguality_type} required={showMultilinguality} setSelectedvalue={this.setSelectListMultiLingualityType} /></div>
                <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.multilinguality_type_details} defaultValueObj={initialValue.multilinguality_type_details || { "en": "" }} multiline={true} maxRows={6} field="multilinguality_type_details" setLanguageSpecificText={this.updateModel__array} /></div>
            </>}

            <div>
                <MediaTypeAutocomplete
                    label={this.props.audio_genre.label}
                    help_text={this.props.audio_genre.help_text}
                    category_help_text={GenericSchemaParser.getFormElement("audio_genre", this.props.audio_genre).formElements.category_label.help_text}
                    category_label={GenericSchemaParser.getFormElement("audio_genre", this.props.audio_genre).formElements.category_label.label}
                    category_required={GenericSchemaParser.getFormElement("audio_genre", this.props.audio_genre).formElements.category_label.required}
                    category_type={GenericSchemaParser.getFormElement("audio_genre", this.props.audio_genre).formElements.category_label.type}
                    mediaType_choices={GenericSchemaParser.getFormElement("audio_genre", this.props.audio_genre).formElements.audio_genre_identifier.formElements.audio_genre_classification_scheme.choices}
                    mediaType_help_text={this.props.audio_genre.child.children.audio_genre_identifier.children.audio_genre_classification_scheme.help_text}
                    mediaType_label={this.props.audio_genre.child.children.audio_genre_identifier.children.audio_genre_classification_scheme.label}
                    mediaType_read_only={this.props.audio_genre.child.children.audio_genre_identifier.children.audio_genre_classification_scheme.read_only}
                    mediaType_required={this.props.audio_genre.child.children.audio_genre_identifier.children.audio_genre_classification_scheme.required}
                    value_label={this.props.audio_genre.child.children.audio_genre_identifier.children.value.label}
                    value_required={this.props.audio_genre.child.children.audio_genre_identifier.children.value.required}
                    value_placeholder={this.props.audio_genre.child.children.audio_genre_identifier.children.value.placeholder ? this.props.text_type.child.children.audio_genre_identifier.children.value.placeholder : ""}
                    value_help_text={this.props.audio_genre.child.children.audio_genre_identifier.children.value.help_text}
                    media_type_identifier_label={this.props.audio_genre.child.children.audio_genre_identifier.label}
                    media_type_identifier_help_text={this.props.audio_genre.child.children.audio_genre_identifier.help_text}
                    mediaTypeObj={audioGenreObj}
                    media_type_identifierObj={audio_genre_identifierObj}
                    media_type_identifier="audio_genre_identifier"
                    entityType={"audio_genre"}
                    media_type_classification_scheme="audio_genre_classification_scheme"
                    default_valueArray={initialValue.audio_genre || []} updateModel_MediaType={this.updateModel__array} /></div>
            <div>

                <MediaTypeAutocomplete
                    label={this.props.speech_genre.label}
                    help_text={this.props.speech_genre.help_text}
                    category_help_text={GenericSchemaParser.getFormElement("speech_genre", this.props.speech_genre).formElements.category_label.help_text}
                    category_label={GenericSchemaParser.getFormElement("speech_genre", this.props.speech_genre).formElements.category_label.label}
                    category_required={GenericSchemaParser.getFormElement("speech_genre", this.props.speech_genre).formElements.category_label.required}
                    category_type={GenericSchemaParser.getFormElement("speech_genre", this.props.speech_genre).formElements.category_label.type}
                    mediaType_choices={GenericSchemaParser.getFormElement("speech_genre", this.props.speech_genre).formElements.speech_genre_identifier.formElements.speech_genre_classification_scheme.choices}
                    mediaType_help_text={this.props.speech_genre.child.children.speech_genre_identifier.children.speech_genre_classification_scheme.help_text}
                    mediaType_label={this.props.speech_genre.child.children.speech_genre_identifier.children.speech_genre_classification_scheme.label}
                    mediaType_read_only={this.props.speech_genre.child.children.speech_genre_identifier.children.speech_genre_classification_scheme.read_only}
                    mediaType_required={this.props.speech_genre.child.children.speech_genre_identifier.children.speech_genre_classification_scheme.required}
                    value_label={this.props.speech_genre.child.children.speech_genre_identifier.children.value.label}
                    value_required={this.props.speech_genre.child.children.speech_genre_identifier.children.value.required}
                    media_type_identifier_label={this.props.speech_genre.child.children.speech_genre_identifier.label}
                    media_type_identifier_help_text={this.props.speech_genre.child.children.speech_genre_identifier.help_text}
                    mediaTypeObj={speechGenreObj}
                    media_type_identifierObj={speech_genre_identifierObj}
                    media_type_identifier="speech_genre_identifier"
                    entityType={"speech_genre"}
                    media_type_classification_scheme="speech_genre_classification_scheme"
                    default_valueArray={initialValue.speech_genre || []} updateModel_MediaType={this.updateModel__array} />
            </div>


            <div className="pt-3 pb-3">
                <TextField
                    className="wd-100"
                    variant="outlined"
                    type="number"
                    required={this.props.number_of_participants.required}
                    disabled={this.props.number_of_participants.read_only}
                    label={this.props.number_of_participants.label}
                    helperText={this.props.number_of_participants.help_text}
                    value={initialValue.number_of_participants === null ? "" : initialValue.number_of_participants}
                    onChange={(e) => { this.setNumber("number_of_participants", e.target.value) }}
                    inputProps={{ min: `${this.props.number_of_participants.min_value}`, max: `${this.props.number_of_participants.max_value}`, step: "1" }}
                />
            </div>
            <div><LanguageSpecificTextList {...this.props.dialect_accent_of_participants} default_valueArray={initialValue.dialect_accent_of_participants || []} field="dialect_accent_of_participants" setLanguageSpecifictextList={this.updateModel__array} /></div>
            <div><LanguageSpecificTextList  {...this.props.geographic_distribution_of_participants} default_valueArray={initialValue.geographic_distribution_of_participants || []} field="geographic_distribution_of_participants" setLanguageSpecifictextList={this.updateModel__array} /></div>

            {showAnnotation && <div><AnnotationArray {...GenericSchemaParser.getFormElement("annotation", this.props.annotation)} initialValueArray={initialValue.annotation || []} field="annotation" updateModel_array={this.updateModel__array} /></div>}
            {/*<div className="pt-3 pb-3"><LinkToOtherMediaArray {...GenericSchemaParser.getFormElement("link_to_other_media", this.props.link_to_other_media)} initialValueArray={initialValue.link_to_other_media || []} field="link_to_other_media" updateModel_array={this.updateModel__array} /></div>*/}

        </div>

    }



}