import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import { sizeObj } from "../Models/CorpusModel";
import SizeItem from "./SizeItem";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";

export default class SizeArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { sizeArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            sizeArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, sizeItemIndex, value) => {
        const { sizeArray } = this.state;
        switch (action) {
            case "addsizeItem":
                sizeArray.push(JSON.parse(JSON.stringify(sizeObj)));
                break;
            case "removesizeItem":
                sizeArray.splice(sizeItemIndex, 1);
                break;
            case "setsizeItem":
                sizeArray[sizeItemIndex] = value;
                this.props.updateModel_array(this.props.field, sizeArray);
                break;
            default: break;
        }
        this.setState({ sizeArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.sizeArray);
    }

    render() {
        const { sizeArray } = this.state;
        //console.log(this.props)
        sizeArray.length === 0 && sizeArray.push(JSON.parse(JSON.stringify(sizeObj)));
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                {/*<Grid item sm={1}>
                    <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addsizeItem")}><AddCircleOutlineIcon className="xsmall-icon"/></Button>      
                </Grid>*/}
            </Grid>
            {
                sizeArray.map((sizeItem, sizeItemIndex) => {
                    return <div key={sizeItemIndex} onBlur={() => this.onBlur()}>
                        <Grid container direction="row" alignItems="center" justifyContent="flex-start" spacing={1} >
                            <Grid item xs={10}>
                                <SizeItem  {...this.props} sizeItem={sizeItem} sizeItemIndex={sizeItemIndex} updateModel={this.setValues} />
                            </Grid>

                            <Grid item xs={2}>
                                <Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button onClick={(e) => this.setValues("removesizeItem", sizeItemIndex)}><RemoveCircleOutlineIcon className="small-icon" /></Button></Tooltip>
                                <Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button onClick={(e) => this.setValues("addsizeItem")}><AddCircleOutlineIcon className="small-icon" /></Button></Tooltip>
                            </Grid>

                        </Grid>

                        <div style={{ marginTop: "1em" }} />
                    </div>
                })
            }
        </div>
    }
}