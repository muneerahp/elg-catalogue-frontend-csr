import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import { annotationObj } from "../Models/LrModel";
import AnnotationItem from "./AnnotationItem";
import messages from "./../../config/messages";
import Tooltip from '@material-ui/core/Tooltip';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';

export default class AnnotationArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { annotationArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            annotationArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, annotationItemIndex, value) => {
        const { annotationArray } = this.state;
        switch (action) {
            case "addannotationItem":
                annotationArray.push(JSON.parse(JSON.stringify(annotationObj)));
                break;
            case "removeannotationItem":
                annotationArray.splice(annotationItemIndex, 1);
                break;
            case "setannotationItem":
                annotationArray[annotationItemIndex] = value;
                this.props.updateModel_array(this.props.field, annotationArray);
                break;
            default: break;
        }
        this.setState({ annotationArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.annotationArray);
    }

    removeWarning = (e, index) => {
        e.stopPropagation();
        confirmAlert({
            message: messages.editor_remove_annotation(index),
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.setValues("removeannotationItem", index)
                },
                {
                    label: 'No',
                    onClick: () => { }
                }
            ]
        });
    }

    render() {
        const { annotationArray } = this.state;
        //annotationArray.length === 0 && annotationArray.push(JSON.parse(JSON.stringify(annotationObj)));
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {annotationArray.length === 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setValues("addannotationItem")}>{messages.group_elements_create}</Button>}
                </Grid>
            </Grid>
            {
                annotationArray.map((annotationItem, annotationItemIndex) => {
                    return <div key={annotationItemIndex} onBlur={() => this.onBlur()}>
                        <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                            <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, annotationItemIndex)}>{messages.array_elements_remove}</Button></Tooltip></Grid>
                        </Grid>

                        <AnnotationItem  {...this.props} annotationItem={annotationItem} annotationItemIndex={annotationItemIndex} updateModel={this.setValues} />
                        <div className="mb2 pt-3">
                            {((annotationItem.annotation_type && annotationItem.annotation_type.length > 0) && ((annotationArray.length - 1) === annotationItemIndex)) ?
                                <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                    <Grid item><Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addannotationItem")}>{messages.array_elements_add}</Button></Tooltip>
                                    </Grid>
                                </Grid>
                                : <span></span>
                            }
                        </div>
                        <div style={{ marginTop: "1em" }} />
                    </div>
                })
            }
        </div>
    }
}