import React from "react";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import LRAutocomplete from "./LRAutocomplete";
import RelationArray from "./RelationArray";
import LRAutocompleteSingle from "./LRAutocompleteSingle";
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';

const options = [
    { value: "replaces", label: "replaces" },
    { value: "is_version_of", label: "is version of" },
    { value: "is_part_of", label: "is part of" },
    { value: "is_similar_to", label: "is similar to" },
    { value: "relation", label: "relation" }
  ];

 

export default class LRDropDown extends React.Component {
    constructor(props) {
        super(props);
        this.state = { selectedOption: "" };
        this.handleChange = this.handleChange.bind(this);
    }
 
    handleChange = (e) => {
        const {selectedOption}  = this.state;
        this.setState({ selectedOption: e.target.value });   
        console.log(e)      
      };

    ///
    setObjectGeneral = (field, valueObj) => {
        const { model } = this.props;
        model.described_entity[field] = valueObj;
        this.props.updateModel(model);
    }
    ///
    updateModel_generic_array = (obj2update, valueArray, lr_subclass) => {
        const { model } = this.props;
        if (lr_subclass) {
            if (!model.described_entity.lr_subclass[obj2update]) {
                model.described_entity.lr_subclass[obj2update] = [];
            }
            model.described_entity.lr_subclass[obj2update] = valueArray;
            this.props.updateModel(model);
        } else {
            if (!model.described_entity[obj2update]) {
                model.described_entity[obj2update] = [];
            }
            model.described_entity[obj2update] = valueArray;
            this.props.updateModel(model);
        }
    }
    ///


    render() {         
        const { model , data } = this.props;
        const { selectedOption } = this.state;
        return <>
        <Typography variant="h3" className="section-links" >Add one or more relations</Typography>

        <FormControl variant="outlined" required={false}  className="wd-100">
        <InputLabel htmlFor="select-native-simple">related LRTs</InputLabel>
        <Select             
              value={selectedOption}
              onChange={this.handleChange}              
              native
              placeholder="Select a relation type..."              
              >
        <option aria-label="None" value="" />
          {options.map((item, index) => <option key={index} value={item.value}>{item.label}</option>)}          
        </Select>
        <FormHelperText>add some help text here</FormHelperText>
        </FormControl>

        {(selectedOption && selectedOption === "replaces") &&  
            <div><LRAutocomplete {...GenericSchemaParser.getFormElement("replaces", data.replaces)} initialValueArray={model.described_entity.replaces || []} field="replaces" updateModel_Array={this.setObjectGeneral} /> </div>
        }  

        {(selectedOption && selectedOption === "is_version_of") && <div> <LRAutocompleteSingle className="wd-100" {...GenericSchemaParser.getFormElement("is_version_of", data.is_version_of)}
                label={GenericSchemaParser.getFormElement("is_version_of", data.is_version_of).label}
                help_text={GenericSchemaParser.getFormElement("is_version_of", data.is_version_of).help_text}
                formStuff={GenericSchemaParser.getFormElement("is_version_of", data.is_version_of).formElements}
                initialValue={model.described_entity.is_version_of || {}}
                index="is_version_of" field="is_version_of" updateModel={this.setObjectGeneral} /> {/**notice the index prop instead of field!!! */}
            </div>}
        {(selectedOption && selectedOption === "is_part_of")   &&
            <div><LRAutocomplete {...GenericSchemaParser.getFormElement("is_part_of", data.is_part_of)} initialValueArray={model.described_entity.is_part_of || []} field="is_part_of" updateModel_Array={this.setObjectGeneral} /> </div>
        }

        {(selectedOption && selectedOption === "is_similar_to") &&
            <div><LRAutocomplete {...GenericSchemaParser.getFormElement("is_similar_to", data.is_similar_to)} initialValueArray={model.described_entity.is_similar_to || []} field="is_similar_to" updateModel_Array={this.setObjectGeneral} /> </div>
        }

        {(selectedOption && selectedOption === "relation") &&
            <div><RelationArray {...GenericSchemaParser.getFormElement("relation", data.relation)} initialValueArray={model.described_entity.relation || []} field="relation" updateModel_array={this.updateModel_generic_array} /></div>
        }

        </>
    }
}