import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import LdSubclassGrammarItem from "./LdSubclassGrammarItem";
import LdSubclassMLModelItem from "./LdSubclassMLModelItem";
import { ldGrammarObj, ldMLModelObj } from "../Models/LdModel";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import { SUBCLASS_TYPE_MODEL, SUBCLASS_TYPE_GRAMMAR, SUBCLASS_TYPE_OTHER, SUBCLASS_TYPE_UNSPECIFIED } from "../../config/editorConstants"


function PartChoice(props) {
    return <div>
        <div className="pt-3 pb-3"><RecordSelectList className="wd-100" {...props} required={true} default_value={props.default_value}
            //choices={["grammar", "ML model", "n-gram model"].map((item) => { return { "display_name": item, "value": item } })}
            choices={props.subclass.ld_subclass.choices}
            setSelectedvalue={props.setPart} />
        </div>
    </div>
}

export default class LdModelSubclass extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            initialValue: props.initialValue || { ld_subclass_type: "ld_subclass_type" },
            expanded: { isExpanded: true }
        };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue || { ld_subclass_type: "ld_subclass_type" }
        };
    }

    setValues = (action, value) => {
        let { initialValue } = this.state;
        switch (action) {
            case "removeItem":
                initialValue = null;
                this.setState({ initialValue });
                this.setState({ expanded: { isExpanded: true } });
                this.props.updateModel(this.props.field, initialValue, "");
                break;
            default: break;
        }
        this.setState({ initialValue });
    }

    setPart = (e, val) => {
        let { initialValue } = this.state;
        let value = {};
        val = (val && val.length > 0) ? val : [{ value: "", display_value: "" }]
        if (val[0].value === SUBCLASS_TYPE_GRAMMAR) {
            value = JSON.parse(JSON.stringify(ldGrammarObj));
        } else if (val[0].value === SUBCLASS_TYPE_MODEL) {
            value = JSON.parse(JSON.stringify(ldMLModelObj));
        } else if (val[0].value === SUBCLASS_TYPE_OTHER) {
            this.props.updateModel(this.props.field, null, val[0].value);
            return;
        } else if (val[0].value === SUBCLASS_TYPE_UNSPECIFIED) {
            this.props.updateModel(this.props.field, null, val[0].value);
            return;
        } else {
            //console.log("not a valid value");
            this.props.updateModel(this.props.field, null, val[0].value);
            return;
        }
        initialValue = value;
        this.setState({ initialValue });
        this.props.updateModel(this.props.field, initialValue, val[0].value);
    }

    updateModel = (value) => {
        let { initialValue } = this.state;
        initialValue = value;
        this.setState({ initialValue });
        this.props.updateModel(this.props.field, this.state.initialValue, this.props.lr_subclass);
    }

    onBlur = () => {
        const valueObj = this.state.initialValue;
        this.props.updateModel(this.props.field, valueObj, this.props.lr_subclass);
    }

    removeWarning = (e, ld_subclass_type) => {
        e.stopPropagation();
        confirmAlert({
            //title: 'Confirm to remove Corpus Part',
            message: messages.editor_remove_ld_subclass_type(ld_subclass_type),
            buttons: [
                {
                    label: 'Yes',
                    onClick: (e) => this.setValues("removeItem")
                },
                {
                    label: 'No',
                    onClick: () => { }
                }
            ]
        });
    }


    render() {
        const { initialValue } = this.state;
        const front_end_grammar_display_value = "Grammar";
        const front_end_ml_display_value = "Model";

        return <div className="mb2" onBlur={this.onBlur}>
            {false && <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={10}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
            </Grid>}

            {
                ((this.props.initialValue_ld_subclass_type && [SUBCLASS_TYPE_OTHER, SUBCLASS_TYPE_UNSPECIFIED].includes(this.props.initialValue_ld_subclass_type)) ||
                    (!this.props.initialValue_ld_subclass_type)
                ) &&
                <div>
                    {!this.props.disabled && <PartChoice {...this.props} {...this.props.formElements.ld_subclass_type} default_value={this.props.initialValue_ld_subclass_type || ""} setPart={this.setPart} />}
                </div>
            }

            {this.props.initialValue_ld_subclass_type === SUBCLASS_TYPE_GRAMMAR &&

                <div>
                    <Accordion className="FunctionBox--accordions--editor" expanded={this.state.expanded && this.state.expanded.isExpanded} onChange={(e) => {
                        if (this.state.expanded) {
                            this.setState({ expanded: { isExpanded: !this.state.expanded.isExpanded } }, this.onBlur)
                        } else {
                            this.setState({ expanded: { isExpanded: true } }, this.onBlur)
                        }
                    }
                    }>
                        <AccordionSummary expandIcon={<ExpandMoreIcon className="purple--font" />} aria-controls="panel1a-content" id="panel1a-header" className="FunctionBox--accordions__editortitle">
                            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                                <Grid item sm={10}>
                                    <Typography variant="h4" className="section-links" >{front_end_grammar_display_value}</Typography>
                                </Grid>
                                {false && <Grid item sm={2}>
                                    {!this.props.disabled && <Tooltip title={`${messages.array_elements_remove} ${front_end_grammar_display_value}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, initialValue.ld_subclass_type)}>{`${messages.array_elements_remove} ${front_end_grammar_display_value}`}</Button></Tooltip>}
                                </Grid>}
                            </Grid>
                        </AccordionSummary>
                        <AccordionDetails>
                            <LdSubclassGrammarItem {...this.props.grammarPart} initialValue={initialValue} setValues={this.updateModel} />
                        </AccordionDetails>
                    </Accordion>
                    {/*just keep for reference in case we need this again ... <div className="mb2 pt1">
                            <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" spacing={1}>
                                <Grid item>
                                    {!this.props.disabled && <Tooltip title={`${messages.array_elements_remove} ${front_end_grammar_display_value}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, initialValue.ld_subclass_type)}>{`${messages.array_elements_remove} ${front_end_grammar_display_value}`}</Button></Tooltip>}
                                </Grid>
                                
                            </Grid>
                        </div>*/}
                </div>
            }

            {this.props.initialValue_ld_subclass_type === SUBCLASS_TYPE_MODEL &&

                <div>
                    <Accordion className="FunctionBox--accordions--editor" expanded={this.state.expanded && this.state.expanded.isExpanded} onChange={() => {
                        if (this.state.expanded) {
                            this.setState({ expanded: { isExpanded: !this.state.expanded.isExpanded } }, this.onBlur)
                        } else {
                            this.setState({ expanded: { isExpanded: true } }, this.onBlur)
                        }
                    }
                    }>
                        <AccordionSummary expandIcon={<ExpandMoreIcon className="purple--font" />} aria-controls="panel1a-content" id="panel1a-header" className="FunctionBox--accordions__editortitle">

                            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                                <Grid item sm={10}>
                                    <Typography variant="h4" className="section-links" >{front_end_ml_display_value}</Typography>
                                </Grid>
                                {false && <Grid item sm={2}>
                                    {!this.props.disabled && <Tooltip title={`${messages.array_elements_remove} ${front_end_ml_display_value}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, initialValue.ld_subclass_type)}>{`${messages.array_elements_remove} ${front_end_ml_display_value}`}</Button></Tooltip>}
                                </Grid>}
                            </Grid>
                        </AccordionSummary>
                        <AccordionDetails>
                            <LdSubclassMLModelItem  {...this.props.mlPart} initialValue={initialValue} setValues={this.updateModel} />
                        </AccordionDetails>
                    </Accordion>

                </div>
            }

        </div>
    }
}