import React from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
//import { ReactComponent as CreateIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
import { organization_identifier_obj } from "../Models/OrganizationModel";
import messages from "./../../config/messages";

export default class OrganizationIdentifier extends React.Component {

    constructor(props) {
        super(props);
        this.state = { organization_identifier: this.props.default_valueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            organization_identifier: nextProps.default_valueArray || [],
        };
    }

    setIdentifier = (event, identifierIndex, buttonAction, identifierSelectionObjArray) => {
        //const organization_identifier = this.props.default_valueArray;
        const { organization_identifier } = this.state;
        const action = (event && event.target.name) || buttonAction;
        const organization_identifier_obj_2_proccess = organization_identifier[identifierIndex] || {};
        switch (action) {
            case "organization_identifier_value":
                organization_identifier_obj_2_proccess.value = event.target.value;
                organization_identifier[identifierIndex] = { ...organization_identifier_obj_2_proccess };
                break;
            case "organization_identifier_selection":
                organization_identifier_obj_2_proccess.organization_identifier_scheme = identifierSelectionObjArray[0].value;
                organization_identifier[identifierIndex] = { ...organization_identifier_obj_2_proccess };
                break;
            case "addIdentifier":
                if (organization_identifier.filter(item => item.value).length !== organization_identifier.length ||
                    organization_identifier.filter(item => item.organization_identifier_scheme).length !== organization_identifier.length) {
                    return;//don't add a new identifier, if there are blanks in others identifiers
                }
                organization_identifier.push(JSON.parse(JSON.stringify(organization_identifier_obj)));
                break;
            case "removeIdentifier":
                organization_identifier.splice(identifierIndex, 1);
                this.props.updateModel_Identifier(this.props.field, organization_identifier);
                break;
            default: break;
        }
        //this.props.updateModel_Identifier(this.props.field, organization_identifier);
        this.setState({ organization_identifier })
    }

    onBlur = () => {
        this.props.updateModel_Identifier(this.props.field, this.state.organization_identifier);
    }


    render() {
        const { label, help_text, required, disable,
            //default_valueArray,type,organization_identifier_value_max_length,pk_organization_identifier_required, pk_organization_identifier_read_only, pk_organization_identifier_label, pk_organization_identifier_type,
            organization_identifier_scheme_choices, organization_identifier_scheme_help_text, organization_identifier_scheme_label, organization_identifier_scheme_required, organization_identifier_scheme_type,
            organization_identifier_value_label, organization_identifier_value_required, organization_identifier_value_help_text, organization_identifier_value_type, organization_identifier_value_placeholder
            //organization_identifier_scheme_read_only,organization_identifier_value_read_only
        } = this.props;
        const default_valueArray = this.state.organization_identifier;
        required && default_valueArray.length === 0 && default_valueArray.push(JSON.parse(JSON.stringify(organization_identifier_obj)));
        return <div className="pb-3 inner--group nested--group" onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" >
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{label} </Typography>
                    <Typography className="section-links" >{help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {default_valueArray === 0 && <Button disabled={disable} className="inner-link-default--purple" onClick={(e) => this.setIdentifier(e, null, "addIdentifier")}>{messages.group_elements_create}</Button>}
                    {default_valueArray.filter(identifier => identifier.organization_identifier_scheme !== "http://w3id.org/meta-share/meta-share/elg").length === 0 && <Button disabled={disable} className="inner-link-default--purple" onClick={(e) => this.setIdentifier(e, null, "addIdentifier")}>{messages.group_elements_create}</Button>}
                </Grid>
            </Grid>
            {
                default_valueArray.map((identifier, identifierIndex) => {
                    const default_value = identifier.value || "";
                    const default_organization_scheme_choice = organization_identifier_scheme_choices.filter(choice => choice.value === identifier.organization_identifier_scheme).length > 0 ? organization_identifier_scheme_choices.filter(choice => choice.value === identifier.organization_identifier_scheme)[0].display_name : "";
                    if (default_organization_scheme_choice === "ELG") {
                        return <div key={identifierIndex}></div>
                    }
                    return <div key={identifierIndex} className="pt-3 pb-3">
                        <Grid container spacing={1} direction="row" justifyContent="space-between" alignItems="baseline" >
                            <Grid item xs={4}>
                                <TextField className="wd-100" type={organization_identifier_scheme_type} required={organization_identifier_scheme_required} label={organization_identifier_scheme_label} helperText={organization_identifier_scheme_help_text} variant="outlined"
                                    disabled={disable}
                                    select
                                    //error={default_organization_scheme_choice === ""}
                                    value={default_organization_scheme_choice}
                                    inputProps={{ name: 'organization_identifier_selection' }}
                                    onChange={(e) => this.setIdentifier(e, identifierIndex, null, organization_identifier_scheme_choices.filter(organization_scheme_choice => organization_scheme_choice.display_name === e.target.value))}
                                >
                                    {
                                        organization_identifier_scheme_choices.filter(option => option.value !== "http://w3id.org/meta-share/meta-share/elg").map((organization_scheme_choice, organization_scheme_choice_index) => (
                                            <MenuItem key={organization_scheme_choice_index} value={organization_scheme_choice.display_name}>
                                                {organization_scheme_choice.display_name}
                                            </MenuItem>
                                        ))
                                    }
                                </TextField>
                            </Grid>
                            <Grid item xs={6}>
                                <TextField className="wd-100" type={organization_identifier_value_type} required={organization_identifier_value_required} label={organization_identifier_value_label} variant="outlined"
                                    //error={default_value === ""}
                                    disabled={disable}
                                    value={default_value}
                                    helperText={organization_identifier_value_help_text}
                                    placeholder={organization_identifier_value_placeholder ? organization_identifier_value_placeholder : ""}
                                    inputProps={{ name: 'organization_identifier_value' }}
                                    onChange={(e) => this.setIdentifier(e, identifierIndex)}
                                />
                            </Grid>

                            <Grid item xs={2} container direction="row" justifyContent="flex-start" alignItems="baseline" >
                                {((default_value !== "" || default_organization_scheme_choice !== "") && ((default_valueArray.length - 1) === identifierIndex)) ?
                                    <Grid item><Button disabled={disable} onClick={(e) => this.setIdentifier(e, identifierIndex, "addIdentifier")}><AddCircleOutlineIcon className="small-icon" /></Button></Grid>
                                    :
                                    <Grid item><span></span></Grid>
                                }
                                <Grid item>{<Button disabled={disable} onClick={(e) => this.setIdentifier(e, identifierIndex, "removeIdentifier")}><RemoveCircleOutlineIcon className="small-icon" /></Button>}</Grid>
                            </Grid>

                        </Grid>

                    </div>
                })
            }
        </div>
    }
}