import React from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
//import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
//import { ReactComponent as EditIcon } from "./../../assets/elg-icons/editor/pencil.svg";
import { ReactComponent as LockIcon } from "./../../assets/elg-icons/editor/lock-1.svg";
import TextField from '@material-ui/core/TextField';
import Tooltip from '@material-ui/core/Tooltip';
import CircularProgress from '@material-ui/core/CircularProgress';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { MATCH_ORGANIZATION_BY_NAME } from "../../config/editorConstants";
import { AUTHENTICATED_KEYCLOAK_USER_USERNAME } from "../../config/constants";
//import { AUTHENTICATED_KEYCLOAK_USER_USERNAME, AUTHENTICATED_KEYCLOAK_USER_ROLES } from "../../config/constants";
//import Can from "../../UserManagement/Can";
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import messages from "./../../config/messages";

class LookupOrganizationName extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [], loading: false, display: true, lookupName: "", finishedLookup: false,
            choose_Divsion_Or_Organization: null, division_name: ""
        };
        this.timeout = 0;
    }

    resetState = () => {
        this.setState({
            data: [], loading: false, display: true, lookupName: "", finishedLookup: false,
            choose_Divsion_Or_Organization: null, division_name: ""
        });
    }

    IfUserIsOwner = (item) => {
        const { keycloak } = this.props;
        if ((item.curator === AUTHENTICATED_KEYCLOAK_USER_USERNAME(keycloak))) { //&& ((item.status === "g")
            return true;
        } else {
            return false;

        }
    }

    lookUpByName = (e) => {
        const userInput = (e && e.target) ? e.target.value : "";
        this.setState({ lookupName: userInput });
        if (userInput.trim().length <= 2) {
            this.setState({ data: [] });
            return;
        }
        if (this.timeout) clearTimeout(this.timeout);
        this.setState({ loading: true });
        this.timeout = setTimeout(() => {
            if (this.state.lookupName && this.state.lookupName.length >= 3) {
                axios.get(MATCH_ORGANIZATION_BY_NAME(userInput))
                    .then((res) => {
                        this.setState({ data: res.data, loading: false })
                    }).catch((err) => {
                        this.setState({ data: [], loading: false })
                        console.log(err);
                    });
            } else {
                this.setState({ loading: false });
            }
        }, 600);
    }

    lookUpByNameSearchButton = (userInput) => {
        this.setState({ lookupName: userInput, finishedLookup: false });
        if (userInput.trim().length <= 2) {
            this.setState({ data: [] });
            return;
        }

        this.setState({ loading: true });
        if (this.state.lookupName && this.state.lookupName.length >= 3) {
            axios.get(MATCH_ORGANIZATION_BY_NAME(userInput))
                .then((res) => {
                    this.setState({ data: res.data, loading: false, finishedLookup: true })
                }).catch((err) => {
                    this.setState({ data: [], loading: false, finishedLookup: true })
                    console.log(err);
                });
        } else {
            this.setState({ loading: false, finishedLookup: true });
        }

    }

    lookUpByDivisionSearchButton = (division_nameInput) => {
        this.setState({ division_name: division_nameInput, finishedLookup: false });
        if (division_nameInput.trim().length <= 2) {
            this.setState({ data: [] });
            return;
        }
        this.setState({ loading: true });
        if (this.state.division_name && this.state.division_name.length >= 3) {
            axios.get(MATCH_ORGANIZATION_BY_NAME(division_nameInput))
                .then((res) => {
                    this.setState({ data: res.data, loading: false, finishedLookup: true })
                }).catch((err) => {
                    this.setState({ data: [], loading: false, finishedLookup: true })
                    console.log(err);
                });
        } else {
            this.setState({ loading: false, finishedLookup: true });
        }

    }


    getList = () => {
        /*if (this.state.data.length === 0 && this.state.lookupName && this.state.lookupName.length >= 3 && !this.state.loading) {
            return <h6>{messages.lookup_nomatch}</h6>
        }*/
        if (this.state.finishedLookup && this.state.data.length === 0 && this.state.lookupName && this.state.lookupName.length >= 3 && !this.state.loading) {
            return <h6>{messages.lookup_nomatch}</h6>
        }
        return <Grid container direction="row" justifyContent="space-between" alignItems="center" spacing={2} className="mt1 mb2">
            {this.state.data.map((item, index) => {
                return <Grid item container justifyContent="flex-start" alignItems="center" className="padding5 bottom-border" key={index}>
                    {item.full_metadata_record ? this.showFullRecord(item) : this.showGenericRecord(item)} </Grid>
            })}
        </Grid>
    }

    showFullRecord = (item) => {
        const url = item.metadata_record;
        //const user_roles = AUTHENTICATED_KEYCLOAK_USER_ROLES(this.props.keycloak);
        //console.log (user_roles.includes("provider")) 
        var n = url.indexOf("/metadatarecord/");
        const pk = url.substring(n + 16, url.length - 2);
        const elg_record_url = `${window.location.origin}/catalogue/organization/${pk}`;
        const parent = this.getParentFull(item);
        function formatParent() {
            return <strong><em className="" style={{}}>{parent ? `${parent} > ` : ""}</em></strong>;
        }
        return <>

            {(item.status === "p") && <Grid item>{formatParent()}<a target="_blank" rel="noopener noreferrer" href={elg_record_url}>{item.organization_name["en"]}</a></Grid>}
            {(item.status === "g") && <><Grid item sm={10}> {formatParent()}{item.organization_name["en"]} </Grid>
                <Grid item sm={2}> <Tooltip title={messages.lookup_lock_tooltip}><Button className="inner-link-default-editor--teal"><LockIcon className="xsmall-icon" /></Button></Tooltip></Grid></>}
            {(item.status === "i" || item.status === "d") &&
                (this.IfUserIsOwner(item) ?
                    <>
                        <Grid item sm={10}>
                            <div>{formatParent()}<a target="_self" rel="noopener noreferrer" href={elg_record_url}>{item.organization_name["en"]}</a></div>
                            {item.metadata_record.website &&
                                <div><Typography className="bold-p--id">Website</Typography><a target="_blank" rel="noopener noreferrer" href={item.metadata_record.website}>{item.metadata_record.website}</a></div>}
                            {!item.metadata_record.website && <div><Typography className="bold-p--id">Website</Typography>empty</div>}
                        </Grid>


                        <Grid item sm={2}><Tooltip title={messages.lookup_edit_tooltip}><Button className="inner-link-default-editor--teal" onClick={() => { this.editFullRecord(pk) }}>{messages.lookup_edit_button}</Button></Tooltip></Grid> </>
                    :
                    <>
                        <Grid item sm={10}> {formatParent()}{item.organization_name["en"]} </Grid>
                        <Grid item sm={2}> <Tooltip title={messages.lookup_lock_tooltip}><Button className="inner-link-default-editor--teal"><LockIcon className="xsmall-icon" /></Button></Tooltip></Grid>
                    </>

                )
            }
            {item.status === "u" &&
                <><Grid item sm={10}> {formatParent()}{item.organization_name["en"]} </Grid>
                    <Grid item sm={2}>
                        <Tooltip title={messages.lookup_lock_unpublish_tooltip}><Button className="inner-link-default-editor--teal"><LockIcon className="xsmall-icon" /></Button></Tooltip>
                    </Grid>
                </>
            }
        </>
    }

    showGenericRecord = (item) => {
        const parent = this.getParentGeneric(item);
        function formatParent() {
            return <strong><em className="" style={{}}>{parent ? `${parent} > ` : ""}</em></strong>;
        }
        return <>
            <Grid item sm={10}>
                <div>{formatParent()} {item.organization_name["en"]}</div>
                {item.metadata_record.website && <div><Typography className="bold-p--id">Website</Typography> <a target="_blank" rel="noopener noreferrer" href={item.metadata_record.website}>{item.metadata_record.website}</a> </div>}
                {!item.metadata_record.website && <div><Typography className="bold-p--id">Website</Typography> empty </div>}
            </Grid>
            <Grid item sm={2}><Tooltip title={messages.lookup_edit_tooltip}><Button className="inner-link-default-editor--teal" disabled={item.locked} onClick={() => { this.editGeneric(item) }}>{messages.lookup_edit_button}</Button></Tooltip></Grid>
        </>
    }

    getParentGeneric = (item) => {
        const division_of = item?.metadata_record?.is_division_of;
        const parent = this.getParents2(division_of);
        if (parent && parent.startsWith(",")) {
            return parent.substring(1);
        }
        return parent;
    }

    getParentFull = (item) => {
        const division_of = item?.division_of;
        const parent = this.getParents2(division_of);
        if (parent && parent.startsWith(",")) {
            return parent.substring(1);
        }
        return parent;
    }

    getParents2 = (division_of) => {
        if (!division_of || division_of.length === 0) {
            return null;
        }
        let parents = "";
        for (let i = 0; i < division_of.length; i++) {
            const { organization_name } = division_of[i];
            if (division_of[i]["is_division_of"] && division_of[i]["is_division_of"].length > 0) {
                parents += this.getParents2(division_of[i]["is_division_of"]) + " > " + organization_name["en"];
            } else {
                parents += ", " + organization_name["en"];
            }
        }
        return parents;
    }

    editFullRecord = (pk) => {
        this.setState({ display: false });
        const editor_url = `/create/Organization/${pk}/`;
        this.props.history.push(editor_url);
    }


    editGeneric = (item) => {
        this.setState({ display: false });
        if (item && item.metadata_record && item.metadata_record.actor_type) {
            delete item.metadata_record.actor_type;
        }
        this.props.hideLookUpByName(null, item.metadata_record);
    }

    cancel = () => {
        this.setState({ display: false })
        this.props.history.push("/createResource");
    }

    create = () => {
        this.setState({ display: false })
        if (this.state.choose_Divsion_Or_Organization === "Organization") {
            this.props.hideLookUpByName(this.state.lookupName);
        } else if (this.state.choose_Divsion_Or_Organization === "Division of organization") {
            this.props.hideLookUpByName(this.state.division_name);
        }
    }

    displayCreateButton = () => {
        if (this.state.loading || !this.state.finishedLookup) {
            return false;
        } else if (this.state.choose_Divsion_Or_Organization === "Orgnization" && this.state.lookupName.length <= 2) {
            return false;
        } else if (this.state.choose_Divsion_Or_Organization === "Division of organization" && this.state.division_name.length <= 2) {
            return false;
        }
        return true;
    }

    disableCreateButton = () => {
        const dataArray = this.state.data || [];
        if (this.state.choose_Divsion_Or_Organization === "Organization") {
            for (let index = 0; index < dataArray.length; index++) {
                const item = dataArray[index];
                if (item && item.exact) {
                    return true;
                }
            }
        } else {

        }
        return false;
    }

    render() {
        return <div>
            {this.state.display && <Dialog open={this.state.display} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" fullWidth={true} maxWidth="md">
                {!this.state.choose_Divsion_Or_Organization &&
                    <DialogContent>
                        <DialogContentText component={"span"} id="alert-dialog-description">
                            <FormControl component="fieldset">
                                <FormLabel component="legend">Do you want to create a metadata record for an <strong>Organization</strong> or a <strong>Division of organization</strong> ?</FormLabel>
                                <RadioGroup defaultValue="female" aria-label="gender" name="customized-radios" onChange={(e) => { this.setState({ choose_Divsion_Or_Organization: e.target.value, activeStep: 1 }) }}>
                                    <FormControlLabel value="Division of organization" control={<Radio />} label="Division of organization" />
                                    <FormControlLabel value="Organization" control={<Radio />} label="Organization" />
                                </RadioGroup>
                            </FormControl>
                        </DialogContentText>
                    </DialogContent>
                }

                {this.state.choose_Divsion_Or_Organization &&
                    <DialogContent>
                        {this.state.choose_Divsion_Or_Organization === "Organization" ? (
                            <div>
                                <div style={{ display: "flex", justifyItems: "space-between" }}>
                                    <div className="wd-100" >
                                        <form onSubmit={(e) => { e.preventDefault(); this.lookUpByNameSearchButton(this.state.lookupName) }}>
                                            <TextField autoFocus className="wd-100" label="organization name" onChange={(e) => this.setState({ lookupName: e.target.value, finishedLookup: false })} /*onChange={(e) => this.lookUpByName(e)}*/ variant="outlined"
                                                helperText={messages.lookup_organization}
                                                InputProps={{
                                                    endAdornment: (
                                                        <>
                                                            {this.state.loading ? <CircularProgress color="inherit" size={20} /> : null}
                                                        </>
                                                    ),
                                                }}
                                            />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        ) : (
                            <div style={{ display: "flex", justifyItems: "space-between" }}>
                                <div className="wd-100" >
                                    <form onSubmit={(e) => { e.preventDefault(); this.lookUpByDivisionSearchButton(this.state.division_name) }}>
                                        <TextField autoFocus className="wd-100" label="Division name" onChange={(e) => this.setState({ division_name: e.target.value, lookupName: "", finishedLookup: false })} variant="outlined"
                                            helperText={"Check if the division name is already listed in the catalogue"}
                                            InputProps={{
                                                endAdornment: (
                                                    <>
                                                        {this.state.loading ? <CircularProgress color="inherit" size={20} /> : null}
                                                    </>
                                                ),
                                            }}
                                        />
                                    </form>
                                </div>
                            </div>
                        )}
                        <div>
                            <>{this.getList()}</>
                        </div>
                    </DialogContent>
                }

                <DialogActions>
                    <Button hidden={!this.state.choose_Divsion_Or_Organization} onClick={() => { this.resetState() }}>
                        <KeyboardArrowLeft />Back
                    </Button>
                    {this.state.choose_Divsion_Or_Organization === "Organization" && <Button hidden={this.displayCreateButton()} disabled={this.state.loading || (!this.state.lookupName || (this.state.lookupName && this.state.lookupName.length <= 2))} color="primary" onClick={(e) => { this.lookUpByNameSearchButton(this.state.lookupName) }}>search</Button>}
                    {
                        this.state.choose_Divsion_Or_Organization === "Division of organization" &&
                        <Button
                            hidden={this.displayCreateButton()}
                            disabled={
                                this.state.loading ||
                                (
                                    ((this.state.division_name && this.state.division_name.length >= 3) ? false : true)
                                )
                            }
                            color="primary"
                            onClick={(e) => { this.lookUpByDivisionSearchButton(this.state.division_name) }}>
                            search
                        </Button>
                    }
                    <Tooltip title={this.disableCreateButton() ? `${this.state.lookupName} already exists` : ""} placement="top">
                        <span>
                            <Button hidden={!this.displayCreateButton()} disabled={this.disableCreateButton()} onClick={this.create} color="primary" >
                                Create {this.state.lookupName}
                            </Button>
                        </span>
                    </Tooltip>
                    <Button disabled={this.state.loading} onClick={this.cancel} color="primary">Cancel</Button>
                </DialogActions>
            </Dialog>
            }

        </div>

    }

}

export default withRouter(LookupOrganizationName);