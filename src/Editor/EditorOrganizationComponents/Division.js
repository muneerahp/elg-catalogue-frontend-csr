import React from "react";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../../componentsAPI/CustomVerticalTabs/VerticalTabPanel';
import Grid from '@material-ui/core/Grid';
//import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import OrganizationAutocomplete from "../EditorProjectComponents/OrganizationAutocomplete";
import AutocompleteOneChoice from "../editorCommonComponents/AutocompleteOneChoice";
//import { generic_organization_obj } from "../Models/GenericModels";
import { ORGANIZATION_TABS_HEADERS_DIVISION } from "../../config/editorConstants";
import { switchIcon } from "../../config/editorConstants";
import messages from "./../../config/messages";
import GenericSchemaParser from "./../../parsers/GenericSchemaParser";

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

export default class Division extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tab: 0 };
    }

    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
    }

    setObjectGeneral = (field, valueObj) => {
        const { model } = this.props;
        model.described_entity[field] = valueObj ? valueObj : null;
        this.props.updateModel(model);
    }

    setObjectArray = (field, valueObj) => {
        const { model } = this.props;
        model.described_entity[field] = valueObj ? valueObj : [];
        this.props.updateModel(model);
    }

    render() {
        const { data, model } = this.props;
        return <>
            <div className="mb2">
                <div className="tabs-main-container">
                    <div className="vertical-tabs-container-forms">
                        <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs-forms">
                            {ORGANIZATION_TABS_HEADERS_DIVISION.map((tab, index) => <Tab key={index} label={<><span>{switchIcon(tab)}</span><span>{tab}</span></>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />)}
                        </Tabs>

                        <VerticalTabPanel value={this.state.tab} index={0} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs={12}>
                                    <div className="pb-3">
                                        <AutocompleteOneChoice className="wd-100"
                                            {...GenericSchemaParser.getFormElement("division_category", data.division_category)}
                                            initialValue={model.described_entity.division_category || ""}
                                            field="division_category"
                                            lr_subclass={false}
                                            updateModel_String={this.setObjectGeneral}
                                        />
                                    </div>
                                    {
                                        <div className="pb-3 inner--group nested--group">
                                            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >

                                                <Grid item sm={1}>
                                                    {((model.described_entity.is_division_of && model.described_entity.is_division_of.organization_name && model.described_entity.is_division_of.organization_name["en"]) || (model.described_entity.is_division_of && model.described_entity.is_division_of.hasOwnProperty("pk"))) && <Button className="inner-link-default--purple" onClick={(e) => this.setObjectGeneral("is_division_of", null)}>{messages.array_elements_remove}</Button>}
                                                </Grid>
                                            </Grid>
                                            <OrganizationAutocomplete {...GenericSchemaParser.getFormElement("is_division_of", data.is_division_of)} initialValueArray={model.described_entity.is_division_of ? JSON.parse(JSON.stringify(model.described_entity.is_division_of)) : []} field="is_division_of" updateModel_Array={this.setObjectArray} />
                                        </div>
                                    }

                                </Grid>
                            </Grid>
                        </VerticalTabPanel>
                    </div>
                </div>
            </div>
        </>
    }
}