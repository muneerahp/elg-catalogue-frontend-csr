export const empty_ld = sessionStorage.org ? JSON.parse(sessionStorage.org) : {
	"described_entity": {
		"entity_type": "LanguageResource",
		"resource_name": { "en": "" },
		"lr_subclass": {
			"lr_type": "LanguageDescription",
			"language_description_subclass": null,
			"ld_subclass": "",
			"unspecified_part": null
		}
	}
};

export const linkToOtherMediaObj = {
	"other_media": [],
	"media_type_details": { "en": "" },
	"synchronized_with_text": null,
	"synchronized_with_audio": null,
	"synchronized_with_video": null,
	"synchronized_with_image": null,
	"synchronized_with_text_numerical": null,
}

export const ldMediaPartTextObj = {
	'editor-placeholder': true,
	"ld_media_type": "LanguageDescriptionTextPart",
	"media_type": "http://w3id.org/meta-share/meta-share/text",
	"multilinguality_type": null,
	"multilinguality_type_details": null,
	"language": [],
	"metalanguage": [],
	"modality_type": [],
	"creation_mode": null,
	"creation_details": null,
	"is_created_by": [],
	"has_original_source": [],
	"original_source_description": null,
	"link_to_other_media": [],
};

export const ldMediaPartVideoObj = {
	'editor-placeholder': true,
	"ld_media_type": "LanguageDescriptionVideoPart",
	"media_type": "http://w3id.org/meta-share/meta-share/video",
	"multilinguality_type": null,
	"multilinguality_type_details": null,
	"language": [],
	"metalanguage": [],
	"modality_type": [],
	"type_of_video_content": null,
	"text_included_in_video": [],
	"dynamic_element": [],
	"creation_mode": null,
	"creation_details": null,
	"is_created_by": [],
	"has_original_source": [],
	"original_source_description": null,
	"link_to_other_media": [],
};

export const ldMediaPartImageObj = {
	'editor-placeholder': true,
	"ld_media_type": "LanguageDescriptionImagePart",
	"media_type": "http://w3id.org/meta-share/meta-share/image",
	"multilinguality_type": null,
	"multilinguality_type_details": null,
	"language": [],
	"metalanguage": [],
	"modality_type": [],
	"type_of_image_content": null,
	"text_included_in_image": [],
	"static_element": [],
	"creation_mode": null,
	"creation_details": null,
	"is_created_by": [],
	"has_original_source": [],
	"original_source_description": null,
	"link_to_other_media": [],
};

export const ldMediaUnspecifiedPartObj = {
	'editor-placeholder': true,
	"language": [],
	"metalanguage": [],
	"multilinguality_type": null,
	"multilinguality_type_details": null,
	"modality_type": [],
	"modality_type_details": null
}

export const ldGrammarObj = {
	"ld_subclass_type": "Grammar",
	"encoding_level": [],
	"theoretic_model": [],
	"formalism": null,
	"ld_task": [],
	"grammatical_phenomena_coverage": [],
	"weighted_grammar": null,
	"requires_lr": [],
	"related_lexicon_type": null,
	"attached_lexicon_position": null,
	"compatible_lexicon_type": null,
	"robustness": null,
	"shallowness": null,
	"output": null
};

export const ldNgramObj = {
	"base_item": [],
	"order": null,
	"perplexity": null,
	"is_factored": null,
	"factor": [],
	"smoothing": "",
	"interpolated": null
};

export const ldMLModelObj = {
	"ld_subclass_type": "Model",
	"model_variant": "",
	"typesystem": [],
	"annotation_schema": [],
	"annotation_resource": [],
	"tagset": [],
	"method": null,
	"development_framework": null,
	"algorithm": null,
	"algorithm_details": null,
	"training_corpus_details": null,
	"training_process_details": null,
	"bias_details": null,
	"has_original_source": [],
	"model_function": [],
	"model_type": null,
	"n_gram_model": null,
	"requires_lr": []
};