export const empty_lcr = sessionStorage.org ? JSON.parse(sessionStorage.org) : {
	"described_entity": {
		"entity_type": "LanguageResource",
		"resource_name": { "en": "" },
		"lr_subclass": {
			"lr_type": "LexicalConceptualResource",
			"lexical_conceptual_resource_media_part": null,
			"unspecified_part": null
		}
	}
};

export const lcrMediaPartTextObj = {
	'editor-placeholder': true,
	"lcr_media_type": "LexicalConceptualResourceTextPart",
	"media_type": "http://w3id.org/meta-share/meta-share/text",
	"linguality_type": null,
	"multilinguality_type": null,
	"multilinguality_type_details": null,
	"language": [],
	"metalanguage": [],
	"modality_type": [],
};

export const lcrMediaPartVideoObj = {
	'editor-placeholder': true,
	"lcr_media_type": "LexicalConceptualResourceVideoPart",
	"media_type": "http://w3id.org/meta-share/meta-share/video",
	"linguality_type": null,
	"multilinguality_type": null,
	"multilinguality_type_details": null,
	"language": [],
	"metalanguage": [],
	"modality_type": [],
	"type_of_video_content": null,
	"text_included_in_video": [],
	"dynamic_element": []
};

export const lcrMediaPartImageObj = {
	'editor-placeholder': true,
	"lcr_media_type": "LexicalConceptualResourceImagePart",
	"media_type": "http://w3id.org/meta-share/meta-share/image",
	"linguality_type": null,
	"multilinguality_type": null,
	"multilinguality_type_details": null,
	"language": [],
	"metalanguage": [],
	"modality_type": [],
	"type_of_image_content": null,
	"text_included_in_image": [],
	"static_element": [],
};

export const lcrMediaPartAudioObj = {
	'editor-placeholder': true,
	"lcr_media_type": "LexicalConceptualResourceAudioPart",
	"media_type": "http://w3id.org/meta-share/meta-share/audio",
	"linguality_type": null,
	"multilinguality_type": null,
	"multilinguality_type_details": null,
	"language": [],
	"metalanguage": [],
	"modality_type": [],
	"speech_item": [],
	"non_speech_item": [],
	"legend": null,
	"noise_level": null,
};
