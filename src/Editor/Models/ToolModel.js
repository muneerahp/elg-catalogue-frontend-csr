export const parameterObj = {
    "parameter_name": "",
    "parameter_label": null,//{ "en": "" },
    "parameter_description": null,// { "en": "" },
    "parameter_type": null,
    "optional": null,
    "multi_value": null,
    "default_value": "",
    "data_format": [],
    "enumeration_value": [],
    'editor-placeholder': true
};

export const performanceIndicatorObj = {
    "metric": null,
    "measure": null,
    "unit_of_measure_metric": "",
};

export const parameterEnumerationObj = {
    "value_name": "",
    "value_label": null,
    "value_description": null,
};

export const sampleObj = {
    "sample_text": "",
    "samples_location": "",
    "tag": "",
};
