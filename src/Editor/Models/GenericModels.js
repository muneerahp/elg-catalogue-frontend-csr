export const subjectObj = {
    "editor-placeholder": true, "category_label": { "en": "" }, "subject_identifier": null
}

export const subject_identifier = {
    "subject_classification_scheme": "", "value": "",
};

export const textTypeObj = {
    "editor-placeholder": true,
    "category_label": { "en": "" },
    "text_type_identifier": null,
}

export const text_type_identifierObj = {
    "text_type_classification_scheme": "", "value": "",
};


export const audioGenreObj = {
    "editor-placeholder": true,
    "category_label": { "en": "" },
    "audio_genre_identifier": null,
}

export const audio_genre_identifierObj = {
    "audio_genre_classification_scheme": "", "value": "",
};

export const speechGenreObj = {
    "editor-placeholder": true,
    "category_label": { "en": "" },
    "speech_genre_identifier": null,
}

export const speech_genre_identifierObj = {
    "speech_genre_classification_scheme": "", "value": "",
};

export const imageGenreObj = {
    "editor-placeholder": true,
    "category_label": { "en": "" },
    "image_genre_identifier": null,
}

export const videoGenreObj = {
    "editor-placeholder": true,
    "category_label": { "en": "" },
    "video_genre_identifier": null,
}

export const video_genre_identifierObj = {
    "video_genre_classification_scheme": "", "value": "",
};

export const textGenreObj = {
    "editor-placeholder": true,
    "category_label": { "en": "" },
    "video_genre_identifier": null,
}

export const text_genre_identifierObj = {
    "video_genre_classification_scheme": "", "value": "",
};



export const image_genre_identifierObj = {
    "image_genre_classification_scheme": "", "value": "",
};

export const generic_person_obj = {
    "editor-placeholder": true,
    "actor_type": "Person",
    "surname": { "en": "" },
    "given_name": { "en": "" },
    "personal_identifier": null,
    "email": []
};

export const generic_organization_obj = {
    "editor-placeholder": true,
    "actor_type": "Organization",
    "organization_name": { "en": "" },
    "organization_identifier": null,
    "website": [],
    "is_division_of": []
};

export const generic_group_obj = {
    "editor-placeholder": true,
    "actor_type": "Group",
    "organization_name": { "en": "" },
    "group_identifier": null,
    "website": []
};
