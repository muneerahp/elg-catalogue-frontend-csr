export const empty_corpus = sessionStorage.org ? JSON.parse(sessionStorage.org) : {
	"described_entity": {
		"entity_type": "LanguageResource",
		"resource_name": { "en": "" },
		"lr_subclass": {
			"lr_type": "Corpus",
			"corpus_media_part": null,
			"unspecified_part": null
		}
	}
};

export const linkToOtherMediaObj = {
	"other_media": [],
	"media_type_details": { "en": "" },
	"synchronized_with_text": null,
	"synchronized_with_audio": null,
	"synchronized_with_video": null,
	"synchronized_with_image": null,
	"synchronized_with_text_numerical": null,
}


export const corpusMediaPartTextObj = {
	'editor-placeholder': true,
	"corpus_media_type": "CorpusTextPart",
	"media_type": "http://w3id.org/meta-share/meta-share/text",
	"linguality_type": null,
	"multilinguality_type": null,
	"multilinguality_type_details": null,
	"language": [],
	"modality_type": [],
	"text_type": [],
	"text_genre": [],
	"annotation": [],
	"creation_mode": null,
	"is_created_by": [],
	"has_original_source": [],
	"original_source_description": null,
	"synthetic_data": null,
	"creation_details": null,
	"link_to_other_media": [],
};

export const corpusMediaPartAudioObj = {
	'editor-placeholder': true,
	"corpus_media_type": "CorpusAudioPart",
	"media_type": "http://w3id.org/meta-share/meta-share/audio",
	"linguality_type": null,
	"multilinguality_type": null,
	"multilinguality_type_details": null,
	"language": [],
	"modality_type": [],
	"audio_genre": [],
	"speech_genre": [],
	"annotation": [],
	"speech_item": [],
	"non_speech_item": [],
	"legend": null,
	"noise_level": null,
	"naturality": null,
	"conversational_type": [],
	"scenario_type": [],
	"audience": null,
	"interactivity": null,
	"interaction": null,
	"recording_device_type": null,
	"recording_device_type_details": null,
	"recording_platform_software": "",
	"recording_environment": null,
	"source_channel": null,
	"source_channel_type": null,
	"source_channel_name": null,
	"source_channel_details": null,
	"recorder": [],
	"capturing_device_type": null,
	"capturing_device_type_details": null,
	"capturing_details": null,
	"capturing_environment": null,
	"sensor_technology": "",
	"scene_illumination": null,
	"number_of_participants": null,
	"age_group_of_participants": null,
	"age_range_start_of_participants": null,
	"age_range_end_of_participants": null,
	"sex_of_participants": null,
	"origin_of_participants": null,
	"dialect_accent_of_participants": null,
	"geographic_distribution_of_participants": null,
	"hearing_impairment_of_participants": null,
	"speaking_impairment_of_participants": null,
	"number_of_trained_speakers": null,
	"speech_influence": null,
	"creation_mode": null,
	"is_created_by": [],
	"has_original_source": [],
	"original_source_description": null,
	"synthetic_data": null,
	"creation_details": null,
	"link_to_other_media": [],
};

export const corpusMediaPartCorpusTextNumericalObj = {
	'editor-placeholder': true,
	"corpus_media_type": "CorpusTextNumericalPart",
	"media_type": "http://w3id.org/meta-share/meta-share/textNumerical",
	"modality_type": [],
	"annotation": [],
	"type_of_text_numerical_content": null,
	"recording_device_type": null,
	"recording_device_type_details": null,
	"recording_platform_software": "",
	"recording_environment": null,
	"source_channel": null,
	"source_channel_type": null,
	"source_channel_name": null,
	"source_channel_details": null,
	"recorder": [],
	"capturing_device_type": null,
	"capturing_device_type_details": null,
	"capturing_details": null,
	"capturing_environment": null,
	"sensor_technology": "",
	"scene_illumination": null,
	"number_of_participants": null,
	"age_group_of_participants": null,
	"age_range_start_of_participants": null,
	"age_range_end_of_participants": null,
	"sex_of_participants": null,
	"origin_of_participants": null,
	"dialect_accent_of_participants": null,
	"geographic_distribution_of_participants": null,
	"hearing_impairment_of_participants": null,
	"speaking_impairment_of_participants": null,
	"number_of_trained_speakers": null,
	"speech_influence": null,
	"creation_mode": null,
	"is_created_by": [],
	"has_original_source": [],
	"original_source_description": null,
	"synthetic_data": null,
	"creation_details": null,
	"link_to_other_media": [],
};

export const corpusMediaPartCorpusVideoObj = {
	'editor-placeholder': true,
	"corpus_media_type": "CorpusVideoPart",
	"media_type": "http://w3id.org/meta-share/meta-share/video",
	"linguality_type": null,
	"multilinguality_type": null,
	"multilinguality_type_details": null,
	"language": [],
	"modality_type": [],
	"video_genre": [],
	"annotation": [],
	"type_of_video_content": null,
	"text_included_in_video": [],
	"dynamic_element": [],
	"naturality": null,
	"conversational_type": [],
	"scenario_type": [],
	"audience": null,
	"interactivity": null,
	"interaction": null,
	"recording_device_type": null,
	"recording_device_type_details": null,
	"recording_platform_software": "",
	"recording_environment": null,
	"source_channel": null,
	"source_channel_type": null,
	"source_channel_name": null,
	"source_channel_details": null,
	"recorder": [],
	"capturing_device_type": null,
	"capturing_device_type_details": null,
	"capturing_details": null,
	"capturing_environment": null,
	"sensor_technology": "",
	"scene_illumination": null,
	"number_of_participants": null,
	"age_group_of_participants": null,
	"age_range_start_of_participants": null,
	"age_range_end_of_participants": null,
	"sex_of_participants": null,
	"origin_of_participants": null,
	"dialect_accent_of_participants": null,
	"geographic_distribution_of_participants": null,
	"hearing_impairment_of_participants": null,
	"speaking_impairment_of_participants": null,
	"number_of_trained_speakers": null,
	"speech_influence": null,
	"creation_mode": null,
	"is_created_by": [],
	"has_original_source": [],
	"original_source_description": null,
	"synthetic_data": null,
	"creation_details": null,
	"link_to_other_media": [],
};

export const corpusMediaPartCorpusImageObj = {
	'editor-placeholder': true,
	"corpus_media_type": "CorpusImagePart",
	"media_type": "http://w3id.org/meta-share/meta-share/image",
	"linguality_type": null,
	"multilinguality_type": null,
	"multilinguality_type_details": null,
	"language": [],
	"image_genre": [],
	"annotation": [],
	"type_of_image_content": null,
	"text_included_in_image": [],
	"static_element": [],
	"capturing_device_type": null,
	"capturing_device_type_details": null,
	"capturing_details": null,
	"capturing_environment": null,
	"sensor_technology": "",
	"scene_illumination": null,
	"creation_mode": null,
	"is_created_by": [],
	"has_original_source": [],
	"original_source_description": null,
	"synthetic_data": null,
	"creation_details": null,
	"link_to_other_media": [],
};

export const sizeObj = {
	"amount": null,
	"size_unit": null,
	"language": [],
	"domain": [],
	"text_genre": [],
	"audio_genre": [],
	"speech_genre": [],
	"video_genre": [],
	"image_genre": [],
};

export const audio_formatObj = {
	'editor-placeholder': true,
	"data_format": null,//"",
	"signal_encoding": null,// [],
	"sampling_rate": null,//"",
	"quantization": null,//"",
	"byte_order": null,//"",
	"sign_convention": null,//"",
	"audio_quality_measure_included": null,// "",
	"number_of_tracks": null,//"",
	"recording_quality": null,//[],
	"compressed": null,
	"compression_name": null,//"",
	"compression_loss": null,
};

export const duration_of_audioObj = {
	'editor-placeholder': true,
	"amount": null,
	"duration_unit": null,
}


export const distribution_text_featureObj = {
	'editor-placeholder': true,
	"size": [],
	"data_format": [],
	"character_encoding": [],
};

export const distribution_video_featureObj = {
	'editor-placeholder': true,
	"size": [],
	"data_format": [],
	"video_format": [],
};

export const distribution_image_featureObj = {
	'editor-placeholder': true,
	"size": [],
	"data_format": [],
	"image_format": [],
};

export const distribution_audio_featureObj = {
	'editor-placeholder': true,
	"size": [],
	"data_format": [],
	"duration_of_audio": [],
	"duration_of_effective_speech": [],
	"audio_format": []
};

export const distribution_text_numerical_featureObj = {
	'editor-placeholder': true,
	"size": [],
	"data_format": [],
};

export const distribution_unspecified_featureObj = {
	'editor-placeholder': true,
	"size": [],
	"data_format": [],
};

export const datasetDistributionObj = {
	"dataset_distribution_form": null,
	"dataset": null,
	"download_location": "",
	"access_location": "",
	"is_accessed_by": [],
	"is_displayed_by": [],
	"is_queried_by": [],
	"samples_location": [],
	"distribution_text_numerical_feature": [],
	"distribution_image_feature": [],
	"distribution_text_feature": [],
	"distribution_audio_feature": [],
	"distribution_video_feature": [],
	"distribution_unspecified_feature": null,
	"licence_terms": [],
	"attribution_text": null,
	"cost": null,
	"membership_institution": [],
	"copyright_statement": null,
	"availability_start_date": null,
	"availability_end_date": null,
	"distribution_rights_holder": [],
	"access_rights": [],
	"private_resource": null,
};

export const CD_ROM = "http://w3id.org/meta-share/meta-share/CD-ROM";
export const DVD_R = "http://w3id.org/meta-share/meta-share/DVD-R";
export const ACCESSIBLE_INTERFACE = "http://w3id.org/meta-share/meta-share/accessibleThroughInterface";
export const ACCESSIBLE_QUERY = "http://w3id.org/meta-share/meta-share/accessibleThroughQuery";
export const BLURAY = "http://w3id.org/meta-share/meta-share/bluRay";
export const DOWNLOADABLE = "http://w3id.org/meta-share/meta-share/downloadable";
export const HARD_DISK = "http://w3id.org/meta-share/meta-share/hardDisk";
export const OTHER = "http://w3id.org/meta-share/meta-share/other";
export const PAPER_COPY = "http://w3id.org/meta-share/meta-share/paperCopy";
export const UNSPECIFIED = "http://w3id.org/meta-share/meta-share/unspecified";
