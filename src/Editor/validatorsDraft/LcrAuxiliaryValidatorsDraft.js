import * as yup from "yup";
import { validateObject, validateObjectOptional } from "../validators/OrganizationValidator";
import { language_validation } from "./commonValidatorSchemaDraft";

export const LcrMediaTypeValidator = (parentLabel, schema_text_part, schema_audio_part, schema_video_part, schema_image_part) => {
    return {
        name: 'lcr media type validation',
        test: async function (lexical_conceptual_resource_media_part) {
            for (let index = 0; lexical_conceptual_resource_media_part && index < lexical_conceptual_resource_media_part.length; index++) {
                const mediaPart = lexical_conceptual_resource_media_part[index];
                const { media_type } = mediaPart;
                let result = null;
                switch (media_type) {
                    case "http://w3id.org/meta-share/meta-share/text":
                        result = await text_part(parentLabel, schema_text_part, index, mediaPart);
                        if (result && result.errors && result.errors.length) {
                            return this.createError({ message: result.errors, path: 'media part' });
                        } else {
                            break;
                        }
                    case "http://w3id.org/meta-share/meta-share/audio":
                        result = await audio_part(parentLabel, schema_audio_part, index, mediaPart);
                        if (result && result.errors && result.errors.length) {
                            return this.createError({ message: result.errors, path: 'media part' });
                        } else {
                            break;
                        }
                    case "http://w3id.org/meta-share/meta-share/video":
                        result = await text_video_part(parentLabel, schema_video_part, index, mediaPart);
                        if (result && result.errors && result.errors.length) {
                            return this.createError({ message: result.errors, path: 'media part' });
                        } else {
                            break;
                        }
                    case "http://w3id.org/meta-share/meta-share/image":
                        result = await text_image_part(parentLabel, schema_image_part, index, mediaPart);
                        if (result && result.errors && result.errors.length) {
                            return this.createError({ message: result.errors, path: 'media part' });
                        } else {
                            break;
                        }
                    default:
                        break;
                }
            }
            return true;
        }
    }
}
//draft discrepancy
const text_part = async (parentLabel, part, index, mediaPart) => {
    parentLabel = parentLabel.replace("</strong>", "Lexical conceptual resouce text part > </strong>");
    const schema = yup.object().shape({
        lcr_media_type: yup.string().notRequired().default(null).nullable().label("lcr media type"),
        media_type: yup.string().notRequired().default(null).nullable().label("media type"),
        //draft discrepancy
        //linguality_type: yup.string().notRequired().default(null).nullable().label(parentLabel + part.linguality_type.label),
        //multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).notRequired(),
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('language', (language, schema) => {
            return (language && language.length >= 2) ? schema.notRequired() : schema.notRequired();
        }),
        multilinguality_type_details: validateObjectOptional(`${parentLabel} ${part.multilinguality_type_details.label}`, part.multilinguality_type_details.max_length).notRequired().nullable().default(null),
        //draft discrepancy
        language: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.language.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.language.label}`),
        metalanguage: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.metalanguage.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.metalanguage.label}`),
        modality_type: yup.array().of(
            yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.modality_type.label}`),
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.modality_type.label}`),
    });
    const result = await schema.validate(mediaPart, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}

//draft discrepancy
const audio_part = async (parentLabel, part, index, mediaPart) => {
    parentLabel = parentLabel.replace("</strong>", "Lexical conceptual resouce audio part > </strong>");
    const schema = yup.object().shape({
        lcr_media_type: yup.string().notRequired().default(null).nullable().label("lcr media type"),
        media_type: yup.string().notRequired().default(null).nullable().label("media type"),
        //draft discrepancy
        //linguality_type: yup.string().notRequired().default(null).nullable().label(parentLabel + part.linguality_type.label),
        //multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).notRequired(),
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('language', (language, schema) => {
            return (language && language.length >= 2) ? schema.notRequired() : schema.notRequired();
        }),
        multilinguality_type_details: validateObjectOptional(`${parentLabel} ${part.multilinguality_type_details.label}`, part.multilinguality_type_details.max_length).notRequired().nullable().default(null),
        //draft discrepancy
        language: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.language.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.language.label}`),
        metalanguage: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.metalanguage.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.metalanguage.label}`),
        modality_type: yup.array().of(
            yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.modality_type.label}`),
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.modality_type.label}`),
        speech_item: yup.array().of(
            yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.speech_item.label}`),
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.speech_item.label}`),
        non_speech_item: yup.array().of(
            yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.non_speech_item.label}`),
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.non_speech_item.label}`),
        legend: validateObjectOptional(`${parentLabel} ${part.legend.label}`, part.legend.max_length).notRequired().nullable().default(null),
        noise_level: yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.noise_level.label}`),
    });
    const result = await schema.validate(mediaPart, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}

//draft discrepancy
const text_video_part = async (parentLabel, part, index, mediaPart) => {
    parentLabel = parentLabel.replace("</strong>", "Lexical conceptual resouce video part > </strong>");
    const schema = yup.object().shape({
        lcr_media_type: yup.string().notRequired().default(null).nullable().label("lcr media type"),
        media_type: yup.string().notRequired().default(null).nullable().label("media type"),
        //draft discrepancy
        //linguality_type: yup.string().notRequired().default(null).nullable().label(parentLabel + part.linguality_type.label),
        //multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).notRequired(),
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('language', (language, schema) => {
            return (language && language.length >= 2) ? schema.notRequired() : schema.notRequired();
        }),
        multilinguality_type_details: validateObjectOptional(`${parentLabel} ${part.multilinguality_type_details.label}`, part.multilinguality_type_details.max_length).notRequired().nullable().default(null),
        //draft discrepancy
        language: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.language.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.language.label}`),
        metalanguage: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.metalanguage.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.metalanguage.label}`),
        modality_type: yup.array().of(
            yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.modality_type.label}`),
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.modality_type.label}`),
        //draft discrepancy
        type_of_video_content: yup.array().notRequired().nullable().default(null).of(
            validateObject(parentLabel + part.type_of_video_content.label, part.type_of_video_content.max_length)
        ).label(parentLabel + part.type_of_video_content.label),
        text_included_in_video: yup.array().of(
            yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.text_included_in_video.label}`),
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.text_included_in_video.label}`),
        //to do -> dynamic_element
    });
    const result = await schema.validate(mediaPart, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}

//draft discrepancy
const text_image_part = async (parentLabel, part, index, mediaPart) => {
    parentLabel = parentLabel.replace("</strong>", "Lexical conceptual resouce image part > </strong>");
    const schema = yup.object().shape({
        lcr_media_type: yup.string().notRequired().default(null).nullable().label("lcr media type"),
        media_type: yup.string().notRequired().default(null).nullable().label("media type"),
        //draft discrepancy
        //linguality_type: yup.string().notRequired().default(null).nullable().label(parentLabel + part.linguality_type.label),
        //multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).notRequired(),
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('language', (language, schema) => {
            return (language && language.length >= 2) ? schema.notRequired() : schema.notRequired();
        }),
        multilinguality_type_details: validateObjectOptional(`${parentLabel} ${part.multilinguality_type_details.label}`, part.multilinguality_type_details.max_length).notRequired().nullable().default(null),
        //draft discrepancy
        language: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.language.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.language.label}`),
        metalanguage: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.metalanguage.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.metalanguage.label}`),
        modality_type: yup.array().of(
            yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.modality_type.label}`),
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.modality_type.label}`),
        //draft discrepancy
        type_of_image_content: yup.array().notRequired().nullable().default(null).of(
            validateObject(parentLabel + part.type_of_image_content.label, part.type_of_image_content.max_length)
        ).label(parentLabel + part.type_of_image_content.label),
        text_included_in_image: yup.array().of(
            yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.text_included_in_image.label}`),
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.text_included_in_image.label}`),
        //to do -> static_element
    });
    const result = await schema.validate(mediaPart, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}