import * as yup from "yup";
import { validateObject, validateObjectOptional } from "./OrganizationValidator";
import { language_validation, lr_validation } from "./commonValidatorSchemas";

export const language_description_subclass_validator = (parentLabel, schema_grammar_part, schema_ml_model_part) => {
    return {
        name: 'ld subclass validation',
        test: async function (language_description_subclass) {
            const ld_subclass_type = (language_description_subclass && language_description_subclass.ld_subclass_type) || "";
            let result = null;
            switch (ld_subclass_type) {
                case "Grammar":
                    result = await grammar_type(parentLabel, schema_grammar_part, language_description_subclass);
                    if (result && result.errors && result.errors.length) {
                        return this.createError({ message: result.errors, path: 'Language description type' });
                    } else {
                        break;
                    }
                case "Model":
                    result = await ml_model_part(parentLabel, schema_ml_model_part, language_description_subclass);
                    if (result && result.errors && result.errors.length) {
                        return this.createError({ message: result.errors, path: 'Language description type' });
                    } else {
                        break;
                    }
                default:
                    break;
            }
            return true;
        }
    }
}

const grammar_type = async (parentLabel, part, ldsubclass) => {
    //parentLabel = parentLabel.replace("</strong>", "Language description grammar type> </strong>");
    parentLabel = parentLabel.replace("</strong>", "Grammar > </strong>");
    const schema = yup.object().shape({
        encoding_level: yup.array().of(
            yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.encoding_level.label}`),
        ).required().nullable().default(null).label(`${parentLabel} ${part.encoding_level.label}`),
        formalism: validateObjectOptional(`${parentLabel} ${part.formalism.label}`, part.formalism.max_length).notRequired().nullable().default(null),
        ld_task: yup.array().of(
            yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.ld_task.label}`),
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.ld_task.label}`),
    });
    const result = await schema.validate(ldsubclass, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}

const ml_model_part = async (parentLabel, part, ldsubclass) => {
    //parentLabel = parentLabel.replace("</strong>", "Language description model type> </strong>");
    parentLabel = parentLabel.replace("</strong>", "Model> </strong>");
    //const ngram_parentLabel = parentLabel.replace("Language description model type> </strong>", "Model > Language description N-gram model> </strong>");
    const ngram_parentLabel = parentLabel.replace("</strong>", " N-gram model> </strong>");
    const schema = yup.object().shape({
        model_function: yup.array().of(
            yup.string().required().label(`${parentLabel} ${part.model_function.label}`).max(part.model_function.child.max_length)
        ).required().label(`${parentLabel} ${part.model_function.label}`).default(null).nullable(),
        model_type: yup.array().of(
            yup.string().required().label(`${parentLabel} ${part.model_type.label}`).max(part.model_type.child.max_length)
        ).notRequired().label(`${parentLabel} ${part.model_type.label}`).default(null).nullable(),
        model_variant: yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.model_variant.label}`).max(part.model_variant.max_length),
        //typesystem: lr_validation(parentLabel + part.typesystem.label),
        //typesystem: yup.array().notRequired().default(null).nullable().of(yup.object().shape(lr_validation(parentLabel + part.typesystem.label))).label(parentLabel + part.typesystem.label),
        //method: yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.model_variant.label}`),
        //ml_framework: yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.ml_framework.label}`).max(part.ml_framework.max_length),//changes at m2.3
        //ml_framework1: yup.string().notRequired().label(`${parentLabel} ${part.ml_framework1.label}`).nullable().default(null),
        development_framework: yup.string().notRequired().label(`${parentLabel} ${part.development_framework.label}`).nullable().default(null),
        training_corpus_details: validateObjectOptional(`${parentLabel} ${part.training_corpus_details.label}`, part.training_corpus_details.max_length).notRequired().nullable().default(null),
        training_process_details: validateObjectOptional(`${parentLabel} ${part.training_process_details.label}`, part.training_process_details.max_length).notRequired().nullable().default(null),
        bias_details: validateObjectOptional(`${parentLabel} ${part.bias_details.label}`, part.bias_details.max_length).notRequired().nullable().default(null),
        has_original_source: yup.array().notRequired().default(null).nullable().of(yup.object().shape(lr_validation(parentLabel + part.has_original_source.label))).label(parentLabel + part.has_original_source.label),
        requires_lr: yup.array().notRequired().default(null).nullable().of(yup.object().shape(lr_validation(parentLabel + part.requires_lr.label))).label(parentLabel + part.requires_lr.label),
        n_gram_model: yup.object().shape({
            base_item: yup.array().of(
                yup.string().required().label(`${ngram_parentLabel} ${part.n_gram_model.children.base_item.label}`)
            ).required(`${ngram_parentLabel} ${part.n_gram_model.children.base_item.label} is required`).label().default(null).nullable(),
            order: yup.number().required(`${ngram_parentLabel} ${part.n_gram_model.children.order.label} is required`).min(0).nullable().default(null).label(`${ngram_parentLabel} ${part.n_gram_model.children.order.label}`)
        }).label(parentLabel + part.n_gram_model.label).default(null).nullable().notRequired()
    });
    const result = await schema.validate(ldsubclass, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}

/*const ngram_model_part = async (parentLabel, part, ldsubclass) => {
    parentLabel = parentLabel.replace("</strong>", "Language description ngram model type> </strong>");
    const schema = yup.object().shape({
        base_item: yup.array().of(
            yup.string().notRequired().nullable().default(null).label(`${parentLabel} ${part.base_item.label}`),
        ).required().nullable().default(null).label(`${parentLabel} ${part.base_item.label}`),
        order: yup.number().required().nullable().default(null).label(`${parentLabel} ${part.order.label}`).min(0),
        perplexity: yup.number().notRequired().nullable().default(null).label(`${parentLabel} ${part.perplexity.label}`)
    });
    const result = await schema.validate(ldsubclass, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}*/

export const LdMediaTypeValidator = (parentLabel, schema_text_part, schema_video_part, schema_image_part) => {
    return {
        name: 'ld media type validation',
        test: async function (language_description_media_part) {
            for (let index = 0; language_description_media_part && index < language_description_media_part.length; index++) {
                const mediaPart = language_description_media_part[index];
                const { media_type } = mediaPart;
                let result = null;
                switch (media_type) {
                    case "http://w3id.org/meta-share/meta-share/text":
                        result = await text_part(parentLabel, schema_text_part, index, mediaPart);
                        if (result && result.errors && result.errors.length) {
                            return this.createError({ message: result.errors, path: 'media part' });
                        } else {
                            break;
                        }
                    case "http://w3id.org/meta-share/meta-share/video":
                        result = await video_part(parentLabel, schema_video_part, index, mediaPart);
                        if (result && result.errors && result.errors.length) {
                            return this.createError({ message: result.errors, path: 'media part' });
                        } else {
                            break;
                        }
                    case "http://w3id.org/meta-share/meta-share/image":
                        result = await image_part(parentLabel, schema_image_part, index, mediaPart);
                        if (result && result.errors && result.errors.length) {
                            return this.createError({ message: result.errors, path: 'media part' });
                        } else {
                            break;
                        }
                    default:
                        break;
                }
            }
            return true;
        }
    }
}
const text_part = async (parentLabel, part, index, mediaPart) => {
    parentLabel = parentLabel.replace("</strong>", "Language description text part > </strong>");
    const schema = yup.object().shape({
        ld_media_type: yup.string().notRequired().default(null).nullable().label("ld media type"),
        media_type: yup.string().notRequired().default(null).nullable().label("media type"),
        multilinguality_type_details: validateObjectOptional(`${parentLabel} ${part.multilinguality_type_details.label}`, part.multilinguality_type_details.max_length).notRequired().nullable().default(null),
        language: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.language.label}`))
        ).required().nullable().default(null).label(`${parentLabel} ${part.language.label}`),
        metalanguage: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.metalanguage.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.metalanguage.label}`),
        //linguality_type: yup.string().required().default(null).nullable().label(parentLabel + part.linguality_type.label),
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('language', (language, schema) => {
            return language && language.length >= 2 ? schema.notRequired() : schema.notRequired();
        })
    });
    const result = await schema.validate(mediaPart, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}

const video_part = async (parentLabel, part, index, mediaPart) => {
    parentLabel = parentLabel.replace("</strong>", "Language description video part > </strong>");
    const schema = yup.object().shape({
        ld_media_type: yup.string().notRequired().default(null).nullable().label("ld media type"),
        media_type: yup.string().notRequired().default(null).nullable().label("media type"),
        language: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.language.label}`))
        ).required().nullable().default(null).label(`${parentLabel} ${part.language.label}`),
        metalanguage: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.metalanguage.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.metalanguage.label}`),
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('language', (language, schema) => {
            return (language && language.length >= 2) ? schema.notRequired() : schema.notRequired();
        }),
        multilinguality_type_details: validateObjectOptional(`${parentLabel} ${part.multilinguality_type_details.label}`, part.multilinguality_type_details.max_length).notRequired().nullable().default(null),
        type_of_video_content: yup.array().required().nullable().default(null).of(
            validateObject(parentLabel + part.type_of_video_content.label, part.type_of_video_content.max_length)
        ).label(parentLabel + part.type_of_video_content.label),
    });
    const result = await schema.validate(mediaPart, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}

const image_part = async (parentLabel, part, index, mediaPart) => {
    parentLabel = parentLabel.replace("</strong>", "Lexical conceptual resouce image part > </strong>");
    const schema = yup.object().shape({
        ld_media_type: yup.string().notRequired().default(null).nullable().label("ld media type"),
        media_type: yup.string().notRequired().default(null).nullable().label("media type"),
        language: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.language.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.language.label}`),
        metalanguage: yup.array().of(
            yup.object().shape(language_validation(`${parentLabel} ${part.metalanguage.label}`))
        ).notRequired().nullable().default(null).label(`${parentLabel} ${part.metalanguage.label}`),
        multilinguality_type: yup.string().default(null).nullable().label(parentLabel + part.multilinguality_type.label).when('language', (language, schema) => {
            return (language && language.length >= 2) ? schema.notRequired() : schema.notRequired();
        }),
        multilinguality_type_details: validateObjectOptional(`${parentLabel} ${part.multilinguality_type_details.label}`, part.multilinguality_type_details.max_length).notRequired().nullable().default(null),
        type_of_image_content: yup.array().required().nullable().default(null).of(
            validateObject(parentLabel + part.type_of_image_content.label, part.type_of_image_content.max_length)
        ).label(parentLabel + part.type_of_image_content.label),
    });
    const result = await schema.validate(mediaPart, { abortEarly: false }).then(s => { return true }).catch(err => { return err });
    return result;
}