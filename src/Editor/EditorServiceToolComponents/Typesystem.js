import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import FreeText from "../editorCommonComponents/FreeText";
import { ReactComponent as CreateIcon } from "./../../assets/elg-icons/editor/navigation-arrows-down-1.svg";
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import Button from '@material-ui/core/Button';
import { typesystemObj, lr_identifier } from "../Models/LrModel";
import LRIdentifier from "./LRIdentifier";

export default class Typesystem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: props.initialValue }
    }

    setValues = (action, value) => {
        let { initialValue } = this.state;
        switch (action) {
            case "initializeTypeSystem":
                initialValue = JSON.parse(JSON.stringify(typesystemObj));
                break;
            case "removeObj":
                initialValue = null;
                break;
            default: break;
        }
        this.setState({ initialValue });
    }

    setObjectGeneral = (field, valueObj) => {
        const { initialValue } = this.state;
        initialValue[field] = valueObj;
        this.setState(initialValue);
    }

    updateModel_generic_String = (field, valueString) => {
        const { initialValue } = this.state;
        initialValue[field] = valueString;
        this.setState(initialValue);
    }

    updateModel_generic_Array = (field, valueArray) => {
        const { initialValue } = this.state;
        initialValue[field] = valueArray;
        this.setState(initialValue);
    }

    onBlur = () => {
        this.props.updateModel_obj(this.props.field, this.state.initialValue);
    }

    render() {
        const { initialValue } = this.state;
        return <div className="pb-3 border-bottom" onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                <Grid item sm={10}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {(!initialValue || (initialValue && !initialValue.resource_name)) && <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("initializeTypeSystem")}><CreateIcon /></Button>}
                    {(initialValue && initialValue.resource_name && Object.keys(initialValue.resource_name).length) && <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("removeObj")}><RemoveCircleOutlineIcon /></Button>}
                </Grid>
            </Grid >
            {initialValue && <div>
                <LanguageSpecificText
                    {...this.props.formElements.resource_name}
                    defaultValueObj={initialValue.resource_name}
                    field="resource_name"
                    setLanguageSpecificText={this.setObjectGeneral}
                />
                <FreeText
                    {...this.props.formElements.version}
                    className="wd-70"
                    initialValue={initialValue.version}
                    field="version"
                    updateModel={this.updateModel_generic_String}
                />
                <LRIdentifier
                    key={initialValue.lr_identifier}
                    {...this.props.formElements.lr_identifier}
                    identifier_scheme="lr_identifier_scheme"
                    scheme_choices={this.props.formElements.lr_identifier.formElements.lr_identifier_scheme.choices}
                    identifier_scheme_required={this.props.formElements.lr_identifier.formElements.lr_identifier_scheme.required}
                    identifier_scheme_label={this.props.formElements.lr_identifier.formElements.lr_identifier_scheme.label}
                    identifier_scheme_help_text={this.props.formElements.lr_identifier.formElements.lr_identifier_scheme.help_text}
                    identifier_value_required={this.props.formElements.lr_identifier.formElements.value.required}
                    identifier_value_label={this.props.formElements.lr_identifier.formElements.value.label}
                    identifier_help_text={this.props.formElements.lr_identifier.formElements.value.help_text}
                    identifier_obj={JSON.parse(JSON.stringify(lr_identifier))}
                    className="wd-70"
                    default_valueArray={initialValue.lr_identifier}
                    field="lr_identifier"
                    updateModel_Identifier={this.updateModel_generic_Array}
                />
            </div>
            }
        </div>

    }
}