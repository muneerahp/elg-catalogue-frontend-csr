import React from "react";
import axios from "axios";
//import Container from '@material-ui/core/Container';
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import { ReactComponent as TechnicalIcon } from "./../../assets/elg-icons/editor/virtual-box.svg";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../../componentsAPI/CustomVerticalTabs/VerticalTabPanel';
import Grid from '@material-ui/core/Grid';
//import ProgressBar from "../../componentsAPI/CommonComponents/ProgressBar";
import { LIST_DATA_OF_RECORD } from "../../config/constants";
import SoftwareDistributionArray from "./SoftwareDistributionArray";
import { SERVICE_THIRD_SECTION_TABS_HEADERS } from "../../config/editorConstants";

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}
export default class ServiceToolThirdStep extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tab: props.tabInSection || 0, source: null, datasets: [], loading: false };
    }

    componentWillUnmount = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    componentDidMount() {
        this.retrieveRecordData();
    }

    retrieveRecordData = () => {
        if (this.props.model.hasOwnProperty("pk")) {
            if (this.state.source) {
                this.state.source.cancel("");
            }
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            this.setState({ source: source, loading: true });
            axios.get(LIST_DATA_OF_RECORD(this.props.model.pk), { cancelToken: source.token })
                .then(res => { this.setState({ loading: false, source: null, datasets: res.data.content_files }); })
                .catch(err => { this.setState({ loading: false, source: null, datasets: [] }); })
        } else {
            this.setState({ datasets: [], loading: false })
        }
    }

    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
        //this.props.settabInSection(tabIndex);
    }

    updateModel_generic_array_Subclass = (obj2update, valueArray) => {
        const { model } = this.props;
        if (!model.described_entity.lr_subclass[obj2update]) {
            model.described_entity.lr_subclass[obj2update] = [];
        }
        model.described_entity.lr_subclass[obj2update] = valueArray;
        this.props.updateModel(model);
    }

    render() {
        if (this.state.loading) {//this is to address a weird error with CurrencyTextField at the Cost element causing the editor to crash. This happens when dataset array is not empty and component gets unmounted
            //  return <ProgressBar />
        }

        const subclass = this.props.schema_lr_subclass;
        const { model } = this.props;


        return <div>
            <form >
                <div className="tabs-main-container">
                    <div className="vertical-tabs-container-forms">
                        <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs-forms">
                            {
                                SERVICE_THIRD_SECTION_TABS_HEADERS.map((tab, index) => {
                                    return <Tab key={index} label={<><div><TechnicalIcon className="small-icon general-icon--tabs mr-05" /></div><div className="pt-2"> {tab} <div className="pt-2"></div></div> </>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />
                                })
                            }
                        </Tabs>
                        <VerticalTabPanel value={this.state.tab} index={0} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3"> <SoftwareDistributionArray
                                        {...this.props}
                                        model={model}
                                        {...GenericSchemaParser.getFormElement("software_distribution", subclass.software_distribution)}
                                        subclass={subclass}
                                        initialValueArray={model.described_entity.lr_subclass.software_distribution || []}
                                        field="software_distribution"
                                        lr_subclass={true}
                                        updateModel_array={this.updateModel_generic_array_Subclass}
                                        datasets={this.state.datasets}
                                    />
                                    </div>
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>
                    </div>
                </div>
            </form>


        </div >
    }
}