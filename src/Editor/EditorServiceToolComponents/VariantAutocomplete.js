import React from "react";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Autocomplete from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import Chip from '@material-ui/core/Chip';
import variant from "../../data/variant.json"
import script from "../../data/script.json"

export default class VariantAutocomplete extends React.Component {

    constructor(props) {
        super(props);
        this.state = { initialValue: this.props.initialValue || "" };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue || "",
        };
    }

    setValues = (newValue) => {
        let { initialValue } = this.state;
        initialValue = newValue.value || newValue;
        this.setState({ initialValue });
        this.props.updateModel_String(this.props.field, initialValue, this.props.lr_subclass);
    }

    renderAutocomplete = (default_value) => {
        const initialValueFilteredArray = this.props.choices.filter(choice => choice.value === default_value);
        let displayValue = default_value;
        if (initialValueFilteredArray.length > 0) {
            displayValue = initialValueFilteredArray[0];
        }
        const lang = this.props.language_id;
        const script_id = this.props.script_id;
        let filter_variants_by_language = this.props.choices.filter(candidate_variant => {
            if (variant.hasOwnProperty(candidate_variant.value)) {
                if (variant[candidate_variant.value].includes(lang)) {
                    return true;
                }
            }
            return false;
        });
        if (script_id) {
            filter_variants_by_language = this.props.choices.filter(candidate_variant => {
                if (script.hasOwnProperty(script_id)) {
                    if (script[script_id].filter(item => item.variant_tag === candidate_variant.value && item.lang === lang).length) {
                        return true;
                    }
                }
                return false;
            })
        }
        const choices = filter_variants_by_language;
        return <div ><Grid container direction="row" alignItems="center" justifyContent="flex-start">
            <Grid item sm={12}>
                <Autocomplete
                    selectOnFocus
                    handleHomeEndKeys
                    autoHighlight
                    clearOnBlur={true}
                    blurOnSelect={true}
                    value={displayValue || null}
                    onChange={(event, newValue) => {
                        if (newValue !== null) {
                            this.setValues(newValue)
                        } else {
                            newValue = "";
                            this.setValues(newValue);
                        }
                    }}
                    options={choices}
                    getOptionLabel={(option) => {
                        return option.display_name || option;
                    }}
                    renderInput={(params) => (
                        <TextField {...params} label={this.props.label} helperText={this.props.help_text} required={this.props.required} variant="outlined" />)}
                    renderTags={(value, getTagProps) =>
                        value.map((option, index) => (
                            <Chip variant="outlined" label={option} {...getTagProps({ index })} />
                        ))
                    }
                    renderOption={(option, { inputValue }) => {
                        const matches = match(option.display_name, inputValue);
                        const parts = parse(option.display_name, matches);
                        return (
                            <div>
                                {parts.map((part, index) => (
                                    <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                        {part.text}
                                    </span>
                                ))}
                            </div>
                        );
                    }
                    }
                />
            </Grid>
        </Grid>
        </div>
    }

    render() {
        const initialValue = this.state.initialValue;
        return <div className="mb2">
            <div className={this.props.className}>
                {this.renderAutocomplete(initialValue)}
            </div>
        </div>
    }


}