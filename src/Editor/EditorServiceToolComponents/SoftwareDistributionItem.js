import React from "react";
import Website from "../editorCommonComponents/Website";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import FreeText from "../editorCommonComponents/FreeText";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import DateComponent from "../editorCommonComponents/DateComponent";
import RecordRadioBoolean from "../editorCommonComponents/RecordRadioBoolean";
//import OnOfSwitch from "../editorCommonComponents/OnOfSwitch";
//import IsDescribedBy from "./IsDescribedBy";
import IsDescribedByAutocomplete from "./IsDescribedByAutocomplete";
import LicenceTerms from "./LicenceTerms";
import DistributionRightsHolderAutocomplete from "./DistributionRightsHolderAutocomplete";
import Cost from "../editorCommonComponents/Cost";
import AccessRightsStatementAutocomplete from "../editorCommonComponents/AccessRightsStatementAutocomplete";
import SelectDataset from "../editorCommonComponents/SelectDataset";
import {
    DOCKER_IMAGE, EXECUTABLE_CODE, GALAXY_WORKFLOW, LIBRARY, OTHER, PLUGIN, SOURCE_AND_EXECUTABLE, SOURCE_CODE,
    UNSPECIFIED, WEB_SERVICE, WORK_FLOW
} from "../../config/editorConstants";

export default class SoftwareDistributionItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { softwareDistributionItem: props.softwareDistributionItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            softwareDistributionItem: nextProps.softwareDistributionItem
        };
    }

    setSingleSelectListGeneric = (event, selectedObjArray, field, lr_subclass) => {
        const { softwareDistributionItem } = this.state;
        if (selectedObjArray.length === 0) {
            selectedObjArray[0] = { value: null };
        }
        softwareDistributionItem[field] = selectedObjArray[0].value;
        this.setState({ softwareDistributionItem });
        this.onBlur();
    }

    setObjectGeneral = (field, valueObj) => {
        const { softwareDistributionItem } = this.state;
        softwareDistributionItem[field] = valueObj;
        this.setState({ softwareDistributionItem });
    }

    setAccessRights = (field, valueObj) => {
        const { softwareDistributionItem } = this.state;
        softwareDistributionItem[field] = valueObj ? valueObj : [];
        this.setState({ softwareDistributionItem }, this.onBlur);
    }

    updateModel__array = (obj2update, valueArray) => {
        const { softwareDistributionItem } = this.state;
        if (!softwareDistributionItem[obj2update]) {
            softwareDistributionItem[obj2update] = [];
        }
        softwareDistributionItem[obj2update] = valueArray;
        this.setState({ softwareDistributionItem });
        this.onBlur();
    }

    setPrivateResource = (e) => {
        const { softwareDistributionItem } = this.state;
        if (softwareDistributionItem.private_resource === null || softwareDistributionItem.private_resource === undefined) {
            softwareDistributionItem.private_resource = null;
        }
        softwareDistributionItem.private_resource = e.target.value === "true";
        this.setState({ softwareDistributionItem });
        this.onBlur();
    }

    setElgCompaatibleDistribution = (e) => {
        const { softwareDistributionItem } = this.state;
        if (softwareDistributionItem.elg_compatible_distribution === null || softwareDistributionItem.elg_compatible_distribution === undefined) {
            softwareDistributionItem.elg_compatible_distribution = null;
        }
        softwareDistributionItem.elg_compatible_distribution = e.target.value === "true";
        if (e.target.value === "true") {
            this.props.action_to_perform_when_elg_compatible_distribution_is_checked(this.props.softwareDistributionItemIndex);
        }
        this.setState({ softwareDistributionItem }, this.onBlur);
    }

    setDataset = (selectedObjArray) => {
        const { softwareDistributionItem } = this.state;
        softwareDistributionItem.package = selectedObjArray && selectedObjArray.length > 0 ? selectedObjArray[0] : null;
        this.setState({ softwareDistributionItem }, this.onBlur);
    }

    onBlur = () => {
        this.props.setValues("setSoftwareDistributionItem", this.props.softwareDistributionItemIndex, this.state.softwareDistributionItem);
    }

    show_docker_download_location = () => {
        let show_docker_download_location = false;
        const { softwareDistributionItem } = this.state;
        if (softwareDistributionItem.software_distribution_form === DOCKER_IMAGE) {
            show_docker_download_location = true;
        }
        return show_docker_download_location;
    }

    required_docker_download_location = () => {
        let req_docker_download_location = false;
        const { softwareDistributionItem } = this.state;
        if (softwareDistributionItem.software_distribution_form === DOCKER_IMAGE) {
            req_docker_download_location = true;
        }
        return req_docker_download_location;
    }
    ////
    show_service_adapter_download_location = (elg_compatible_distribution) => {
        let show_service_adapter_download_location = false;
        if (this.props.functional_service && elg_compatible_distribution) {
            show_service_adapter_download_location = true;
        }
        return show_service_adapter_download_location;
    }
    ////
    show_download_location = () => {
        let show_download_location = false;
        const { softwareDistributionItem } = this.state;
        if ([LIBRARY, OTHER, PLUGIN, SOURCE_AND_EXECUTABLE, SOURCE_CODE, UNSPECIFIED, WORK_FLOW, EXECUTABLE_CODE].includes(softwareDistributionItem.software_distribution_form)) {
            show_download_location = true;
        }
        return show_download_location;
    }
    ////
    show_execution_location = () => {
        let execution_location = false;
        const { softwareDistributionItem } = this.state;
        if ([DOCKER_IMAGE, WEB_SERVICE, EXECUTABLE_CODE].includes(softwareDistributionItem.software_distribution_form)) {
            execution_location = true;
        }
        return execution_location;
    }
    required_execution_location = (elg_compatible_distribution) => {
        let req_execution_location = false;
        const { softwareDistributionItem } = this.state;
        if (softwareDistributionItem.software_distribution_form === DOCKER_IMAGE && this.props.functional_service === true && elg_compatible_distribution) {
            req_execution_location = true;
        }
        return req_execution_location;
    }
    ////
    show_access_location = () => {
        let show_access_location = false;
        const { softwareDistributionItem } = this.state;
        if ([LIBRARY, PLUGIN, SOURCE_CODE, UNSPECIFIED, WORK_FLOW, EXECUTABLE_CODE, WEB_SERVICE, GALAXY_WORKFLOW, OTHER, SOURCE_AND_EXECUTABLE, WORK_FLOW].includes(softwareDistributionItem.software_distribution_form)) {
            show_access_location = true;
        }
        return show_access_location;
    }
    required_access_location = () => {
        let req_access_location = false;
        /*const { softwareDistributionItem } = this.state;
        if ([GALAXY_WORKFLOW, OTHER, SOURCE_AND_EXECUTABLE, WORK_FLOW].includes(softwareDistributionItem.software_distribution_form)) {
            req_access_location = true;
        }*/
        return req_access_location;
    }
    //
    show_demo_location = (elg_compatible_distribution) => {
        let show_demo_location = true;
        if (elg_compatible_distribution) {
            if (this.props.functional_service) {//do not show demo location for functional services
                show_demo_location = false;
            }
        }
        return show_demo_location;
    }
    ////
    show_web_service_type = () => {
        let show_web_service_type = false;
        const { softwareDistributionItem } = this.state;
        if (softwareDistributionItem.software_distribution_form === WEB_SERVICE) {
            show_web_service_type = true;
        }
        return show_web_service_type;
    }
    required_web_service_type = () => {
        let req_web_service_type = false;
        const { softwareDistributionItem } = this.state;
        if (softwareDistributionItem.software_distribution_form === WEB_SERVICE) {
            req_web_service_type = true;
        }
        return req_web_service_type;
    }

    render() {
        const { softwareDistributionItem } = this.state;

        if (this.props.numberOfDistributions <= 1) {
            if (this.props.functional_service) {
                softwareDistributionItem.software_distribution_form = DOCKER_IMAGE;
                softwareDistributionItem.elg_compatible_distribution = true;
            }
        }
        if (softwareDistributionItem.elg_compatible_distribution) {
            softwareDistributionItem.software_distribution_form = DOCKER_IMAGE;
        }

        const elg_compatible_distribution = softwareDistributionItem.elg_compatible_distribution;
        /* if (this.props.functional_service && this.props.softwareDistributionItemIndex === 0) {
             softwareDistributionItem.software_distribution_form = DOCKER_IMAGE;
             softwareDistributionItem.elg_compatible_distribution = true;
         }*/


        let show_docker_download_location = this.show_docker_download_location();
        let req_docker_download_location = this.required_docker_download_location();
        show_docker_download_location ? void 0 : softwareDistributionItem["docker_download_location"] = "";

        let show_service_adapter_download_location = this.show_service_adapter_download_location(elg_compatible_distribution);
        show_service_adapter_download_location ? void 0 : softwareDistributionItem["service_adapter_download_location"] = "";

        let show_download_location = this.show_download_location();
        show_download_location ? void 0 : softwareDistributionItem["download_location"] = "";

        let show_execution_location = this.show_execution_location();
        let req_execution_location = this.required_execution_location(elg_compatible_distribution);
        show_execution_location ? void 0 : softwareDistributionItem["execution_location"] = "";

        let show_access_location = this.show_access_location();
        let req_access_location = this.required_access_location();
        show_access_location ? void 0 : softwareDistributionItem["access_location"] = "";

        let show_demo_location = this.show_demo_location(elg_compatible_distribution);
        show_demo_location ? void 0 : softwareDistributionItem["demo_location"] = "";

        let show_web_service_type = this.show_web_service_type();
        let req_web_service_type = this.required_web_service_type();
        show_web_service_type ? void 0 : softwareDistributionItem["web_service_type"] = null;

        if (![EXECUTABLE_CODE, LIBRARY, PLUGIN, SOURCE_AND_EXECUTABLE, SOURCE_CODE, WORK_FLOW].includes(softwareDistributionItem.software_distribution_form)) {
            softwareDistributionItem.package = null;
        }

        return <div onBlur={this.onBlur}>
            <div className="pt-3 pb-3"><RecordSelectList className="wd-100" {...this.props.formElements.software_distribution_form} choices={this.props.formElements.software_distribution_form.choices.filter(item => item.value !== GALAXY_WORKFLOW)} default_value={softwareDistributionItem.software_distribution_form || ""} field="software_distribution_form" disabled={this.props.functional_service && softwareDistributionItem.elg_compatible_distribution} setSelectedvalue={this.setSingleSelectListGeneric} /></div>
            {(this.props.should_choose_elg_compatible_distribution && softwareDistributionItem.software_distribution_form === DOCKER_IMAGE) && <RecordRadioBoolean
                className="pb-3"
                label={"ELG COMPATIBLE DISTRIBUTION"}
                required={true}
                help_text={'Select "yes" if this distribution should be used for the ELG integration of this service'}
                field="elg_compatible_distribution"
                default_value={softwareDistributionItem.elg_compatible_distribution}
                handleBooleanChange={this.setElgCompaatibleDistribution}
            />}
            <div className="pt-3 pb-3"><RecordRadioBoolean className="wd-100"
                required={softwareDistributionItem.software_distribution_form === DOCKER_IMAGE}
                label={this.props.formElements.private_resource.label}
                help_text={this.props.formElements.private_resource.help_text}
                default_value={softwareDistributionItem.private_resource} handleBooleanChange={this.setPrivateResource} />
            </div>
            {this.props.datasets && this.props.datasets.length > 0 && [EXECUTABLE_CODE, LIBRARY, PLUGIN, SOURCE_AND_EXECUTABLE, SOURCE_CODE, WORK_FLOW].includes(softwareDistributionItem.software_distribution_form) && <div className="pt-3" ><SelectDataset className="wd-100" datasets={this.props.datasets} initialValue={softwareDistributionItem.package} service_tool={true} setSelectedvalue={this.setDataset} /></div>}
            {show_docker_download_location && <div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.formElements.docker_download_location} required={req_docker_download_location} initialValue={softwareDistributionItem.docker_download_location} field="docker_download_location" updateModel={this.setObjectGeneral} /></div>}
            {show_service_adapter_download_location && <div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.formElements.service_adapter_download_location} initialValue={softwareDistributionItem.service_adapter_download_location} field="service_adapter_download_location" updateModel={this.setObjectGeneral} /></div>}
            {show_download_location && <div className="pt-3 pb-3"><Website key={softwareDistributionItem.download_location + "_download_location_" + this.props.softwareDistributionItemIndex} className="wd-100"  {...this.props.formElements.download_location} website={softwareDistributionItem.download_location || ""} field="download_location" updateModel_website={this.setObjectGeneral} /></div>}
            {show_execution_location && <div className="pt-3 pb-3"><Website key={softwareDistributionItem.execution_location + "_execution_location_" + this.props.softwareDistributionItemIndex} className="wd-100"  {...this.props.formElements.execution_location} required={req_execution_location} website={softwareDistributionItem.execution_location || ""} field="execution_location" updateModel_website={this.setObjectGeneral} /></div>}
            {show_access_location && <div className="pt-3 pb-3"><Website key={softwareDistributionItem.access_location + "_access_location_" + this.props.softwareDistributionItemIndex} className="wd-100"  {...this.props.formElements.access_location} required={req_access_location} website={softwareDistributionItem.access_location || ""} field="access_location" updateModel_website={this.setObjectGeneral} /></div>}
            {show_demo_location && <div className="pt-3 pb-3"><Website key={softwareDistributionItem.demo_location + "_demo_location_" + this.props.softwareDistributionItemIndex} className="wd-100"  {...this.props.formElements.demo_location} website={softwareDistributionItem.demo_location || ""} field="demo_location" updateModel_website={this.setObjectGeneral} /></div>}
            {show_web_service_type && <div className="pt-3 pb-3"><RecordSelectList className="wd-100" {...this.props.formElements.web_service_type} required={req_web_service_type} default_value={softwareDistributionItem.web_service_type || ""} field="web_service_type" setSelectedvalue={this.setSingleSelectListGeneric} /></div>}

            <div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.formElements.additional_hw_requirements} initialValue={softwareDistributionItem.additional_hw_requirements} field="additional_hw_requirements" updateModel={this.setObjectGeneral} /></div>

            {false && <div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.formElements.command} initialValue={softwareDistributionItem.command} field="command" updateModel={this.setObjectGeneral} /></div>}
            {false && <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100"  {...this.props.formElements.operating_system} initialValuesArray={softwareDistributionItem.operating_system || []} field="operating_system" updateModel_array={this.updateModel__array} /></div>}
            <div>
                <IsDescribedByAutocomplete
                    className="wd-100" {...this.props} index={this.props.softwareDistributionItemIndex}
                    {...this.props.formElements.is_described_by} initialValueArray={softwareDistributionItem.is_described_by || []}
                    field="is_described_by"
                    updateModel_Array={this.updateModel__array}
                />
            </div>
            <div><LicenceTerms {...this.props.formElements.licence_terms} initialValueArray={softwareDistributionItem.licence_terms || []} field="licence_terms" updateModel_Array={this.updateModel__array} /></div>
            <div>
                <Cost
                    {...this.props.formElements.cost} initialValue={softwareDistributionItem.cost || ""} field="cost" updateModel={this.setObjectGeneral}
                />
            </div>
            <div className="pt-3">
                <AccessRightsStatementAutocomplete
                    {...this.props.formElements.access_rights}
                    initialValue={softwareDistributionItem.access_rights || []}
                    field="access_rights"
                    setModelField={this.setAccessRights}
                />
            </div>
            {false && <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.attribution_text} defaultValueObj={softwareDistributionItem.attribution_text || { "en": "" }} field="attribution_text" setLanguageSpecificText={this.setObjectGeneral} /></div>}
            <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100"  {...this.props.formElements.membership_institution} initialValuesArray={softwareDistributionItem.membership_institution || []} field="membership_institution" updateModel_array={this.updateModel__array} /></div>
            {false && <div className="pt-3 pb-3"><DateComponent className="wd-100" {...this.props.formElements.availability_start_date} initialValue={softwareDistributionItem.availability_start_date || null} maxDate={softwareDistributionItem.availability_end_date || null} field="availability_start_date" updateModel={this.setObjectGeneral} /></div>}
            {false && <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.copyright_statement} defaultValueObj={softwareDistributionItem.copyright_statement || { "en": "" }} field="copyright_statement" setLanguageSpecificText={this.setObjectGeneral} /></div>}
            {false && <div className="pt-3 pb-3"><DateComponent className="wd-100" {...this.props.formElements.availability_end_date} initialValue={softwareDistributionItem.availability_end_date || null} minDate={softwareDistributionItem.availability_start_date || null} field="availability_end_date" updateModel={this.setObjectGeneral} /></div>}
            {false && <div><DistributionRightsHolderAutocomplete softwareDistributionItemIndex={this.props.softwareDistributionItemIndex} {...this.props.formElements.distribution_rights_holder} initialValueArray={softwareDistributionItem.distribution_rights_holder || []} field="distribution_rights_holder" updateModel_Array={this.updateModel__array} /></div>}
        </div >
    }
}