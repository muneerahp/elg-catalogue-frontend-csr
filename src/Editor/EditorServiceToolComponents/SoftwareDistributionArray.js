import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { softwareDistributionObj } from "../Models/LrModel";
import SoftwareDistributionItem from "./SoftwareDistributionItem";
import messages from "./../../config/messages";
import Tooltip from '@material-ui/core/Tooltip';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { DOCKER_IMAGE } from "../../config/editorConstants";

export default class SoftwareDistributionArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { softwareDistributionArray: props.initialValueArray || [], expanded: { index: 0, isExpanded: true } }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            softwareDistributionArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, softwareDistributionItemIndex, value) => {
        const { softwareDistributionArray } = this.state;
        switch (action) {
            case "addSoftwareDistributionItem":
                softwareDistributionArray.push(JSON.parse(JSON.stringify(softwareDistributionObj)));
                this.setState({ expanded: { index: softwareDistributionArray.length - 1, isExpanded: true } });
                break;
            case "removeSoftwareDistributionItem":
                softwareDistributionArray.splice(softwareDistributionItemIndex, 1);
                break;
            case "setSoftwareDistributionItem":
                softwareDistributionArray[softwareDistributionItemIndex] = value;
                this.props.updateModel_array(this.props.field, softwareDistributionArray, this.props.lr_subclass);
                break;
            default: break;
        }
        this.setState({ softwareDistributionArray });
        this.onBlur();
    }

    removeWarning = (e, index) => {
        e.stopPropagation();
        /* if (window.confirm(messages.editor_remove_division)) {
             this.setHasDivision(e, divisionIndex, "remove_division");
         }*/
        confirmAlert({
            //title: 'Confirm to remove Distribution ',
            message: messages.editor_remove_distribution(index),
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.setValues("removeSoftwareDistributionItem", index)
                },
                {
                    label: 'No',
                    onClick: () => { }
                }
            ]
        });
    }

    action_to_perform_when_elg_compatible_distribution_is_checked = (softwareDistributionItemIndex) => {
        this.setState({ expanded: { index: 0, isExpanded: true } }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.softwareDistributionArray, this.props.lr_subclass);
    }

    render() {
        const { softwareDistributionArray } = this.state;
        softwareDistributionArray.length === 0 && softwareDistributionArray.push(JSON.parse(JSON.stringify(softwareDistributionObj)));



        if (this.props.functional_service) {
            const docker_image_distributions_counter = softwareDistributionArray.filter(item => item && item.software_distribution_form === DOCKER_IMAGE).length;

            let force_selection = false;
            if (docker_image_distributions_counter === 1) {
                softwareDistributionArray.filter(item => item && item.software_distribution_form === DOCKER_IMAGE).map(distribution => {
                    if (distribution.elg_compatible_distribution !== true) {
                        force_selection = true;
                    }
                    distribution.elg_compatible_distribution = true;
                    return distribution;
                })
            }
            if (force_selection) {
                this.action_to_perform_when_elg_compatible_distribution_is_checked();
            }
        }

        softwareDistributionArray.sort((item1, item2) => {
            if (item1 && item2) {
                if (item1.elg_compatible_distribution) {
                    return -1;
                } else if (item2.elg_compatible_distribution) {
                    return 1;
                }
                return 0;
            }
            return 0;
        });

        const should_choose_elg_compatible_distribution = softwareDistributionArray.filter(item => this.props.functional_service && item && item.software_distribution_form === DOCKER_IMAGE && !item.elg_compatible_distribution).length > 1;

        return <div onBlur={this.onBlur}>
            {
                softwareDistributionArray.map((softwareDistributionItem, softwareDistributionItemIndex) => {
                    return <div key={softwareDistributionItemIndex} onBlur={() => this.onBlur()}>
                        <Accordion className="FunctionBox--accordions--editor" expanded={(this.state.expanded.index === softwareDistributionItemIndex && this.state.expanded.isExpanded)} onChange={() => {
                            if (this.state.expanded.index === softwareDistributionItemIndex) {
                                this.setState({ expanded: { index: softwareDistributionItemIndex, isExpanded: !this.state.expanded.isExpanded } }, this.onBlur)
                            } else {
                                this.setState({ expanded: { index: softwareDistributionItemIndex, isExpanded: true } }, this.onBlur)
                            }
                        }
                        }>
                            <AccordionSummary expandIcon={<ExpandMoreIcon className="purple--font" />} aria-controls="panel1a-content" id="panel1a-header" className="FunctionBox--accordions__editortitle">
                                <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                                    <Grid item sm={9}>
                                        <Typography variant="h3" className="section-links" >{this.props.label} {softwareDistributionItemIndex + 1}</Typography>
                                        <Typography className="section-links" >{this.props.help_text} </Typography>
                                    </Grid>
                                    {
                                        <Grid item sm={3}><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button disabled={softwareDistributionArray.length === 1} className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, softwareDistributionItemIndex)}>{messages.array_elements_remove} {` ${this.props.label}`} {softwareDistributionItemIndex ? softwareDistributionItemIndex + 1 : ""}</Button></Tooltip></Grid>
                                    }
                                </Grid>
                            </AccordionSummary>
                            <AccordionDetails>
                                <SoftwareDistributionItem
                                    {...this.props} softwareDistributionItem={softwareDistributionItem} softwareDistributionItemIndex={softwareDistributionItemIndex} should_choose_elg_compatible_distribution={should_choose_elg_compatible_distribution} numberOfDistributions={softwareDistributionArray.length} action_to_perform_when_elg_compatible_distribution_is_checked={this.action_to_perform_when_elg_compatible_distribution_is_checked} setValues={this.setValues} />
                            </AccordionDetails>
                        </Accordion>

                        <div className="mb2 pt-3">
                            {((softwareDistributionItem.software_distribution_form && softwareDistributionItem.software_distribution_form !== "") && ((softwareDistributionArray.length - 1) === softwareDistributionItemIndex)) ?
                                <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                    <Grid item>
                                        <Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addSoftwareDistributionItem")}>{messages.array_elements_add}</Button></Tooltip>
                                    </Grid>
                                </Grid>
                                : <span></span>
                            }
                        </div>
                    </div>
                })
            }
        </div>
    }
}