import React from "react";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/Typography';
const filter = createFilterOptions();

export default class AutocompleteRecommendedChoicesSelectOneOption extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: this.props.initialValue || "" };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue || "",
        };
    }

    setValues = (newValue) => {
        let { initialValue } = this.state;
        initialValue = newValue.value || newValue;
        this.setState({ initialValue });
        this.props.updateModel_String(this.props.field, initialValue || null, this.props.lr_subclass);
    }

    renderAutocomplete = (default_value) => {
        const initialValueFilteredArray = this.props.choices.filter(choice => choice.value === default_value);
        let displayValue = default_value;
        if (initialValueFilteredArray.length > 0) {
            displayValue = initialValueFilteredArray[0];
        }
        return <div ><Grid container direction="row" alignItems="center" justifyContent="flex-start">
            <Grid item sm={12}>
                <Autocomplete
                    freeSolo
                    selectOnFocus
                    handleHomeEndKeys
                    autoHighlight
                    clearOnBlur={true}
                    blurOnSelect={true}
                    value={displayValue || null}
                    onChange={(event, newValue) => {
                        if (newValue !== null) {
                            this.setValues(newValue)
                        } else {
                            newValue = "";
                            this.setValues(newValue);
                        }
                    }}
                    options={this.props.choices}
                    filterOptions={(options, params) => {
                        const filtered = filter(options, params);
                        if (params.inputValue !== '') {
                            filtered.splice(0, 0, {
                                value: params.inputValue,
                                display_name: `Missing ${params.inputValue}? Add "${params.inputValue}"`,
                            });
                        }
                        return filtered;
                    }}
                    getOptionLabel={(option) => {
                        return option.display_name || option;
                    }}
                    renderInput={(params) => (
                        <TextField {...params} label={this.props.label} helperText={this.props.help_text} required={this.props.required} variant="outlined" />)}
                    renderTags={(value, getTagProps) =>
                        value.map((option, index) => (
                            <Chip variant="outlined" label={option} {...getTagProps({ index })} />
                        ))
                    }
                    renderOption={(option, { inputValue }) => {
                        const matches = match(option.display_name, inputValue);
                        const parts = parse(option.display_name, matches);
                        return (
                            <div>
                                {parts.map((part, index) => (
                                    <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                        {part.text}
                                    </span>
                                ))}
                            </div>
                        );
                    }
                    }
                />
            </Grid>
        </Grid>
        </div>
    }

    render() {
        const initialValue = this.state.initialValue;
        return <div onBlur={this.onBlur}>
            <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
            {/*<Typography className="section-links" >{this.props.help_text} </Typography>*/}
            {this.renderAutocomplete(initialValue)}
        </div>
    }

}