import React from "react";
import axios from "axios";
import CircularProgress from '@material-ui/core/CircularProgress';
import { EDITOR_METADATARECORD_MODEL_SCHEMA, LOOK_UP_REPOSITORY } from "../../config/editorConstants";
import { source_of_metadata_record_obj, repository_identifier_obj } from "../Models/LrModel";
import { generic_organization_obj } from "../Models/GenericModels";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import LRIdentifier from "../EditorServiceToolComponents/LRIdentifier";
import Website from "../editorCommonComponents/Website";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
//import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import GenericOrganization from "../EditorGenericComponents/GenericOrganization";
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import messages from "./../../config/messages";
//const filter = createFilterOptions();
const checkElg = obj => obj.document_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg";

export default class SourceOfMetadataRecord extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: props.initialValue, schema: null, source: null, schemaLoading: false, autocompleteLoading: false, options: [] };
    }

    setValue = (action, newValue) => {
        let initialValue = this.state.initialValue;
        switch (action) {
            case "fill_in_object":
                initialValue = JSON.parse(JSON.stringify(source_of_metadata_record_obj));
                break;
            case "remove_object":
                initialValue = null;
                break;
            case "setRepositoryName":
                initialValue.repository_name = newValue;
                if (initialValue.hasOwnProperty('editor-placeholder')) {
                    delete initialValue['editor-placeholder'];
                }
                break;
            case "autoCompleteSelected":
                if (newValue.similarity) {
                    delete newValue["similarity"];
                }
                if (newValue.display_name) {
                    delete newValue["display_name"];
                }
                if (newValue.hasOwnProperty('editor-placeholder')) {
                    delete newValue['editor-placeholder'];
                }
                initialValue = JSON.parse(JSON.stringify(newValue));
                break;
            default:
                break;
        }
        this.setState({ initialValue: initialValue, options: [] });
        this.props.setModelField(this.props.field, initialValue);
    }

    set_repository = (index = 0, value) => {
        const { initialValue } = this.state;
        initialValue.repository_name = value;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] });
    }

    setObjectGeneral = (field, valueObj) => {
        const { initialValue } = this.state;
        initialValue[field] = valueObj;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] }, this.onBlur());
    }

    updateModel = (index, value) => {
        const { initialValue } = this.state;
        initialValue[index] = value;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] }, this.onBlur());
    }

    update_identifier_Array = (index = 0, valueArray) => {
        const { initialValue } = this.state;
        initialValue.repository_identifier = valueArray;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] }, this.onBlur());
    }

    onBlur = () => {
        this.setState({ options: [], autocompleteLoading: false });
        this.props.setModelField(this.props.field, this.state.initialValue);
    }

    componentDidMount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, schemaLoading: true });
        axios.options(EDITOR_METADATARECORD_MODEL_SCHEMA, { cancelToken: source.token }).then(res => {
            if (res.data.actions && res.data.actions.POST) {
                this.setState({ schema: res.data.actions.POST, source: null, schemaLoading: false });
            }
        }).catch(err => { console.log(err); this.setState({ source: null, schemaLoading: false }); });
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    getChoices = (value) => {
        if (value.trim().length <= 2) {
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, autocompleteLoading: true });
        axios.get(LOOK_UP_REPOSITORY(value), { cancelToken: source.token }).then(res => {
            this.setState({ options: res.data, source: null, autocompleteLoading: false });
        }).catch((err) => {
            console.log(err);
            this.setState({ source: null, autocompleteLoading: false });
        });
    }

    autocompleteTextField = (schema) => {
        const activeLanguage = this.props.activeLanguage || "en";
        const choices = schema.children.repository_identifier.child.children.repository_identifier_scheme.choices;
        return <span>
            <Autocomplete className={"wd-100"}
                freeSolo
                clearOnBlur
                autoHighlight
                loading={this.state.autocompleteLoading}
                value={this.state.initialValue || ""}
                onChange={(event, newValue) => {
                    if (!newValue) {
                        return;
                    }
                    if (typeof newValue === "object") {
                        this.setValue("autoCompleteSelected", newValue);
                    } else {
                        this.setValue("setRepositoryName", { "en": newValue })
                    }
                }}
                options={this.state.autocompleteLoading ? [] : this.state.options}
                filterOptions={(options, params) => {
                    //const filtered = filter(options, params);
                    const filtered = options;
                    if (!this.state.autocompleteLoading) {
                        if (params.inputValue !== '') {
                            filtered.splice(0, 0, {
                                repository_name: { 'en': params.inputValue },
                                display_name: `Missing ${params.inputValue}? Add "${params.inputValue}"`,
                            });
                        }
                    }
                    if (filtered && filtered.length === 0 && this.state.loading) {
                        return [{ display_name: "Loading please wait...", }]
                    }
                    return filtered;
                }}
                //getOptionLabel={(option) => option.repository_name ? Object.values(option.repository_name)[0] : ""}
                getOptionLabel={(option) => option.repository_name ? Object.keys(option.repository_name).includes("en") ? option.repository_name["en"] : Object.values(option.repository_name)[0] : ""}
                renderInput={(params) => (
                    <TextField {...params} label={schema.children.repository_name.label} helperText={schema.children.repository_name.help_text} placeholder={schema.children.repository_name.placeholder ? schema.children.repository_name.placeholder : ""} variant="outlined"
                        onChange={(e) => { this.getChoices(e.target.value) }}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {this.state.loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            )
                        }}
                    />)}
                renderOption={(option, { inputValue }) => {
                    if (option.display_name) {
                        const matches1 = match(option.display_name, inputValue);
                        const parts1 = parse(option.display_name, matches1);
                        return parts1.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                {part.highlight ? '\u00A0' : ''}{part.text}
                            </span>
                        ))
                    }
                    //const matches = match(Object.values(option.repository_name)[0], inputValue);
                    //const parts = parse(Object.values(option.repository_name)[0], matches);
                    const matches = match(option.repository_name[activeLanguage] || Object.values(option.repository_name)[0], inputValue);
                    const parts = parse(option.repository_name[activeLanguage] || Object.values(option.repository_name)[0], matches);
                    return (
                        <div>
                            {
                                option.repository_name && <div>
                                    {parts.map((part, index) => (
                                        <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                            {part.text}
                                        </span>
                                    ))}
                                </div>
                            }
                            {
                                option.repository_url && <div className="info_url">
                                    {option.repository_url}
                                </div>
                            }
                            {
                                option.repository_identifier && option.repository_identifier.map((identifier, identifierIndex) => {
                                    return <div key={identifierIndex}>
                                        {
                                            identifier.repository_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg" ? "" :
                                                (`${identifier.value} - ${choices.filter(repositoryChoiceUrl => repositoryChoiceUrl.value === identifier.repository_identifier_scheme)[0].display_name}`)
                                        }
                                    </div>
                                })

                            }
                        </div>
                    );
                }
                }
            />
        </span>
    }

    render() {
        if (this.state.schemaLoading) {
            return <CircularProgress size={20} />;
        }
        if (!this.state.schema) {
            return <div></div>
        }
        const schema = this.state.schema.source_of_metadata_record;
        const hide_help_text = (this.state.initialValue && this.state.initialValue.hasOwnProperty("pk")) ? true : false;
        const initialValue = this.state.initialValue;
        return <div onBlur={this.onBlur}>
            <div className="pb-3 inner--group nested--group">
                <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                    <Grid item sm={11}>
                        <Typography variant="h3" className="section-links" >{schema.label} </Typography>
                        <Typography className="section-links" >{schema.help_text} </Typography>
                    </Grid>
                    <Grid item sm={1}>
                        {!this.state.initialValue && <Button disabled={this.props.disable} className="inner-link-default--purple" onClick={(e) => this.setValue("fill_in_object")}>{messages.group_elements_create}</Button>}
                        {this.state.initialValue && <Button disabled={this.props.disable} className="inner-link-default--purple" onClick={(e) => this.setValue("remove_object")}>{messages.group_elements_remove}</Button>}
                    </Grid>
                </Grid>
                {this.state.initialValue && <Grid>
                    {this.state.initialValue.hasOwnProperty("editor-placeholder") && !this.state.initialValue.hasOwnProperty("pk") ?
                        this.autocompleteTextField(schema)
                        : <div>
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item>
                                    {/*<Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", arrayItemIndex)}>{"Remove"}</Button>*/}
                                </Grid>
                            </Grid>

                            <LanguageSpecificText
                                {...schema.children.repository_name}
                                help_text={hide_help_text ? '' : schema.children.repository_name.help_text}
                                defaultValueObj={initialValue.repository_name || { "en": "" }}
                                disable={initialValue.pk >= 0 ? true : false}
                                field="repository_name"//don't care about field, track index instead
                                setLanguageSpecificText={this.set_repository}
                            />

                            <Grid container spacing={1}>
                                <Grid item xs={initialValue.hasOwnProperty("pk") ? 12 : 9}>
                                    {(initialValue.pk && (!initialValue.repository_identifier || (initialValue.repository_identifier.length === 1 && initialValue.repository_identifier.some(checkElg))))
                                        ?
                                        (<></>)
                                        :
                                        (
                                            <LRIdentifier
                                                key={JSON.stringify(initialValue.repository_identifier)}
                                                {...schema.children.repository_identifier}
                                                identifier_scheme="repository_identifier_scheme"
                                                scheme_choices={schema.children.repository_identifier.child.children.repository_identifier_scheme.choices}
                                                identifier_scheme_required={schema.children.repository_identifier.child.children.repository_identifier_scheme.required}
                                                identifier_scheme_label={schema.children.repository_identifier.child.children.repository_identifier_scheme.label}
                                                identifier_scheme_help_text={hide_help_text ? '' : schema.children.repository_identifier.child.children.repository_identifier_scheme.help_text}
                                                identifier_value_required={schema.children.repository_identifier.child.children.value.required}
                                                identifier_value_label={schema.children.repository_identifier.child.children.value.label}
                                                identifier_value_placeholder={hide_help_text ? '' : schema.children.repository_identifier.child.children.value.placeholder}
                                                identifier_value_help_text={hide_help_text ? '' : schema.children.repository_identifier.child.children.value.help_text}
                                                identifier_obj={JSON.parse(JSON.stringify(repository_identifier_obj))}
                                                className="wd-100"
                                                default_valueArray={initialValue.repository_identifier}
                                                disable={initialValue.pk >= 0 ? true : false}
                                                field={"repository_identifier"}
                                                updateModel_Identifier={this.update_identifier_Array}
                                            ></LRIdentifier>)
                                    }
                                </Grid>
                            </Grid>

                            <Website
                                key={initialValue.repository_url || "reposiotry_url"}
                                className="wd-100"  {...schema.children.repository_url}
                                website={initialValue.repository_url || ""}
                                disable={initialValue.pk >= 0 ? true : false}
                                field="repository_url"
                                updateModel_website={this.setObjectGeneral}
                            />

                            {((initialValue.hasOwnProperty("pk") && initialValue.repository_institution && initialValue.repository_institution.hasOwnProperty("pk")) || (!initialValue.hasOwnProperty("pk"))) && <GenericOrganization
                                index={"repository_institution"}
                                initialValue={initialValue.repository_institution || JSON.parse(JSON.stringify(generic_organization_obj))}
                                updateModel={this.setObjectGeneral}
                            />}
                        </div>
                    }
                </Grid>}
            </div>
        </div>
    }

}