import React from "react";
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
//import Grid from '@material-ui/core/Grid';
//import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import { toast } from "react-toastify";
import { keycloak } from "../../App";
import { PROXY_2_S3, getAuthorizationHeader } from "../../config/constants";
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
//import messages from "../../config/messages";

registerPlugin(FilePondPluginImagePreview, FilePondPluginFileValidateType);
const zipTypes = ["application/gzip", "application/tar", "application/tar+gzip", "application/zip", "application/octet-stream", "application/x-zip-compressed", "multipart/x-zip"];

export default class DatasetUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = { displayDialog: true, loading: false, source: null, zipFiles: [], proxyError: null, error: null, isReplaceAction: false };
        this.pond = null;
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    disableDisplay = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        this.setState({ displayDialog: false, loading: false, source: null, zipFiles: [], proxyError: null, error: null, isReplaceAction: false });
        this.pond = null;
        this.props.hideUploadDataset();
    }

    handleUpload = () => {
        this.pond.processFile();
    }

    handleS3Proxy = (response) => {
        if (!response) {//for some reason this method is triggered when the user presses the cancel button with an undifined response. Dirty workaround
            this.disableDisplay();
            return;
        }
        if (this.state.isReplaceAction) {
            toast.success("File has been replaced successfully.", { autoClose: 3500 });
        } else {
            toast.success("File has been successfully uploaded.", { autoClose: 3500 });
        }
        this.disableDisplay();
    }

    handleS3ProxyError = (responseError) => {
        toast.error("Dataset upload failed.", { autoClose: 3500 });
        try {
            if (responseError && responseError.indexOf("html") >= 0) {//render html response page as error report
                this.setState({ proxyError: responseError });
            } else {
                this.setState({ error: responseError });//render json as error report
            }
            console.log(JSON.stringify(responseError));
        } catch (err) {
        }
        this.disableDisplay();
    }

    ShowErrorMessage(error) {
        return <div>
            {error && <div style={{ marginBottom: "20px" }}>
                <h3>Error</h3>
                <div className=" boxed">
                    <Paper elevation={13} >
                        <code>
                            <pre id="special">
                                {error}
                            </pre>
                        </code>
                    </Paper>
                </div>
            </div>
            }
        </div>
    }

    ShowReplaceMessage() {
        return <div>
            {<div style={{ marginBottom: "1px", paddingBottom: "1px" }}>
                <h3>Warning</h3>
                <div className=" boxed">
                    <Paper elevation={13} >
                        <span className="yup-error-link" style={{ color: "red" }}>
                            There is already a file with the same name. By proceeding you are going to delete the already uploaded file and replace it with this one. Are you sure?
                        </span>
                    </Paper>
                </div>
            </div>
            }
        </div>
    }

    checkForReplace = () => {
        /* try {
             const file = this.state.zipFiles && this.state.zipFiles.length === 1 && this.state.zipFiles[0];
             if (this.props.datasets && this.props.datasets.length > 0) {
                 for (let i = 0; i < this.props.datasets.length; i++) {
                     if (this.props.datasets[i].file.endsWith(file.name)) {
                         this.setState({ isReplaceAction: true });
                         return;
                     }
                 }
             }
         } catch (err) {
             console.log(err);
         }*/
    }


    upload_process_zip = (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
        if (this.props.datasets && this.props.datasets.length > 0) {
            for (let i = 0; i < this.props.datasets.length; i++) {
                if (this.props.datasets[i].file.endsWith(file.name)) {
                    this.setState({ error: "There is already a file with this name" });
                    abort();
                    return;
                }
            }
        }
        const formData = new FormData();
        formData.append("file", file, file.name);
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        axios.put(PROXY_2_S3, file, {
            headers: {
                'Content-Type': 'application/octet-stream',
                'Authorization': getAuthorizationHeader(keycloak),
                'ELG-RESOURCE-ID': this.props.model.pk,
                'scope': "private",
                'filetype': "und",
                'filename': encodeURI(file.name)
            },
            cancelToken: source.token,
            onUploadProgress: (e) => {
                progress(e.lengthComputable, e.loaded, e.total);
            }
        }).then(response => {
            load(response);
            this.setState({ zipFiles: [] });
            this.handleS3Proxy(response);
        }).catch(err => {
            console.log("error, ", err);
            this.setState({ zipFiles: [] });
            error(err);
            if (axios.isCancel(err)) {
                console.log('Request canceled', err.message);
            } else {
                this.handleS3ProxyError(err)
            }
        });
        return {
            abort: () => {
                source.cancel('Operation canceled by the user.');
                abort();
            }
        }
    }


    render() {
        return <>
            <Dialog open={this.state.displayDialog} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" disableBackdropClick={true} maxWidth='lg'>
                {!this.state.loading && <DialogTitle> Please select a .zip file in order to upload a dataset for this record</DialogTitle>}
                {this.state.loading && <DialogTitle> Please wait while we are uploading your dataset<CircularProgress size={20} /></DialogTitle>}
                <DialogContent>
                    <DialogContentText component={"div"}>
                        <FilePond
                            dropOnPage={true}
                            credits={false}
                            allowMultiple={false}
                            name="file"
                            instantUpload={false}
                            acceptedFileTypes={zipTypes}
                            ref={ref => (this.pond = ref)}
                            files={this.state.zipFiles}
                            onupdatefiles={(fileItems) => {
                                this.setState({
                                    zipFiles: fileItems.map(fileItem => fileItem.file)
                                }, this.checkForReplace);
                            }}
                            server={
                                {
                                    url: PROXY_2_S3,
                                    process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => { this.upload_process_zip(fieldName, file, metadata, load, error, progress, abort, transfer, options) },
                                    revert: null,
                                    load: null,
                                    restore: null,
                                    fetch: null,
                                    //onload: (response) => this.handleS3Proxy(response),
                                    onerror: (response) => this.handleS3ProxyError(response),
                                }
                            }
                            //onremovefile={(file) => { this.setState({ loading: false, zipFiles: [], proxyError: null, error: null }) }}
                            onremovefile={(file) => { this.setState({ loading: false, zipFiles: [], proxyError: null, error: null, isReplaceAction: false }); }}
                        />
                    </DialogContentText>
                    <DialogContentText component={"div"}>
                        {this.state.error && this.ShowErrorMessage(this.state.error)}
                        {this.state.proxyError && <div dangerouslySetInnerHTML={{ __html: this.state.proxyError }} />}
                        {this.state.isReplaceAction && this.ShowReplaceMessage()}
                    </DialogContentText>
                </DialogContent>

                <DialogActions>
                    <Button
                        color="primary"
                        onClick={() => this.disableDisplay()}
                        autoFocus
                    >
                        cancel
                    </Button>
                    <Button
                        color="primary"
                        disabled={this.state.loading || this.state.zipFiles.length === 0}
                        onClick={() => this.handleUpload()}
                    >
                        {this.state.isReplaceAction ? "replace dataset" : "upload dataset"}
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    }
}