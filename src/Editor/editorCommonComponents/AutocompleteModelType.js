import React from "react";
import axios from "axios";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import CircularProgress from "@material-ui/core/CircularProgress";
import Autocomplete, { /*createFilterOptions */ } from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/Typography';
import { LOOK_UP_CV_RECOMMENDED } from "../../config/editorConstants";
//const filter = createFilterOptions();

export default class AutocompleteModelType extends React.Component {

    constructor(props) {
        super(props);
        this.state = { initialValuesArray: this.props.initialValuesArray || [], choices: [], loading: false, source: null };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValuesArray: nextProps.initialValuesArray || [],
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    getRecommendedChoices = (query) => {
        if (query.trim().length <= 2) {
            //this.setState({ choices: [] });
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ loading: true, source: source });
        const params = `${this.props.field}=${encodeURI(query)}`;
        axios.get(LOOK_UP_CV_RECOMMENDED(params), { cancelToken: source.token })
            .then((res) => {
                this.setState({ choices: res.data, loading: false, source: null })
            }).catch((err) => {
                console.log(err);
                this.setState({ loading: false, source: null, choices: [] })
            });
    }

    setValues = (action, newValue) => {
        const { initialValuesArray } = this.state;
        switch (action) {
            case "onchange":
                console.log(action, newValue);
                initialValuesArray.length = 0;
                const a = newValue.map(item => item.value || item);
                //const a = newValue.map(item => item.display_name || item);
                for (let index = 0; index < a.length; index++) {
                    let item2Add = a[index];
                    initialValuesArray.push(item2Add);
                }
                break;
            default: break;
        }
        this.setState({ initialValuesArray, choices: [] });
        this.onBlur();
    }

    onBlur = () => {
        this.props.updateModel_Array(this.props.field, this.state.initialValuesArray);
    }

    renderAutocomplete = (default_valueArray) => {
        return <div onBlur={this.onBlur}><Grid container direction="row" alignItems="center" justifyContent="flex-start">
            <Grid item sm={12}>
                <Autocomplete
                    multiple
                    selectOnFocus
                    handleHomeEndKeys
                    freeSolo
                    //autoHighlight
                    clearOnBlur={true}
                    blurOnSelect={true}
                    value={default_valueArray}
                    loading={this.state.loading}
                    onChange={(event, newValue) => {
                        if (!newValue) {
                            return;
                        }
                        if (typeof newValue === "object") {
                            this.setValues("onchange", newValue);
                        } else {
                            this.setValues("onchange", newValue);
                        }
                        //this.setValues("onchange", newValue);
                    }}
                    options={this.state.loading ? [] : this.state.choices.map((item) => { return { "value": item[1], "display_name": item[1] } })}
                    //options={this.state.loading ? [] : this.state.choices.map((item) => { return { "value": item[0], "display_name": item[1] } })}
                    //options={this.state.loading ? [] : this.state.choices.map(item => item[1])}
                    filterOptions={(options, params) => {
                        //const filtered = filter(options, params);
                        const filtered = options;//filter(options, params);
                        if (!this.state.loading) {
                            if (params.inputValue !== '') {
                                filtered.splice(0, 0, {
                                    value: params.inputValue,
                                    display_name: `Missing ${params.inputValue}? Add "${params.inputValue}"`,
                                });
                            }
                        }
                        if (filtered && filtered.length === 0 && this.state.loading) {
                            return [{ display_name: "Loading please wait...", }]
                        }
                        return filtered;
                    }}
                    getOptionLabel={(option) => {
                        //const ar = this.props.recommended_choices.filter(recommended_choice => recommended_choice.value === option.value);
                        //if (ar.length > 0) {
                        //  return ar[0].display_name;
                        //}
                        return option.display_name || option;
                    }}
                    renderInput={(params) => (
                        <TextField {...params}
                            label={this.props.label}
                            helperText={this.props.help_text}
                            placeholder={this.props.placeholder ? this.props.placeholder : ""}
                            required={this.props.required}
                            variant="outlined"
                            onChange={(e) => { this.getRecommendedChoices(e.target.value) }}
                            InputProps={{
                                ...params.InputProps,
                                endAdornment: (
                                    <React.Fragment>
                                        {this.state.loading ? (
                                            <CircularProgress color="inherit" size={20} />
                                        ) : null}
                                        {params.InputProps.endAdornment}
                                    </React.Fragment>
                                )
                            }}
                        />)}
                    renderTags={(value, getTagProps) =>
                        value.map((option, index) => (
                            <Chip variant="outlined" label={option} {...getTagProps({ index })} />
                        ))
                    }
                    renderOption={(option, { inputValue }) => {
                        const matches = match(option.display_name, inputValue);
                        const parts = parse(option.display_name, matches);
                        return (
                            <div>
                                {parts.map((part, index) => (
                                    <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                        {part.text}
                                    </span>
                                ))}
                            </div>
                        );
                    }
                    }
                />
            </Grid>
        </Grid>
        </div>
    }



    render() {
        const initialValuesArray = this.state.initialValuesArray;
        return <div className="mb1" onBlur={this.onBlur}>
            <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
            {/*<Typography className="section-links" >{this.props.help_text} </Typography>*/}
            <div>
                {this.renderAutocomplete(initialValuesArray)}
            </div>
        </div>
    }


}