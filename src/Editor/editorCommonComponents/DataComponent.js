import React from "react";
import axios from "axios";
import { toast } from "react-toastify";
import ProgressBar from "../../componentsAPI/CommonComponents/ProgressBar";
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { LIST_DATA_OF_RECORD, DELETE_DATA_ENDPOINT, IS_ADMIN } from "../../config/constants";
import FilePondData from "./FilePondData";
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
//import TablePagination from '@material-ui/core/TablePagination';
//import TableSortLabel from '@material-ui/core/TableSortLabel';
//import Toolbar from '@material-ui/core/Toolbar';
//import Checkbox from '@material-ui/core/Checkbox';
//import IconButton from '@material-ui/core/IconButton';
//import Tooltip from '@material-ui/core/Tooltip';
//import FormControlLabel from '@material-ui/core/FormControlLabel';
//import Switch from '@material-ui/core/Switch';
//import DeleteIcon from '@material-ui/icons/Delete';
//import FilterListIcon from '@material-ui/icons/FilterList';
//import clsx from 'clsx';
//import { lighten, makeStyles } from '@material-ui/core/styles';
//import Container from '@material-ui/core/Container';
//import { Typography } from "@material-ui/core";
//import Checkbox from '@material-ui/core/Checkbox';
//import ArrowDownward from '@material-ui/icons/ArrowDownward';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import Typography from "@material-ui/core/Typography";
import Tooltip from "@material-ui/core/Tooltip";
import { keycloak } from "../../App";

export default class DataComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = { source: null, loading: true, datasets: [], showUploadDialogue: false };
    }

    componentWillUnmount = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    componentDidMount() {
        this.retrieveRecordData();
    }

    retrieveRecordData = () => {
        if (this.props.model.hasOwnProperty("pk")) {
            if (this.state.source) {
                this.state.source.cancel("");
            }
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            this.setState({ source: source, loading: true });
            axios.get(LIST_DATA_OF_RECORD(this.props.model.pk), { cancelToken: source.token })
                .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
                .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err) })
        } else {
            this.setState({ loading: false, datasets: [] })
        }
    }

    successResponse = (res) => {
        //{"pk":73,"datasets":[]}
        //{"pk":73,"datasets":[{"id":33,"file":"0c87605483cc4a1a944a809464333a15/9109766257ab41b9ba86065531a34568/dataset.zip","date_stored":"2021-02-22T14:55:02.631996Z"matching_distributions": [51]}
        this.setState({ datasets: res.data.content_files });
        try {
            if (this.props.updateButtonText) {
                this.props.updateButtonText(res.data.content_files.length > 0 ? "Finish" : "Skip");
            }
        } catch (err) {
        }
    }

    errorResponse = (err) => {
        console.log(JSON.stringify(err));
    }

    hideUploadDialogue = () => {
        this.setState({ showUploadDialogue: false }, this.retrieveRecordData());
    }

    disableUploadButton = () => {
        if (IS_ADMIN(keycloak)) {
            return false;
        } else if (this.props.isSupervisor && (this.props.model?.management_object?.status === "i" || this.props.model?.management_object?.status === "u")) {
            return false;
        } else if (this.props.model?.management_object?.status === "i" || this.props.model?.management_object?.status === "d") {
            return false;
        }
        return true;
    }

    deleteItem = (row) => {
        axios.post(DELETE_DATA_ENDPOINT(this.props.model.pk), { "action": "delete" }, {
            headers: {
                'Cache-Control': 'no-store, max-age=0',
                'Pragma': 'no-cache',
                'Expires': 'Wed, 21 Oct 2015 07:28:00 GMT',
                'scope': "private",
                'ELG-RESOURCE-ID': this.props.model.pk,
                'filename': row.file
            },
        }).then((response) => {
            toast.success("File deleted.", { autoClose: 3500 });
            this.retrieveRecordData();
        }).catch((errorResponse) => {
            console.log("error while deleting dataset", errorResponse);
        });
    }

    render() {
        if (this.state.loading) {
            return <ProgressBar />
        }
        if (!this.props.model.hasOwnProperty("pk")) {
            return <></>;
        }
        if (this.state.showUploadDialogue) {
            return <FilePondData model={this.props.model} hideUploadDataset={this.hideUploadDialogue} datasets={this.state.datasets || []} />
        }

        const data = this.state.datasets.map(item => {
            return {
                ...item,
                name: item.file.substring(item.file.lastIndexOf("/") + 1),
                "date_stored": new Intl.DateTimeFormat("en-GB", {
                    year: "numeric",
                    month: "long",
                    day: "2-digit"
                }).format(new Date(item.date_stored))
            }
        });

        return <div>
            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                <Grid item xs>
                    <div className="pb-3 wd-100">
                        <Button disabled={this.disableUploadButton() ? true : false} color="primary" variant="outlined" fullWidth onClick={() => { this.setState({ showUploadDialogue: true }) }}>Upload data</Button>
                    </div>
                    {
                        data.length > 0 && <div>
                            <TableContainer component={Paper}>
                                <Table aria-label="uploaded dataset table" size="small">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell align="center">Name</TableCell>
                                            <TableCell align="center">Upload date</TableCell>
                                            <TableCell align="center">Assigned to distribution</TableCell>
                                            <TableCell align="right">Actions</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {data.map((row) => (
                                            <TableRow key={row.id}>
                                                <TableCell align="center">{row.name}</TableCell>
                                                <TableCell align="center">{row.date_stored}</TableCell>
                                                <TableCell align="center">{row.matching_distributions.length > 0 ? "Yes" : "No"}</TableCell>
                                                {row.matching_distributions.length > 0 ? <TableCell scope="row" align="right">
                                                    <Tooltip title="In order to delete a dataset you should first unlink it from the corresponding distribution and submit/save your record.">
                                                        <span>
                                                            <IconButton disabled={true}>
                                                                <DeleteForeverIcon color="disabled" />
                                                            </IconButton>
                                                        </span>
                                                    </Tooltip>
                                                </TableCell> : <TableCell scope="row" align="right">
                                                    <IconButton disabled={this.disableUploadButton() ? true : false} onClick={() => { this.deleteItem(row) }}>
                                                        <DeleteForeverIcon color="secondary" />
                                                    </IconButton>
                                                </TableCell>}
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                                {<Typography variant="caption">*In order to delete a dataset you should first unlink it from the corresponding distribution and save your record.</Typography>}
                            </TableContainer>
                        </div>
                    }
                </Grid>
            </Grid>
        </div >
    }
}