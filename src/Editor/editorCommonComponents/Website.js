import React from "react";
import TextField from '@material-ui/core/TextField';
//import Button from '@material-ui/core/Button';
//import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
//import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import validator from 'validator';
//import Autocomplete from '@material-ui/lab/Autocomplete';

const websiteValidationError = "Enter a valid website. e.g. https://www.example.com \n";
const options = { protocols: ['http', 'https', 'ftp'], require_tld: false, require_protocol: true, require_host: true, require_valid_protocol: true, allow_underscores: false, host_whitelist: false, host_blacklist: false, allow_trailing_dot: false, allow_protocol_relative_urls: false, disallow_auth: false }

export default class Website extends React.Component {
    constructor(props) {
        super(props);
        this.state = { website: props.website, error: false };
    }

    componentDidMount() {
        const { website } = this.state;
        if (website) {
            const valid = validator.isURL(website || "", options);
            const error = !valid;
            this.setState({ error });
        }
    }

    setWebsite = (value) => {
        this.setState({ website: value });
        //this.props.updateModel_website(this.props.field, value);
    }

    blur = () => {
        const { website } = this.state;
        if (website) {
            const valid = validator.isURL(website || "", options);
            const error = !valid;
            this.setState({ error });
        }
        this.props.updateModel_website(this.props.field, this.state.website);
    }

    render() {
        const { type, required, label, help_text, className, disable, placeholder } = this.props;
        const { website, error } = this.state;
        return <div className="mb2">
            <TextField className={className}
                type={type}
                error={error ? true : false}
                required={required}
                disabled={disable}
                label={label}
                placeholder={placeholder ? placeholder : ""}
                variant="outlined"
                helperText={error ? `${websiteValidationError}` : help_text}
                inputProps={{ name: 'websiteValue' }}
                value={website}
                onChange={(e) => this.setWebsite(e.target.value)}
                onBlur={(e) => this.blur()}
            />
        </div>
    }
}