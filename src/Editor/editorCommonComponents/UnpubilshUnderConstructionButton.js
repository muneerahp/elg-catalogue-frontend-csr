import React from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import messages from "../../config/messages";
import { EDITOR_UNPUBLISH_UNDER_CONSTRUCTION } from "../../config/editorConstants";
import { toast } from "react-toastify";
import {
    getEditorPath_labeless_Schema,
    TOOL_SERVICE,
    CORPUS,
    LCR,
    LD,
    ML_MODEL,
    GRAMMAR,
    N_GRAM_MODEL,
    OTHER,
    Uncategorized_Language_Description
} from "../../config/constants";

class UnpublishUnderConstructionButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = { disableButton: false }
    }

    unpublishRequest = () => {
        this.setState({ disableButton: true })
        const entity_type = [TOOL_SERVICE, CORPUS, LCR, LD, ML_MODEL, GRAMMAR, N_GRAM_MODEL, OTHER, Uncategorized_Language_Description].includes(this.props.entity_type) ? "LanguageResource" : this.props.entity_type// => Project||Organization
        const asssembledData = {
            pk: this.props.pk,
            described_entity: {
                entity_type: entity_type,
                lr_subclass: {
                    lr_type: this.props.entity_type
                }
            }
        };
        //console.log(asssembledData);
        //console.log(this.props);
        axios.patch(EDITOR_UNPUBLISH_UNDER_CONSTRUCTION(this.props.pk), { reason: "default message: completion of under construction record" }).then((res) => {
            toast.success("Request submitted successfully.", { autoClose: 3500 });
            const editorUrl = getEditorPath_labeless_Schema(asssembledData);
            //console.log(editorUrl);
            if (editorUrl) {
                this.props.history.push({ pathname: editorUrl });
            }
        }).catch((err) => {
            this.setState({ disableButton: false })
            console.log(JSON.stringify(err));
            toast.error("Request submission failed", { autoClose: 3500 });
        });
    }

    render() {
        return <span>
            <Tooltip title={this.props.under_construction || messages.lookup_unpublish_button_tooltip} placement="top">
                <span>
                    <Button disabled={this.state.disableButton} className="inner-link-default-editor--teal" color="primary" onClick={() => { this.unpublishRequest() }}>{messages.lookup_unpublish_button_text}</Button>
                </span>
            </Tooltip>
        </span>
    }
}

export default withRouter(UnpublishUnderConstructionButton);