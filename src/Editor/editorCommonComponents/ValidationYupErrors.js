import React from "react";
import { withRouter } from "react-router-dom";
import { Alert, AlertTitle } from '@material-ui/lab';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
import CancelIcon from '@material-ui/icons/Cancel';
import ErrorIcon from '@material-ui/icons/Error';
import messages from "../../config/messages";
import UserInfo from "./UserInfo";

class ValidationYupErrors extends React.Component {
    constructor(props) {
        super(props);
        this.state = { open: true }
    }

    clickHanlder = (item) => {
        const { pathname = "" } = this.props.location;
        if (pathname.toLowerCase().includes("/create/corpus")) {
            this.props.handleYupErrorClick(item);
        } else if (pathname.toLowerCase().includes("/create/service")) {
            this.props.handleYupErrorClick(item);
        } else if (pathname.toLowerCase().includes("/create/lexical_conceptual_resource")) {
            this.props.handleYupErrorClick(item);
        } else if (pathname.toLowerCase().includes("/create/language_description")) {
            this.props.handleYupErrorClick(item);
        }
    }

    render() {
        const error = this.props.yupError;
        if (!error || (error && error.errors && error.errors.length === 0)) {
            return <UserInfo requiredFields={this.props.requiredFields || []} hide_under_construction={this.props.hide_under_construction} />;
        }
        return <div>
            <Collapse in={this.state.open} >
                <Alert
                    //variant="standard"
                    //variant="outlined"
                    //variant="filled"
                    severity="error"
                    action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                                this.setState({ open: false })
                            }}
                        >
                            <CancelIcon />
                        </IconButton>
                    }
                >
                    <AlertTitle>{messages.dialog_foundValidationError}</AlertTitle>
                    <ol>
                        {error && error.errors && error.errors.map((item, index) => {
                            return <li key={index} onClick={() => { this.clickHanlder(item) }} className="yup-error-link">
                                <h5 style={{ "color": "Tomato" }}>
                                    <div dangerouslySetInnerHTML={{ __html: item }} />
                                </h5>
                            </li>;
                        })}
                    </ol>
                </Alert>
            </Collapse>
            {!this.state.open && <span style={{ backgroundColor: "red" }}>
                <ErrorIcon onClick={(e) => { this.setState({ open: true }) }} />
            </span>}
        </div>
    }
}
export default withRouter(ValidationYupErrors);