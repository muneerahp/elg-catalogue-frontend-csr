import React from "react";
import axios from "axios";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
import Typography from '@material-ui/core/Typography';
//import { mediaTypeObj, media_type_identifier } from "../Models/GenericModels";
import Grid from '@material-ui/core/Grid';
import { LOOK_UP_ANY_TYPE } from "../../config/editorConstants";
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import IdentifierSingle from "./IdentifierSingle";
import LanguageSpecificText from "./LanguageSpecificText";
const filter = createFilterOptions();

export default class MediaTypeAutocomplete extends React.Component {

    constructor(props) {
        super(props);
        this.state = { mediaTypeInitialArray: this.props.default_valueArray || [], mediaTypeChoices: [], loading: false };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            mediaTypeInitialArray: nextProps.default_valueArray || [],
        };
    }

    getTextTypeChoices = (entityType, inputTextType) => {
        if (inputTextType.trim().length <= 2) {
            return;
        }
        this.setState({ loading: true })
        axios.get(LOOK_UP_ANY_TYPE(entityType, inputTextType))
            .then((res) => {
                this.setState({ mediaTypeChoices: res.data, loading: false })
            }).catch((err) => {
                console.log(err);
                this.setState({ loading: false })
            });
    }

    setMediaType = (action, mediaTypeIndex, autocompleteSelectedObj) => {
        const mediaType = this.state.mediaTypeInitialArray;
        var oldmediaTypeObj = (mediaType && mediaType.length >= mediaTypeIndex) ? mediaType[mediaTypeIndex] : {};
        var { category_label = {}, media_type_identifier = {} } = oldmediaTypeObj || "";
        switch (action) {
            case "categoryValue":
                if (!autocompleteSelectedObj) {
                    return;
                }
                category_label["en"] = autocompleteSelectedObj;
                if (oldmediaTypeObj.hasOwnProperty('editor-placeholder')) {
                    delete oldmediaTypeObj['editor-placeholder'];
                }
                break;
            case "addMediaType":
                const filteredArray = mediaType.filter(item => {
                    if (item && item.category_label) {
                        return item.category_label["en"];
                    }
                    return false;
                });
                if (filteredArray.length !== mediaType.length) {
                    return;//do not add element if there are empty values
                }
                mediaType.push(JSON.parse(JSON.stringify(this.props.mediaTypeObj)));
                this.props.updateModel_MediaType(this.props.entityType, mediaType);
                return mediaType;
            case "removeMediaType":
                mediaType.splice(mediaTypeIndex, 1);
                if (mediaType && mediaType.length === 0) {
                    this.props.updateModel_MediaType(this.props.entityType, null);
                    return;
                }
                this.props.updateModel_MediaType(this.props.entityType, mediaType);
                return mediaType;
            case "autoCompleteSelected":
                if (autocompleteSelectedObj.similarity) {
                    delete autocompleteSelectedObj["similarity"];
                }
                if (autocompleteSelectedObj.display_name) {
                    delete autocompleteSelectedObj["display_name"];
                }
                if (autocompleteSelectedObj.hasOwnProperty('editor-placeholder')) {
                    delete autocompleteSelectedObj['editor-placeholder'];
                }
                mediaType[mediaTypeIndex] = autocompleteSelectedObj;
                this.props.updateModel_MediaType(this.props.entityType, mediaType);
                return mediaType;
            default: break;
        }
        oldmediaTypeObj = { ...oldmediaTypeObj, category_label: category_label, media_type_identifier: media_type_identifier }
        mediaType[mediaTypeIndex] = oldmediaTypeObj;
        this.props.updateModel_MediaType(this.props.entityType, mediaType);
        return mediaType;
    }

    setLanguageSpecificText = (mediaTypeIndex, obj) => {
        const mediaType = this.state.mediaTypeInitialArray;
        mediaType[mediaTypeIndex].category_label = obj;
        if (mediaType[mediaTypeIndex].hasOwnProperty('editor-placeholder')) {
            delete mediaType[mediaTypeIndex]['editor-placeholder'];
        }
        this.props.updateModel_MediaType(this.props.entityType, mediaType);
    }

    updateIdentifier = (mediaTypeIndex, obj) => {
        const mediaType = this.state.mediaTypeInitialArray;
        mediaType[mediaTypeIndex][this.props.media_type_identifier] = obj;
        if (mediaType[mediaTypeIndex].hasOwnProperty('editor-placeholder')) {
            delete mediaType[mediaTypeIndex]['editor-placeholder'];
        }
        this.props.updateModel_MediaType(this.props.entityType, mediaType);
    }

    autocompleteTextField = (mediaType, mediaTypeIndex, languageKey) => {
        return <span>
            <Autocomplete className={"wd-100"}
                freeSolo
                clearOnBlur
                autoHighlight
                value={mediaType || ""}
                onChange={(event, newValue) => {
                    if (!newValue) {
                        return;
                    }
                    if (typeof newValue === "object") {
                        this.setMediaType("autoCompleteSelected", mediaTypeIndex, newValue);
                    } else {
                        this.setMediaType("categoryValue", mediaTypeIndex, newValue);
                    }
                }}
                options={this.state.mediaTypeChoices}
                filterOptions={(options, params) => {
                    const filtered = filter(options, params);
                    if (params.inputValue !== '') {
                        filtered.push({
                            category_label: { 'en': params.inputValue },
                            display_name: `Missing ${this.props.label}? Add "${params.inputValue}"`,
                        });
                    }
                    return filtered;
                }}
                getOptionLabel={(option) => option.category_label ? Object.values(option.category_label)[0] : option}
                renderInput={(params) => (
                    <TextField {...params} label={this.props.category_label} help_text={this.props.category_help_text} variant="outlined"
                        //onBlur={(e) => { this.setMediaType("categoryValue", mediaTypeIndex, e.target.value); }}
                        placeholder={this.props.category_placeholder}
                        onChange={(e) => { this.getTextTypeChoices(this.props.entityType, e.target.value) }}
                    />)}
                renderOption={(option, { inputValue }) => {
                    if (option.display_name) {
                        const matches1 = match(option.display_name, inputValue);
                        const parts1 = parse(option.display_name, matches1);
                        return parts1.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                {part.text}
                            </span>
                        ))
                    }

                    const matches = match(Object.values(option.category_label)[0], inputValue);
                    const parts = parse(Object.values(option.category_label)[0], matches);
                    const identifier = option[this.props.media_type_identifier];
                    //console.log(option , identifier)
                    return (
                        <div>
                            {
                                option.category_label && <div>
                                    {parts.map((part, index) => (
                                        <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                            {part.text}
                                        </span>
                                    ))}
                                </div>
                            }
                            {identifier && <div>
                                {identifier[this.props.media_type_classification_scheme] === "http://w3id.org/meta-share/meta-share/elg" ? "" :
                                    (`${identifier.value} - ${this.props.mediaType_choices.filter(mediaTypeChoice => mediaTypeChoice.value === identifier[this.props.media_type_classification_scheme])[0].display_name}`)
                                }
                            </div>
                            }

                        </div>
                    );
                }
                }
            />
            {/* {<Button className="inner-link-outlined--purple" onClick={(e) => this.setMediaType("addMediaType", mediaTypeIndex)}>{"Add"}</Button>}*/}
            {mediaTypeIndex >= 1 && <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                <Grid item><Button className="inner-link-outlined--purple" onClick={(e) => this.setMediaType("removeMediaType", mediaTypeIndex)}>{"Remove"}</Button></Grid>
            </Grid>
            }

        </span>
    }

    onBlur = () => {
        if (this.state.mediaTypeInitialArray.length === 0) {
            this.props.updateModel_MediaType(this.props.entityType, []);
        } else if (this.state.mediaTypeInitialArray.length === 1) {
            if (!this.state.mediaTypeInitialArray[0].hasOwnProperty('pk')) {
                if (this.state.mediaTypeInitialArray[0].category_label && !this.state.mediaTypeInitialArray[0].category_label["en"]) {
                    this.props.updateModel_MediaType(this.props.entityType, []);
                }
            }
        }
    }


    render() {
        const {
            label, help_text,
            category_help_text, category_label, category_required, category_type,
            mediaType_choices, mediaType_help_text, mediaType_label, mediaType_read_only, mediaType_required,
            value_label, value_required,value_help_text, value_placeholder,
            media_type_identifier_label, media_type_identifier_help_text,
            media_type_identifier, media_type_identifierObj
        } = this.props;
        if (this.state.mediaTypeInitialArray.length === 0) {
            this.state.mediaTypeInitialArray.push(JSON.parse(JSON.stringify(this.props.mediaTypeObj)));
        }
        return <div className="pb-3" onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{label} </Typography>
                    <Typography className="section-links" >{help_text} </Typography>
                </Grid>
                {/* <Grid item sm={1}>
                    {this.state.mediaTypeInitialArray.length === 0 && <Button className="inner-link-outlined--purple" onClick={(e) => this.setMediaType("addMediaType", 0)}><CreateIcon className="xsmall-icon" /></Button>}
                </Grid>*/}
            </Grid>
            {
                this.state.mediaTypeInitialArray.map((mediaType, mediaTypeIndex) => {
                    return <div key={mediaTypeIndex}>
                        {
                            mediaType.category_label && !Object.values(mediaType.category_label)[0] ? this.autocompleteTextField(mediaType, mediaTypeIndex) :
                                <>
                                    <div>
                                        <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                            <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setMediaType("removeMediaType", mediaTypeIndex)}>{"Remove"}</Button></Grid>
                                        </Grid>

                                        <LanguageSpecificText
                                            defaultValueObj={mediaType.category_label || { "en": "" }}
                                            field={mediaTypeIndex}//don't care about category_label, instead track mediaTypeIndex
                                            type={category_type}
                                            required={category_required}
                                            label={category_label}
                                            help_text={mediaType.pk ? '': category_help_text}
                                            disable={mediaType.pk ? true : false}
                                            setLanguageSpecificText={this.setLanguageSpecificText}
                                        />
                                    </div>
                                    {mediaType.pk && !mediaType[media_type_identifier]
                                        ?
                                        (<></>)
                                        :
                                        (

                                            <div className="inner--group nested--group">
                                                <IdentifierSingle
                                                    initialIdentifierObj={mediaType[media_type_identifier] || {}}
                                                    field={mediaTypeIndex}
                                                    Identifier_Label={media_type_identifier_label}
                                                    Identifier_help_text={media_type_identifier_help_text}
                                                    identifier_scheme_choices={mediaType_choices}
                                                    identifier_scheme_help_text={mediaType.pk ? '': mediaType_help_text}
                                                    identifier_scheme_label={mediaType_label}
                                                    identifier_scheme_read_only={mediaType_read_only}
                                                    identifier_scheme_required={mediaType_required}
                                                    identifier_scheme_type={""}
                                                    identifier_value_label={value_label}
                                                    identifier_value_required={value_required}
                                                    identifier_value_type={""}
                                                    identifier_value_read_only={mediaType_read_only}
                                                    identifier_value_placeholder={value_placeholder}
                                                    identifier_value_helptext={mediaType.pk ? '': value_help_text}
                                                    classification_scheme={this.props.media_type_classification_scheme}
                                                    cheme_choice_to_hide="ELG domain classification"
                                                    scheme_choice_uri_to_hide="http://w3id.org/meta-share/meta-share/ELG_domainClassification"
                                                    disable={mediaType.pk ? true : false}
                                                    identifier_obj={media_type_identifierObj}
                                                    updateModel_Identifier={this.updateIdentifier}
                                                />
                                            </div>

                                        )}
                                    {((mediaType.category_label !== "") && ((this.state.mediaTypeInitialArray.length - 1) === mediaTypeIndex)) ?
                                        <div className="pt-3 form-block-area">
                                            <Grid container direction="row" justifyContent="flex-start" alignItems="baseline" >
                                                <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setMediaType("addMediaType", mediaTypeIndex)}>{"Add"}</Button></Grid>
                                            </Grid>
                                        </div>
                                        : <span></span>
                                    }
                                </>

                        }
                        <div style={{ marginTop: "1em" }} />
                    </div>
                })
            }

        </div >


    }
}