import React from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import ErrorIcon from '@material-ui/icons/Error';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import PublishIcon from '@material-ui/icons/Publish';
import BuildIcon from '@material-ui/icons/Build';
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { ReactComponent as CreateNewFolderIcon } from "./../../assets/elg-icons/editor/add-circle-1.svg";
import { ReactComponent as CreateDraft } from "./../../assets/elg-icons/editor/paper-write.svg";
import { toast } from "react-toastify";
import { EDITOR_UPLOAD_NEW_RECORD_ENDPOINT, EDITOR_UPDATE_EXISTING_RECORD, EDITOR_DRAFT_NEW_RECORD, EDITOR_UPDATE_DRAFT_RECORD, SUBCLASS_TYPE_OTHER, DOCKER_IMAGE } from "../../config/editorConstants";
import { Typography } from "@material-ui/core";
import { validateOrganization } from "../validators/OrganizationValidator";
import { draft_validateOrganization } from "../validatorsDraft/OrganizationValidatorDraft";
import { validateProject } from "../validators/ProjectValidator";//same for draft
import { validateToolService, validateDistributionLinks } from "../validators/ToolServiceValidator";
import { validateCorpus } from "../validators/CorpusValidator";
import { validateLcr } from "../validators/LcrValidator";
import { validateLd } from "../validators/LdValidator";
import { LIST_DATA_OF_RECORD } from "../../config/constants";
//draft valiation
import { draft_validateToolService, draft_validateDistributionLinks } from "../validatorsDraft/ToolServiceValidatorDraft";
import { draft_validateCorpus } from "../validatorsDraft/CorpusValidatorDraft";
import { draft_validateLd } from "../validatorsDraft/LdValidatorDraft";
import { draft_validateLcr } from "../validatorsDraft/LcrValidatorDraft";
//
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";

class DisplayModel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            display: false, draft_display: false, deactivateButton: false, validationInProgress: false, validationResponse: null, foundValidationError: false, under_construction: props.under_construction === true ? true : false, err: null,
            datasets: []
        };
    }

    retrieveRecordData = async () => {
        try {
            if (this.props.model.hasOwnProperty("pk")) {
                const res = axios.get(LIST_DATA_OF_RECORD(this.props.model.pk))
                return res;
            } else {
                return [];
            }
        } catch (err) {
        }
        return [];
    }

    isUpdate = () => {
        const { model } = this.props;
        const { pk } = model;
        return pk;
    }

    sendToBackend = () => {
        const { model } = this.props;
        const { pk } = model;
        this.setState({ deactivateButton: true });
        if (pk) {
            this.updateExistingRecord(pk)
        } else {
            this.uploadNewRecord();
        }
    }

    uploadNewRecord = () => {
        const { model } = this.props;
        axios.post(EDITOR_UPLOAD_NEW_RECORD_ENDPOINT, model, {
            headers: {
                'UNDER-CONSTRUCTION': this.props.under_construction === true ? true : false,
                'FUNCTIONAL-SERVICE': this.props.functional_service === true ? true : false,
                "SERVICE-COMPLIANT-DATASET": this.props.service_compliant_dataset === true ? true : false
            }
        }).then((res) => {
            this.success("Record created successfully.", res, true)
        }).catch((err) => {
            this.failure("Record creation failed.", err);
        });
    }

    updateExistingRecord = (pk) => {
        const { model } = this.props;
        axios.put(EDITOR_UPDATE_EXISTING_RECORD(pk), model, {
            headers: {
                'UNDER-CONSTRUCTION': this.props.under_construction === true ? true : false,
                'FUNCTIONAL-SERVICE': this.props.functional_service === true ? true : false,
                "SERVICE-COMPLIANT-DATASET": this.props.service_compliant_dataset === true ? true : false
            }
        }).then((res) => {
            this.success("Record updated successfully.", res, true);
        }).catch((err) => {
            this.failure("Record update failed.", err);
        });

    }

    success = (msg, res, preview) => {
        let under_construction = false;
        if (res.data.management_object) {
            res.data.management_object.under_construction === true ? under_construction = true : under_construction = false;
            this.props.tool_service && this.props.handleFunctionalService(res.data.management_object.functional_service === true ? true : false);
        }
        this.setState({ deactivateButton: false, under_construction: under_construction });
        toast.success(msg, { autoClose: 3500 });
        this.disableDisplay();
        this.props.set_backend_successful_response(res.data);
        if (preview) {
            const pk = res.data.pk;
            let elg_record_url = "";
            if (res.data.described_entity.entity_type === "Project") {
                elg_record_url = `/project/${pk}`;
            } else if (res.data.described_entity.entity_type === "Organization") {
                elg_record_url = `/organization/${pk}`;
            } else if (res.data.described_entity.entity_type === "LanguageResource" && res.data.described_entity.lr_subclass && res.data.described_entity.lr_subclass.lr_type === "ToolService") {
                elg_record_url = `/tool-service/${pk}`;
            } else if (res.data.described_entity.entity_type === "LanguageResource" && res.data.described_entity.lr_subclass && res.data.described_entity.lr_subclass.lr_type === "Corpus") {
                elg_record_url = `/corpus/${pk}`;
            } else if (res.data.described_entity.entity_type === "LanguageResource" && res.data.described_entity.lr_subclass && res.data.described_entity.lr_subclass.lr_type === "LexicalConceptualResource") {
                elg_record_url = `/lcr/${pk}`;
            } else if (res.data.described_entity.entity_type === "LanguageResource" && res.data.described_entity.lr_subclass && res.data.described_entity.lr_subclass.lr_type === "LanguageDescription") {
                elg_record_url = `/ld/${pk}`;
            } else {
                return;
            }
            //elg_record_url = `${window.location.origin}/catalogue/#${elg_record_url}`;
            //window.open(elg_record_url, "_self");
            this.props.history.push({ pathname: elg_record_url });
        }
    }

    duplicateName = (errData) => {
        if (errData.hasOwnProperty("duplication_error")) {
            const duplication_error = errData.duplication_error;
            const duplicate_records = errData.duplicate_records;
            if (duplication_error && duplicate_records) {
                const errorMessageArray = [];
                //duplication_error.map((errorMessage, index) => errorMessageArray.push(errorMessage));
                //duplicate_records.map((errorMessage, index) => errorMessageArray.push(errorMessage));
                errorMessageArray.push("There is already a record with this name. Please enter a different name.")
                return errorMessageArray;
            }
        } else {
            return null;
        }
    }

    versionError = (errData) => {
        if (errData.hasOwnProperty("described_entity") && errData.described_entity.hasOwnProperty("version")) {
            const veresionErrorArray = errData.described_entity.version.map(item => item);
            //return veresionErrorArray;
            return veresionErrorArray && veresionErrorArray.length > 0 && ["Version error. " + veresionErrorArray[0]];
        } else {
            return null;
        }
    }

    accessibleThroughQueryFunctionalServiceError = (errData) => {
        try {
            const model = this.props.model;
            if (model.described_entity.entity_type === "LanguageResource" && model.described_entity.lr_subclass.lr_type === "LexicalConceptualResource" && this.props.service_compliant_dataset) {
                if (errData.hasOwnProperty("described_entity") && errData.described_entity.hasOwnProperty("lr_subclass") && errData.described_entity.lr_subclass.hasOwnProperty("dataset_distribution")) {
                    if (Array.isArray(errData.described_entity.lr_subclass.dataset_distribution) && errData.described_entity.lr_subclass.dataset_distribution.length > 0) {
                        return [errData.described_entity.lr_subclass.dataset_distribution[0]];
                    }
                } else {
                    return null;
                }
            }
        } catch (err) {
            return null;
        }
        return null;
    }

    glottologError = (errData) => {
        try {
            let stringi = (errData && JSON.stringify(errData)) || '';
            stringi = stringi.toLocaleLowerCase();
            if (stringi.indexOf("glottolog_code") >= 0) {
                return ["Invalid value for Glottolog code. "];
            }
        } catch (err) {
            return null;
        }
        return null;
    }


    failure = (msg, err = "") => {
        this.setState({ deactivateButton: false });
        toast.error(msg, { autoClose: 3500 });
        this.disableDisplay();
        try {
            if (err && err.response && err.response.data) {
                const isDuplicateError = this.duplicateName(err.response.data);
                const isVersionError = this.versionError(err.response.data);
                const isAccessibleThroughQueryError = this.accessibleThroughQueryFunctionalServiceError(err.response.data);
                const isGlottologError = this.glottologError(err.response.data);
                if (isDuplicateError) {
                    this.props.set_yup_error({ errors: isDuplicateError });
                    return;
                } else if (isVersionError && isVersionError.length > 0) {
                    this.props.set_yup_error({ errors: isVersionError });
                    return;
                } else if (isAccessibleThroughQueryError && isAccessibleThroughQueryError.length > 0) {
                    this.props.set_yup_error({ errors: isAccessibleThroughQueryError });
                    return;
                } else if (isGlottologError && isGlottologError.length > 0) {
                    this.props.set_yup_error({ errors: isGlottologError });
                    return;
                }
                this.props.set_backend_error_response(err.response.data);
            } else {
                this.props.set_backend_error_response("unknown server error");
            }
        } catch (catcherr) {
            console.log(err);
            this.props.set_backend_error_response("unknown server error");
        }
    }

    enableDisplay = (draft = false) => {
        if (draft) {
            this.setState({ display: false, draft_display: true });
            setTimeout(() => this.draft_validate(), 1000);
        } else {
            this.setState({ display: true, draft_display: false });
            setTimeout(() => this.validate(), 1000);
        }
    }

    disableDisplay = () => {
        this.props.set_backend_successful_response(this.props.model);
        if (!this.state.foundValidationError) {
            this.props.set_yup_error(null);
        }
        this.setState({ display: false, draft_display: false, validationInProgress: false, validationResponse: null, foundValidationError: false });
        if (this.state.err) {
            this.props.set_yup_error(this.state.err);
        }
    }

    clearEmptiesObj = (o) => {
        for (var k in o) {
            if (!o[k] || typeof o[k] !== "object" || (Array.isArray(o[k]) && o[k].length === 0)) {
                continue;
            }
            this.clearEmptiesObj(o[k]);
            if (Object.keys(o[k]).length === 0) {
                delete o[k];
            }
            if (o[k] && o[k].hasOwnProperty("ld_subclass_type")) {
                if (o[k].ld_subclass_type === "ld_subclass_type") {
                    delete o[k];
                }
            }
            /*if (o[k] && o[k].hasOwnProperty("editor-placeholder")) {
                delete o[k];
            }*/
            if (o[k] && o[k].hasOwnProperty("editor-placeholder")) {
                o[k] = null;
            }
            if (Array.isArray(o[k])) {
                if (o[k].length === 0) {
                    return true;
                }
                o[k] = o[k].filter(item => {
                    if (!item) {
                        return false;
                    }
                    if (typeof item === "object") {
                        if (item.hasOwnProperty("selection")) {
                            delete item["selection"];
                            if (item.email) {
                                item.landing_page = "";
                            }
                            if (item.landing_page) {
                                item.email = "";
                            }
                        }
                        if (item.hasOwnProperty("actor_type")) {
                            if (item.actor_type === "actor_type") {
                                return false;
                            }
                        }
                        if (item.hasOwnProperty("corpus_media_type")) {
                            if (item.corpus_media_type === "corpus_media_type") {
                                return false;
                            }
                        }
                        if (item.hasOwnProperty("lcr_media_type")) {
                            if (item.lcr_media_type === "lcr_media_type") {
                                return false;
                            }
                        }
                        if (item.hasOwnProperty("ld_media_type")) {
                            if (item.ld_media_type === "ld_media_type") {
                                return false;
                            }
                        }
                        if (item.hasOwnProperty('editor-placeholder')) {
                            return false;
                        }
                        if (item.hasOwnProperty('en')) {
                            //return item["en"];
                            return true;
                        }
                        if (Object.keys(item).length === 0) {
                            return false;
                        }
                        if (Object.values(item).filter(v => v).length === 0) {
                            return false;
                        }
                        for (let index = 0; index < Object.keys(item).length; index++) {
                            const obj_key = Object.keys(item)[index];
                            const value = item[obj_key];
                            if (obj_key && obj_key.indexOf("identifier") > 0) {
                                if (value && Object.values(value).filter(obj_key_value => obj_key_value).length === 0) {
                                    delete item[obj_key];
                                }
                            }
                            /*if (!obj_key) {
                                delete item[obj_key];
                            }
                            if (!value) {
                                delete item[obj_key];
                            }*/
                            if (!obj_key && !value) {
                                delete item[obj_key];
                                //console.log('delete: ', obj_key, item[obj_key])
                            }

                        }
                        return true;
                    } else if (typeof item === "string") {
                        return item;
                    } else {
                        return true;
                    }
                });
            }
        }
        return o;
    }

    unlinkDeletedFiles = (model, dataFiles) => {
        if (model?.described_entity?.lr_subclass) {
            if (model.described_entity.lr_subclass.software_distribution) {

                //-----------------------------ELG_COMPATIBLE_DISTRIBUTION assign
                if (this.props.functional_service) {
                    const docker_image_distributions_counter = model.described_entity.lr_subclass.software_distribution.filter(item => item && item.software_distribution_form === DOCKER_IMAGE).length;
                    if (docker_image_distributions_counter === 1) {
                        model.described_entity.lr_subclass.software_distribution.filter(item => item && item.software_distribution_form === DOCKER_IMAGE).map(distribution => {
                            distribution.elg_compatible_distribution = true;
                            return distribution;
                        })
                    }
                }

                model.described_entity.lr_subclass.software_distribution.map(distribution => {
                    //-----------------------------ELG_COMPATIBLE_DISTRIBUTION unassign
                    if (!this.props.functional_service) {
                        if (distribution.elg_compatible_distribution) {
                            distribution.elg_compatible_distribution = false;
                        }
                    }
                    //
                    if (distribution.package) {
                        const foundMatchingDataFile = dataFiles.some(item => item.id === distribution.package.id && item.file === distribution.package.file);
                        if (foundMatchingDataFile) {
                            const matchingFile = dataFiles.find(item => item.id === distribution.package.id && item.file === distribution.package.file);
                            distribution.package = matchingFile;//in case of replace the timestamp changes so replace the whole object
                        } else {
                            distribution.package = null;
                        }
                    }
                    return distribution;
                })
            } else if (model.described_entity.lr_subclass.dataset_distribution) {
                model.described_entity.lr_subclass.dataset_distribution.map(distribution => {
                    if (distribution.dataset) {
                        const foundMatchingDataFile = dataFiles.some(item => item.id === distribution.dataset.id && item.file === distribution.dataset.file);
                        if (foundMatchingDataFile) {
                            const matchingFile = dataFiles.find(item => item.id === distribution.dataset.id && item.file === distribution.dataset.file);
                            distribution.dataset = matchingFile;//in case of replace the timestamp changes so replace the whole object
                        } else {
                            distribution.dataset = null;
                        }
                    }
                    return distribution;
                })
            }
        }
        return model;
    }

    deleteUnspecifiedDistribution = (model) => {
        if (model?.described_entity?.lr_subclass) {
            if (!model.described_entity.lr_subclass.unspecified_part) {
                if (model.described_entity.lr_subclass.dataset_distribution) {
                    model.described_entity.lr_subclass.dataset_distribution.map(distribution => {
                        distribution.distribution_unspecified_feature = [];
                        return distribution;
                    })
                }
            }
        }
        return model;
    }

    saveDraft = () => {
        this.setState({ deactivateButton: true });
        let model = this.props.model;
        if (!model.hasOwnProperty("pk")) {//create new draft record
            axios.post(EDITOR_DRAFT_NEW_RECORD, model, {
                headers: {
                    'UNDER-CONSTRUCTION': this.props.under_construction === true ? true : false,
                    'FUNCTIONAL-SERVICE': this.props.functional_service === true ? true : false,
                    "SERVICE-COMPLIANT-DATASET": this.props.service_compliant_dataset === true ? true : false
                }
            }).then((res) => {
                let under_construction = false;
                if (res.data.management_object) {
                    res.data.management_object.under_construction === true ? under_construction = true : under_construction = false;
                    this.props.tool_service && this.props.handleFunctionalService(res.data.management_object.functional_service === true ? true : false);
                }
                this.setState({ deactivateButton: false, under_construction: under_construction });
                toast.success("Record saved as draft successfully.", { autoClose: 3500 });
                this.disableDisplay();
                this.props.set_backend_successful_response(res.data);
            }).catch((err) => {
                this.failure("Record could not be saved as draft.", err);
            });
        } else {//update existing draft record
            const send_obj = JSON.parse(JSON.stringify(model));
            delete send_obj["management_object"];
            axios.put(EDITOR_UPDATE_DRAFT_RECORD(send_obj.pk), send_obj, {
                headers: {
                    'UNDER-CONSTRUCTION': this.props.under_construction === true ? true : false,
                    'FUNCTIONAL-SERVICE': this.props.functional_service === true ? true : false,
                    "SERVICE-COMPLIANT-DATASET": this.props.service_compliant_dataset === true ? true : false
                }
            }).then((res) => {
                let under_construction = false;
                if (res.data.management_object) {
                    res.data.management_object.under_construction === true ? under_construction = true : under_construction = false;
                    this.props.tool_service && this.props.handleFunctionalService(res.data.management_object.functional_service === true ? true : false);
                }
                this.setState({ deactivateButton: false, under_construction: under_construction });
                toast.success("Record saved as draft successfully.", { autoClose: 3500 });
                this.disableDisplay();
                this.props.set_backend_successful_response(res.data);
            }).catch((err) => {
                this.failure("Record could not be saved as draft.", err);
            });
        }
    }

    draft_validate = async () => {
        const dataSet = await this.retrieveRecordData();
        let dataFiles = [];
        try {
            if (dataSet) {
                dataFiles = dataSet?.data?.content_files || [];
            }
        } catch (err) {
        }
        let model = this.props.model;
        model = this.unlinkDeletedFiles(model, dataFiles);
        //model = this.deleteUnspecifiedDistribution(model);
        model = this.clearEmptiesObj(model);
        model = this.clearEmptiesObj(model);//to remove any nulls from first run of clearEmptiesObj
        model = this.clearEmptiesObj(model);//to remove any nulls from first run of clearEmptiesObj

        const for_info = false;
        let validated = null;
        let message = (entityType, name) => (<div>
            <Typography>{messages.editor_create_first_part} {'save as draft the '}<span className="teal--font">{name}</span> {'record'} {messages.editor_create_confirmation}</Typography>
            <Typography variant="overline">Draft records do not have a landing page. Submit your record in order to visit and preview its landing page.</Typography>
        </div>);
        if (model.described_entity.entity_type === "Organization") {
            message = message("Organization", this.props.model.described_entity.organization_name ? this.props.model.described_entity.organization_name["en"] : "");
            validated = draft_validateOrganization(model, this.props.data);
        } else if (model.described_entity.entity_type === "Project") {
            message = message("Project", this.props.model.described_entity.project_name ? this.props.model.described_entity.project_name["en"] : "");
            validated = validateProject(model, this.props.data);//SAME FOR DRAFT AS FOR SUBMIT
        } else if (model.described_entity.entity_type === "LanguageResource" && model.described_entity.lr_subclass.lr_type === "ToolService") {
            message = message("Tool/Service", this.props.model.described_entity.resource_name ? this.props.model.described_entity.resource_name["en"] : "");
            validated = draft_validateToolService(model, this.props.data, this.props.schema_lr_subclass, this.props.functional_service);
        } else if (model.described_entity.entity_type === "LanguageResource" && model.described_entity.lr_subclass.lr_type === "Corpus") {
            message = message("Corpus", this.props.model.described_entity.resource_name ? this.props.model.described_entity.resource_name["en"] : "");
            validated = draft_validateCorpus(model, this.props.data, this.props.schema_lr_subclass, this.props.schema_text_part, this.props.schema_audio_part, this.props.schema_video_part, this.props.schema_text_numerical, this.props.schema_image_part, for_info);
        } else if (model.described_entity.entity_type === "LanguageResource" && model.described_entity.lr_subclass.lr_type === "LexicalConceptualResource") {
            message = message("Lexical Conceptual Resource", this.props.model.described_entity.resource_name ? this.props.model.described_entity.resource_name["en"] : "");
            validated = draft_validateLcr(model, this.props.data, this.props.schema_lr_subclass, this.props.schema_text_part, this.props.schema_audio_part, this.props.schema_video_part, null, this.props.schema_image_part, for_info);
        } else if (model.described_entity.entity_type === "LanguageResource" && model.described_entity.lr_subclass.lr_type === "LanguageDescription") {
            message = message("Language Description", this.props.model.described_entity.resource_name ? this.props.model.described_entity.resource_name["en"] : "");
            if (!model.described_entity?.lr_subclass?.ld_subclass) {
                model.described_entity.lr_subclass.ld_subclass = SUBCLASS_TYPE_OTHER;
            }
            validated = draft_validateLd(model, this.props.data, this.props.schema_lr_subclass, this.props.schema_text_part, null, this.props.schema_video_part, null, this.props.schema_image_part, this.props.schema_grammar_part, this.props.schema_ml_model_part, for_info);
        }
        if (validated) {
            if (model.described_entity.entity_type === "LanguageResource" && model.described_entity.lr_subclass.lr_type === "ToolService") {
                validated
                    .then(
                        draft_validateDistributionLinks(model, this.props.data, this.props.schema_lr_subclass, this.props.functional_service)
                            .then(success => {
                                this.setState({ validationInProgress: false, foundValidationError: false, validationResponse: message, err: null });
                            }).catch(err => {
                                const errorMessage = <Typography component="div"><ul>{err.errors.map((validationError, index) => <li key={index}><h5 style={{ "color": "Tomato" }}><div dangerouslySetInnerHTML={{ __html: validationError }} /></h5></li>)}</ul></Typography>;
                                this.setState({ validationInProgress: false, foundValidationError: true, validationResponse: errorMessage, err: err });
                            })
                    )
                    .catch(err => {
                        console.log(err);
                        const errorMessage = <Typography component="div"><ul>{err.errors.map((validationError, index) => <li key={index}><h5 style={{ "color": "Tomato" }}><div dangerouslySetInnerHTML={{ __html: validationError }} /></h5></li>)}</ul></Typography>;
                        this.setState({ validationInProgress: false, foundValidationError: true, validationResponse: errorMessage, err: err });
                    });
            } else {
                validated
                    .then(success => {
                        this.setState({ validationInProgress: false, foundValidationError: false, validationResponse: message, err: null });
                    })
                    .catch(err => {
                        console.log(err);
                        //const errorMessage = <Typography component="div"><ul>{err.errors.map((validationError, index) => <li key={index}><h5 style={{ "color": "Tomato" }}>{validationError}</h5></li>)}</ul></Typography>;
                        const errorMessage = <Typography component="div"><ul>{(err && err.errors) ? err.errors.map((validationError, index) => <li key={index}><h5 style={{ "color": "Tomato" }}><div dangerouslySetInnerHTML={{ __html: validationError }} /></h5></li>) : ''}</ul></Typography>;
                        this.setState({ validationInProgress: false, foundValidationError: true, validationResponse: errorMessage, err: err });
                    });
            }
        }
    }

    validate = async () => {
        const dataSet = await this.retrieveRecordData();
        let dataFiles = [];
        try {
            if (dataSet) {
                dataFiles = dataSet?.data?.content_files || [];
            }
        } catch (err) {
        }

        let model = this.props.model;
        model = this.unlinkDeletedFiles(model, dataFiles);
        //model = this.deleteUnspecifiedDistribution(model);
        model = this.clearEmptiesObj(model);
        model = this.clearEmptiesObj(model);//to remove any nulls from first run of clearEmptiesObj
        model = this.clearEmptiesObj(model);//to remove any nulls from first run of clearEmptiesObj
        const for_info = false;

        let validated = null;
        let message = (entityType, name) => <Typography>{messages.editor_create_first_part} {this.isUpdate() ? "update " : "ceate "}<span className="teal--font">{name}</span>{messages.editor_create_confirmation}</Typography>;
        if (model.described_entity.entity_type === "Organization") {
            message = message("Organization", this.props.model.described_entity.organization_name ? this.props.model.described_entity.organization_name["en"] : "");
            validated = validateOrganization(model, this.props.data);
        } else if (model.described_entity.entity_type === "Project") {
            message = message("Project", this.props.model.described_entity.project_name ? this.props.model.described_entity.project_name["en"] : "");
            validated = validateProject(model, this.props.data);
        } else if (model.described_entity.entity_type === "LanguageResource" && model.described_entity.lr_subclass.lr_type === "ToolService") {
            message = message("Tool/Service", this.props.model.described_entity.resource_name ? this.props.model.described_entity.resource_name["en"] : "");
            validated = validateToolService(model, this.props.data, this.props.schema_lr_subclass, this.props.functional_service);
        } else if (model.described_entity.entity_type === "LanguageResource" && model.described_entity.lr_subclass.lr_type === "Corpus") {
            message = message("Corpus", this.props.model.described_entity.resource_name ? this.props.model.described_entity.resource_name["en"] : "");
            validated = validateCorpus(model, this.props.data, this.props.schema_lr_subclass, this.props.schema_text_part, this.props.schema_audio_part, this.props.schema_video_part, this.props.schema_text_numerical, this.props.schema_image_part, dataFiles, for_info);
        } else if (model.described_entity.entity_type === "LanguageResource" && model.described_entity.lr_subclass.lr_type === "LexicalConceptualResource") {
            message = message("Lexical Conceptual Resource", this.props.model.described_entity.resource_name ? this.props.model.described_entity.resource_name["en"] : "");
            validated = validateLcr(model, this.props.data, this.props.schema_lr_subclass, this.props.schema_text_part, this.props.schema_audio_part, this.props.schema_video_part, null, this.props.schema_image_part, dataFiles, this.props.service_compliant_dataset, for_info);
        } else if (model.described_entity.entity_type === "LanguageResource" && model.described_entity.lr_subclass.lr_type === "LanguageDescription") {
            message = message("Language Description", this.props.model.described_entity.resource_name ? this.props.model.described_entity.resource_name["en"] : "");
            if (!model.described_entity?.lr_subclass?.ld_subclass) {
                model.described_entity.lr_subclass.ld_subclass = SUBCLASS_TYPE_OTHER;
            }
            validated = validateLd(model, this.props.data, this.props.schema_lr_subclass, this.props.schema_text_part, null, this.props.schema_video_part, null, this.props.schema_image_part, this.props.schema_grammar_part, this.props.schema_ml_model_part, dataFiles, for_info);
        }
        if (validated) {
            if (model.described_entity.entity_type === "LanguageResource" && model.described_entity.lr_subclass.lr_type === "ToolService") {
                validated
                    .then(
                        validateDistributionLinks(model, this.props.data, this.props.schema_lr_subclass, this.props.functional_service, dataFiles)
                            .then(success => {
                                this.setState({ validationInProgress: false, foundValidationError: false, validationResponse: message, err: null });
                            }).catch(err => {
                                const errorMessage = <Typography component="div"><ul>{err.errors.map((validationError, index) => <li key={index}><h5 style={{ "color": "Tomato" }}><div dangerouslySetInnerHTML={{ __html: validationError }} /></h5></li>)}</ul></Typography>;
                                this.setState({ validationInProgress: false, foundValidationError: true, validationResponse: errorMessage, err: err });
                            })
                    )
                    .catch(err => {
                        console.log(err);
                        const errorMessage = <Typography component="div"><ul>{err.errors.map((validationError, index) => <li key={index}><h5 style={{ "color": "Tomato" }}><div dangerouslySetInnerHTML={{ __html: validationError }} /></h5></li>)}</ul></Typography>;
                        this.setState({ validationInProgress: false, foundValidationError: true, validationResponse: errorMessage, err: err });
                    });
            } else {
                validated
                    .then(success => {
                        this.setState({ validationInProgress: false, foundValidationError: false, validationResponse: message, err: null });
                    })
                    .catch(err => {
                        console.log(err);
                        //const errorMessage = <Typography component="div"><ul>{err.errors.map((validationError, index) => <li key={index}><h5 style={{ "color": "Tomato" }}>{validationError}</h5></li>)}</ul></Typography>;
                        const errorMessage = <Typography component="div"><ul>{(err && err.errors) ? err.errors.map((validationError, index) => <li key={index}><h5 style={{ "color": "Tomato" }}><div dangerouslySetInnerHTML={{ __html: validationError }} /></h5></li>) : ''}</ul></Typography>;
                        this.setState({ validationInProgress: false, foundValidationError: true, validationResponse: errorMessage, err: err });
                    });
            }
        }
    }

    hanldeUnderConstruction = (e) => {
        //this.setState({ under_construction: e.target.checked });
        this.props.handleUnderConstruction(e.target.checked);
    }

    handleFunctionalService = (e) => {
        this.props.handleFunctionalService(e.target.checked);
    }

    handle_service_compliant_dataset = (e) => {
        this.props.handle_service_compliant_dataset(e.target.checked);
    }

    render() {
        //console.log(JSON.stringify(this.props.model, null, 4));
        return <>
            <Grid item container xs={4} alignItems="center" justifyContent="flex-start">
                {
                    !this.props.hide_checkboxes && this.props.model && this.props.model.described_entity && this.props.model.described_entity.entity_type === "LanguageResource" && <Grid item><Tooltip title={messages.editor_under_construction}><FormControlLabel
                        control={<Checkbox
                            checked={this.props.under_construction === true ? true : false}
                            color="primary"
                            inputProps={{ 'aria-label': 'under construction checkbox' }}
                            onChange={e => { this.hanldeUnderConstruction(e) }}
                        />}
                        label={messages.label_under_construction} />
                    </Tooltip></Grid>
                }
                {
                    !this.props.hide_checkboxes && this.props.model && this.props.model.described_entity && this.props.model.described_entity.entity_type === "LanguageResource" && this.props.model.described_entity.lr_subclass.lr_type === "ToolService" && <Grid item><Tooltip title={messages.dialog_functional_service_title_message}><FormControlLabel
                        control={<Checkbox
                            checked={this.props.functional_service === true ? true : false}
                            color="primary"
                            inputProps={{ 'aria-label': 'functional service checkbox' }}
                            onChange={e => { this.handleFunctionalService(e) }}
                        />}
                        label={messages.label_functional_service} />
                    </Tooltip></Grid>
                }
                {
                    !this.props.hide_checkboxes && this.props.show_lcr_elg_compatible_checkbox && this.props.model && this.props.model.described_entity && this.props.model.described_entity.entity_type === "LanguageResource" && this.props.model.described_entity.lr_subclass.lr_type === "LexicalConceptualResource" && <Grid item><Tooltip title={messages.dialog_service_compliant_dataset_title_message}><FormControlLabel
                        control={<Checkbox
                            checked={this.props.service_compliant_dataset === true ? true : false}
                            color="primary"
                            inputProps={{ 'aria-label': 'service compliant dataset checkbox' }}
                            onChange={e => { this.handle_service_compliant_dataset(e) }}
                        />}
                        label={messages.label_service_compliant_dataset} />
                    </Tooltip></Grid>
                }
            </Grid>
            <Grid item container xs={8} alignItems="center" justifyContent="flex-end">
                {<Grid item>{((this.props.model && !this.props.model.management_object) || (this.props.model && this.props.model.management_object && this.props.model.management_object.status === "d")) && <Button id="save-button" className="inner-link-default--purple" onClick={() => this.enableDisplay(true)}><CreateDraft className="xsmall-icon" /> <span className="p-1">{messages.editor_button_savedraft}</span></Button>}</Grid>}
                {this.isUpdate() && <Grid item><Button id="save-button" className="inner-link-default--purple" onClick={() => this.enableDisplay(false)}><PublishIcon className="xsmall-icon" /> <span className="p-1">{messages.editor_button_save}</span></Button></Grid>}
                {!this.isUpdate() && <Grid item><Button id="save-button" className="inner-link-default--purple" onClick={() => this.enableDisplay(false)}><CreateNewFolderIcon className="xsmall-icon" /><span className="p-1">{messages.editor_button_save}</span></Button></Grid>}

            </Grid>

            {
                this.state.display && <Dialog open={this.state.display} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" maxWidth="lg" disableBackdropClick={true} fullWidth={true}>
                    {!this.state.validationResponse && <DialogTitle> <BuildIcon /> {' '}{messages.dialog_validationResponse}</DialogTitle>}
                    {this.state.validationResponse && <DialogTitle>{this.state.foundValidationError ? <div><ErrorIcon /> {messages.dialog_foundValidationError}</div> : <div><CheckBoxIcon />{messages.editor_button_save}</div>}</DialogTitle>}
                    {this.state.validationResponse && <DialogContent>
                        <DialogContentText component={"div"}>
                            {this.state.validationResponse}
                        </DialogContentText>
                    </DialogContent>
                    }
                    {this.state.validationResponse && <DialogActions>
                        <Button disabled={this.state.deactivateButton} onClick={this.disableDisplay} color="primary" autoFocus>{messages.dialog_validationResponse_close_button}</Button>
                        <Button autoFocus disabled={this.state.deactivateButton || this.state.foundValidationError} onClick={this.sendToBackend} color="primary">{messages.dialog_validationResponse_proceed_button}</Button>
                    </DialogActions>}
                </Dialog>
            }

            {
                this.state.draft_display && <Dialog open={this.state.draft_display} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" maxWidth="lg" disableBackdropClick={true} fullWidth={true}>
                    {!this.state.validationResponse && <DialogTitle> <BuildIcon /> {' '}{messages.dialog_validationResponse}</DialogTitle>}
                    {this.state.validationResponse && <DialogTitle>{this.state.foundValidationError ? <div><ErrorIcon /> {messages.dialog_foundValidationError}</div> : <div><CheckBoxIcon />{messages.editor_button_savedraft}</div>}</DialogTitle>}
                    {this.state.validationResponse && <DialogContent>
                        <DialogContentText component={"div"}>
                            {this.state.validationResponse}
                        </DialogContentText>
                    </DialogContent>
                    }
                    {this.state.validationResponse && <DialogActions>
                        <Button disabled={this.state.deactivateButton} onClick={this.disableDisplay} color="primary" autoFocus>{messages.dialog_validationResponse_close_button}</Button>
                        <Button autoFocus disabled={this.state.deactivateButton || this.state.foundValidationError} onClick={this.saveDraft} color="primary">{messages.dialog_validationResponse_proceed_button}</Button>
                    </DialogActions>}
                </Dialog>
            }
        </ >
    }

}

export default withRouter(DisplayModel)