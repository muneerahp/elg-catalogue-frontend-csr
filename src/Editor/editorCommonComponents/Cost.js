import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
//import { ReactComponent as CreateIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
import Button from '@material-ui/core/Button';
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
//import CurrencyTextField from '@unicef/material-ui-currency-textfield'
import TextField from '@material-ui/core/TextField';
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import { cost_obj } from "../Models/LrModel";
//import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";

export default class Cost extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: this.props.initialValue };
    }

    setValue = (action, value) => {
        let { initialValue } = this.state;
        switch (action) {
            case "amount":
                initialValue.amount = value;
                break;
            case "addCost":
                initialValue = JSON.parse(JSON.stringify(cost_obj));
                break;
            case "removeCost":
                initialValue = null;
                this.setState({ initialValue });
                this.props.updateModel(this.props.field, initialValue);
                return;
            default:
                break;
        }
        this.setState({ initialValue });
    }

    setCurrency = (event, selectedObjArray) => {
        const { initialValue } = this.state;
        initialValue.currency = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ initialValue });
    }

    onBlur = () => {
        this.props.updateModel(this.props.field, this.state.initialValue);
    }

    render() {
        const { initialValue } = this.state;
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {!initialValue && <Button className="inner-link-default--purple" onClick={(e) => this.setValue("addCost")}>{messages.group_elements_create}</Button>}
                </Grid>
            </Grid>
            {initialValue && <>
                <div className="pt-3">
                    <Grid container spacing={1} direction="row" justifyContent="space-between" alignItems="flex-end" >
                        <Grid item xs={7}>
                            <TextField className="wd-100 editor-ammount"
                                type={"number"}
                                required={this.props.formElements.amount.required}
                                placeholder={this.props.formElements.amount.placeholder || ""}
                                label={this.props.formElements.amount.label}
                                helperText={this.props.formElements.amount.help_text}
                                value={initialValue.amount === null ? "" : initialValue.amount}
                                inputProps={{ name: 'amount' }}
                                onChange={(e) => { this.setValue("amount", e.target.value) }}
                            />
                            {/* <CurrencyTextField className="wd-100"
                                placeholder={this.props.formElements.amount.placeholder || ""}
                                label={this.props.formElements.amount.label}
                                required={this.props.formElements.amount.required}
                                helperText={this.props.formElements.amount.help_text}
                                variant="outlined"
                                minimumValue="0"
                                value={initialValue.amount}
                                currencySymbol={initialValue.currency === "http://w3id.org/meta-share/meta-share/euro" ? "€" : ""}
                                outputFormat="number"
                                onChange={(event, value) => { this.setValue("amount", value) }}
            />*/}
                        </Grid>
                        <Grid item xs={5}>    <RecordSelectList
                            className="wd-100"
                            {...this.props.formElements.currency}
                            default_value={initialValue.currency || ""}
                            setSelectedvalue={this.setCurrency} />
                        </Grid>
                    </Grid>
                </div>
                <div>
                    <Grid container direction="row" alignItems="center" justifyContent="flex-start">
                        <Grid item>
                            {initialValue && <Button className="inner-link-default--purple" onClick={(e) => this.setValue("removeCost")}>{messages.group_elements_remove}</Button>}
                        </Grid>
                    </Grid>
                </div>
            </>}
        </div>
    }
}