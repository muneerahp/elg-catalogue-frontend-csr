import React from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import messages from "./../../config/messages";
//import { ReactComponent as CreateIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
//import { domain_identifier } from "../Models/OrganizationModel";

export default class OrganizationIdentifier extends React.Component {

    constructor(props) {
        super(props);
        this.state = { initialIdentifierObj: props.initialIdentifierObj || {} };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialIdentifierObj: nextProps.initialIdentifierObj || {},
        };
    }

    setIdentifier = (action, value) => {
        const identifier = this.props.initialIdentifierObj || {};
        identifier[this.props.classification_scheme] = identifier[this.props.classification_scheme] || "";
        identifier.value = identifier.value || "";
        switch (action) {
            case "identifier_value":
                identifier.value = value;
                //this.props.updateModel_Identifier(this.props.field, identifier);
                //this.setState({ identifier }, this.onblur);
                this.setState({ identifier });
                break;
            case "identifier_selection":
                identifier[this.props.classification_scheme] = value[0].value;
                this.props.updateModel_Identifier(this.props.field, identifier);
                break;
            case "addIdentifier":
                const new_identifier = { ...JSON.parse(JSON.stringify(this.props.identifier_obj)) };
                this.props.updateModel_Identifier(this.props.field, new_identifier);
                break;
            case "removeIdentifier":
                this.props.updateModel_Identifier(this.props.field, null);
                break;
            default: break;
        }
    }

    onblur = () => {
        if (Object.values(this.state.initialIdentifierObj).filter(value => value).length === 0) {
            //return;
        }
        this.props.updateModel_Identifier(this.props.field, this.state.initialIdentifierObj);
    }


    render() {
        const { identifierIndex,
            classification_scheme, scheme_choice_to_hide, scheme_choice_uri_to_hide,
            identifier_scheme_choices, identifier_scheme_help_text, identifier_scheme_label, identifier_scheme_required, identifier_scheme_type,
            identifier_value_label, identifier_value_required, identifier_value_type, identifier_value_placeholder, identifier_value_helptext, disable, Identifier_Label, Identifier_help_text
        } = this.props;

        const { initialIdentifierObj } = this.state;

        const default_value = initialIdentifierObj.value || "";
        const default_organization_scheme_choice = identifier_scheme_choices.filter(choice => choice.value === initialIdentifierObj[classification_scheme]).length > 0 ? identifier_scheme_choices.filter(choice => choice.value === initialIdentifierObj[classification_scheme])[0].display_name : "";

        if (default_organization_scheme_choice === scheme_choice_to_hide) {
            return <div key={identifierIndex}></div>
        }

        return <div className="pb-3 inner--group" key={identifierIndex} >
            <Grid container direction="row" alignItems="center" justifyContent="flex-start" >
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{Identifier_Label} </Typography>
                    <Typography className="section-links" >{Identifier_help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {!Object.keys(initialIdentifierObj).includes("value") && <Button disabled={disable} className="inner-link-default--purple" onClick={(e) => this.setIdentifier("addIdentifier")}>{messages.group_elements_create}</Button>}
                </Grid>
            </Grid>
            {Object.keys(initialIdentifierObj).includes("value") &&
                < >
                    <Grid container spacing={1} direction="row" justifyContent="space-between" alignItems="baseline" >
                        <Grid item xs={4}>
                            <TextField className="wd-100" type={identifier_scheme_type} required={identifier_scheme_required} label={identifier_scheme_label} helperText={identifier_scheme_help_text} disabled={disable} variant="outlined"
                                select
                                // error={default_organization_scheme_choice === ""}
                                value={default_organization_scheme_choice}
                                inputProps={{ name: 'identifier_selection' }}
                                onChange={(e) => this.setIdentifier("identifier_selection", identifier_scheme_choices.filter(scheme_choice => scheme_choice.display_name === e.target.value))}
                                onBlur={this.onblur}
                            >
                                {
                                    identifier_scheme_choices.filter(option => option.value !== scheme_choice_uri_to_hide).map((scheme_choice, scheme_choice_index) => (
                                        <MenuItem key={scheme_choice_index} value={scheme_choice.display_name}>
                                            {scheme_choice.display_name}
                                        </MenuItem>
                                    ))
                                }
                            </TextField>
                        </Grid>

                        <Grid item xs={7}>
                            <TextField className="wd-100" type={identifier_value_type} required={identifier_value_required} label={identifier_value_label} variant="outlined"
                                //error={default_value === ""}
                                placeholder={identifier_value_placeholder ? identifier_value_placeholder : ""}
                                disabled={disable}
                                value={default_value}
                                helperText={identifier_value_helptext}
                                onChange={(e) => this.setIdentifier("identifier_value", e.target.value)}
                                onBlur={this.onblur}
                            />
                        </Grid>

                        <Grid item xs={1}>{<Button disabled={disable} onClick={(e) => this.setIdentifier("removeIdentifier")}><RemoveCircleOutlineIcon className="small-icon" /></Button>}</Grid>

                    </Grid>
                </ >
            }
        </div>
    }
}