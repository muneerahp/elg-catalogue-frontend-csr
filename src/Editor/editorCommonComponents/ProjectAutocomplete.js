import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
//import { ReactComponent as CreateIcon } from "./../../assets/elg-icons/editor/navigation-arrows-down-1.svg";
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
import RelatedProject from "../EditorGenericComponents/RelatedProject";
import { genericProjectObj } from "../Models/ProjectModel";

export default class ProjectAutocomplete extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValueArray: props.initialValueArray || [] };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValueArray: nextProps.initialValueArray || []
        };
    }

    setValues = (action, index) => {
        const { initialValueArray } = this.state;
        switch (action) {
            case "addArrayItem":
                const filteredArray = initialValueArray.filter(item => {
                    if (item && item.project_name && item.project_name.hasOwnProperty("en") && item.project_name["en"]) {
                        return true;
                    }
                    return false;
                })
                if (filteredArray.length !== initialValueArray.length) {
                    return;//do not add element if there are empty ones
                }
                initialValueArray.push(JSON.parse(JSON.stringify(genericProjectObj)));
                this.setState({ initialValueArray }); //this.props.updateModel_Array(this.props.field, initialValueArray);
                break;
            case "removeArrayItem":
                initialValueArray.splice(index, 1);
                if (initialValueArray.length === 0) {
                    this.props.updateModel_Array(this.props.field, []);
                    return;
                }
                this.props.updateModel_Array(this.props.field, initialValueArray);
                break;
            default:
                break;
        }
        this.setState({ initialValueArray });
    }

    updateModel = (index, value) => {
        const { initialValueArray } = this.state;
        initialValueArray[index] = value;
        this.setState(initialValueArray);
        this.props.updateModel_Array(this.props.field, this.state.initialValueArray);
    }

    onBlur = () => {
        if (this.state.initialValueArray.length === 1) {
            const item = this.state.initialValueArray[0];
            if (item && item.project_name && item.project_name.hasOwnProperty("en") && !item.project_name["en"]) {///maybe we need more exploration on the other fields in order to see if any of them is filled
                this.props.updateModel_Array(this.props.field, []);
            }
        } else {
            this.props.updateModel_Array(this.props.field, this.state.initialValueArray);
        }

    }

    render() {
        const { initialValueArray } = this.state;
        initialValueArray.length === 0 && initialValueArray.push(JSON.parse(JSON.stringify(genericProjectObj)));
        return <div onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {/*initialValueArray.length === 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem")}><CreateIcon /></Button>*/}
                    {/*initialValueArray.length !== 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem")}><AddCircleOutlineIcon /></Button>*/}
                </Grid>
            </Grid>
            {
                initialValueArray.map((item, index) => {
                    return <div key={index}>
                        <Grid container className="mb1" direction="row" justifyContent="space-between" alignItems="baseline" >

                            {(item.project_name && item.project_name["en"] === "" && index !== 0) &&
                                <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                    <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", index)}>{"Remove"}</Button></Grid>
                                </Grid>
                            }
                            {(item.project_name === null || (item.project_name && item.project_name["en"] !== "")) && <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item>  <Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", index)}>{"Remove"}</Button></Grid>
                            </Grid>
                            }
                            <Grid item xs={12}><RelatedProject index={index} formStuff={this.props.formElements} initialValue={item} updateModel={this.updateModel} /></Grid>

                            {((item.project_name && item.project_name["en"] !== "") && ((initialValueArray.length - 1) === index)) ?
                                <Grid container className="pb1 pt1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                    <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", index)}>{"Add"}</Button></Grid>
                                </Grid>
                                : <span></span>
                            }


                        </Grid>
                    </div>
                })
            }
        </div>
    }
}