import React from "react";
import { Helmet } from "react-helmet";
import axios from "axios";
import ProgressBar from "../componentsAPI/CommonComponents/ProgressBar";
import { EDITOR_MODELS_SCHEMA } from "../config/editorConstants";
import Container from '@material-ui/core/Container';
//import { organization3 } from "./Models/OrganizationModel";
import { empty_organization } from "./Models/OrganizationModel";
//import org_schema from "../data/org_schema.json";
//import Stepper from '@material-ui/core/Stepper';
//import Step from '@material-ui/core/Step';
//import StepLabel from '@material-ui/core/StepLabel';
//import StepButton from '@material-ui/core/StepButton';
import Grid from '@material-ui/core/Grid';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import HorizontalTabPanel from "../componentsAPI/CustomVerticalTabs/HorizontalTabPanel";
//import StepIcon from "@material-ui/core/StepIcon";
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import OrganizationFirstStep from "./EditorOrganizationComponents/OrganizationFirstStep";
//import organizationSchemaParser from "../parsers/organizationSchemaParser";
//import RecordHasDivision from "./EditorOrganizationComponents/RecordHasDivision"
//import WavesIcon from "@material-ui/icons/Waves";
//import SaveIcon from '@material-ui/icons/Save';
//import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
//import RefreshIcon from '@material-ui/icons/Refresh';
import DisplayModel from "./editorCommonComponents/DisplayModel";
import LookupOrganizationName from "./EditorOrganizationComponents/LookupOrganizationName";
import { ORGANIZATION_TOP_TABS_HEADERS } from "./../config/editorConstants";
import DashboardAppBar from "../DashboardComponents/DashboardAppBar";
import ValidationYupErrors from "./editorCommonComponents/ValidationYupErrors";
import Division from "./EditorOrganizationComponents/Division";
import UserWarning from "./editorCommonComponents/UserWarning";
function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default class OrganizationEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      schema: null, keycloak: props.keycloak, model: props.model ? JSON.parse(JSON.stringify(props.model)) : JSON.parse(JSON.stringify(empty_organization)), tab: 0, backend_error_response: null, lookupOrganizationName: props.model ? false : true, yupError: null,
      showWarning: false
    };
    this.isUnmound = false;
  }

  componentDidMount() {
    this.isUnmound = true;
    axios.options(EDITOR_MODELS_SCHEMA("organization")).then(res => {
      if (this.isUnmound) {
        if (res.data.actions && res.data.actions.POST) {
          this.setState({ schema: res.data.actions.POST });
        }
      }
    });
  }

  set_backend_error_response = (error) => {
    this.setState({ backend_error_response: error })
  }

  set_backend_successful_response = (response) => {
    this.setState({ model: response, backend_error_response: null })
  }

  set_yup_error = (yupError) => {
    this.setState({ yupError });
  }

  updateModel = (model) => {
    this.setState({ model });
  }

  toggleTab = (index) => {
    this.setState({ tab: index });
  };

  hideLookUpByName = (organizationName, restProperties) => {
    const { model } = this.state;
    if (restProperties && restProperties.pk) {
      //model.pk = restProperties.pk;
      model.described_entity.pk = restProperties.pk;//add pk of generic record inside described_entity
      delete restProperties.pk;
    }
    model.described_entity.organization_name = { "en": organizationName };
    model.described_entity = { ...model.described_entity, ...restProperties };
    this.setState({ lookupOrganizationName: false, model: model });
  }

  handleDescriptionChange = () => {
    //this.setState({showWarning: true});
    const { model } = this.state;
    const { organization_bio = "" } = model.described_entity || {};
    (organization_bio && organization_bio["en"] && organization_bio["en"].length < 50) ? this.setState({ showWarning: true }) : this.setState({ showWarning: false })
  }

  render() {
    if (!this.state.schema) {
      return <ProgressBar />
    }

    const { model,
      //activeStep 
    } = this.state;
    //const hasDivision_default_Array = model.described_entity.has_division || [];
    const data = this.state.schema;

    const { lookupOrganizationName } = this.state;
    if (lookupOrganizationName) {
      return <LookupOrganizationName hideLookUpByName={this.hideLookUpByName} keycloak={this.props.keycloak} />
    }

    return (
      <>
        <Helmet>
          <title>ELG - Create Organization</title>
        </Helmet>
        <DashboardAppBar />
        <div className="editor-container-white pb-2">
          <Container maxWidth="xl">
            <div className="empty"></div>
            <Typography className="dashboard-title-box pb-05">Create a new organization</Typography>
            <div>
              {this.state.backend_error_response && <div>
                <h3>Error</h3>
                <div className=" boxed">
                  <Paper elevation={13} >
                    <code>
                      <pre id="special">
                        {JSON.stringify(this.state.backend_error_response, null, 2)}
                      </pre>
                    </code>
                  </Paper>
                </div>
              </div>
              }
            </div>
            {!this.state.backend_error_response && <ValidationYupErrors key={this.state.yupError ? JSON.stringify(this.state.yupError) : "1"} yupError={this.state.yupError} requiredFields={[]} hide_under_construction="true" />}
            {<UserWarning showWarning={this.state.showWarning} />}
          </Container>


          <div className="editor-actions-header">
            <Container maxWidth="xl">
              <Grid container direction="row" justifyContent="space-between" alignItems="center" className="grays--offwhite">
                <Grid item xs={6}>
                  <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="horizontal" aria-label="simple tabs example" className="simple-tabs-forms">
                    {ORGANIZATION_TOP_TABS_HEADERS.map((tab, index) => <Tab key={index} label={<div className="pt-2"> {tab}</div>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />)}
                  </Tabs>
                </Grid>
                <Grid item container xs={6} alignItems="center">
                  <DisplayModel data={data} model={this.state.model} set_backend_error_response={this.set_backend_error_response} set_backend_successful_response={this.set_backend_successful_response} set_yup_error={this.set_yup_error} />
                </Grid>
              </Grid>
            </Container>
          </div>
          <Container maxWidth="xl">
            <div className="editor-header-card-container organization">
              <HorizontalTabPanel value={this.state.tab} index={0} className="horizontal-tab-pannel">
                <OrganizationFirstStep  onDescriptionChange={this.handleDescriptionChange} {...this.props}  {...this.state} updateModel={this.updateModel} keycloak={this.props.keycloak} />
              </HorizontalTabPanel>
              <HorizontalTabPanel value={this.state.tab} index={1} className="horizontal-tab-pannel">
                <Division data={data} updateModel={this.updateModel} model={model} keycloak={this.props.keycloak} />
              </HorizontalTabPanel>
              {/* <HorizontalTabPanel value={this.state.tab} index={1} className="horizontal-tab-pannel">
                <RecordHasDivision  {...organizationSchemaParser.getHasDivision(data)} data={data} updateModel={this.updateModel} model={model} keycloak={this.props.keycloak} default_valueArray={hasDivision_default_Array} />
    </HorizontalTabPanel>*/}
            </div>
          </Container>
          <div className="editor-actions-bottom">
            <Container maxWidth="xl">
              <Grid container direction="row" justifyContent="flex-end" alignItems="center" className="pb-3 pt-3 grays--offwhite">
                <Grid item container xs={12} alignItems="center">
                  <DisplayModel data={data} model={this.state.model} set_backend_error_response={this.set_backend_error_response} set_backend_successful_response={this.set_backend_successful_response} set_yup_error={this.set_yup_error} />
                </Grid>
              </Grid>
            </Container>
          </div>


          {/*<div className="padding15" >
            <Button onClick={() => { sessionStorage.setItem("org", JSON.stringify(this.state.model)); }}><SaveIcon />SAVE</Button>
            <Button onClick={() => { sessionStorage.removeItem("org") }}><DeleteForeverIcon />Delete</Button>
            <Button onClick={() => { window.location.reload(true); }}><RefreshIcon />Refresh</Button>
            <Button onClick={() => {
              this.setState({ model: empty_organization })
            }}><DeleteForeverIcon />Clear all</Button>
            <DisplayModel data={data} model={this.state.model} set_backend_error_response={this.set_backend_error_response} set_backend_successful_response={this.set_backend_successful_response} />
          </div>*/}
          <div>
            {this.state.backend_error_response && <div style={{ marginBottom: "100px", paddingBottom: "100px" }}>
              <h3>Error</h3>
              <div className=" boxed">
                <Paper elevation={13} >
                  <code>
                    <pre id="special">
                      {JSON.stringify(this.state.backend_error_response, null, 2)}
                    </pre>
                  </code>
                </Paper>
              </div>
            </div>
            }
          </div>
        </div>
      </>
    );
  }
}