import React from "react";
import { withRouter } from "react-router-dom";
//import BottomNavigation from '@material-ui/core/BottomNavigation';
//import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
//import RestoreIcon from '@material-ui/icons/Restore';
//import FavoriteIcon from '@material-ui/icons/Favorite';
//import LocationOnIcon from '@material-ui/icons/LocationOn';
import { Helmet } from "react-helmet";
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import BlurLinearIcon from '@material-ui/icons/BlurLinear';
import { ReactComponent as OrganizationIcon } from "./../assets/elg-icons/buildings-modern.svg";
import { ReactComponent as LCRIcon } from "./../assets/elg-icons/archive-folder.svg";
import { ReactComponent as ToolServiceIcon } from "./../assets/elg-icons/settings-user.svg";
import { ReactComponent as ProjectIcon } from "./../assets/elg-icons/human-resources-team-settings.svg";
import { ReactComponent as ResourceIcon } from "./../assets/elg-icons/database.svg";

import DashboardAppBar from "../DashboardComponents/DashboardAppBar";

class SelectResource extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: 0, keycloak: props.keycloak };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(newValue) {
        this.setState({ "value": newValue });
        switch (newValue) {
            case 0:
                this.props.history.push("/create/service");
                break;
            case 1:
                this.props.history.push("/create/corpus");
                break;
            case 2:
                this.props.history.push("/create/project");
                break;
            case 3:
                this.props.history.push("/create/organization");
                break;
            case 4:
                this.props.history.push("/upload");
                break;
            case 5:
                this.props.history.push("/create/language_description?subclass=model");
                break;
            case 6:
                this.props.history.push("/create/lexical_conceptual_resource");
                break;
            case 7:
                this.props.history.push("/create/language_description?subclass=grammar");
                break;
            case 8:
                this.props.history.push("/create/language_description?subclass=other");
                break;
            default:
                break;
        }
    }


    render() {
        return (
            <div>
                <Helmet>
                    <title>ELG - Create resource</title>
                </Helmet>

                <DashboardAppBar {...this.props} />
                <div className="editor-container pb-150">
                    <Container maxWidth="xl">
                        <div className="empty"></div>
                        <Typography className="dashboard-title-box pb-3">
                            Add items
                        </Typography>
                        <div className="editor-main-card-container">
                            <Grid container alignItems="stretch" justifyContent="center" spacing={2}>
                                <Grid item style={{ display: 'flex' }} sm={4}>
                                    <Card style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <CardContent>
                                            <CardMedia>{<ToolServiceIcon className="editor--grid-image-mat-icon ToolIcon" />}</CardMedia>
                                            <Typography component="h3">Service or Tool</Typography>
                                            <Typography variant="body2">Services that run in the cloud, downloadable tools, source code, etc., that perform language processing and/or any Language Technology related operation, such as Machine Translation, Information extraction, linguistic annotation, automatic speech recognition, etc.</Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Button classes={{ root: 'inner-link-outlined--teal editor-card--button' }} value={this.state.value} onClick={(event, newValue) => { this.handleChange(0) }}> Go to form </Button>
                                        </CardActions>
                                    </Card >
                                </Grid>

                                <Grid item style={{ display: 'flex' }} sm={4}>
                                    <Card style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <CardContent>
                                            <CardMedia>{<ResourceIcon className="editor--grid-image-mat-icon ResourceIcon" />}</CardMedia>
                                            <Typography component="h3">Corpus</Typography>
                                            <Typography variant="body2">Structured collections of pieces of data, such as collections of text documents, audio transcripts, audio and video recordings, parallel corpora, linguistically annotated corpora, treebanks, etc. </Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Button classes={{ root: 'inner-link-outlined--teal editor-card--button' }} value={this.state.value} onClick={(event, newValue) => { this.handleChange(1) }}> Go to form </Button>
                                        </CardActions>
                                    </Card >
                                </Grid>
                                <Grid item style={{ display: 'flex' }} sm={4}>
                                    <Card style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <CardContent>
                                            <CardMedia>{<BlurLinearIcon className="editor--grid-image-mat-icon LDIcon" />}</CardMedia>
                                            <Typography component="h3">Model </Typography>
                                            <Typography variant="body2">Machine learning models, embeddings, language models, n-gram models</Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Button classes={{ root: 'inner-link-outlined--teal editor-card--button' }} value={this.state.value} onClick={(event, newValue) => { this.handleChange(5) }}> Go to form </Button>
                                        </CardActions>
                                    </Card >
                                </Grid>

                                <Grid item style={{ display: 'flex' }} sm={4}>
                                    <Card style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <CardContent>
                                            <CardMedia>{<BlurLinearIcon className="editor--grid-image-mat-icon LDIcon" />}</CardMedia>
                                            <Typography component="h3">Grammar</Typography>
                                            <Typography variant="body2">Computational grammars, rule-based descriptions</Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Button classes={{ root: 'inner-link-outlined--teal editor-card--button' }} value={this.state.value} onClick={(event, newValue) => { this.handleChange(7) }}> Go to form </Button>
                                        </CardActions>
                                    </Card >
                                </Grid>

                                <Grid item style={{ display: 'flex' }} sm={4}>
                                    <Card style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <CardContent>
                                            <CardMedia>{<LCRIcon className="editor--grid-image-mat-icon LCRIcon" />}</CardMedia>
                                            <Typography component="h3">Lexical/Conceptual resource</Typography>
                                            <Typography variant="body2">Computational lexica, terminological lexica, glossaries, thesauri, ontologies, gazeteers, word or phrase lists, etc.</Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Button classes={{ root: 'inner-link-outlined--teal editor-card--button' }} value={this.state.value} onClick={(event, newValue) => { this.handleChange(6) }}> Go to form </Button>
                                        </CardActions>
                                    </Card >
                                </Grid>

                                <Grid item style={{ display: 'flex' }} sm={4}>
                                    <Card style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <CardContent>
                                            <CardMedia>{<BlurLinearIcon className="editor--grid-image-mat-icon LDIcon" />}</CardMedia>
                                            <Typography component="h3">Uncategorized language description</Typography>
                                            <Typography variant="body2">Language descriptions that are not models or grammars (e.g. typological databases)</Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Button classes={{ root: 'inner-link-outlined--teal editor-card--button' }} value={this.state.value} onClick={(event, newValue) => { this.handleChange(8) }}> Go to form </Button>
                                        </CardActions>
                                    </Card >
                                </Grid>

                                <Grid item style={{ display: 'flex' }} sm={4}>
                                    <Card style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <CardContent>
                                            <CardMedia><ProjectIcon className="editor--grid-image-mat-icon ProjectIcon" /> </CardMedia>
                                            <Typography component="h3">Project</Typography>
                                            <Typography variant="body2">Projects that have funded the development or used language resources and technologies.</Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Button classes={{ root: 'inner-link-outlined--teal editor-card--button' }} value={this.state.value} onClick={(event, newValue) => { this.handleChange(2) }}> Go to form </Button>
                                        </CardActions>
                                    </Card >
                                </Grid>


                                <Grid item style={{ display: 'flex' }} sm={4}>
                                    <Card style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <CardMedia><OrganizationIcon className="editor--grid-image OrganizationIcon" /> </CardMedia>
                                        <CardContent>
                                            <Typography component="h3">
                                                Organization
                                            </Typography>
                                            <Typography variant="body2">Companies, research organizations, academic organizations, etc. active in Language Technology in Europe.</Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Button classes={{ root: 'inner-link-outlined--teal editor-card--button' }} value={this.state.value} onClick={(event, newValue) => { this.handleChange(3) }}> Go to form </Button>
                                        </CardActions>
                                    </Card >
                                </Grid>

                                {/*<Grid item style={{ display: 'flex' }} sm={4}>
                                    <Card style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
                                        <CardContent>
                                            <CardMedia><UploadIcon className="editor--grid-image mint--font" /> </CardMedia>
                                            <Typography component="h3">Upload metadata record</Typography>
                                            <Typography variant="body2">Create item with upload of metadata file(s)</Typography>
                                        </CardContent>
                                        <CardActions>
                                            <Button classes={{ root: 'inner-link-outlined--teal editor-card--button' }} value={this.state.value} onClick={(event, newValue) => { this.handleChange(4) }}> Upload</Button>
                                        </CardActions>
                                    </Card >
                                </Grid>*/}

                            </Grid>

                        </div>

                    </Container>
                </div>
            </div>
        )
    }
}
export default withRouter(SelectResource);