import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { ReactComponent as LaunchIcon } from "./../../assets/elg-icons/network-arrow.svg";
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import { ReactComponent as DistributionsIcon } from "./../../assets/elg-icons/database-share-1.svg";
import CardContent from '@material-ui/core/CardContent';
import AcceptLicence from "../CommonComponents/AcceptLicence";
import ResourceActor from "../CommonComponents/ResourceActor";
import DistributionVideoFeature from "./DistributionVideoFeature";
//import { OPEN_LICENCES } from "../../config/constants";
//import { OPEN_LICENCES_IDS } from "../../config/constants";
import { ALLOWS_ACCESS_WITH_SIGNATURE, ALLOWS_DIRECT_aCCESS, REQUIRES_USER_AUTHENTICATION, ALLOWS_PROCESSING } from "../../config/constants";
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";
import { IS_ADMIN, IS_CONTENT_MANAGER, SHOW_BILLING_CORPUS_PK, IS_CUSTOMER } from "../../config/constants";
import { keycloak } from "../../App";
import DownloadDatasetCurator from "../CommonComponents/DownloadDatasetCurator";
import Languages from "../CommonComponents/Languages";
import Chip from '@material-ui/core/Chip';
import { Helmet } from "react-helmet";
import Tooltip from '@material-ui/core/Tooltip';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';

export default class Distributions extends React.Component {

    language_domain_textGenre_audioGenre_speechGenre_videoGenre_imageGenre = (sizeItem, sizeItemIndex, metadataLanguage, amount, size_unit, size_text, size_text_label) => {

        let domainArray = sizeItem.domain ? sizeItem.domain.field_value.map((domain, domainIndex) => {
            const category_value = domain.category_label ? domain.category_label.field_value[metadataLanguage] || domain.category_label.field_value[Object.keys(domain.category_label.field_value)[0]] : "";
            //const category_label = domain.category_label ? domain.category_label.field_label[metadataLanguage] || domain.category_label.field_label["en"] : "";
            //const domain_identifier_label = domain.domain_identifier ? domain.domain_identifier.field_label[metadataLanguage] || domain.domain_identifier.field_label["en"] : "";
            const domain_identifier_value = (domain.domain_identifier && domain.domain_identifier.field_value.value) ? domain.domain_identifier.field_value.value.field_value : "";
            //const domain_classification_scheme = (domain.domain_identifier && domain.domain_identifier.field_value.domain_classification_scheme) ? domain.domain_identifier.field_value.domain_classification_scheme.field_value : "";
            return <div key={domainIndex}>
                <span className="info_value">
                    {category_value}
                    {/*(domain_classification_scheme && domain_identifier_value) ? " - " : ""} {<a target="_blank" rel="noopener noreferrer" href={domain_classification_scheme}>{domain_identifier_value}</a>*/}
                    {domain_identifier_value ? " - " : ""} {domain_identifier_value}
                </span>
            </div>
        }) : [];
        let text_genreArray = sizeItem.text_genre ? sizeItem.text_genre.field_value.map((text, textIndex) => {
            const category_value = text.category_label ? text.category_label.field_value[metadataLanguage] || text.category_label.field_value[Object.keys(text.category_label.field_value)[0]] : "";
            //const category_label = text.category_label ? text.category_label.field_label[metadataLanguage] || text.category_label.field_label["en"] : "";
            const text_genre_identifier_value = (text.text_genre_identifier && text.text_genre_identifier.field_value.value) ? text.text_genre_identifier.field_value.value.field_value : "";
            //const text_classification_scheme = (text.text_genre_identifier && text.text_genre_identifier.field_value.text_genre_classification_scheme) ? text.text_genre_identifier.field_value.text_genre_classification_scheme.field_value : "";
            return <div key={textIndex}>
                <span className="info_value">
                    {category_value}
                    {/*(text_classification_scheme && text_genre_identifier_value) ? " - " : ""} {<a target="_blank" rel="noopener noreferrer" href={text_classification_scheme}>{text_genre_identifier_value}</a>*/}
                    {text_genre_identifier_value ? " - " : ""} {text_genre_identifier_value}
                </span>
            </div>
        }) : [];;
        let audio_genre = sizeItem.audio_genre ? sizeItem.audio_genre.field_value.map((audio, audioIndex) => {
            const category_value = audio.category_label ? audio.category_label.field_value[metadataLanguage] || audio.category_label.field_value[Object.keys(audio.category_label.field_value)[0]] : "";
            //const category_label = audio.category_label ? audio.category_label.field_label[metadataLanguage] || audio.category_label.field_label["en"] : "";
            const audio_genre_identifier_value = (audio.audio_genre_identifier && audio.audio_genre_identifier.field_value.value) ? audio.audio_genre_identifier.field_value.value.field_value : "";
            //const audio_classification_scheme = (audio.audio_genre_identifier && audio.audio_genre_identifier.field_value.audio_genre_classification_scheme) ? audio.audio_genre_identifier.field_value.audio_genre_classification_scheme.field_value : "";
            return <div key={audioIndex}>
                <span className="info_value">
                    {category_value}
                    {/*(audio_classification_scheme && audio_genre_identifier_value) ? " - " : ""} {<a target="_blank" rel="noopener noreferrer" href={audio_classification_scheme}>{audio_genre_identifier_value}</a>*/}
                    {audio_genre_identifier_value ? " - " : ""} {audio_genre_identifier_value}
                </span>
            </div>
        }) : [];
        let speech_genreArray = sizeItem.speech_genre ? sizeItem.speech_genre.field_value.map((speach, speachIndex) => {
            const category_value = speach.category_label ? speach.category_label.field_value[metadataLanguage] || speach.category_label.field_value[Object.keys(speach.category_label.field_value)[0]] : "";
            //const category_label = speach.category_label ? speach.category_label.field_label[metadataLanguage] || speach.category_label.field_label["en"] : "";
            const speach_genre_identifier_value = (speach.speech_genre_identifier && speach.speech_genre_identifier.field_value.value) ? speach.speech_genre_identifier.field_value.value.field_value : "";
            //const speach_classification_scheme = (speach.speech_genre_identifier && speach.speech_genre_identifier.field_value.speech_genre_classification_scheme) ? speach.speech_genre_identifier.field_value.speech_genre_classification_scheme.field_value : "";
            return <div key={speachIndex}>
                <span className="info_value">
                    {category_value}
                    {/*(speach_classification_scheme && speach_genre_identifier_value) ? " - " : ""} {<a target="_blank" rel="noopener noreferrer" href={speach_classification_scheme}>{speach_genre_identifier_value}</a>*/}
                    {speach_genre_identifier_value ? " - " : ""} {speach_genre_identifier_value}
                </span>
            </div>
        }) : [];;
        let video_genreArray = sizeItem.video_genre ? sizeItem.video_genre.field_value.map((video, videoIndex) => {
            const category_value = video.category_label ? video.category_label.field_value[metadataLanguage] || video.category_label.field_value[Object.keys(video.category_label.field_value)[0]] : "";
            //const category_label = video.category_label ? video.category_label.field_label[metadataLanguage] || video.category_label.field_label["en"] : "";
            const video_genre_identifier_value = (video.video_genre_identifier && video.video_genre_identifier.field_value.value) ? video.video_genre_identifier.field_value.value.field_value : "";
            //const video_classification_scheme = (video.video_genre_identifier && video.video_genre_identifier.field_value.video_genre_classification_scheme) ? video.video_genre_identifier.field_value.video_genre_classification_scheme.field_value : "";
            return <div key={videoIndex}>
                <span className="info_value">
                    {category_value}
                    {/*(video_classification_scheme && video_genre_identifier_value) ? " - " : ""} {<a target="_blank" rel="noopener noreferrer" href={video_classification_scheme}>{video_genre_identifier_value}</a>*/}
                    {video_genre_identifier_value ? " - " : ""} {video_genre_identifier_value}
                </span>
            </div>
        }) : [];
        //let image_genre; doesn't exist in full corpus
        let image_genreArray = sizeItem.image_genre ? sizeItem.image_genre.field_value.map((image, imageIndex) => {
            const category_value = image.category_label ? image.category_label.field_value[metadataLanguage] || image.category_label.field_value[Object.keys(image.category_label.field_value)[0]] : "";
            //const category_label = image.category_label ? image.category_label.field_label[metadataLanguage] || image.category_label.field_label["en"] : "";
            const image_genre_identifier_value = (image.image_genre_identifier && image.image_genre_identifier.field_value.value) ? image.image_genre_identifier.field_value.value.field_value : "";
            //const image_classification_scheme = (image.image_genre_identifier && image.image_genre_identifier.field_value.image_genre_classification_scheme) ? image.image_genre_identifier.field_value.image_genre_classification_scheme.field_value : "";
            return <div key={imageIndex}>
                <span className="info_value">
                    {category_value}
                    {/*(image_classification_scheme && image_genre_identifier_value) ? " - " : ""} {<a target="_blank" rel="noopener noreferrer" href={image_classification_scheme}>{image_genre_identifier_value}</a>*/}
                    {image_genre_identifier_value ? " - " : ""} {image_genre_identifier_value}
                </span>
            </div>
        }) : [];
        let computed = sizeItem.computed ? sizeItem.computed.field_value : null;
        const hascontent = (amount !== null && amount !== 0) || (size_text) || domainArray.length > 0 || video_genreArray.length > 0 || image_genreArray.length > 0 || image_genreArray.length > 0 || speech_genreArray.length > 0 || text_genreArray.length > 0 || audio_genre.length > 0 || (sizeItem && sizeItem.language)
        return <div key={sizeItemIndex} hascontent={hascontent+""}>
            {amount !== null && amount !== 0 && <> <span className="info_value">{Intl.NumberFormat('en').format(amount)} {size_unit}</span> {computed === true && <> <Tooltip title="automatically computed"><InfoOutlinedIcon fontSize="medium" className="teal--font" /></Tooltip></>} </>}
            {size_text && <div className="padding15"><Typography className="bold-p--id">{size_text_label}</Typography><span className="info_value">{size_text}</span></div>}
            {/*languageArray.length > 0 && languageArray.map(lang => lang)*/}
            {<Languages language={sizeItem.language} from_tool={false} />}
            {domainArray.length > 0 && domainArray.map(domain => domain)}
            {video_genreArray.length > 0 && video_genreArray.map(video => video)}
            {image_genreArray.length > 0 && image_genreArray.map(image => image)}
            {speech_genreArray.length > 0 && speech_genreArray.map(speach => speach)}
            {text_genreArray.length > 0 && text_genreArray.map(text => text)}
            {audio_genre.length > 0 && audio_genre.map(audio => audio)}
        </div>
    }

    showAccessLocation = (data, distribution) => {
        let show_download_button = false;
        const { dataset = "" } = distribution;
        if (!dataset) {
            return true;//size is zero => do not show download button => show access location
        }
        let { amount = 0 } = distribution.cost ? distribution.cost.field_value : "";
        amount = amount.hasOwnProperty("field_value") ? amount.field_value : amount;
        if (distribution.licence_terms) {
            for (let licenceIndex = 0; distribution.licence_terms && licenceIndex < distribution.licence_terms.field_value.length; licenceIndex++) {
                const licence = distribution.licence_terms.field_value[licenceIndex];
                let licence_category = (licence.licence_category && licence.licence_category.field_value) || [];
                licence_category = licence_category.map(item => item.value);
                licence_category = licence_category.filter(item => item !== ALLOWS_PROCESSING);//remove processing from elg
                //console.log(licenceIndex, licence_category, licence);
                if (licence_category.includes(ALLOWS_DIRECT_aCCESS) && licence_category.length === 1) {
                    show_download_button = true;
                    return !show_download_button;
                } else if (licence_category.includes(ALLOWS_ACCESS_WITH_SIGNATURE) && licence_category.length === 1) {
                    if (amount === 0) {
                        show_download_button = true;
                        return !show_download_button;
                    }
                } else if (licence_category.includes(REQUIRES_USER_AUTHENTICATION) &&
                    (licence_category.includes(ALLOWS_DIRECT_aCCESS) || licence_category.includes(ALLOWS_ACCESS_WITH_SIGNATURE)) &&
                    licence_category.length === 2) {
                    if (keycloak && keycloak.authenticated) {
                        show_download_button = true;
                        return !show_download_button;
                    }
                }
            }
        }
        return !show_download_button; //show_download_button===true? show download button & do not show access loction
    }

    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }
        if (!data.described_entity.field_value.lr_subclass) {
            return <div></div>
        }
        const { dataset_distribution } = data.described_entity.field_value.lr_subclass.field_value || [];
        //console.log(dataset_distribution)
        if (!dataset_distribution) {
            return <div></div>
        }
        const show_billing = data.pk === SHOW_BILLING_CORPUS_PK && IS_CUSTOMER(keycloak);
        return <>
            {show_billing && <Helmet>
                <script src="https://js.chargebee.com/v2/chargebee.js" data-cb-site="european-language-grid-eu-test" > </script>
            </Helmet>}
            <Grid container className="upperSpace" spacing={2} direction="row">
                {dataset_distribution.field_value.map((item, index) => {
                    let dataset_distribution_form = item.dataset_distribution_form ? (item.dataset_distribution_form.label[metadataLanguage] || item.dataset_distribution_form.label[Object.keys(item.dataset_distribution_form.label)[0]]) : "";
                    let dataset_distribution_form_label = item.dataset_distribution_form ? (item.dataset_distribution_form.field_label[metadataLanguage] || item.dataset_distribution_form.field_label["en"]) : "";
                    let distribution_location = item.distribution_location ? item.distribution_location.field_value : "";
                    let distribution_location_label = item.distribution_location ? (item.distribution_location.field_label[metadataLanguage] || item.distribution_location.field_label["en"]) : "";
                    let download_location = item.download_location ? item.download_location.field_value : "";
                    let download_location_label = item.download_location ? (item.download_location.field_label[metadataLanguage] || item.download_location.field_label["en"]) : "";
                    let access_location = item.access_location ? item.access_location.field_value : "";
                    let access_location_label = item.access_location ? (item.access_location.field_label[metadataLanguage] || item.access_location.field_label["en"]) : "";
                    let samples_location = item.samples_location ? item.samples_location.field_value : "";
                    let samples_location_label = item.samples_location ? (item.samples_location.field_label[metadataLanguage] || item.samples_location.field_label["en"]) : "";
                    let is_accessed_by_label = item.is_accessed_by ? (item.is_accessed_by.field_label[metadataLanguage] || item.is_accessed_by.field_label["en"]) : "";
                    let package_format_label = item.package_format ? (item.package_format.field_label[metadataLanguage] || item.package_format.field_label["en"]) : "";
                    let package_format = item.package_format ? (item.package_format.label[metadataLanguage] || item.package_format.label["en"]) : "";


                    let is_accessed_byArray = (item.is_accessed_by && item.is_accessed_by.field_value.map((accessedBy, accessedByIndex) => {
                        let resource_name = accessedBy.resource_name ? accessedBy.resource_name.field_value[metadataLanguage] || accessedBy.resource_name.field_value[Object.keys(accessedBy.resource_name.field_value)[0]] : "";
                        let version = accessedBy.version ? accessedBy.version.field_value : "";
                        let full_metadata_record = commonParser.getFullMetadata(accessedBy.full_metadata_record);
                        return <div key={accessedByIndex}>
                            {full_metadata_record ?
                                <div className="padding5 internal_url">
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        <span className="info_value">{resource_name} ({version})</span>
                                    </Link>
                                </div> :
                                <span className="info_value">{resource_name} ({version})</span>
                            }
                        </div>
                    })) || [];
                    let is_displayed_by_label = item.is_displayed_by ? (item.is_displayed_by.field_label[metadataLanguage] || item.is_displayed_by.field_label["en"]) : "";
                    let is_displayed_by = (item.is_displayed_by && item.is_displayed_by.field_value.map((displayedBy, displayedByIndex) => {
                        let resource_name = displayedBy.resource_name ? displayedBy.resource_name.field_value[metadataLanguage] || displayedBy.resource_name.field_value[Object.keys(displayedBy.resource_name.field_value)[0]] : "";
                        let version = displayedBy.version ? displayedBy.version.field_value : "";
                        let full_metadata_record = commonParser.getFullMetadata(displayedBy.full_metadata_record);
                        return <div key={displayedByIndex}>
                            {full_metadata_record ?
                                <div className="padding5 internal_url">
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        <span className="info_value">{resource_name} ({version})</span>
                                    </Link>
                                </div> :
                                <span className="info_value">{resource_name} ({version})</span>
                            }
                        </div>
                    })) || [];
                    let is_queried_by_label = item.is_queried_by ? (item.is_queried_by.field_label[metadataLanguage] || item.is_queried_by.field_label["en"]) : "";
                    let is_queried_by = (item.is_queried_by && item.is_queried_by.field_value.map((queriedBy, queriedByIndex) => {
                        let resource_name = queriedBy.resource_name.field_value ? queriedBy.resource_name.field_value[metadataLanguage] || queriedBy.resource_name.field_value[Object.keys(queriedBy.resource_name.field_value)[0]] : "";
                        let version = queriedBy.version ? queriedBy.version.field_value : "";
                        let full_metadata_record = commonParser.getFullMetadata(queriedBy.full_metadata_record);
                        return <div key={queriedByIndex}>
                            {full_metadata_record ?
                                <div className="padding5 internal_url">
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        <span className="info_value">{resource_name} ({version})</span>
                                    </Link>
                                </div> :
                                <span className="info_value">{resource_name} ({version})</span>
                            }
                        </div>
                    })) || [];

                    let distribution_unspecified_feature_label = item.distribution_unspecified_feature ? (item.distribution_unspecified_feature.field_label[metadataLanguage] || item.distribution_unspecified_feature.field_label["en"]) : "";
                    let unspecifiedFeature = item.distribution_unspecified_feature ? item.distribution_unspecified_feature.field_value : "";
                    let sizeArray = (unspecifiedFeature.size && unspecifiedFeature.size.field_value.map((sizeItem, sizeItemIndex) => {
                        let amount = sizeItem.amount ? sizeItem.amount.field_value : "";
                        let size_unit = sizeItem.size_unit ? (sizeItem.size_unit.field_value.label ? (sizeItem.size_unit.field_value.label[metadataLanguage] || sizeItem.size_unit.field_value.label[Object.keys(sizeItem.size_unit.field_value.label)[0]]) : sizeItem.size_unit.field_value.value) : "";
                        let size_text = sizeItem.size_text ? sizeItem.size_text.field_value : "";
                        let size_text_label = sizeItem.size_text ? (sizeItem.size_text.field_label[metadataLanguage] || sizeItem.size_text.field_label["en"]) : "";
                        return this.language_domain_textGenre_audioGenre_speechGenre_videoGenre_imageGenre(sizeItem, sizeItemIndex, metadataLanguage, amount, size_unit, size_text, size_text_label);
                    })) || [];
                    sizeArray = sizeArray.filter(item => item && item.props && item.props.hascontent==="true");//filter out empty div
                    let data_format_label = unspecifiedFeature.data_format ? (unspecifiedFeature.data_format.field_label[metadataLanguage] || unspecifiedFeature.data_format.field_label["en"]) : "";
                    let data_formatArray = (unspecifiedFeature.data_format && unspecifiedFeature.data_format.field_value.map(format => format.label ? (format.label[metadataLanguage] || format.label[Object.keys(format.label)[0]]) : format.value)) || [];

                    let distribution_text_feature_label = item.distribution_text_feature ? (item.distribution_text_feature.field_label[metadataLanguage] || item.distribution_text_feature.field_label["en"]) : "";
                    let distribution_text_featureArray = (item.distribution_text_feature && item.distribution_text_feature.field_value.map((textFeature, textFeatureIndex) => {
                        let sizeArray = (textFeature.size && textFeature.size.field_value.map((sizeItem, sizeItemIndex) => {
                            let amount = sizeItem.amount ? sizeItem.amount.field_value : "";
                            let size_unit = sizeItem.size_unit ? (sizeItem.size_unit.field_value.label ? (sizeItem.size_unit.field_value.label[metadataLanguage] || sizeItem.size_unit.field_value.label[Object.keys(sizeItem.size_unit.field_value.label)[0]]) : sizeItem.size_unit.field_value.value) : "";
                            let size_text = sizeItem.size_text ? sizeItem.size_text.field_value : "";
                            let size_text_label = sizeItem.size_text ? (sizeItem.size_text.field_label[metadataLanguage] || sizeItem.size_text.field_label["en"]) : "";
                            return this.language_domain_textGenre_audioGenre_speechGenre_videoGenre_imageGenre(sizeItem, sizeItemIndex, metadataLanguage, amount, size_unit, size_text, size_text_label);
                        })) || [];
                        sizeArray = sizeArray.filter(item => item && item.props && item.props.hascontent==="true");//filter out empty div 
                        let data_format_label = textFeature.data_format ? (textFeature.data_format.field_label[metadataLanguage] || textFeature.data_format.field_label["en"]) : "";
                        let data_formatArray = (textFeature.data_format && textFeature.data_format.field_value.map(format => format.label ? (format.label[metadataLanguage] || format.label[Object.keys(format.label)[0]]) : format.value)) || [];
                        let character_encoding = (textFeature.character_encoding && textFeature.character_encoding.field_value.map(encoding => encoding.label[metadataLanguage] || encoding.label[Object.keys(encoding.label)[0]])) || [];
                        let character_encoding_label = textFeature.character_encoding ? (textFeature.character_encoding.field_label[metadataLanguage] || textFeature.character_encoding.field_label["en"]) : "";
                        let mimetype_label = textFeature.mimetype ? (textFeature.mimetype.field_label[metadataLanguage] || textFeature.mimetype.field_label["en"]) : "";
                        let mimetypeArray = (textFeature.mimetype && textFeature.mimetype.field_value.map(mimetype => mimetype)) || [];
                        return <div key={textFeatureIndex}>
                            {sizeArray && sizeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">size</Typography>{sizeArray.map((size, sizeIndex) => <span className="info_value" key={sizeIndex}>{size}</span>)}</div>}
                            {data_formatArray && data_formatArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{data_format_label} </Typography>{data_formatArray.map((format, formatIndex) => <Chip key={formatIndex} size="small" label={format} className="ChipTagTeal" />)}</div>}
                            {character_encoding && character_encoding.length > 0 && <div className="padding5"><Typography className="bold-p--id">{character_encoding_label}</Typography>{character_encoding.map((encoding, encodingIndex) => <Chip key={encodingIndex} size="small" label={encoding} className="ChipTagTeal" />)}</div>}
                            {mimetypeArray && mimetypeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{mimetype_label} </Typography>{mimetypeArray.map((mimetype, mimetypeIndex) => <span key={mimetypeIndex} className="info-url" >{mimetype}</span>)}</div>}
                        </div>
                    })) || []

                    let distribution_text_numerical_feature_label = item.distribution_text_numerical_feature ? (item.distribution_text_numerical_feature.field_label[metadataLanguage] || item.distribution_text_numerical_feature.field_label["en"]) : "";
                    let distribution_text_numerical_featureArray = (item.distribution_text_numerical_feature && item.distribution_text_numerical_feature.field_value.map((textFeature, textFeatureIndex) => {
                        let sizeArray = (textFeature.size && textFeature.size.field_value.map((sizeItem, sizeItemIndex) => {
                            let amount = sizeItem.amount ? sizeItem.amount.field_value : "";
                            let size_unit = sizeItem.size_unit ? (sizeItem.size_unit.field_value.label ? (sizeItem.size_unit.field_value.label[metadataLanguage] || sizeItem.size_unit.field_value.label[Object.keys(sizeItem.size_unit.field_value.label)[0]]) : sizeItem.size_unit.field_value.value) : "";
                            let size_text = sizeItem.size_text ? sizeItem.size_text.field_value : "";
                            let size_text_label = sizeItem.size_text ? (sizeItem.size_text.field_label[metadataLanguage] || sizeItem.size_text.field_label["en"]) : "";
                            return this.language_domain_textGenre_audioGenre_speechGenre_videoGenre_imageGenre(sizeItem, sizeItemIndex, metadataLanguage, amount, size_unit, size_text, size_text_label);
                        })) || [];
                        sizeArray = sizeArray.filter(item => item && item.props && item.props.hascontent==="true");//filter out empty div 
                        let data_format_label = textFeature.data_format ? (textFeature.data_format.field_label[metadataLanguage] || textFeature.data_format.field_label["en"]) : "";
                        let data_formatArray = (textFeature.data_format && textFeature.data_format.field_value.map(format => format.label ? (format.label[metadataLanguage] || format.label[Object.keys(format.label)[0]]) : format.value)) || [];
                        let mimetype_label = textFeature.mimetype ? (textFeature.mimetype.field_label[metadataLanguage] || textFeature.mimetype.field_label["en"]) : "";
                        let mimetypeArray = (textFeature.mimetype && textFeature.mimetype.field_value.map(mimetype => mimetype)) || [];

                        return <div key={textFeatureIndex}>
                            {sizeArray && sizeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">size</Typography>{sizeArray.map((size, sizeIndex) => <span className="info_value" key={sizeIndex}>{size}</span>)}</div>}
                            {data_formatArray && data_formatArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{data_format_label} </Typography>{data_formatArray.map((format, formatIndex) => <Chip key={formatIndex} size="small" label={format} className="ChipTagTeal" />)}</div>}
                            {mimetypeArray && mimetypeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{mimetype_label} </Typography>{mimetypeArray.map((mimetype, mimetypeIndex) => <span key={mimetypeIndex} className="info-url" >{mimetype}</span>)}</div>}

                        </div>
                    })) || []


                    let distribution_audio_feature_label = item.distribution_audio_feature ? (item.distribution_audio_feature.field_label[metadataLanguage] || item.distribution_audio_feature.field_label["en"]) : "";
                    let distribution_audio_feature = (item.distribution_audio_feature && item.distribution_audio_feature.field_value.map((audioF, audioFIndex) => {
                        let size_label = audioF.size ? (audioF.size.field_label[metadataLanguage] || audioF.size.field_label["en"]) : "";
                        let sizeArray = (audioF.size && audioF.size.field_value.map((sizeItem, sizeIndex) => {
                            let amount = sizeItem.amount ? sizeItem.amount.field_value : "";
                            let size_unit = sizeItem.size_unit ? (sizeItem.size_unit.field_value.label ? (sizeItem.size_unit.field_value.label[metadataLanguage] || sizeItem.size_unit.field_value.label[Object.keys(sizeItem.size_unit.field_value.label)[0]]) : sizeItem.size_unit.field_value.value) : "";
                            let size_text = sizeItem.size_text ? sizeItem.size_text.field_value : "";
                            let size_text_label = sizeItem.size_text ? (sizeItem.size_text.field_label[metadataLanguage] || sizeItem.size_text.field_label["en"]) : "";
                            return this.language_domain_textGenre_audioGenre_speechGenre_videoGenre_imageGenre(sizeItem, sizeIndex, metadataLanguage, amount, size_unit, size_text, size_text_label);
                        })) || [];
                        sizeArray = sizeArray.filter(item => item && item.props && item.props.hascontent==="true");//filter out empty div 
                        let duration_of_audio_label = audioF.duration_of_audio ? (audioF.duration_of_audio.field_label[metadataLanguage] || audioF.duration_of_audio.field_label["en"]) : "";
                        let duration_of_audioArray = (audioF.duration_of_audio && audioF.duration_of_audio.field_value.map((duration, durationIndex) => {
                            let amount = duration.amount ? duration.amount.field_value : "";
                            let duration_unit = duration.duration_unit ? duration.duration_unit.label[metadataLanguage] || duration.duration_unit.label[Object.keys(duration.duration_unit.label)[0]] : "";
                            return <div key={durationIndex}>
                                {amount && <span className="info_value">{Intl.NumberFormat('en').format(amount)} {duration_unit}</span>}
                            </div>
                        })) || [];
                        let duration_of_effective_speech_label = audioF.duration_of_effective_speech ? (audioF.duration_of_effective_speech.field_label[metadataLanguage] || audioF.duration_of_effective_speech.field_label["en"]) : "";
                        let duration_of_effective_speech = (audioF.duration_of_effective_speech && audioF.duration_of_effective_speech.field_value.map((duration, durationIndex) => {
                            let amount = duration.amount ? duration.amount.field_value : "";
                            let duration_unit = duration.duration_unit ? duration.duration_unit.label[metadataLanguage] || duration.duration_unit.label[Object.keys(duration.duration_unit.label)[0]] : "";
                            return <div key={durationIndex}>
                                {amount && <span className="info_value">{Intl.NumberFormat('en').format(amount)} {duration_unit}</span>}
                            </div>
                        })) || [];

                        let data_format_label = audioF.data_format ? (audioF.data_format.field_label[metadataLanguage] || audioF.data_format.field_label["en"]) : "";
                        let data_formatArray = (audioF.data_format && audioF.data_format.field_value.map(format => format.label ? (format.label[metadataLanguage] || format.label[Object.keys(format.label)[0]]) : format.value)) || [];
                        let mimetype_label = audioF.mimetype ? (audioF.mimetype.field_label[metadataLanguage] || audioF.mimetype.field_label["en"]) : "";
                        let mimetypeArray = (audioF.mimetype && audioF.mimetype.field_value.map(mimetype => mimetype)) || [];

                        let audio_format_label = audioF.audio_format ? (audioF.audio_format.field_label[metadataLanguage] || audioF.audio_format.field_label["en"]) : "";
                        let audio_format = (audioF.audio_format && audioF.audio_format.field_value.map((audioFormatItem, audioFormatIndex) => {
                            let data_format = audioFormatItem.data_format ? (audioFormatItem.data_format.field_value.label ? (audioFormatItem.data_format.field_value.label[metadataLanguage] || audioFormatItem.data_format.field_value.label[Object.keys(audioFormatItem.data_format.field_value.label)[0]]) : audioFormatItem.data_format.field_value.value) : "";
                            let data_format_label = audioFormatItem.data_format ? (audioFormatItem.data_format.field_label[metadataLanguage] || audioFormatItem.data_format.field_label["en"]) : "";
                            let signal_encoding_label = audioFormatItem.signal_encoding ? (audioFormatItem.signal_encoding.field_label[metadataLanguage] || audioFormatItem.signal_encoding.field_label["en"]) : "";
                            let signal_encodingArray = audioFormatItem.signal_encoding && audioFormatItem.signal_encoding.field_value.map(item => item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]]);

                            let sampling_rate = audioFormatItem.sampling_rate ? audioFormatItem.sampling_rate.field_value : "";
                            let sampling_rate_label = audioFormatItem.sampling_rate ? (audioFormatItem.sampling_rate.field_label[metadataLanguage] || audioFormatItem.sampling_rate.field_label["en"]) : "";

                            let quantization = audioFormatItem.quantization ? audioFormatItem.quantization.field_value : "";
                            let quantization_label = audioFormatItem.quantization ? (audioFormatItem.quantization.field_label[metadataLanguage] || audioFormatItem.quantization.field_label["en"]) : "";

                            let byte_order = audioFormatItem.byte_order ? audioFormatItem.byte_order.label[metadataLanguage] || audioFormatItem.byte_order.label[Object.keys(audioFormatItem.byte_order.label)[0]] : "";
                            let byte_order_label = audioFormatItem.byte_order ? (audioFormatItem.byte_order.field_label[metadataLanguage] || audioFormatItem.byte_order.field_label["en"]) : "";

                            let sign_convention = audioFormatItem.sign_convention ? audioFormatItem.sign_convention.label[metadataLanguage] || audioFormatItem.sign_convention.label[Object.keys(audioFormatItem.sign_convention.label)[0]] : "";
                            let sign_convention_label = audioFormatItem.sign_convention ? (audioFormatItem.sign_convention.field_label[metadataLanguage] || audioFormatItem.sign_convention.field_label["en"]) : "";

                            let audio_quality_measure_included = audioFormatItem.audio_quality_measure_included ? audioFormatItem.audio_quality_measure_included.label[metadataLanguage] || audioFormatItem.audio_quality_measure_included.label[Object.keys(audioFormatItem.audio_quality_measure_included.label)[0]] : "";
                            let audio_quality_measure_included_label = audioFormatItem.audio_quality_measure_included ? (audioFormatItem.audio_quality_measure_included.field_label[metadataLanguage] || audioFormatItem.audio_quality_measure_included.field_label["en"]) : "";

                            let number_of_tracks = audioFormatItem.number_of_tracks ? audioFormatItem.number_of_tracks.field_value : "";
                            let number_of_tracks_label = audioFormatItem.number_of_tracks ? (audioFormatItem.number_of_tracks.field_label[metadataLanguage] || audioFormatItem.number_of_tracks.field_label["en"]) : "";

                            let recording_quality_label = audioFormatItem.recording_quality ? (audioFormatItem.recording_quality.field_label[metadataLanguage] || audioFormatItem.recording_quality.field_label["en"]) : "";
                            let recording_qualityArray = (audioFormatItem.recording_quality && audioFormatItem.recording_quality.field_value.map(item => (item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]]))) || [];

                            let compressed = audioFormatItem.compressed ? Boolean(audioFormatItem.compressed.field_value) : null;
                            let compressed_label = audioFormatItem.compressed ? (audioFormatItem.compressed.field_label[metadataLanguage] || audioFormatItem.compressed.field_label["en"]) : "";

                            let compression_name = audioFormatItem.compression_name ? audioFormatItem.compression_name.label[metadataLanguage] || audioFormatItem.compression_name.label[Object.keys(audioFormatItem.compression_name.label)[0]] : "";
                            let compression_name_label = audioFormatItem.compression_name ? (audioFormatItem.compression_name.field_label[metadataLanguage] || audioFormatItem.compression_name.field_label["en"]) : "";

                            let compression_loss = audioFormatItem.compression_loss ? (audioFormatItem.compression_loss ? Boolean(audioFormatItem.compression_loss.field_value) : "") : "";
                            let compression_loss_label = audioFormatItem.compression_loss ? (audioFormatItem.compression_loss.field_label[metadataLanguage] || audioFormatItem.compression_loss.field_label["en"]) : "";
                            //console.log(quantization)
                            return <div key={audioFormatIndex}>
                                {data_format && <div className="padding5"><Typography className="bold-p--id">{data_format_label}</Typography><span className="info_value">{data_format}</span></div>}
                                {signal_encodingArray && signal_encodingArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{signal_encoding_label}</Typography><span className="info_value">{signal_encodingArray.map((item, index) => <div key={index}>{item}</div>)}</span></div>}
                                {sampling_rate !== null && <div className="padding5"><Typography className="bold-p--id">{sampling_rate_label}</Typography><span className="info_value">{sampling_rate}</span></div>}
                                {quantization !== null && <div className="padding5"><Typography className="bold-p--id">{quantization_label}</Typography><span className="info_value">{quantization}</span></div>}
                                {byte_order && <div className="padding5"><Typography className="bold-p--id">{byte_order_label}</Typography><span className="info_value">{byte_order}</span></div>}
                                {sign_convention && <div className="padding5"><Typography className="bold-p--id">{sign_convention_label}</Typography><span className="info_value">{sign_convention}</span></div>}
                                {audio_quality_measure_included && <div className="padding5"><Typography className="bold-p--id">{audio_quality_measure_included_label}</Typography><span className="info_value">{audio_quality_measure_included}</span></div>}
                                {number_of_tracks !== null && <div className="padding5"><Typography className="bold-p--id">{number_of_tracks_label}</Typography><span className="info_value">{number_of_tracks}</span></div>}
                                {sign_convention && <div className="padding5"><Typography className="bold-p--id">{sign_convention_label}</Typography><span className="info_value">{sign_convention}</span></div>}
                                {recording_qualityArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{recording_quality_label}</Typography><span className="info_value">{recording_qualityArray.map((item, index) => <div key={index}>{item}</div>)}</span></div>}
                                {compressed !== null && <div className="padding5"><Typography className="bold-p--id">{compressed_label}</Typography><span className="info_value">{`${compressed}`}</span></div>}
                                {compression_name && <div className="padding5"><Typography className="bold-p--id">{compression_name_label}</Typography><span className="info_value">{compression_name}</span></div>}
                                {compression_loss && <div className="padding5"><Typography className="bold-p--id">{compression_loss_label}</Typography><span className="info_value">{`${compression_loss}`}</span></div>}
                                {mimetypeArray && mimetypeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{mimetype_label} </Typography>{mimetypeArray.map((mimetype, mimetypeIndex) => <span key={mimetypeIndex} className="info-url" >{mimetype}</span>)}</div>}

                            </div>
                        })) || []

                        return <div key={audioFIndex}>
                            {sizeArray && sizeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{size_label}</Typography>{sizeArray.map((size, sizeIndex) => <span key={sizeIndex}>{size}</span>)}</div>}
                            {duration_of_audioArray && duration_of_audioArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{duration_of_audio_label} </Typography>{duration_of_audioArray.map((duration, durationIndex) => <span key={durationIndex}>{duration}</span>)}</div>}
                            {duration_of_effective_speech && duration_of_effective_speech.length > 0 && <div className="padding5"><Typography className="bold-p--id">{duration_of_effective_speech_label} </Typography>{duration_of_effective_speech.map((durationOfEffectiveSpeech, durationOfEffectiveSpeechIndex) => <span key={durationOfEffectiveSpeechIndex}>{durationOfEffectiveSpeech}</span>)}</div>}
                            {data_formatArray && data_formatArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{data_format_label} </Typography>{data_formatArray.map((format, formatIndex) => <Chip key={formatIndex} size="small" label={format} className="ChipTagTeal" />)}</div>}
                            {audio_format && audio_format.length > 0 && <div className="padding5"><Typography className="section-headings">{audio_format_label} </Typography><span className="info_value">{audio_format.map((item, index) => <div key={index}>{item}</div>)}</span></div>}
                        </div>
                    })) || [];
                    //let distribution_image_feature = [];
                    let distribution_image_feature_label = item.distribution_image_feature ? (item.distribution_image_feature.field_label[metadataLanguage] || item.distribution_image_feature.field_label["en"]) : "";;

                    let distribution_image_feature = (item.distribution_image_feature && item.distribution_image_feature.field_value.map((imageF, imageFIndex) => {
                        let size_label = imageF.size ? (imageF.size.field_label[metadataLanguage] || imageF.size.field_label["en"]) : "";
                        let sizeArray = (imageF.size && imageF.size.field_value.map((sizeItem, sizeIndex) => {
                            let amount = sizeItem.amount ? sizeItem.amount.field_value : "";
                            let size_unit = sizeItem.size_unit ? (sizeItem.size_unit.field_value.label ? (sizeItem.size_unit.field_value.label[metadataLanguage] || sizeItem.size_unit.field_value.label[Object.keys(sizeItem.size_unit.field_value.label)[0]]) : sizeItem.size_unit.field_value.value) : "";
                            return this.language_domain_textGenre_audioGenre_speechGenre_videoGenre_imageGenre(sizeItem, sizeIndex, metadataLanguage, amount, size_unit);
                        })) || [];
                        sizeArray = sizeArray.filter(item => item && item.props && item.props.hascontent==="true");//filter out empty div 
                        let data_format_label = imageF.data_format ? (imageF.data_format.field_label[metadataLanguage] || imageF.data_format.field_label["en"]) : "";
                        let data_formatArray = (imageF.data_format && imageF.data_format.field_value.map(format => format.label ? (format.label[metadataLanguage] || format.label[Object.keys(format.label)[0]]) : format.value)) || [];
                        let mimetype_label = imageF.mimetype ? (imageF.mimetype.field_label[metadataLanguage] || imageF.mimetype.field_label["en"]) : "";
                        let mimetypeArray = (imageF.mimetype && imageF.mimetype.field_value.map(mimetype => mimetype)) || [];

                        let image_format_label = imageF.image_format ? (imageF.image_format.field_label[metadataLanguage] || imageF.image_format.field_label["en"]) : "";
                        let image_format = (imageF.image_format && imageF.image_format.field_value.map((imageFormatItem, imageFormatIndex) => {
                            const data_format = imageFormatItem.data_format ? (imageFormatItem.data_format.field_value.label ? (imageFormatItem.data_format.field_value.label[metadataLanguage] || imageFormatItem.data_format.field_value.label[Object.keys(imageFormatItem.data_format.field_value.label)[0]]) : imageFormatItem.data_format.field_value.value) : "";
                            const data_format_label = imageFormatItem.data_format ? (imageFormatItem.data_format.field_label[metadataLanguage] || imageFormatItem.data_format.field_label["en"]) : "";

                            const quality = imageFormatItem.quality ? imageFormatItem.quality.label[metadataLanguage] || imageFormatItem.quality.label[Object.keys(imageFormatItem.quality.label)[0]] : "";
                            const quality_label = imageFormatItem.quality ? (imageFormatItem.quality.field_label[metadataLanguage] || imageFormatItem.quality.field_label["en"]) : "";

                            const raster_or_vector_graphics = imageFormatItem.raster_or_vector_graphics ? imageFormatItem.raster_or_vector_graphics.label[metadataLanguage] || imageFormatItem.raster_or_vector_graphics.label[Object.keys(imageFormatItem.raster_or_vector_graphics.label)[0]] : "";
                            const raster_or_vector_graphics_label = imageFormatItem.raster_or_vector_graphics ? (imageFormatItem.raster_or_vector_graphics.field_label[metadataLanguage] || imageFormatItem.raster_or_vector_graphics.field_label["en"]) : "";

                            const visual_modelling = imageFormatItem.visual_modelling ? imageFormatItem.visual_modelling.label[metadataLanguage] || imageFormatItem.visual_modelling.label[Object.keys(imageFormatItem.visual_modelling.label)[0]] : "";
                            const visual_modelling_label = imageFormatItem.visual_modelling ? (imageFormatItem.visual_modelling.field_label[metadataLanguage] || imageFormatItem.visual_modelling.field_label["en"]) : "";

                            const compressed = imageFormatItem.compressed ? Boolean(imageFormatItem.compressed.field_value) : null;
                            const compressed_label = imageFormatItem.compressed ? (imageFormatItem.compressed.field_label[metadataLanguage] || imageFormatItem.compressed.field_label["en"]) : "";

                            const compression_name = imageFormatItem.compression_name ? imageFormatItem.compression_name.label[metadataLanguage] || imageFormatItem.compression_name.label[Object.keys(imageFormatItem.compression_name.label)[0]] : "";
                            const compression_name_label = imageFormatItem.compression_name ? (imageFormatItem.compression_name.field_label[metadataLanguage] || imageFormatItem.compression_name.field_label["en"]) : "";

                            const compression_loss = imageFormatItem.compression_loss ? (imageFormatItem.compression_loss ? Boolean(imageFormatItem.compression_loss.field_value) : "") : "";
                            const compression_loss_label = imageFormatItem.compression_loss ? (imageFormatItem.compression_loss.field_label[metadataLanguage] || imageFormatItem.compression_loss.field_label["en"]) : "";

                            const colour_space = imageFormatItem.colour_space ? imageFormatItem.colour_space.field_value.map((color_space, color_space_index) => color_space.label[metadataLanguage] || color_space.label[Object.keys(color_space.label)[0]]) : [];
                            const colour_space_label = imageFormatItem.colour_space ? (imageFormatItem.colour_space.field_label[metadataLanguage] || imageFormatItem.colour_space.field_label["en"]) : "";

                            const colour_depth = imageFormatItem.colour_depth ? imageFormatItem.colour_depth.field_value.map(colour_depth => colour_depth) : [];
                            const colour_depth_label = imageFormatItem.colour_depth ? (imageFormatItem.colour_depth.field_label[metadataLanguage] || imageFormatItem.colour_depth.field_label["en"]) : "";

                            const resolution = imageFormatItem.resolution ? imageFormatItem.resolution.field_value.map((resolutionItem, resolutionIndex) => {
                                let size_width = resolutionItem.size_width ? resolutionItem.size_width.field_value : null;
                                let size_width_label = resolutionItem.size_width ? resolutionItem.size_width.field_label[metadataLanguage] || resolutionItem.size_width.field_label["en"] : "";

                                let size_height = resolutionItem.size_height ? resolutionItem.size_height.field_value : null;
                                let size_height_label = resolutionItem.size_height ? resolutionItem.size_height.field_label[metadataLanguage] || resolutionItem.size_height.field_label["en"] : "";

                                let resolution_standard = resolutionItem.resolution_standard ? resolutionItem.resolution_standard.label[metadataLanguage] || resolutionItem.resolution_standard.label[Object.keys(resolutionItem.resolution_standard.label)[0]] : "";
                                let resolution_standard_label = resolutionItem.resolution_standard ? resolutionItem.resolution_standard.field_label[metadataLanguage] || resolutionItem.resolution_standard.field_label["en"] : "";

                                return <span className="info_value" index={resolutionIndex}>
                                    {size_width !== null && <div className="padding5"><Typography className="bold-p--id">{size_width_label}</Typography><span className="info_value">{size_width}</span></div>}
                                    {size_height !== null && <div className="padding5"><Typography className="bold-p--id">{size_height_label}</Typography><span className="info_value">{size_height}</span></div>}
                                    {resolution_standard && <div className="padding5"><Typography className="bold-p--id">{resolution_standard_label}</Typography><span className="info_value">{resolution_standard}</span></div>}
                                </span>
                            }) : []

                            const resolution_label = imageFormatItem.resolution ? (imageFormatItem.resolution.field_label[metadataLanguage] || imageFormatItem.resolution.field_label["en"]) : "";

                            return <div key={imageFormatIndex}>
                                {data_format && <div className="padding5"><Typography className="bold-p--id">{data_format_label}</Typography><span className="info_value">{data_format}</span></div>}
                                {quality && <div className="padding5"><Typography className="bold-p--id">{quality_label}</Typography><span className="info_value">{quality}</span></div>}
                                {raster_or_vector_graphics && <div className="padding5"><Typography className="bold-p--id">{raster_or_vector_graphics_label}</Typography><span className="info_value">{raster_or_vector_graphics}</span></div>}
                                {visual_modelling && <div className="padding5"><Typography className="bold-p--id">{visual_modelling_label}</Typography><span className="info_value">{visual_modelling}</span></div>}
                                {colour_space && colour_space.length > 0 && <div className="padding5"><Typography className="bold-p--id">{colour_space_label}</Typography>{colour_space.map((color_space, color_space_index) => <span key={color_space_index}>{color_space}&nbsp;</span>)}</div>}
                                {colour_depth && colour_depth.length > 0 && <div className="padding5"><Typography className="bold-p--id">{colour_depth_label}</Typography>{colour_depth.map((colour_depth, colour_depth_index) => <span key={colour_depth_index}>{colour_depth},&nbsp;</span>)}</div>}
                                {resolution && resolution.length > 0 && <div className="padding5"><Typography className="bold-p--id">{resolution_label}</Typography><span className="info_value">{resolution.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                                {compressed !== null && <div className="padding5"><Typography className="bold-p--id">{compressed_label}</Typography><span className="info_value">{`${compressed}`}</span></div>}
                                {compression_name && <div className="padding5"><Typography className="bold-p--id">{compression_name_label}</Typography><span className="info_value">{compression_name}</span></div>}
                                {compression_loss && <div className="padding5"><Typography className="bold-p--id">{compression_loss_label}</Typography><span className="info_value">{`${compression_loss}`}</span></div>}
                            </div>
                        })) || []

                        return <div key={imageFIndex}>
                            {sizeArray && sizeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{size_label}</Typography>{sizeArray.map((size, sizeIndex) => <span key={sizeIndex}>{size}</span>)}</div>}
                            {data_formatArray && data_formatArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{data_format_label} </Typography>{data_formatArray.map((format, formatIndex) => <Chip key={formatIndex} size="small" label={format} className="ChipTagTeal" />)}</div>}
                            {image_format && image_format.length > 0 && <div className="padding5"><Typography className="section-headings">{image_format_label} </Typography><span className="info_value">{image_format.map((item, index) => <div key={index}>{item}</div>)}</span></div>}
                            {mimetypeArray && mimetypeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{mimetype_label} </Typography>{mimetypeArray.map((mimetype, mimetypeIndex) => <span key={mimetypeIndex} className="info-url" >{mimetype}</span>)}</div>}
                        </div>
                    })) || [];


                    let licence_terms_label = item.licence_terms ? (item.licence_terms.field_label[metadataLanguage] || item.licence_terms.field_label["en"]) : "";
                    let licence_terms = (item.licence_terms && item.licence_terms.field_value.map((licence, licenceIndex) => {
                        let licence_terms_name = licence.licence_terms_name ? licence.licence_terms_name.field_value[metadataLanguage] || licence.licence_terms_name.field_value[Object.keys(licence.licence_terms_name.field_value)[0]] : "";
                        let licence_terms_url = (licence.licence_terms_url && licence.licence_terms_url.field_value.map((url, urlIndex) => <a key={urlIndex} href={url} target="_blank" rel="noreferrer noopener">{`${url} `}</a>)) || [];
                        return <div key={licenceIndex}>
                            {licence_terms_name && <div> <span className="info_value">{licence_terms_name}</span></div>}
                            {licence_terms_url && licence_terms_url.length && <div> <span className="info_url">{licence_terms_url.map(item => item)}</span></div>}
                        </div>
                    })) || [];
                    let attribution_text_label = item.attribution_text ? (item.attribution_text.field_label[metadataLanguage] || item.attribution_text.field_label["en"]) : "";
                    let attribution_text = item.attribution_text ? (item.attribution_text.field_value[metadataLanguage] || item.attribution_text.field_value[Object.keys(item.attribution_text.field_value)[0]]) : "";
                    let cost_label = item.cost ? (item.cost.field_label[metadataLanguage] || item.cost.field_label["en"]) : "";
                    let cost_amount = (item.cost && item.cost.field_value.amount) ? item.cost.field_value.amount.field_value : null;// parseFloat(item.cost.amount) || "";
                    let cost_currency = item.cost ? (item.cost.field_value.currency && (item.cost.field_value.currency.label[metadataLanguage] || item.cost.field_value.currency.label[Object.keys(item.cost.field_value.currency.label)[0]])) : null;//item.cost.currency.substring(item.cost.currency.lastIndexOf("/") + 1) || null;
                    let membership_institution_label = item.membership_institution ? (item.membership_institution.field_label[metadataLanguage] || item.membership_institution.field_label["en"]) : "";
                    let membership_institution = (item.membership_institution && item.membership_institution.field_value.map(item => item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]])) || [];
                    let copyright_statement_label = item.copyright_statement ? (item.copyright_statement.field_label[metadataLanguage] || item.copyright_statement.field_label["en"]) : "";
                    let copyright_statement = item.copyright_statement ? (item.copyright_statement.field_value[metadataLanguage] || item.copyright_statement.field_value[Object.keys(item.copyright_statement.field_value)[0]]) : "";
                    let availability_start_date = item.availability_start_date ? item.availability_start_date.field_value : "";
                    let availability_end_date = item.availability_end_date ? item.availability_end_date.field_value : "";
                    let distribution_rights_holder = item.distribution_rights_holder ? item.distribution_rights_holder : [];

                    let access_rights_label = item.access_rights ? (item.access_rights.field_label[metadataLanguage] || item.access_rights.field_label["en"]) : "";
                    let access_rightsArray = (item.access_rights && item.access_rights.field_value.map((accessRightsItem, access_rightsIndex) => {
                        let category_label = accessRightsItem.category_label ? accessRightsItem.category_label.field_value[metadataLanguage] || accessRightsItem.category_label.field_value[Object.keys(accessRightsItem.category_label.field_value)[0]] : "";
                        //let access_rights_statement_identifier_label = accessRightsItem.access_rights_statement_identifier ? ( accessRightsItem.access_rights_statement_identifier.field_label[metadataLanguage] || accessRightsItem.access_rights_statement_identifier.field_label["en"]) : ""; 
                        let identifier = accessRightsItem.access_rights_statement_identifier && accessRightsItem.access_rights_statement_identifier.field_value.access_rights_statement_scheme.label["en"] !== "ELG" ? accessRightsItem.access_rights_statement_identifier.field_value.access_rights_statement_scheme.label["en"] : "";//filter out elg identifiers 
                        let value = accessRightsItem.access_rights_statement_identifier ? accessRightsItem.access_rights_statement_identifier.field_value.value.field_value : "";
                        return <div key={access_rightsIndex}>
                            {category_label && <div className="padding5"><span className="info_value">{category_label}</span></div>}
                            {false && identifier && value.includes('http') && <div>
                                <a href={value} target="_blank" rel="noopener noreferrer" className="info_url"> {value} </a> ({identifier})</div>}
                            {false && identifier && !value.includes('http') && <div className="info_value">{value} ({identifier})  </div>}
                        </div>
                    })) || [];


                    return <Grid item style={{ display: 'flex' }} xs={6} sm={6} md={6} key={index}>
                        <Card style={{ display: 'flex', justifyContent: 'flex-start', flexDirection: 'column' }} className="distribution-cards wd-100">
                            <CardHeader avatar={<Avatar aria-label="distribution" className="distribution-avatar"> <DistributionsIcon className="xsmall-icon" /> </Avatar>}
                                title={<Typography variant="h3" className="title-links">Distribution</Typography>}
                                action={
                                    <div style={{ display: "flex" }}>
                                        {(this.props.isOwner || IS_ADMIN(keycloak) || IS_CONTENT_MANAGER(keycloak) || (this.props.isLegalValidator && data.management_object.status === "g") || (this.props.isMetadataValidator && data.management_object.status === "g") || (this.props.isTechnicalValidator && data.management_object.status === "g")) && <DownloadDatasetCurator data={data} distribution={item} />}
                                        {!(this.props.isOwner || IS_ADMIN(keycloak) || IS_CONTENT_MANAGER(keycloak) || (this.props.isLegalValidator && data.management_object.status === "g") || (this.props.isMetadataValidator && data.management_object.status === "g") || (this.props.isTechnicalValidator && data.management_object.status === "g")) && <AcceptLicence data={data} distribution={item} {...this.props} keycloak={keycloak} />}
                                        {/* eslint-disable-next-line*/}
                                        {show_billing && <a href="javascript:void(0)" data-cb-type="checkout" data-cb-item-0="ACL-RD-TEC-EUR-Monthly">Purchase</a>}
                                    </div>
                                }
                            />
                            <CardContent>
                                {dataset_distribution_form && <div className="padding5"><Typography className="bold-p--id">{dataset_distribution_form_label}</Typography><span className="info_value">{dataset_distribution_form}</span></div>}
                                {distribution_location && !["http://www.hiddenLocation.org", "Hidden value", "http://fixme.com"].includes(distribution_location) && <div className="padding5"><a className="info_url" href={distribution_location} target="_blank" rel="noopener noreferrer">{<div ><span><LaunchIcon className="xsmall-icon" /> </span> <span>{distribution_location_label}</span></div>} </a></div>}
                                {download_location && !["http://www.hiddenLocation.org", "Hidden value"].includes(download_location) && <div className="padding5"><a className="info_url" href={download_location} target="_blank" rel="noopener noreferrer">{<div ><span><LaunchIcon className="xsmall-icon" /> </span> <span>{download_location_label}</span></div>} </a></div>}
                                {this.showAccessLocation(data, item) && access_location && !["http://www.hiddenLocation.org", "Hidden value"].includes(access_location) && <div className="padding5"><span className="info_url"><a href={access_location} target="blank">{<div ><span><LaunchIcon className="xsmall-icon" /> </span> <span>{access_location_label}</span></div>}</a></span></div>}
                                {is_accessed_byArray && is_accessed_byArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_accessed_by_label}</Typography><span className="info_value">{is_accessed_byArray.map((item, index) => <div key={index}>{item}</div>)}</span></div>}
                                {is_displayed_by && is_displayed_by.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_displayed_by_label}</Typography><span className="info_value">{is_displayed_by.map((item, index) => <div key={index}>{item}</div>)}</span></div>}
                                {is_queried_by && is_queried_by.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_queried_by_label}</Typography><span className="info_value">{is_queried_by.map((item, index) => <div key={index}>{item}</div>)}</span></div>}
                                {samples_location && samples_location.length > 0 && <div className="padding5"><span className="info_url">{samples_location.map((item, index) => <div key={index}><span><LaunchIcon className="xsmall-icon" /> </span> <span><a href={item} target="blank">{samples_location_label}</a></span></div>)}</span></div>}
                                {package_format && <div className="padding5"><Typography className="bold-p--id">{package_format_label}</Typography><span className="info_value">{package_format}</span></div>}
                                {distribution_text_featureArray && distribution_text_featureArray.length > 0 && <div className="padding15"><Typography className="section-headings">{distribution_text_feature_label} </Typography><span className="info_value">{distribution_text_featureArray.map((item, index) => <div key={index}>{item}</div>)}</span></div>}
                                {distribution_text_numerical_featureArray && distribution_text_numerical_featureArray.length > 0 && <div className="padding5"><Typography className="section-headings">{distribution_text_numerical_feature_label} </Typography><span className="info_value">{distribution_text_numerical_featureArray.map((item, index) => <div className="bottom-border" key={index}>{item}</div>)}</span></div>}
                                {distribution_audio_feature && distribution_audio_feature.length > 0 && <div className="padding5"><Typography className="section-headings">{distribution_audio_feature_label} </Typography> {distribution_audio_feature.map((item, index) => <div className="bottom-border" key={index}>{item}</div>)} </div>}
                                {distribution_image_feature && distribution_image_feature.length > 0 && <div className="padding5"><Typography className="section-headings">{distribution_image_feature_label} </Typography> {distribution_image_feature.map((item, index) => <div className="bottom-border" key={index}>{item}</div>)} </div>}

                                {/*distribution_video_feature && distribution_video_feature.length > 0 && <div className="padding5"><Typography className="section-headings">{distribution_video_feature_label}</Typography><span className="info_value">{distribution_video_feature.map((item, index) => <div key={index}>{item}</div>)}</span></div>*/}
                                <DistributionVideoFeature distribution={item} metadataLanguage={metadataLanguage} language_domain_textGenre_audioGenre_speechGenre_videoGenre_imageGenre={this.language_domain_textGenre_audioGenre_speechGenre_videoGenre_imageGenre} />
                                {unspecifiedFeature && <>
                                    <div className="padding15"><Typography className="section-headings">{distribution_unspecified_feature_label} </Typography> </div>

                                    {sizeArray && sizeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">size</Typography>{sizeArray.map((size, sizeIndex) => <span className="info_value" key={sizeIndex}>{size}</span>)}</div>}
                                    {data_formatArray && data_formatArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{data_format_label} </Typography>{data_formatArray.map((format, formatIndex) => <Chip key={formatIndex} size="small" label={format} className="ChipTagTeal" />)}</div>}


                                </>}

                                {licence_terms && licence_terms.length > 0 && <div className="padding15"><Typography className="bold-p--id">{licence_terms_label}</Typography><span className="info_value">{licence_terms.map((item, index) => <div key={index}>{item}</div>)}</span></div>}
                                {attribution_text && <div className="padding5"><Typography className="bold-p--id">{attribution_text_label}</Typography><span className="info_value">{attribution_text}</span></div>}
                                {cost_amount !== null && <div className="padding5"><Typography className="bold-p--id">{cost_label}</Typography><span className="info_value">{Intl.NumberFormat('en').format(cost_amount)} {cost_currency}</span></div>}
                                {membership_institution && membership_institution.length > 0 && <div className="padding5"><Typography className="bold-p--id">{membership_institution_label}</Typography><span className="info_value">{membership_institution.map((item, index) => <div key={index}>{item}</div>)}</span></div>}
                                {copyright_statement && <div className="padding5"><Typography className="bold-p--id">{copyright_statement_label}</Typography><span className="info_value">{copyright_statement}</span></div>}
                                {(availability_start_date || availability_end_date) && <div className="padding5"><Typography className="bold-p--id">Availability</Typography><span className="info_value">{availability_start_date && Intl.DateTimeFormat("en-GB", {
                                    year: "numeric",
                                    month: "long",
                                    day: "2-digit"
                                }).format(new Date(availability_start_date))} - {availability_end_date && Intl.DateTimeFormat("en-GB", {
                                    year: "numeric",
                                    month: "long",
                                    day: "2-digit"
                                }).format(new Date(availability_end_date))}</span></div>}
                                {distribution_rights_holder && <div className="width60"><ResourceActor data={distribution_rights_holder} metadataLanguage={metadataLanguage} /></div>}
                                {/*(this.state.isCurator || IS_ADMIN(keycloak) || IS_CONTENT_MANAGER(keycloak)) && <DownloadDatasetCurator data={data} />*/}
                                {/*!(this.state.isCurator || IS_ADMIN(keycloak) || IS_CONTENT_MANAGER(keycloak)) && <AcceptLicence data={data} distribution={item} {...this.props} keycloak={keycloak} />*/}
                                {access_rightsArray && access_rightsArray.length > 0 && <div className="padding15"><Typography variant="h3" className="title-links">{access_rights_label}</Typography><span className="info_value">{access_rightsArray.map((item, index) => <div key={index}>{item}</div>)}</span></div>}

                            </CardContent>
                        </Card>
                    </Grid>
                })}
            </Grid>
        </>
    }
}