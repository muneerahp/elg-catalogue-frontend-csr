import React from "react";
import Typography from '@material-ui/core/Typography';

export default class Revision extends React.Component {
    render() {
        const { revision , revision_label} = this.props;
        return < div className="padding5" >
            {revision &&
                <div className="padding5">
                     <Typography className="bold-p--id"> {revision_label}  </Typography>
                    <span className="info_value">{revision}</span>
                </div>
            }
        </div>
    }
}