import React from "react";
import Typography from '@material-ui/core/Typography';
import ResourceActor from "../CommonComponents/ResourceActor";
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";

export default class Annotation extends React.Component {
    componentDidMount() {
        const { data } = this.props;
        if (!data) {
            return false;
        }
        const { annotation } = data.described_entity.field_value.lr_subclass.field_value || [];
        //console.log(annotation)
        if (!annotation) {
            return false;
        }
        const { displayTabGeneral } = this.props;
        if (annotation && annotation.field_value.length > 0) {
            displayTabGeneral ? void 0 : this.props.displayTabGeneralFunction(true);
        }
    }
    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }
        const { annotation } = data.described_entity.field_value.lr_subclass.field_value || [];
        if (!annotation) {
            return <div></div>
        }
        return <>
            {annotation && annotation.field_value.length > 0 && <h3>Annotation</h3>}
            {annotation && annotation.field_value.map((annotationItem, annotationIndex) => {
                let annotation_type = annotationItem.annotation_type? annotationItem.annotation_type.label[metadataLanguage] || annotationItem.annotation_type.label[Object.keys(annotationItem.annotation_type.label)[0]]: "";
                let annotation_type_label = annotationItem.annotation_type? annotationItem.annotation_type.field_label[metadataLanguage] || annotationItem.annotation_type.field_label["en"]:"";
                let annotated_elementArray = annotationItem.annotated_element ? annotationItem.annotated_element.field_value.map(item => (item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]])) : [];
                let annotated_element_label = annotationItem.annotated_element? annotationItem.annotated_element.field_label[metadataLanguage] || annotationItem.annotated_element.field_label["en"]: "";
                let segmentation_levelArray = annotationItem.segmentation_level ? annotationItem.segmentation_level.field_value.map(item => (item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]])) : [];
                let segmentation_level_label = annotationItem.segmentation_level? annotationItem.segmentation_level.field_label[metadataLanguage] || annotationItem.segmentation_level.field_label["en"]:"";
                let annotation_standoff = annotationItem.annotation_standoff ? Boolean(annotationItem.annotation_standoff.field_value) : undefined;
                let annotation_standoff_label = annotationItem.annotation_standoff? annotationItem.annotation_standoff.field_label[metadataLanguage] || annotationItem.annotation_standoff.field_label["en"]:"";
                let guidelines_label = annotationItem.guidelines ? annotationItem.guidelines.field_label[metadataLanguage] || annotationItem.guidelines.field_label["en"]:"";

                let guidelinesArray = annotationItem.guidelines ? annotationItem.guidelines.field_value.map((item, index) => {
                    let title = item.title.field_value[metadataLanguage] || item.title.field_value[Object.keys(item.title.field_value)[0]]; //corpusParser.getLanguageDependentValue(item.title, metadataLanguage);
                    return <div key={index}>
                        {title && <div className="padding5"><span className="info_value">{title}</span></div>}
                    </div>
                }) : [];
                let tagset_label = annotationItem.tagset? annotationItem.tagset.field_label[metadataLanguage] || annotationItem.tagset.field_label["en"]:"";
                let tagsetArray = annotationItem.tagset ? annotationItem.tagset.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]:"";//corpusParser.getLanguageDependentValue(item.resource_name, metadataLanguage);
                    let version = item.version ? item.version.field_value : "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                       {full_metadata_record ?
                                <div className="padding5 internal_url">
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        <span className="info_value">{resource_name}, {version}</span>
                                    </Link>
                                </div> :
                                <span className="info_value">{resource_name}, {version}</span>
                            }    
                    </div>
                }) : [];
                let theoretic_model = annotationItem.theoretic_model?annotationItem.theoretic_model.field_value[metadataLanguage] || annotationItem.theoretic_model.field_value[Object.keys(annotationItem.theoretic_model.field_value)[0]]:"";
                let theoretic_model_label = annotationItem.theoretic_model? annotationItem.theoretic_model.field_label[metadataLanguage] || annotationItem.theoretic_model.field_label["en"]:"";
                let annotation_mode = annotationItem.annotation_mode? annotationItem.annotation_mode.label[metadataLanguage] || annotationItem.annotation_mode.label[Object.keys(annotationItem.annotation_mode.label)[0]]:"";
                let annotation_mode_label = annotationItem.annotation_mode? annotationItem.annotation_mode.field_label[metadataLanguage] || annotationItem.annotation_mode.field_label["en"]:"";
                let annotation_mode_details = annotationItem.annotation_mode_details ? annotationItem.annotation_mode_details.field_value[metadataLanguage] || annotationItem.annotation_mode_details.field_value[Object.keys(annotationItem.annotation_mode_details.field_value)[0]]:"";
                let annotation_mode_details_label = annotationItem.annotation_mode_details? annotationItem.annotation_mode_details.field_label[metadataLanguage] || annotationItem.annotation_mode_details.field_label["en"]:"";
                let is_annotated_by_label = annotationItem.is_annotated_by? annotationItem.is_annotated_by.field_label[metadataLanguage] || annotationItem.is_annotated_by.field_label["en"]:"";
                let is_annotated_byArray = annotationItem.is_annotated_by ? annotationItem.is_annotated_by.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]:"";
                    let version = item.version ? item.version.field_value : "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                       {full_metadata_record ?
                                <div className="padding5 internal_url">
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        <span className="info_value">{resource_name}, {version}</span>
                                    </Link>
                                </div> :
                                <span className="info_value">{resource_name}, {version}</span>
                            }    
                    </div>
                }) : [];
                let annotator_label =  annotationItem.annotator? annotationItem.annotator.field_label[metadataLanguage] || annotationItem.annotator.field_label["en"]:"";
                let annotatorArray = annotationItem.annotator? annotationItem.annotator :[];

                let annotation_start_date = annotationItem.annotation_start_date ? annotationItem.annotation_start_date.field_value : "";
                //let annotation_start_date_label=annotationItem.annotation_start_date.field_label[metadataLanguage]||annotationItem.annotation_start_date.field_label["en"];
                let annotation_end_date = annotationItem.annotation_end_date ? annotationItem.annotation_end_date.field_value : "";
                //let annotation_end_date_label=annotationItem.annotation_end_date.field_label[metadataLanguage]||annotationItem.annotation_end_date.field_label["en"];

                let interannotator_agreement = annotationItem.interannotator_agreement ? annotationItem.interannotator_agreement.field_value[metadataLanguage] || annotationItem.interannotator_agreement.field_value[Object.keys(annotationItem.interannotator_agreement.field_label)[0]]:"";
                let interannotator_agreement_label = annotationItem.interannotator_agreement ? annotationItem.interannotator_agreement.field_label[metadataLanguage] || annotationItem.interannotator_agreement.field_label["en"]:"";
                let intraannotator_agreement = annotationItem.intraannotator_agreement? annotationItem.intraannotator_agreement.field_value[metadataLanguage] || annotationItem.intraannotator_agreement.field_value[Object.keys(annotationItem.interannotator_agreement.field_label)[0]]:"";
                let intraannotator_agreement_label = annotationItem.intraannotator_agreement? annotationItem.intraannotator_agreement.field_label[metadataLanguage] || annotationItem.intraannotator_agreement.field_label["en"]:"";
                let annotation_report_label = annotationItem.annotation_report? annotationItem.annotation_report.field_label[metadataLanguage] || annotationItem.annotation_report.field_label["en"]:"";


                let annotation_reportArray = annotationItem.annotation_report ? annotationItem.annotation_report.field_value.map((item, index) => {
                    let title = item.title ? item.title.field_value[metadataLanguage] || item.title.field_value[Object.keys(item.title.field_value)[0]]:"";
                    return <div key={index}>
                        {title && <div><span className="info_value">{title}</span></div>}
                    </div>
                }) : [];
                return <div key={annotationIndex} className="bottom-border">
                    {annotation_type && <div className="padding5"><Typography className="bold-p--id">{annotation_type_label}</Typography><span className="info_value">{annotation_type}</span></div>}
                    {annotated_elementArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{annotated_element_label}</Typography><span className="info_value">{annotated_elementArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {segmentation_levelArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{segmentation_level_label}</Typography><span className="info_value">{segmentation_levelArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {annotation_standoff !== undefined && <div className="padding5"><Typography className="bold-p--id">{annotation_standoff_label}</Typography><span className="info_value">{`${annotation_standoff}`}</span></div>}
                    {guidelinesArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{guidelines_label}</Typography><span className="info_value">{guidelinesArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {tagsetArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{tagset_label}</Typography><span className="info_value">{tagsetArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {theoretic_model && <div className="padding5"><Typography className="bold-p--id">{theoretic_model_label}</Typography><span className="info_value">{theoretic_model}</span></div>}
                    {annotation_mode && <div className="padding5"><Typography className="bold-p--id">{annotation_mode_label}</Typography><span className="info_value">{annotation_mode}</span></div>}
                    {annotation_mode_details && <div className="padding5"><Typography className="bold-p--id">{annotation_mode_details_label}</Typography><span className="info_value">{annotation_mode_details}</span></div>}
                    {is_annotated_byArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_annotated_by_label}</Typography><span className="info_value">{is_annotated_byArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {annotatorArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{annotator_label}</Typography><ResourceActor data={annotatorArray} metadataLanguage={metadataLanguage} /></div>}
                    {(annotation_start_date || annotation_end_date) && <div className="padding5"><Typography className="bold-p--id">{annotation_start_date} - {annotation_end_date}</Typography><span className="info_value">{annotation_start_date} - {annotation_end_date}</span></div>}
                    {interannotator_agreement && <div className="padding5"><Typography className="bold-p--id">{interannotator_agreement_label}</Typography><span className="info_value">{interannotator_agreement}</span></div>}
                    {intraannotator_agreement && <div className="padding5"><Typography className="bold-p--id">{intraannotator_agreement_label}</Typography><span className="info_value">{intraannotator_agreement}</span></div>}
                    {annotation_reportArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{annotation_report_label}</Typography><span className="info_value">{annotation_reportArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    
                </div>
            })}
        </>
    }
}