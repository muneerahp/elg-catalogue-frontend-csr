import React from "react";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import Eu from "../../assets/images/eu.svg";
import { TILDE_FOOTER_MENU_API, TILDE_FEEDBACK_MENU_API, FOOTER_BASE_URL } from "../../config/constants";

export default class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.state = { footerData: '', feedbackData: '', error: "" };
        this.isUnmount = false;
        this.getInitialData = this.getInitialData.bind(this);
        this.getFeedBackData = this.getFeedBackData.bind(this);
        //this.NON_CLICKABLE_MENUS = ['Technologies', 'Resources', 'Community'];
        //this.isMenuClickable = this.isMenuClickable.bind(this);
        this.fixMenuPathPrefix = this.fixMenuPathPrefix.bind(this);
        this.errorFooter = this.errorFooter.bind(this);
    }

    componentDidMount() {
        this.getInitialData();
        this.getFeedBackData();
    }

    getInitialData = () => {
        this.isUnmount = true;
        axios.get(TILDE_FOOTER_MENU_API).then(res => {
            if (this.isUnmount) {
                this.setState({ footerData: res.data, error: "" });
            }
        }).catch(res => {
            if (this.isUnmount) {
                this.setState({ error: res })
            }
        })
    }

    getFeedBackData = () => {
        this.isUnmount = true;
        axios.get(TILDE_FEEDBACK_MENU_API).then(res => {
            if (this.isUnmount) {
                this.setState({ feedbackData: res.data });
            }
        }).catch(res => {
            if (this.isUnmount) {
                this.setState({ error: res })
            }
        })
    }

    /*isMenuClickable(menuTitle) {
        return !this.NON_CLICKABLE_MENUS.includes(menuTitle);
    }*/

    fixMenuPathPrefix(field_elg_menu_link_type, relative) {
        switch (field_elg_menu_link_type) {
            case "CMS":
                //relative = `page/${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                relative = `${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
            case "React":
                relative = `catalogue/${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
            case "External":
                break;
            case "Angular":
                relative = `${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
            default:
                relative = `${relative.substring(relative.lastIndexOf("/cms/") + 5)}`;
                break;
        }
        return relative;
    }

    errorFooter() {
        return <footer >
            <Grid container direction="row" justifyContent="center" alignItems="center" className="footer-container">
                <Grid item xs={11} sm={11} container direction="row" justifyContent="flex-start" alignItems="center" >
                    <Grid item container direction="row" xs={12} className="pb-3 footer-menu__wrapper no-gutters">
                        <Grid item className="footer-menu__main" xs={9} sm={9}></Grid>
                        <Grid item xs={3} sm={3} className="text-right footer-menu__feedback"></Grid>
                    </Grid>
                    <Grid item container direction="row" xs={12} className="py-2 no-gutters">
                        <Grid item container direction="row" xs={12}>
                            <Grid item sm={6} xs={12}>
                                <small><img className="img--inline footerImg" src={Eu} alt="european" />
                                    <span _ngcontent-c2="">
                                        The European Language Grid has received funding from the European
                                        Union’s Horizon 2020 research and innovation programme under grant agreement №&nbsp;825627&nbsp;
                                        <abbr _ngcontent-c2="" className="acronym">(ELG)</abbr>
                                    </span>
                                </small>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item container direction="row" xs={12} className="py-2">
                        <div _ngcontent-c2="" className="col copyright-col">
                            <small _ngcontent-c2="">© {new Date().getFullYear()} ELG Consortium &nbsp;
                                <a _ngcontent-c2="" className="copyright__developer" href={`${FOOTER_BASE_URL}page/terms-of-use`}>Terms of Use</a></small></div>
                    </Grid>
                </Grid>
            </Grid>
        </footer>
    }


    render() {
        if (this.state.error) {
            return this.errorFooter();
        }
        if (!this.state.footerData || !this.state.feedbackData) {
            return <div></div>
        }
        const footerMenuArray = this.state.footerData;
        const feedBackMenuArray = this.state.feedbackData;
        const baseURL = `${FOOTER_BASE_URL}`;
        return <footer >
            <Grid container direction="row" justifyContent="center" alignItems="center" className="footer-container">
                <Grid item xs={11} sm={11} container direction="row" justifyContent="flex-start" alignItems="center" >
                    <Grid item container direction="row" xs={12} className="pb-3 footer-menu__wrapper no-gutters">
                        <Grid item className="footer-menu__main" xs={9} sm={9}>
                            {
                                footerMenuArray.map((menuItem, menuItemIndex) => {
                                    const { title } = menuItem || "";
                                    const { field_show_as_not_clickable, field_elg_menu_link_type } = menuItem || null;
                                    let { relative } = menuItem || "";
                                    relative = this.fixMenuPathPrefix(field_elg_menu_link_type, relative);
                                    const final_menu_url = field_elg_menu_link_type === "External" ? relative : `${baseURL}${relative}`;
                                    return field_show_as_not_clickable !== "1" ?
                                        <a key={menuItemIndex} className="footerItems" href={`${final_menu_url}`}>{title}</a>
                                        :
                                        <button disabled style={{ cursor: 'auto' }} key={menuItemIndex} className="linkbutton footerItems">{title}</button>
                                })

                            }
                        </Grid>
                        <Grid item xs={3} sm={3} className="text-right footer-menu__feedback">
                            {
                                feedBackMenuArray.map((menuItem, menuItemIndex) => {
                                    const { title } = menuItem || "";
                                    const { field_elg_menu_link_type } = menuItem || null;
                                    let { relative } = menuItem || "";
                                    relative = this.fixMenuPathPrefix(field_elg_menu_link_type, relative);
                                    const final_menu_url = field_elg_menu_link_type === "External" ? relative : `${baseURL}${relative}`;
                                    return <a key={menuItemIndex} className=" footerItems" href={`${final_menu_url}`}>{title}</a>
                                })
                            }
                        </Grid>
                    </Grid>

                    <Grid item container direction="row" xs={12} className="py-2 no-gutters">                        {
                        <Grid item container direction="row" xs={12}>
                            <Grid item sm={6} xs={12}>
                                <small style={{ display: "flex" }}>
                                    <img className="img--inline footerImg" src={Eu} alt="european" />
                                    <span>
                                        The European Language Grid has received funding from the European
                                        Union’s Horizon 2020 research and innovation programme under grant agreement №&nbsp;825627&nbsp;
                                        <abbr className="acronym">(ELG)</abbr>
                                    </span>
                                </small>
                            </Grid>
                        </Grid>
                    }
                    </Grid>
                    <Grid item container direction="row" xs={12} className="py-2">
                        <div _ngcontent-c2="" className="col copyright-col">
                            <small _ngcontent-c2="">© {new Date().getFullYear()} ELG Consortium &nbsp;
                                <a _ngcontent-c2="" className="copyright__developer" href={`${baseURL}terms-of-use`}>Terms of Use</a></small></div>
                    </Grid>
                </Grid>
            </Grid>
        </footer>
    }
}