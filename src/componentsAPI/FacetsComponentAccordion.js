import React from "react";
import { FormattedMessage } from 'react-intl';
//import {FormattedHTMLMessage} from 'react-intl';
import Link from '@material-ui/core/Link';
//import "./FacetsComponent.css";
import { Button, TextField } from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import { languages2Move } from "../data/MoveFacetEuropeanLanguages";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "../config/messages";
import Typography from '@material-ui/core/Typography';

class FacetsComponentAccordion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expandedLanguages: false, expandedEuropeanUnion: false, expandedNonEuropean: false, expandedOther: false, expandedLicences: false, expandedResourceType: false, expandedEntityType: false, expandedFunctionService: false, expandedIntendedApplication: false, expandedcondition_of_use: false,
            itemsToShowLanguages: 5, itemsToShowEuropeanUnion: 5, itemsToShowNonEuropean: 5, itemsToShowOther: 5, itemsToShowLicenses: 5, itemsToShowResourceType: 5, itemsToShowEntityType: 5, itemsToShowFunctions: 5, itemsToShowIntendedApplication: 5, itemsToShowConditionOfUse: 5, searchKeyword: '',
            euLanguage_searchKeyword: "", eu_langauge_displayTextField: false,
            eu_other_Language_searchKeyword: "", eu_other_Language_displayTextField: false,
            other_language_searchKeyword: "", other_language_displayTextField: false,
            function_searchKeyword: "", function_displayTextField: false
        };
        this.toggleloadMoreLanguages = this.toggleloadMoreLanguages.bind(this);
        this.toggleloadMoreEuropean = this.toggleloadMoreEuropean.bind(this);
        this.toggleloadMoreNonEuropean = this.toggleloadMoreNonEuropean.bind(this);
        this.toggleloadMoreOther = this.toggleloadMoreOther.bind(this);
        this.toggleloadMoreLicenses = this.toggleloadMoreLicenses.bind(this);
        this.toggleloadMoreResourceType = this.toggleloadMoreResourceType.bind(this);
        this.toggleloadMoreEntityType = this.toggleloadMoreEntityType.bind(this);
        this.toggleloadMoreServiceFunction = this.toggleloadMoreServiceFunction.bind(this);
        this.toggleloadMoreIntendedApplication = this.toggleloadMoreIntendedApplication.bind(this);
        this.toggleloadMoreconditionΟfUse = this.toggleloadMoreconditionΟfUse.bind(this);
        this.handleResourceTypeSearch = this.handleResourceTypeSearch.bind(this);
        this.handleEntityTypeSearch = this.handleEntityTypeSearch.bind(this);
        this.handleLanguageEUSearch = this.handleLanguageEUSearch.bind(this);
        this.handleLanguageSearch = this.handleLanguageSearch.bind(this);
        this.handleLicenceSearch = this.handleLicenceSearch.bind(this);
        //this.markFacetAsSelected = this.markFacetAsSelected.bind(this);
        this.clearAllFilters = this.clearAllFilters.bind(this);
        this.handleUnselected = this.handleUnselected.bind(this);
    }

    toggleloadMoreconditionΟfUse() {
        this.state.itemsToShowConditionOfUse === 5 ? (
            this.setState({ itemsToShowConditionOfUse: this.props.facets._filter_condition_of_use.condition_of_use.buckets.length, expandedcondition_of_use: true })
        ) : (
                this.setState({ itemsToShowConditionOfUse: 5, expandedcondition_of_use: false })
            )
    }


    toggleloadMoreServiceFunction() {
        this.state.itemsToShowFunctions === 5 ? (
            this.setState({ itemsToShowFunctions: this.props.facets._filter_function.function.buckets.length, expandedFunctionService: true })
        ) : (
                this.setState({ itemsToShowFunctions: 5, expandedFunctionService: false })
            )
    }

    toggleloadMoreIntendedApplication() {
        this.state.itemsToShowIntendedApplication === 5 ? (
            this.setState({ itemsToShowIntendedApplication: this.props.facets._filter_intended_application.intended_application.buckets.length, expandedIntendedApplication: true })
        ) : (
                this.setState({ itemsToShowIntendedApplication: 5, expandedIntendedApplication: false })
            )
    }

    toggleloadMoreLanguages() {
        this.state.itemsToShowLanguages === 5 ? (
            this.setState({ itemsToShowLanguages: this.props.facets._filter_language.language.buckets.length, expandedLanguages: true })
        ) : (
                this.setState({ itemsToShowLanguages: 5, expandedLanguages: false })
            )
    }

    toggleloadMoreEuropean() {
        this.state.itemsToShowEuropeanUnion === 5 ? (
            this.setState({ itemsToShowEuropeanUnion: this.props.facets._filter_language_eu.language_eu.buckets.length, expandedEuropeanUnion: true })
        ) : (
                this.setState({ itemsToShowEuropeanUnion: 5, expandedEuropeanUnion: false })
            )
    }

    toggleloadMoreNonEuropean() {
        this.state.itemsToShowNonEuropean === 5 ? (
            this.setState({ itemsToShowNonEuropean: this.props.facets._filter_language_eu_other.language_eu_other.buckets.length, expandedNonEuropean: true })
        ) : (
                this.setState({ itemsToShowNonEuropean: 5, expandedNonEuropean: false })
            )
    }

    toggleloadMoreOther() {
        this.state.itemsToShowOther === 5 ? (
            this.setState({ itemsToShowOther: this.props.facets._filter_language_rest.language_rest.buckets.length, expandedOther: true })
        ) : (
                this.setState({ itemsToShowOther: 5, expandedOther: false })
            )
    }

    toggleloadMoreLicenses() {
        this.state.itemsToShowLicenses === 5 ? (
            this.setState({ itemsToShowLicenses: this.props.facets._filter_licence.licence.buckets.length, expandedLicences: true })
        ) : (
                this.setState({ itemsToShowLicenses: 5, expandedLicences: false })
            )
    }

    toggleloadMoreResourceType() {
        this.state.itemsToShowResourceType === 3 ? (
            this.setState({ itemsToShowResourceType: this.props.facets._filter_licence.licence.buckets.length, expandedResourceType: true })
        ) : (
                this.setState({ itemsToShowResourceType: 3, expandedResourceType: false })
            )
    }

    toggleloadMoreEntityType() {
        this.state.itemsToShowEntityType === 3 ? (
            this.setState({ itemsToShowEntityType: this.props.facets._filter_entity_type.entity_type.buckets.length, expandedEntityType: true })
        ) : (
                this.setState({ itemsToShowEntityType: 3, expandedEntityType: false })
            )
    }

    handleUnselected(index, keyword) {
        /*const selectedArray = [...this.state.selectedFacetsNames];
        selectedArray.splice(index, 1);
        this.setState({ selectedFacetsNames: selectedArray });*/
        this.props.handleUnselectedFacet(index);
        this.props.unselectFacet(keyword);
    }


    handleResourceTypeSearch(event, value, selectedFacets) {
        const index = selectedFacets.indexOf(value);
        const keyword = "resource_type__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }

    handleEntityTypeSearch(value, selectedFacets) {
        const index = selectedFacets.indexOf(value);
        let keyword = "entity_type__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {
            this.handleUnselected(index, keyword);
        }
    }

    handleLanguageSearch(value, selectedFacets) {
        const index = selectedFacets.indexOf(value);
        let keyword = "language__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {
            this.handleUnselected(index, keyword);
        }
    }

    handleFunctionsSearch(value, selectedFacets) {
        this.setState({ function_searchKeyword: "", function_displayTextField: false });
        const index = selectedFacets.indexOf(value);
        let keyword = "function__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {
            this.handleUnselected(index, keyword);
        }
    }

    handleIntendedApplicationSearch(value, selectedFacets) {
        const index = selectedFacets.indexOf(value);
        let keyword = "intended_application__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {
            this.handleUnselected(index, keyword);
        }
    }

    handleLanguageEUSearch(value, selectedFacets) {
        this.setState({ euLanguage_searchKeyword: "", eu_other_Language_searchKeyword: "", other_language_searchKeyword: "" });
        const index = selectedFacets.indexOf(value);
        let keyword = "language__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {
            this.handleUnselected(index, keyword);
        }
    }

    handlelanguageEUOtherSearch(value, selectedFacets) {
        this.setState({ euLanguage_searchKeyword: "", eu_other_Language_searchKeyword: "", other_language_searchKeyword: "" });
        const index = selectedFacets.indexOf(value);
        let keyword = "language__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {
            this.handleUnselected(index, keyword);
        }
    }

    handlelanguageRestSearch(value, selectedFacets) {
        this.setState({ euLanguage_searchKeyword: "", eu_other_Language_searchKeyword: "", other_language_searchKeyword: "" });
        const index = selectedFacets.indexOf(value);
        let keyword = "language__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {
            this.handleUnselected(index, keyword);
        }
    }


    handleLicenceSearch(value, selectedFacets) {
        const index = selectedFacets.indexOf(value);
        let keyword = "licence__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {
            this.handleUnselected(index, keyword);
        }
    }

    handleconditionΟfUseSearch(event, value, selectedFacets) {
        const index = selectedFacets.indexOf(value);
        const keyword = "condition_of_use__term=" + value;
        if (index === -1) {
            //this.markFacetAsSelected(value);
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now i should unselected
            this.handleUnselected(index, keyword);
        }
    }


    /*markFacetAsSelected(facetName) {
        if (this.state.selectedFacetsNames.indexOf(facetName) === -1) {//checks if i have allready selected the facet, if not add it to array
            this.setState(prevState => ({ selectedFacetsNames: [...prevState.selectedFacetsNames, facetName] }));
            //this.state.selectedFacetsNames.forEach(facetName => document.getElementById(facetName).classList.add('FacetActive'));
        }
        //document.getElementById(facetName) && document.getElementById(facetName).classList.add('FacetActive');//comment out for release. need to find solution 
    }*/

    clearAllFilters() {
        this.setState({
            expandedLanguages: false, expandedEuropeanUnion: false, expandedNonEuropean: false, expandedOther: false, expandedLicences: false, expandedResourceType: false, expandedEntityType: false, expandedFunctionService: false, expandedIntendedApplication: false,
            itemsToShowLanguages: 5, itemsToShowEuropeanUnion: 5, itemsToShowNonEuropean: 5, itemsToShowOther: 5, itemsToShowLicenses: 5, itemsToShowResourceType: 5, itemsToShowEntityType: 5, itemsToShowFunctions: 5, itemsToShowIntendedApplication: 5, itemsToShowConditionOfUse: 5, searchKeyword: ''
        });
        this.props.onClearAllSearchFilters();
    }


    render() {
        const { facets } = this.props;
        ////////////////////////
        const move = JSON.parse(JSON.stringify(facets._filter_language_rest.language_rest.buckets.filter(item => languages2Move.includes(item.key))));
        facets._filter_language_rest.language_rest.buckets = facets._filter_language_rest.language_rest.buckets.filter(item => !languages2Move.includes(item.key));
        facets._filter_language_eu_other.language_eu_other.buckets = JSON.parse(JSON.stringify(facets._filter_language_eu_other.language_eu_other.buckets.filter(item => item.key !== "Croatian").concat(move).sort((a, b) => b.doc_count - a.doc_count)));
        ////////////////////////
        const { /*itemsToShowLanguages,*/ itemsToShowLicenses, itemsToShowResourceType, itemsToShowEntityType, itemsToShowFunctions } = this.state;
        const { itemsToShowEuropeanUnion, itemsToShowNonEuropean, itemsToShowOther, itemsToShowConditionOfUse } = this.state;
        //const { itemsToShowIntendedApplication } = this.state;      
        const selectedFacetsUriEncoded = this.props.selectedFacets || [];
        const selectedFacets = [];
        selectedFacetsUriEncoded.forEach(element => {
            selectedFacets.push(decodeURIComponent(element));
        });

        return (
            <div>
                <div style={{ marginBottom: "20px" }}>
                    {selectedFacets.length > 0 && <Button classes={{ root: 'inner-link-outlined--teal' }} endIcon={<HighlightOffIcon />} onClick={() => this.clearAllFilters()}>
                        <FormattedMessage id="facetsComponent.clearAllFilters" defaultMessage="Clear all filters" >
                        </FormattedMessage>
                    </Button>}
                </div>


                {facets._filter_resource_type.resource_type.buckets.length > 0 &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_resource_type.resource_type.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-resource_type"
                                id="panel1a-header-resource_type"
                            >
                                <h4><FormattedMessage id="facetsComponent.resourceType" defaultMessage="Language Resources & Technologies" >
                                </FormattedMessage></h4>
                            </AccordionSummary>
                            <AccordionDetails>

                                <ul className="facets-list">
                                    {facets._filter_resource_type.resource_type.buckets.slice(0, itemsToShowResourceType).map((resource_type, index) =>
                                        <li className="FacetButton" button="true" key={resource_type.key} onClick={(e) => this.handleResourceTypeSearch(e, resource_type.key, selectedFacets)} >
                                            {selectedFacets.indexOf(resource_type.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {resource_type.key} </span>
                                            <span className="count" > ({resource_type.doc_count}) </span>

                                        </li>)
                                    }
                                    {facets._filter_resource_type.resource_type.buckets.length > 5 &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={this.toggleloadMoreResourceType} >
                                                {this.state.expandedResourceType ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }


                {facets._filter_function.function.buckets.length > 0 &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_function.function.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-function"
                                id="panel1a-header-function"
                            >
                                <h4><FormattedMessage id="facetsComponent.Functions" defaultMessage="Service functions" >
                                </FormattedMessage></h4>
                            </AccordionSummary>
                            <AccordionDetails>
                                <div className="id--small" >
                                    {(!this.state.function_displayTextField && !this.state.function_searchKeyword) && <span onMouseEnter={(e) => { this.setState({ function_displayTextField: true }) }}><SearchIcon /></span>}
                                    {(this.state.function_displayTextField || this.state.function_searchKeyword) && <TextField
                                        helperText="Type to narrow down service functions"
                                        //placeholder="Type to narrow down service functions"
                                        //label="Type to narrow down service functions"
                                        variant="outlined"
                                        style={{ "width": "100%", marginLeft: "1px", paddingBottom: "20px" }}
                                        size="small"
                                        value={this.state.function_searchKeyword} onChange={(e) => { this.setState({ function_searchKeyword: e.target.value }) }}
                                        InputProps={{
                                            endAdornment: (
                                                <SearchIcon />
                                            )
                                        }}
                                        onMouseOut={(e) => { this.setState({ function_displayTextField: false }) }}
                                    />}
                                </div>

                                <ul className="facets-list">
                                    {facets._filter_function.function.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.function_searchKeyword.toLocaleLowerCase()) >= 0).slice(0, itemsToShowFunctions).map((functionService, index) =>
                                        <li className="filterItem" button="true" key={functionService.key} onClick={() => this.handleFunctionsSearch(functionService.key, selectedFacets)}>
                                            {selectedFacets.indexOf(functionService.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {functionService.key} </span>
                                            <span className="count" > ({functionService.doc_count}) </span>
                                        </li>)
                                    }
                                    {facets._filter_function.function.buckets.length > 5 &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={this.toggleloadMoreServiceFunction} >
                                                {this.state.expandedFunctionService ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>}
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }

              


                <div >
                    <Accordion className="facets__accordion languages" defaultExpanded={facets._filter_language_eu.language_eu.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 || facets._filter_language_eu_other.language_eu_other.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 || facets._filter_language_rest.language_rest.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon className="grey--font" />}
                            aria-controls="panel1a-content-languages"
                            id="panel1a-header-languages"
                        >
                            <h4>Languages</h4>
                        </AccordionSummary>
                        <AccordionDetails>
                            {facets._filter_language_eu.language_eu.buckets.length > 0 &&
                                <div style={{ marginBottom: "1em" }}>
                                    <div className="flex">
                                        <span className="id--small">Official EU languages</span>
                                        {(!this.state.eu_langauge_displayTextField && !this.state.euLanguage_searchKeyword) && <span onMouseEnter={(e) => { this.setState({ eu_langauge_displayTextField: true }) }}><SearchIcon /></span>}
                                        {(this.state.eu_langauge_displayTextField || this.state.euLanguage_searchKeyword) && <TextField
                                            style={{ "width": "30%", marginLeft: "10px" }}
                                            size="small"
                                            value={this.state.euLanguage_searchKeyword} onChange={(e) => { this.setState({ euLanguage_searchKeyword: e.target.value }) }}
                                            InputProps={{
                                                endAdornment: (
                                                    <SearchIcon />
                                                )
                                            }}
                                            onMouseOut={(e) => { this.setState({ eu_langauge_displayTextField: false }) }}
                                        />}
                                    </div>

                                    <ul className={(this.state.expandedEuropeanUnion && facets._filter_language_eu.language_eu.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.euLanguage_searchKeyword.toLowerCase()) >= 0).slice(0, itemsToShowEuropeanUnion).length > 5) ? "facets-list scroll-facet" : "facets-list"}>
                                        {facets._filter_language_eu.language_eu.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.euLanguage_searchKeyword.toLowerCase()) >= 0).slice(0, itemsToShowEuropeanUnion).map((language, index) =>

                                            <li className="FacetButton" button="true" key={language.key} onClick={() => this.handleLanguageEUSearch(language.key, selectedFacets)}>
                                                {selectedFacets.indexOf(language.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                                <span className="FacetText" > {language.key} </span>
                                                <span className="count" > ({language.doc_count}) </span>
                                            </li>)
                                        }
                                        {facets._filter_language_eu.language_eu.buckets.length > 5 &&
                                            <div className="ShowMore">
                                                <Link component="button" onClick={this.toggleloadMoreEuropean} >
                                                    {this.state.expandedEuropeanUnion ? (<span>
                                                        <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                        </FormattedMessage>
                                                    </span>) : (<span>
                                                        <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                        </FormattedMessage>
                                                    </span>)}
                                                </Link>
                                            </div>
                                        }
                                    </ul>
                                </div>
                            }

                            {facets._filter_language_eu_other.language_eu_other.buckets.length > 0 &&
                                <div >
                                    <div className="flex">
                                        <span className="id--small">Other EU/European languages</span>
                                        {(!this.state.eu_other_Language_displayTextField && !this.state.eu_other_Language_searchKeyword) && <span onMouseEnter={(e) => { this.setState({ eu_other_Language_displayTextField: true }) }}><SearchIcon /></span>}
                                        {(this.state.eu_other_Language_displayTextField || this.state.eu_other_Language_searchKeyword) && <TextField
                                            style={{ "width": "30%", marginLeft: "10px" }}
                                            size="small"
                                            value={this.state.eu_other_Language_searchKeyword} onChange={(e) => { this.setState({ eu_other_Language_searchKeyword: e.target.value }) }}
                                            InputProps={{
                                                endAdornment: (
                                                    <SearchIcon />
                                                )
                                            }}
                                            onMouseOut={(e) => { this.setState({ eu_other_Language_displayTextField: false }) }}
                                        />}
                                    </div>
                                    <ul className={(this.state.expandedNonEuropean && facets._filter_language_eu_other.language_eu_other.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.eu_other_Language_searchKeyword.toLowerCase()) >= 0).slice(0, itemsToShowNonEuropean).length > 5) ? "facets-list scroll-facet" : "facets-list"}>
                                        {facets._filter_language_eu_other.language_eu_other.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.eu_other_Language_searchKeyword.toLowerCase()) >= 0).slice(0, itemsToShowNonEuropean).map((language_eu_other, index) =>
                                            <li className="FacetButton" button="true" key={language_eu_other.key} onClick={() => this.handlelanguageEUOtherSearch(language_eu_other.key, selectedFacets)}>
                                                {selectedFacets.indexOf(language_eu_other.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                                <span className="FacetText" > {language_eu_other.key} </span>
                                                <span className="count" > ({language_eu_other.doc_count}) </span>
                                            </li>)
                                        }
                                        {facets._filter_language_eu_other.language_eu_other.buckets.length > 5 &&
                                            <div className="ShowMore">
                                                <Link component="button" onClick={this.toggleloadMoreNonEuropean} >
                                                    {this.state.expandedNonEuropean ? (<span>
                                                        <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                        </FormattedMessage>
                                                    </span>) : (<span>
                                                        <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                        </FormattedMessage>
                                                    </span>)}
                                                </Link>
                                            </div>
                                        }
                                    </ul>
                                </div>
                            }
                            {facets._filter_language_rest.language_rest.buckets.length > 0 &&
                                <div>
                                    <div className="flex">
                                        <span className="id--small">Other languages</span>
                                        {(!this.state.other_language_displayTextField && !this.state.other_language_searchKeyword) && <span onMouseEnter={(e) => { this.setState({ other_language_displayTextField: true }) }}><SearchIcon /></span>}
                                        {(this.state.other_language_displayTextField || this.state.other_language_searchKeyword) && <TextField
                                            style={{ "width": "30%", marginLeft: "10px" }}
                                            size="small"
                                            value={this.state.other_language_searchKeyword} onChange={(e) => { this.setState({ other_language_searchKeyword: e.target.value }) }}
                                            InputProps={{
                                                endAdornment: (
                                                    <SearchIcon />
                                                )
                                            }}
                                            onMouseOut={(e) => { this.setState({ other_language_displayTextField: false }) }}
                                        />}
                                    </div>
                                    <ul className={(this.state.expandedOther && facets._filter_language_rest.language_rest.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.other_language_searchKeyword.toLowerCase()) >= 0).slice(0, itemsToShowOther).length > 5) ? "facets-list scroll-facet" : "facets-list"}>
                                        {facets._filter_language_rest.language_rest.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.other_language_searchKeyword.toLowerCase()) >= 0).slice(0, itemsToShowOther).map((language_rest, index) =>
                                            <li className="FacetButton" button="true" key={language_rest.key} onClick={() => this.handlelanguageRestSearch(language_rest.key, selectedFacets)}>
                                                {selectedFacets.indexOf(language_rest.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                                <span className="FacetText" > {language_rest.key} </span>
                                                <span className="count" > ({language_rest.doc_count}) </span>
                                            </li>)
                                        }
                                        {facets._filter_language_rest.language_rest.buckets.length > 5 &&
                                            <div className="ShowMore">
                                                <Link component="button" onClick={this.toggleloadMoreOther} >
                                                    {this.state.expandedOther ? (<span>
                                                        <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                        </FormattedMessage>
                                                    </span>) : (<span>
                                                        <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                        </FormattedMessage>
                                                    </span>)}
                                                </Link>
                                            </div>
                                        }
                                    </ul>
                                </div>
                            }

                        </AccordionDetails>
                    </Accordion>
                </div>




                {facets._filter_licence.licence.buckets.length > 0 &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_licence.licence.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-licence"
                                id="panel1a-header-licence"
                            >
                                <h4><FormattedMessage id="facetsComponent.licences" defaultMessage="Licences" >
                                </FormattedMessage></h4>
                            </AccordionSummary>
                            <AccordionDetails>
                                <ul className="facets-list">
                                    {facets._filter_licence.licence.buckets.slice(0, itemsToShowLicenses).map((license, index) =>
                                        <li className="FacetButton" button="true" key={license.key} onClick={() => this.handleLicenceSearch(license.key, selectedFacets)} >
                                            {selectedFacets.indexOf(license.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {license.key} </span>
                                            <span className="count" > ({license.doc_count}) </span>
                                        </li>)
                                    }
                                    {facets._filter_licence.licence.buckets.length > 5 &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={this.toggleloadMoreLicenses} >
                                                {this.state.expandedLicences ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }

                {facets._filter_condition_of_use.condition_of_use.buckets.length > 0 &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_condition_of_use.condition_of_use.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-condition_of_use"
                                id="panel1a-header-condition_of_use"
                            >
                                <Tooltip title={messages.condition_of_use_hover_text}><h4><FormattedMessage id="facetsComponent.conditionΟfUse" defaultMessage="Conditions of use for data" >
                                </FormattedMessage></h4></Tooltip>
                            </AccordionSummary>
                            <AccordionDetails>
                                {<Typography variant="caption">{messages.condition_of_use_hover_text}</Typography>}
                                <ul className="facets-list">
                                    {facets._filter_condition_of_use.condition_of_use.buckets.slice(0, itemsToShowConditionOfUse).map((condition_of_use, index) =>
                                        <li className="FacetButton" button="true" key={condition_of_use.key} onClick={(e) => this.handleconditionΟfUseSearch(e, condition_of_use.key, selectedFacets)} >
                                            {selectedFacets.indexOf(condition_of_use.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {condition_of_use.key} </span>
                                            <span className="count" > ({condition_of_use.doc_count}) </span>

                                        </li>)
                                    }
                                    {facets._filter_condition_of_use.condition_of_use.buckets.length > 5 &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={this.toggleloadMoreconditionΟfUse} >
                                                {this.state.expandedcondition_of_use ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }


                {facets._filter_entity_type.entity_type.buckets.length > 0 && !(facets._filter_entity_type.entity_type.buckets.length === 1 && facets._filter_entity_type.entity_type.buckets[0].key === "LanguageResource") &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_entity_type.entity_type.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-entity_type"
                                id="panel1a-header-entity_type"
                            >
                                <h4><FormattedMessage id="facetsComponent.entityType" defaultMessage="Related Enitites" >
                                </FormattedMessage></h4>
                            </AccordionSummary>
                            <AccordionDetails>
                                <ul className="facets-list">
                                    {facets._filter_entity_type.entity_type.buckets.slice(0, itemsToShowEntityType).map((entity_type, index) =>
                                        entity_type.key !== "LanguageResource" && <li className="FacetButton" button="true" key={entity_type.key} onClick={() => this.handleEntityTypeSearch(entity_type.key, selectedFacets)} >
                                            {selectedFacets.indexOf(entity_type.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {entity_type.key}</span>
                                            <span className="count" > ({entity_type.doc_count}) </span>
                                        </li>)
                                    }
                                    {facets._filter_entity_type.entity_type.buckets.length > 5 &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={this.toggleloadMoreEntityType} >
                                                {this.state.expandedEntityType ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }
            </div >
        )
    }
}

export default FacetsComponentAccordion;