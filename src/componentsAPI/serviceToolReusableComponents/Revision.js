import React from "react";
import Typography from '@material-ui/core/Typography';

export default class Revision extends React.Component {
    render() {
        const { revision } = this.props;
        if (!revision) {
            return <div></div>
        }
        return < div className="padding5" >
            {revision &&
                <div>
                    <Typography className="bold-p--id"> {revision.label}  </Typography>
                    <span className="info_value">{revision.value}</span>
                </div>
            }
        </div>
    }
}