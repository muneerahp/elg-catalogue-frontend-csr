import React from "react";
import Typography from '@material-ui/core/Typography';
//import ExpansionPanel from '@material-ui/core/ExpansionPanel';
//import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
//import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ResourceActor from "../CommonComponents/ResourceActor";
import DocumentIdentifier from "../CommonComponents/DocumentIdentifier";
import { ReactComponent as DocIcon } from "./../../assets/elg-icons/document.svg";

export default class Evaluation extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
        if (!data.described_entity.field_value.lr_subclass) {
            return <div></div>
        }
        const evaluation = data.described_entity.field_value.lr_subclass.field_value.evaluation ? data.described_entity.field_value.lr_subclass.field_value.evaluation.field_value : [];
        const evaluated = data.described_entity.field_value.lr_subclass.field_value.evaluated ? data.described_entity.field_value.lr_subclass.field_value.evaluated.field_value : null;
        const trl = data.described_entity.field_value.lr_subclass.field_value.trl || "";
        const evaluated_color_scheme = evaluated === true ? "true" : "false";        

        if (evaluation.length === 0) {
            return <div className="padding5">
                {evaluated && <Typography variant="h3" className="title-links">{data.described_entity.field_value.lr_subclass.field_value.evaluated.field_label[metadataLanguage] || data.described_entity.field_value.lr_subclass.field_value.evaluated.field_label["en"]}</Typography>}
                {(evaluated !== undefined && evaluated !== null) && <div className="padding15">
                    <div>
                        <span className="info_value">
                            {data.described_entity.field_value.lr_subclass.field_value.evaluated.field_label[metadataLanguage] || data.described_entity.field_value.lr_subclass.field_value.evaluated.field_label["en"]}: </span>
                        <span className={"general-flag ml-05 "+ evaluated_color_scheme }>{`${evaluated}`}</span></div></div>}
                {trl && <div><span>{trl.field_label[metadataLanguage] || trl.field_label["en"]}: </span> <span>{trl.label[metadataLanguage] || trl.label[Object.keys(trl.label)[0]]}</span></div>}
            </div>
        }
        return <>
            <Typography variant="h3" className="title-links">{data.described_entity.field_value.lr_subclass.field_value.evaluation.field_label[metadataLanguage] || data.described_entity.field_value.lr_subclass.field_value.evaluation.field_label["en"]}</Typography>
            <div>{(evaluated !== undefined || evaluated !== null) && <div className="padding15">
                <div>
                    <span className="info_value">
                        {data.described_entity.field_value.lr_subclass.field_value.evaluated.field_label[metadataLanguage] || data.described_entity.field_value.lr_subclass.field_value.evaluated.field_label["en"]}:
            </span> <span className={"general-flag ml-05 "+ evaluated_color_scheme }>{`${evaluated}`}</span></div></div>}</div>
            {trl && <div className="padding5"><Typography className="bold-p--id">{trl.field_label[metadataLanguage] || trl.field_label["en"]}</Typography> <span className="info_value">{trl.label[metadataLanguage] || trl.label[Object.keys(trl.label)[0]]}</span></div>}
            {
                evaluation.map((evaluationItem, evaluationIndex) => {
                    let evaluation_levelArray = evaluationItem.evaluation_level ? evaluationItem.evaluation_level.field_value.map(evaluation_levelItem => evaluation_levelItem.label[metadataLanguage] || evaluation_levelItem.label[Object.keys(evaluation_levelItem.label)[0]]) : [];
                    let evalutaion_level_label = evaluationItem.evaluation_level ? (evaluationItem.evaluation_level.field_label[metadataLanguage] || evaluationItem.evaluation_level.field_label["en"]) : "Evaluation level";
                    let evaluation_typeArray = evaluationItem.evaluation_type ? evaluationItem.evaluation_type.field_value.map(evaluation_typeItem => evaluation_typeItem.label[metadataLanguage] || evaluation_typeItem.label[Object.keys(evaluation_typeItem.label)[0]]) : [];
                    let evalutaion_type_label = evaluationItem.evaluation_type ? (evaluationItem.evaluation_type.field_label[metadataLanguage] || evaluationItem.evaluation_type.field_label["en"]) : "Evaluation type";
                    let evaluation_criterionArray = evaluationItem.evaluation_criterion ? evaluationItem.evaluation_criterion.field_value.map(evaluation_criterionItem => evaluation_criterionItem.label[metadataLanguage] || evaluation_criterionItem.label[Object.keys(evaluation_criterionItem.label)[0]]) : [];
                    let evalutaion_criterion_label = evaluationItem.evaluation_criterion ? (evaluationItem.evaluation_criterion.field_label[metadataLanguage] || evaluationItem.evaluation_criterion.field_label["en"]) : "Evaluation criterion";
                    let evaluation_measureArray = evaluationItem.evaluation_measure ? evaluationItem.evaluation_measure.field_value.map(evaluation_measureItem => evaluation_measureItem.label[metadataLanguage] || evaluation_measureItem.label[Object.keys(evaluation_measureItem.label)[0]]) : [];
                    let evalutaion_measure_label = evaluationItem.evaluation_measure ? (evaluationItem.evaluation_measure.field_label[metadataLanguage] || evaluationItem.evaluation_measure.field_label["en"]) : "Evaluation measure";
                    let gold_standard_location = evaluationItem.gold_standard_location ? evaluationItem.gold_standard_location.field_value : "";
                    let gold_standard_location_label = evaluationItem.gold_standard_location ? (evaluationItem.gold_standard_location.field_label[metadataLanguage] || evaluationItem.gold_standard_location.field_label["en"]) : "Gold standard location";
                    let performance_indicatorArray = evaluationItem.performance_indicator ? evaluationItem.performance_indicator.field_value.map((performance_indicatorItem, performance_indicatorIndex) => {
                        let performance_indicatorItem_metric = performance_indicatorItem.metric ? (performance_indicatorItem.metric.label[metadataLanguage] || performance_indicatorItem.metric.label[Object.keys(performance_indicatorItem.metric.label)[0]]) : "";
                        let performance_indicator_metric_label = performance_indicatorItem.metric ? (performance_indicatorItem.metric.field_label[metadataLanguage] || performance_indicatorItem.metric.field_label["en"]) : "Metric";
                        let performance_indicatorItem_measure = performance_indicatorItem.measure ? performance_indicatorItem.measure.field_value : "";
                        let performance_indicator_measure_label = performance_indicatorItem.measure ? (performance_indicatorItem.measure.field_label[metadataLanguage] || performance_indicatorItem.measure.field_label["en"]) : "Measure";
                        let performance_indicatorItem_unit_of_measure_metric = performance_indicatorItem.unit_of_measure_metric ? performance_indicatorItem.unit_of_measure_metric.field_value : "";
                        return <div key={performance_indicatorIndex}>
                            {performance_indicatorItem_metric && <div className="padding5"><Typography className="bold-p--id">{performance_indicator_metric_label}</Typography> <span className="info_value">{performance_indicatorItem_metric}</span></div>}
                            {performance_indicatorItem_measure && <div className="padding5"><Typography className="bold-p--id">{performance_indicator_measure_label}</Typography> <span className="info_value"> {performance_indicatorItem_measure} {performance_indicatorItem_unit_of_measure_metric}</span></div>}
                        </div>
                    }) : [];
                    let performance_indicatorLabel = evaluationItem.performance_indicator ? (evaluationItem.performance_indicator[metadataLanguage] || evaluationItem.performance_indicator["en"]) : "Performance indicator";
                    let evaluation_reportArray = evaluationItem.evaluation_report ? evaluationItem.evaluation_report.field_value.map((evaluation_reportItem, evaluation_reportIndex) => {
                        let bibliographic_record = evaluation_reportItem.bibliographic_record ? evaluation_reportItem.bibliographic_record.field_value : "";                         
                        return <div key={evaluation_reportIndex}className="padding15 flex-centered">
                        <span><DocIcon className="mr-05" /></span>
                        <span>                                                          
                            <DocumentIdentifier data={evaluation_reportItem} identifier_name={"document_identifier"} identifier_scheme={"document_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            {bibliographic_record && <> 
                                <pre className="cite"><code>{bibliographic_record}</code></pre></>}</span>
                        </div>
                    }) : [];
                    let evaluation_report_label = evaluationItem.evaluation_report ? (evaluationItem.evaluation_report.field_label[metadataLanguage] || evaluationItem.evaluation_report.field_label["en"]) : "Evaluation report";
                    let is_evaluated_byArray = evaluationItem.is_evaluated_by ? evaluationItem.is_evaluated_by.field_value.map((isEvaluatedByItem, isEvaluatedByIndex) => {
                        let isEvaluatedByItem_resource_name = isEvaluatedByItem.resource_name ? (isEvaluatedByItem.resource_name.field_value[metadataLanguage] || isEvaluatedByItem.resource_name.field_value[Object.keys(isEvaluatedByItem.resource_name.field_value)[0]]) : "";
                        let isEvaluatedByItem_version = isEvaluatedByItem.version ? isEvaluatedByItem.version.field_value : "";
                        return `${isEvaluatedByItem_resource_name} , ${isEvaluatedByItem_version}`;
                    }) : [];
                    let is_evaluated_by_label = evaluationItem.is_evaluated_by ? (evaluationItem.is_evaluated_by.field_label[metadataLanguage] || evaluationItem.is_evaluated_by.field_label["en"]) : "Is evaluated by";
                    let evaluation_details = evaluationItem.evaluation_details ? (evaluationItem.evaluation_details.field_value[metadataLanguage] || evaluationItem.evaluation_details.field_value[Object.keys(evaluationItem.evaluation_details.field_value)[0]]) : "";
                    let evalutation_details_label = evaluationItem.evaluation_details ? (evaluationItem.evaluation_details.field_label[metadataLanguage] || evaluationItem.evaluation_details.field_label["en"]) : "Evaluation details";
                    let evaluatorArray = evaluationItem.evaluator || [];
                    return <div key={evaluationIndex}>
                        {evaluation_levelArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{evalutaion_level_label}</Typography> <span className="info_value">{evaluation_levelArray.map(item => `${item} `)}</span></div>}
                        {evaluation_typeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{evalutaion_type_label}</Typography> <span className="info_value">{evaluation_typeArray.map(item => `${item} `)}</span></div>}
                        <div className="padding5">
                            <Accordion className="FunctionBox--accordions">
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon className="white--font" />}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header" className="FunctionBox--accordions__title-background">
                                    <Typography className="bold-p--id--small white--font">more</Typography>
                                </AccordionSummary>
                                <AccordionDetails className="expanded-details-background">
                                    <div>
                                        {evaluation_criterionArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{evalutaion_criterion_label}</Typography> <span className="info_value">{evaluation_criterionArray.map(item => item)}</span></div>}
                                        {evaluation_measureArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{evalutaion_measure_label}</Typography> <span className="info_value">{evaluation_measureArray.map(item => item)}</span></div>}
                                        {gold_standard_location && <div className="padding5"><Typography className="bold-p--id">{gold_standard_location_label}</Typography><span className="info_url"> <a href={gold_standard_location} target="blank">{gold_standard_location}</a></span></div>}
                                        {performance_indicatorArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{performance_indicatorLabel}</Typography> <span className="info_value">{performance_indicatorArray.map(item => item)}</span></div>}
                                        {evaluation_reportArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{evaluation_report_label}</Typography> <span className="info_value">{evaluation_reportArray.map(item => item)}</span></div>}
                                        {is_evaluated_byArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_evaluated_by_label}</Typography><div>{is_evaluated_byArray.map((item, index) => <div className="info_value" key={index}>{item}</div>)}</div></div>}
                                        {evaluation_details && <div className="padding5"><Typography className="bold-p--id">{evalutation_details_label}</Typography><span className="info_value">{evaluation_details}</span></div>}
                                        {evaluatorArray.length > 0 && <ResourceActor data={evaluatorArray} metadataLanguage={metadataLanguage} />}
                                    </div>
                                </AccordionDetails>
                            </Accordion>
                        </div>
                    </div>
                })
            }
        </>
    }
}