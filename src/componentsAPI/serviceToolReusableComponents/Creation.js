import React from "react";
import Revision from "./Revision";
import serviceToolParser from "../../parsers/ServiceToolParser";
import ResourceActor from "../CommonComponents/ResourceActor";
import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";
import commonParser from "../../parsers/CommonParser";
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";

export default class Creation extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }
        const { creation_mode, has_original_source, original_source_description,
            creation_details,is_created_by, support_type } = data.described_entity.field_value || "";
        const publication_date = (data.described_entity.field_value.publication_date && data.described_entity.field_value.publication_date.field_value) || undefined;
        const publication_date_label = (data.described_entity.field_value.publication_date && (data.described_entity.field_value.publication_date.field_label[metadataLanguage] || data.described_entity.field_value.publication_date.field_label["en"])) || "Publication date ";
        const creation_start_date = (data.described_entity.field_value.creation_start_date && data.described_entity.field_value.creation_start_date.field_value) || "";
        const creation_end_date = (data.described_entity.field_value.creation_end_date && data.described_entity.field_value.creation_end_date.field_value) || "";
        const update_frequency = serviceToolParser.getUpdateFrequency(data, metadataLanguage) ? serviceToolParser.getUpdateFrequency(data, metadataLanguage).value : null;
        const update_frequency_label = serviceToolParser.getUpdateFrequency(data, metadataLanguage) ? serviceToolParser.getUpdateFrequency(data, metadataLanguage).label : "update frequency";
        const revision = serviceToolParser.getRevision(data, metadataLanguage);
        return <div className="padding15">
            {((data.described_entity.field_value.resource_creator && data.described_entity.field_value.resource_creator.field_value.length > 0) ||
                publication_date ||
                creation_start_date ||
                creation_end_date ||
                update_frequency) && 
                <ResourceActor data={data.described_entity.field_value.resource_creator} metadataLanguage={metadataLanguage} /> 
            }
            

            {publication_date && <div className="padding5">
                <Typography className="bold-p--id"> {publication_date_label}  </Typography>
                <span className="info_value">{Intl.DateTimeFormat("en-GB", {
                                    year: "numeric",
                                    month: "long",
                                    day: "2-digit"
                                }).format(new Date(publication_date))}</span>
            </div>
            }
            {(creation_start_date || creation_end_date) && <div className="padding5">
                <Typography className="bold-p--id"> Creation dates </Typography>
                <span className="info_value">{creation_start_date && Intl.DateTimeFormat("en-GB", {
                                    year: "numeric",
                                    month: "long",
                                    day: "2-digit"
                                }).format(new Date(creation_start_date))}  {creation_end_date && <span>-</span>} {creation_end_date && Intl.DateTimeFormat("en-GB", {
                                    year: "numeric",
                                    month: "long",
                                    day: "2-digit"
                                }).format(new Date(creation_end_date))}</span>
            </div>
            }
            {update_frequency && <div className="padding5">
                <Typography className="bold-p--id"> {update_frequency_label}   </Typography>
                <div className="info_value">{update_frequency}</div>
            </div>
            }
            {creation_mode && <div className="padding15">
                     <Typography className="bold-p--id">{creation_mode.field_label[metadataLanguage] || creation_mode.field_label["en"]}</Typography>
                      <span className="info_value"> {creation_mode.label[metadataLanguage] || creation_mode.label[Object.keys(creation_mode.label)[0]]}</span>
                    </div>
            }

            {has_original_source && has_original_source.field_value.length > 0 && <div className="padding15">
                 <Typography className="bold-p--id">{has_original_source.field_label[metadataLanguage] || has_original_source.field_label["en"]}</Typography>
                 {has_original_source.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]] || "") : "";
                    let version = item.version ? item.version.field_value : "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>                        
                     {full_metadata_record ? 
                        ( <div className="padding5 internal_url">
                        <span><NavIcon className="xsmall-icon mr-05" /></span>
                        <Link to={full_metadata_record.internalELGUrl}>
                         {resource_name}  ({version})
                          </Link>
                          </div>) 
                         :
                          (<div className="info_value" key={index}>{resource_name}  ({version}) </div>)
                      }
                    </div>
                    })
                }
                </div>
                }
                 {original_source_description && <div className="padding15">
                    <Typography className="bold-p--id">{original_source_description.field_label[metadataLanguage] || original_source_description.field_label["en"]}</Typography>
                    <span className="info_value">{original_source_description.field_value[metadataLanguage] || original_source_description.field_value[Object.keys(original_source_description.field_value)[0]]}</span>
                    </div>
                 }
                 {creation_details && <div className="padding15">
                     <Typography className="bold-p--id">{creation_details.field_label[metadataLanguage] || creation_details.field_label["en"]}</Typography>
                      <span className="info_value"> {creation_details.field_value[metadataLanguage] || creation_details.field_value[Object.keys(creation_details.field_value)[0]]}</span>
                    </div>
                 }
                 { is_created_by && is_created_by.field_value.length > 0 && <div className="padding15"> 
                 <Typography className="bold-p--id">{is_created_by.field_label[metadataLanguage] || is_created_by.field_label["en"]}</Typography>
                    
                    {   is_created_by.field_value.map((createdBy, createdByIndex) => {
                        let resource_name = createdBy.resource_name ? (createdBy.resource_name.field_value[metadataLanguage] || createdBy.resource_name.field_value[Object.keys(createdBy.resource_name.field_value)[0]]) : "";
                        let version = createdBy.version ? createdBy.version.field_value : "";
                        let full_metadata_record = commonParser.getFullMetadata(createdBy.full_metadata_record);
                        return < div key={createdByIndex}>
                            {full_metadata_record ?
                                <div className="padding5 internal_url">
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        <span className="info_value">{resource_name} ({version})</span>
                                    </Link>
                                </div> :
                                <span className="info_value">{resource_name} ({version})</span>
                            }                            
                       </div>
                    })
                }
                </div>
                 }
                 {support_type && <div className="padding15">
                     <Typography className="bold-p--id">{support_type.field_label[metadataLanguage] || support_type.field_label["en"]}</Typography>
                      <span className="info_value"> {support_type.label[metadataLanguage] || support_type.label[Object.keys(support_type.label)[0]]}</span>
                    </div>
                }

            <Revision revision={revision} />
        </div>
    }
}