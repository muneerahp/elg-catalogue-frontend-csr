import React from "react";
import Typography from '@material-ui/core/Typography';

export default class Paramater extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }
        const { parameter = null } = data.described_entity.field_value.lr_subclass.field_value || [];
        const { previous_annotation_types_policy = null } = data.described_entity.field_value.lr_subclass.field_value || "";
        return <>
            {parameter && parameter.field_value.length > 0 && <Typography variant="h3" className="title-links">{parameter.field_label[metadataLanguage] || parameter.field_label["en"]}</Typography>}
            {previous_annotation_types_policy && <div>
                <span>
                    <Typography className="bold-p--id">{previous_annotation_types_policy.field_label[metadataLanguage] || previous_annotation_types_policy.field_label["en"]}</Typography>
                    <span className="info_value">{previous_annotation_types_policy.label[metadataLanguage] || previous_annotation_types_policy.label[Object.keys(previous_annotation_types_policy.label)[0]]}</span>
                </span>
            </div>
            }

            {parameter && parameter.field_value.length > 0 && parameter.field_value.map((parameterItem, parameterIndex) => {
                const parameter_name = parameterItem.parameter_name ? parameterItem.parameter_name.field_value : "";
                const parameter_name_label = parameterItem.parameter_name ? (parameterItem.parameter_name.field_label[metadataLanguage] || parameterItem.parameter_name.field_label["en"]) : "Parameter name";

                const parameter_label = parameterItem.parameter_label ? (parameterItem.parameter_label.field_value[metadataLanguage] || parameterItem.parameter_label.field_value[Object.keys(parameterItem.parameter_label.field_value)[0]]) : "";
                const parameter_name_label_label = parameterItem.parameter_label ? (parameterItem.parameter_label.field_label[metadataLanguage] || parameterItem.parameter_label.field_label["en"]) : "Parameter label";

                const parameter_description = parameterItem.parameter_description ? (parameterItem.parameter_description.field_value[metadataLanguage] || parameterItem.parameter_description.field_value[Object.keys(parameterItem.parameter_description.field_value)[0]]) : "";
                const parameter_description_label = parameterItem.parameter_description ? parameterItem.parameter_description.field_label[metadataLanguage] || parameterItem.parameter_description.field_label["en"] : "Parameter description";
                const parameter_type = parameterItem.parameter_type ? (parameterItem.parameter_type.label[metadataLanguage] || parameterItem.parameter_type.label[Object.keys(parameterItem.parameter_type.label)[0]]) : "";
                const parameter_type_label = parameterItem.parameter_type ? parameterItem.parameter_type.field_label[metadataLanguage] || parameterItem.parameter_type.field_label["en"] : "Parameter type";
                const optional = parameterItem.optional ? parameterItem.optional.field_value : "";
                const optional_label = parameterItem.optional ? parameterItem.optional.field_label[metadataLanguage] || parameterItem.optional.field_label["en"] : "Optional";
                const multi_value = parameterItem.multi_value ? parameterItem.multi_value.field_value : " ";
                const multi_value_label = parameterItem.multi_value ? parameterItem.multi_value.field_label[metadataLanguage] || parameterItem.multi_value.field_label["en"] : "Multi-value";
                const default_value = parameterItem.default_value ? parameterItem.default_value.field_value : "";
                const default_value_label = parameterItem.default_value ? parameterItem.default_value.field_label[metadataLanguage] || parameterItem.default_value.field_label["en"] : "Default value";
                const data_formatArray = parameterItem.data_format ? parameterItem.data_format.field_value.map(item => item.label ? (item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]]) : item.value) : [];
                const data_format_label = parameterItem.data_format ? parameterItem.data_format.field_label[metadataLanguage] || parameterItem.data_format.field_label["en"] : "Data format";
                const enumeration_value_label = parameterItem.enumeration_value ? parameterItem.enumeration_value.field_label[metadataLanguage] || parameterItem.enumeration_value.field_label["en"] : "Enumeration value";
                const enumerationArray = parameterItem.enumeration_value ? parameterItem.enumeration_value.field_value.map((enumeration, enumerationIndex) => {
                    const value_name_label = enumeration.value_name ? enumeration.value_name.field_label[metadataLanguage] || enumeration.value_name.field_label["en"] : "Value name";
                    const value_name = enumeration.value_name ? enumeration.value_name.field_value : "";
                    const value_label_label = enumeration.value_label ? enumeration.value_label.field_label[metadataLanguage] || enumeration.value_label.field_label["en"] : "Value name";
                    const value_label = enumeration.value_label ? enumeration.value_label.field_value[metadataLanguage] || enumeration.value_label.field_value["en"] : "";
                    const value_description_label = enumeration.value_description ? enumeration.value_description.field_label[metadataLanguage] || enumeration.value_description.field_label["en"] : "Value name";
                    const value_description = enumeration.value_description ? enumeration.value_description.field_value[metadataLanguage] || enumeration.value_description.field_value["en"] : "";
                    return <div key={enumerationIndex} className="padding5">
                        <Typography className="bold-p--id">{value_name_label}</Typography> <span className="info_value">{value_name}</span>
                        <Typography className="bold-p--id">{value_label_label}</Typography> <span className="info_value">{value_label}</span>
                        <Typography className="bold-p--id">{value_description_label}</Typography> <span className="info_value">{value_description}</span>
                    </div>
                }) : [];

                return <div key={parameterIndex}>
                    {parameter_name && <div className="padding5"><Typography className="bold-p--id">{parameter_name_label}</Typography> <span className="info_value">{parameter_name}</span></div>}
                    {parameter_label && <div className="padding5"><Typography className="bold-p--id">{parameter_name_label_label}</Typography> <span className="info_value">{parameter_label}</span></div>}
                    {parameter_description && <div className="padding5"><Typography className="bold-p--id">{parameter_description_label}</Typography> <span className="info_value">{parameter_description}</span></div>}
                    {parameter_type && <div className="padding5"><Typography className="bold-p--id">{parameter_type_label}</Typography> <span className="info_value">{parameter_type}</span></div>}
                    {optional !== undefined && <div className="padding5"><Typography className="bold-p--id">{optional_label}</Typography> <span className="info_value">{`${optional}`}</span></div>}
                    {multi_value !== undefined && <div className="padding5"><Typography className="bold-p--id">{multi_value_label}</Typography> <span className="info_value">{`${multi_value}`}</span></div>}
                    {default_value && <div className="padding5"><Typography className="bold-p--id">{default_value_label}</Typography> <span className="info_value">{default_value}</span></div>}
                    {data_formatArray && data_formatArray.length > 0 && <div className="padding5"><Typography className="bold-p--id"> {data_format_label}</Typography> <span className="info_value">{data_formatArray.map(format => `${format} `)}</span></div>}
                    {enumerationArray && enumerationArray.length > 0 && <div className="padding5"><Typography className="bold-p--id"> {enumeration_value_label}</Typography> <span className="info_value">{enumerationArray.map((enumItem, enumIndex) => <span key={enumIndex}>{enumItem}</span>)}</span></div>}

                </div>
            })
            }



        </ >
    }
}