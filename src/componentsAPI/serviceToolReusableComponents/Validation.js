import React from "react";
import Typography from '@material-ui/core/Typography';
import DocumentIdentifier from "../CommonComponents/DocumentIdentifier";
import ResourceActor from "../CommonComponents/ResourceActor";
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";
import { ReactComponent as DocIcon } from "./../../assets/elg-icons/document.svg";

export default class Validation extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }
        const validated = data.described_entity.field_value.validated ? data.described_entity.field_value.validated.field_value : null;
        const validated_color_scheme = validated === true ? "true" : "false"; 
        return <div className="pt1">
            {data.described_entity.field_value.validation && data.described_entity.field_value.validation.field_value.length > 0 && <Typography variant="h3" className="title-links">{data.described_entity.field_value.validation.field_label[metadataLanguage] || data.described_entity.field_value.validation.field_label["en"]}</Typography>}
            {(validated !== undefined && validated !== null) && <div className="padding5"><div><span className="info_value">{data.described_entity.field_value.validated.field_label[metadataLanguage] || data.described_entity.field_value.validated.field_label["en"]}: </span> <span className={"general-flag ml-05 "+ validated_color_scheme }>{`${validated}`}</span></div></div>}

            {
                data.described_entity.field_value.validation && data.described_entity.field_value.validation.field_value.map((validationItem, validationIndex) => {
                    let validation_type = validationItem.validation_type ? (validationItem.validation_type.label[metadataLanguage] || validationItem.validation_type.label[Object.keys(validationItem.validation_type.label)[0]]) : "";
                    let validation_type_label = validationItem.validation_type ? (validationItem.validation_type.field_label[metadataLanguage] || validationItem.validation_type.field_label["en"]) : "Validation type";

                    let validation_mode = validationItem.validation_mode ? (validationItem.validation_mode.label[metadataLanguage] || validationItem.validation_mode.label[Object.keys(validationItem.validation_mode.label)[0]]) : "";
                    let validation_mode_label = validationItem.validation_mode ? (validationItem.validation_mode.field_label[metadataLanguage] || validationItem.validation_mode.field_label["en"]) : "Validation mode";

                    let validation_details = validationItem.validation_details ? (validationItem.validation_details.field_value[metadataLanguage] || validationItem.validation_details.field_value[Object.keys(validationItem.validation_details.field_value)[0]]) : "";
                    let validation_details_label = validationItem.validation_details ? (validationItem.validation_details.field_label[metadataLanguage] || validationItem.validation_details.field_label["en"]) : "Validation details";

                    let validation_extent = validationItem.validation_extent ? (validationItem.validation_extent.label[metadataLanguage] || validationItem.validation_extent.label[Object.keys(validationItem.validation_extent.label)[0]]) : "";
                    let validation_extent_label = validationItem.validation_extent ? (validationItem.validation_extent.field_label[metadataLanguage] || validationItem.validation_extent.field_label["en"]) : "Validation extent";

                    let is_validated_byArray = validationItem.is_validated_by ? validationItem.is_validated_by.field_value.map((isValidatedByItem, isValidatedByIndex) => {
                        let isValidatedByItem_resource_name = isValidatedByItem.resource_name ? (isValidatedByItem.resource_name.field_value[metadataLanguage] || isValidatedByItem.resource_name.field_value[Object.keys(isValidatedByItem.resource_name.field_value)[0]]) : "";
                        let isValidatedByItem_version = isValidatedByItem.version ? isValidatedByItem.version.field_value : "";
                        let full_metadata_record = commonParser.getFullMetadata(isValidatedByItem.full_metadata_record);
                        //return `${isValidatedByItem_resource_name} , ${isValidatedByItem_version}`;
                        return { resource_name: isValidatedByItem_resource_name, version: isValidatedByItem_version, full_metadata_record: full_metadata_record }
                    }) : [];
                    let is_validated_by_label = validationItem.is_validated_by ? (validationItem.is_validated_by.field_label[metadataLanguage] || validationItem.is_validated_by.field_label["en"]) : "Is validated by";

                    let validation_reportArray = validationItem.validation_report ? validationItem.validation_report.field_value.map((validationReportItem, validationReportIndex) => {
                        //let validationReport_title = (validationReportItem.title.field_value[metadataLanguage] || validationReportItem.title.field_value[Object.keys(validationReportItem.title.field_value)[0]]) || "";
                        //let bibliographic_record_label = validationReportItem.bibliographic_record ? validationReportItem.bibliographic_record.field_label[metadataLanguage] || validationReportItem.bibliographic_record.field_label["en"] : "";
                        let bibliographic_record = validationReportItem.bibliographic_record ? validationReportItem.bibliographic_record.field_value : "";
                        //let document_identifierArray = validationReportItem.document_identifier ? validationReportItem.document_identifier.field_value.map((document_identifierItem, document_identifierIndex) => {
                        //    if (document_identifierItem.document_identifier_scheme.label["en"] !== "ELG") {
                        //        return <a key={document_identifierIndex} href={document_identifierItem.document_identifier_scheme.field_value} target="blank">{validationReport_title}</a>
                        //    }
                        //    return void 0;
                        //}) : [];
                        return <div key={validationReportIndex} className="padding15 flex-centered">
                            <span><DocIcon className="mr-05" /></span>
                            <span>
                                <DocumentIdentifier data={validationReportItem} identifier_name={"document_identifier"} identifier_scheme={"document_identifier_scheme"} metadataLanguage={metadataLanguage} />
                                {bibliographic_record && <>
                                    <pre className="cite"><code>{bibliographic_record}</code></pre></>}</span>
                        </div>
                    }) : [];
                    let validation_report_label = validationItem.validation_report ? (validationItem.validation_report.field_label[metadataLanguage] || validationItem.validation_report.field_label["en"]) : "Validation report";

                    let validatorArray = validationItem.validator || [];
                    return <div key={validationIndex} className="bottom-border mt-1">
                        {validation_type && <div className="padding5"><Typography className="bold-p--id">{validation_type_label}</Typography><span className="info_value">{validation_type}</span></div>}
                        {validation_mode && <div className="padding5"><Typography className="bold-p--id">{validation_mode_label}</Typography><span className="info_value">{validation_mode}</span></div>}

                        {validation_details && <div className="padding5"><Typography className="bold-p--id">{validation_details_label}</Typography><span className="info_value">{validation_details}</span></div>}
                        {validation_extent && <div className="padding5"><Typography className="bold-p--id">{validation_extent_label}</Typography><span className="info_value">{validation_extent}</span></div>}
                        {/*is_validated_byArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_validated_by_label}</Typography><div>{is_validated_byArray.map((item, index) => <div className="info_value" key={index}>{item}</div>)}</div></div>*/}
                        {is_validated_byArray.length > 0 && <div className="padding5">
                            <Typography className="bold-p--id">{is_validated_by_label}</Typography>
                            <span className="info_value">{is_validated_byArray.map((is_validated, is_validatedIndex) =>
                                <div className="info_value" key={is_validatedIndex}>
                                    {is_validated.full_metadata_record ?
                                        (<div className="padding5 internal_url">
                                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                                            <Link to={is_validated.full_metadata_record.internalELGUrl}>
                                                {is_validated.resource_name}  ({is_validated.version})
                                            </Link>
                                        </div>)
                                        :
                                        (<div>{is_validated.resource_name} ({is_validated.version})</div>)
                                    }
                                </div>)}
                            </span>
                        </div>
                        }
                        {validation_reportArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{validation_report_label}</Typography><span className="info_value">{validation_reportArray.map(item => item)}</span></div>}
                        {<div><ResourceActor data={validatorArray} metadataLanguage={metadataLanguage} /> </div>}
                    </div>



                })
            }

        </div>
    }
}