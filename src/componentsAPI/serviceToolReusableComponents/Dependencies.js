import React from "react";
import Typography from '@material-ui/core/Typography';
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";
export default class Dependencies extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }
        const { typesystem = "", annotation_schema = "" } = data.described_entity.field_value.lr_subclass.field_value || "";
        const { annotation_resource = [], ml_model = [] } = data.described_entity.field_value.lr_subclass.field_value || [];
        const showComponent = typesystem || annotation_schema || (annotation_resource && annotation_resource.field_value && annotation_resource.field_value.length > 0) || (ml_model && ml_model.field_value && ml_model.field_value.length > 0);
        if (!showComponent) {
            return <div></div>
        }
        return <>
            <Typography variant="h3" className="title-links">Dependencies</Typography>
            {
                typesystem && <div className="padding15">
                    <Typography className="bold-p--id">{typesystem.field_label[metadataLanguage] || typesystem.field_label["en"]} </Typography>
                    {typesystem.field_value.full_metadata_record ?
                        <div className="padding5 internal_url">
                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                            <Link to={commonParser.getFullMetadata(typesystem.field_value.full_metadata_record).internalELGUrl}>
                                {typesystem.field_value.resource_name && (typesystem.field_value.resource_name.field_value[metadataLanguage] || typesystem.field_value.resource_name.field_value[Object.keys(typesystem.field_value.resource_name.field_value)[0]])}
                                {typesystem.field_value.version && (<span> , {typesystem.field_value.version.field_value}</span>)}
                            </Link>
                        </div>
                        :
                        <span className="info_value">
                            {typesystem.field_value.resource_name && (typesystem.field_value.resource_name.field_value[metadataLanguage] || typesystem.field_value.resource_name.field_value[Object.keys(typesystem.field_value.resource_name.field_value)[0]])}
                            {typesystem.field_value.version && (<span> , {typesystem.field_value.version.field_value}</span>)}
                        </span>
                    }
                </div>
            }
            {annotation_schema && <div className="padding15">
                <Typography className="bold-p--id">{annotation_schema.field_label[metadataLanguage] || annotation_schema.field_label["en"]}</Typography>
                {annotation_schema.field_value.full_metadata_record ?
                    <div className="padding5 internal_url">
                        <span><NavIcon className="xsmall-icon mr-05" /></span>
                        <Link to={commonParser.getFullMetadata(annotation_schema.field_value.full_metadata_record).internalELGUrl}>
                            {annotation_schema.field_value.resource_name && (annotation_schema.field_value.resource_name.field_value[metadataLanguage] || annotation_schema.field_value.resource_name.field_value[Object.keys(annotation_schema.field_value.resource_name.field_value)[0]])}
                            {annotation_schema.field_value.version && (<span> , {annotation_schema.field_value.version.field_value}</span>)}
                        </Link>
                    </div>
                    :
                    <span className="info_value">
                        {annotation_schema.field_value.resource_name && (annotation_schema.field_value.resource_name.field_value[metadataLanguage] || annotation_schema.field_value.resource_name.field_value[Object.keys(annotation_schema.field_value.resource_name.field_value)[0]])}
                        {annotation_schema.field_value.version && (<span> , {annotation_schema.field_value.version.field_value}</span>)}
                    </span>
                }
            </div>
            }

            {annotation_resource && annotation_resource.field_value.length > 0 && <div className="padding15">
                <Typography className="bold-p--id">{annotation_resource.field_label[metadataLanguage] || annotation_resource.field_label["en"]}</Typography>

                {annotation_resource.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    let version = item.version ? item.version.field_value : "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            (<span className="padding5 internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    {resource_name} - {version}
                                </Link>
                            </span>)
                            :
                            (<span className="info_value">{resource_name} - {version}</span>)
                        }
                    </div>
                })
                }

            </div>
            }
            {ml_model && ml_model.field_value.length > 0 && <div className="padding15">
                <Typography className="bold-p--id">{ml_model.field_label[metadataLanguage] || ml_model.field_label["en"]}</Typography>

                {ml_model.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    let version = item.version ? item.version.field_value : "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            (<span className="padding5 internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    {resource_name} - {version}
                                </Link>
                            </span>)
                            :
                            (<span className="info_value">{resource_name} - {version}</span>)
                        }
                    </div>
                })
                }

            </div>
            }
        </>
    }
}