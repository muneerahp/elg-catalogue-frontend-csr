
import React from 'react';
import { withRouter } from "react-router-dom";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Helmet } from "react-helmet";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { BATCH_UPLOAD_XML_METADATARECORD_ENDPOINT, getAuthorizationHeader } from "../../config/constants";
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
import { toast } from "react-toastify";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import messages from "../../config/messages";

registerPlugin(FilePondPluginImagePreview, FilePondPluginFileValidateType);

class BatchXmlUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activeStep: 0, xmlUploadError: null, under_construction: false, functional_service: false }
  }

  handleReset = () => {
    this.setState({ activeStep: 0, xmlUploadError: null, under_construction: false, functional_service: false });
  }

  handle_batcch_xml_error = (errorResponse) => {
    let obj = {};
    try {
      obj = JSON.parse(errorResponse);
    } catch (err) {
      obj.message = "Internal server error";
    }
    const format = JSON.stringify(obj, null, 2);
    this.setState({ xmlUploadError: format });
    toast.error("Upload failed.", { autoClose: 3500 });
  }

  batch_handleXMLResponse = (response) => {
    const jsonResponse = JSON.parse(response);
    const errorMessages = [];
    for (let i = 0; i < jsonResponse.length; i++) {
      const recordName = jsonResponse[i].record;
      const isSuccessfull = jsonResponse[i].metadata.pk;
      if (!isSuccessfull) {
        const obj = { "file": recordName, errors: jsonResponse[i].metadata };
        errorMessages.push(obj);
      }
    }
    if (errorMessages.length === 0) {
      this.setState({ activeStep: 1, xmlUploadError: null, under_construction: false, functional_service: false });
      toast.success("The zip file with the xml files has been sucessfully uploaded.", { autoClose: 3500 });
    } else {
      const format = JSON.stringify(errorMessages, null, 2);
      this.setState({ xmlUploadError: format, activeStep: 1 });
      toast.error(`Found ${errorMessages.length}/${jsonResponse.length} files with errors`, { autoClose: 3500 });
    }
  }

  ShowErrorMessage(error) {
    return <div>
      {error && <div style={{ marginBottom: "100px", paddingBottom: "100px" }}>
        <h3>Error</h3>
        <div className=" boxed">
          <Paper elevation={13} >
            <code>
              <pre id="special">
                {error}
              </pre>
            </code>
          </Paper>
        </div>
      </div>
      }
    </div>
  }


  render() {
    return (
      <>
        <Helmet>
          <title>ELG - {this.props.helmet_value} </title>
        </Helmet>

        <div>

          <div className="centered-text mb2"><Typography variant="subtitle1">You can upload one <span className="bold-titles">zip</span> file each time (with multiple XML files).</Typography></div>

          <Grid container direction="row" justifyContent="center" alignItems="center" spacing={2}>

            <Grid item container xs={9} className="mb2" spacing={1}>

              <Grid item xs={4}>
                <FormControlLabel className="upload-checkboxes p-05 wd-100"
                  control={<Checkbox
                    checked={this.state.under_construction === true ? true : false}
                    color="primary"
                    inputProps={{ 'aria-label': 'under construction checkbox' }}
                    onChange={e => { this.setState({ under_construction: e.target.checked }); }}
                  />}
                  label={messages.label_under_construction}
                />
              </Grid>

              <Grid item xs={8}>
                <Typography variant="subtitle1">{messages.editor_under_construction}</Typography>
              </Grid>

              <Grid item xs={4}>
                <FormControlLabel className="upload-checkboxes p-05 wd-100"
                  control={<Checkbox
                    checked={this.state.functional_service === true ? true : false}
                    color="primary"
                    inputProps={{ 'aria-label': 'functional service checkbox' }}
                    onChange={e => { this.setState({ functional_service: e.target.checked }); }}
                  />}
                  label={messages.label_functional_service} 
                />
              </Grid>

              <Grid item xs={8}>
                <Typography variant="subtitle1">If the metadata records are for services to be integrated in ELG <span><a href={messages.dialog_functional_service_infoLink_message} target="_blank" rel="noopener noreferrer">({messages.dialog_functional_service_infoLink_message})</a></span>, please check the box "ELG-compatible service". </Typography>
              </Grid>

            </Grid>

          </Grid>

          {
            this.state.activeStep === 0 && <FilePond
              credits={false}
              allowMultiple={false}
              name="file"
              instantUpload={false}
              labelIdle={'Drag & Drop your file or <span class="filepond--label-action"> Browse </span>'}
              server={
                {
                  url: BATCH_UPLOAD_XML_METADATARECORD_ENDPOINT,
                  process: {
                    method: 'POST',
                    //timeout:18000
                    headers: {
                      'Authorization': getAuthorizationHeader(this.props.keycloak),
                      'UNDER-CONSTRUCTION': this.state.under_construction === true ? true : false,
                      'FUNCTIONAL-SERVICE': this.state.functional_service === true ? true : false
                    },
                    remove: null,
                    revert: null,
                    fetch: null,
                    restore: null,
                    load: null,
                    onload: (response) => this.batch_handleXMLResponse(response),
                    onerror: (response) => this.handle_batcch_xml_error(response),
                  },
                }
              }
            />
          }

          <div className="upload-container">
            {
              this.state.activeStep === 1 &&
              <div className="padding15">
                <Typography className="padding15"> The upload of your file has been completed and it will now be processed by the system. When the processing is completed, you will receive an email informing you of their successful import or of any validation errors. Following actions are described in the email.</Typography>
                <Button classes={{ root: 'inner-link-outlined--teal' }} onClick={this.handleReset}>New upload </Button>
              </div>
            }
          </div>
          {this.state.xmlUploadError && this.ShowErrorMessage(this.state.xmlUploadError)}
        </div>

      </>
    );
  }
}

export default withRouter(BatchXmlUpload);