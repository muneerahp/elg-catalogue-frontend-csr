import React from "react";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
//import Tooltip from '@material-ui/core/Tooltip';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
//import { ReactComponent as BackIcon } from "./../assets/elg-icons/navigation-arrows-left-1.svg";
import { TabContent, TabPane, } from "reactstrap";
import ProgressBar from "./CommonComponents/ProgressBar";
import GoToCatalogue from "./CommonComponents/GoToCatalogue";
import Container from '@material-ui/core/Container';
//import OpenAPI from "./OpenAPI";
import ProjectImage from "./../assets/images/project.svg";
import ToolServiceImage from "./../assets/elg-icons/settings-user.svg";
import HelmetMetaData from "./CommonComponents/HelmetMetaData";
import serviceToolParser from "../parsers/ServiceToolParser";
import commonParser from "../parsers/CommonParser";
import FunctionBox from "./serviceToolReusableComponents/FunctionBox";
import Keywords from "./CommonComponents/Keywords";
import ExportMetadata from "./CommonComponents/ExportMetadata";
import ShareMetadata from "./CommonComponents/ShareMetadata";
//import SelectMetadataLanguage from "./serviceToolReusableComponents/SelectMetadataLanguage"; 
//import Replaces from "./CommonComponents/Replaces";
//import IsReplacedWith from "./CommonComponents/IsReplacedWith";
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import Relations from "./CommonComponents/Relations";
import ResourceActor from "./CommonComponents/ResourceActor";
import FundedBy from "./serviceToolReusableComponents/FundedBy";
import DescriptionRichText from "./CommonComponents/DescriptionRichText";
import AdditionalInfo from "./CommonComponents/AdditionalInfo";
import NavigationTabs from "./serviceToolReusableComponents/NavigationTabs";
import ResourceHeader from "./CommonComponents/ResourceHeader";
import ResourceLifeCyrcleInfo from "./CommonComponents/ResourceLifeCyrcleInfo";
import Documentations from "./serviceToolReusableComponents/Documentations";
import Creation from "./serviceToolReusableComponents/Creation";
import ActualUse from "./serviceToolReusableComponents/ActualUse";
import Validation from "./serviceToolReusableComponents/Validation";
import Evaluation from "./serviceToolReusableComponents/Evaluation";
import TechnicalInfo from "./serviceToolReusableComponents/TechnicalInfo";
import Distributions from "./serviceToolReusableComponents/Distributions";
import { SERVER_API_METADATARECORD, SERVICE_CODE_SAMPLE, SERVICE_CODE_SAMPLE_TEXT_DESCRIPTION, SERVICE_CODE_SAMPLE_AUDIO, SERVER_API_METADATARECORD_LABELESS_SCHEMA, RETRIEVE_RECORD_INFO, BASE_URL, getLogoutUrl } from "../config/constants";
import { RESOURCE_ACCESS } from "../config/editorConstants";
import Versions from "./CommonComponents/Versions";
import RecordStats from "./CommonComponents/RecordStats";

export default class ServiceTool extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '', metadataLanguage: '', tab: '1', tryitOut: "", codeSamples: "", pk: "",
            loading: true, number_of_failed_loads: 0,
            isOwner: false, isLegalValidator: false, isMetadataValidator: false, isTechnicalValidator: false, source: null,
            force_update_id: '1'
        };
    }

    componentDidMount() {
        this.getMetadataResourceDetail();
        window.addEventListener('message', this.respondToGuiMessage, false);
        this.setState({ number_of_failed_loads: 0 });
    }

    componentWillUnmount() {
        window.removeEventListener('message', this.respondToGuiMessage, false);
        if (this.state.source) {
            this.state.source.cancel("");
        }
        clearTimeout(this.timeOutId);
    }

    updateRecord = () => {
        this.setState({ "force_update_id": Math.random() })
        this.getMetadataResourceDetail();
    }

    displayTabGeneralFunction = (flag) => {
        this.setState({ displayTabGeneral: flag });
    }

    retrieveRecordInfo = (id, source) => {
        if (this.props.keycloak && this.props.keycloak.authenticated) {
            axios.get(RETRIEVE_RECORD_INFO(id), { cancelToken: source.token })
                .then(res => {
                    const { curator = false, legal_validator = false, metadata_validator = false, technical_validator = false } = res.data;
                    this.setState({ isOwner: curator, isLegalValidator: legal_validator, isMetadataValidator: metadata_validator, isTechnicalValidator: technical_validator });
                })
                .catch(err => console.log(err));
        }
    }

    getMetadataResourceDetail = () => {
        const id = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const url = SERVER_API_METADATARECORD(id);
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        this.retrieveRecordInfo(id, source);
        axios.get(url, { cancelToken: source.token })
            .then(res => {
                this.setState(res);
                const lang = Object.keys(res.data.described_entity.field_value.resource_name.field_value).includes("en") ? "en" : Object.keys(res.data.described_entity.field_value.resource_name.field_value)[0];
                this.setState({ metadataLanguage: lang, pk: id, source: null, loading: false, number_of_failed_loads: 0 });
            }).catch(err => {
                console.log(err);
                this.setState({ source: null, loading: false });
            });
    }

    componentDidUpdate(prevProps, prevState) {
        const thisId = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const prevId = prevProps && prevProps.match && prevProps.match.params && prevProps.match.params.id;
        if (thisId !== prevId) {
            this.setState({ data: '', metadataLanguage: '', tab: '1', tryitOut: "", codeSamples: "" });
            this.getMetadataResourceDetail();
        }
    }

    selectLanguage = (event) => {
        this.setState({ metadataLanguage: event.target.value });
        console.log("select language: ", event.target.value);
    }

    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
    }

    getCodeSapmlesText = (resourceName, elg_execution_location, media_type, elg_execution_location_sync) => {
        let response = "";
        let Token = null;
        const { keycloak } = this.props;
        if (keycloak && keycloak.authenticated) {
            let keyclockToken = keycloak.idTokenParsed ? keycloak.idTokenParsed.jti : "";
            Token = keyclockToken ? "$TOKEN" : Token;
        }
        if (media_type === "audio") {
            response = `${response}${SERVICE_CODE_SAMPLE_AUDIO(Token, elg_execution_location_sync)}`;
        } else {
            response = `${response}${SERVICE_CODE_SAMPLE(Token, elg_execution_location_sync)}`;
        }
        return response;
    }

    getKeycloakToken = () => {
        const { keycloak } = this.props;
        if (keycloak && keycloak.authenticated) {
            let keyclockToken = keycloak.token || "";
            return keyclockToken;
        }
        return null;
    }

    respondToGuiMessage = (event) => {
        if (event.origin !== window.location.origin) {
            //console.log("incorrect event origin. terminate....");
            return;
        }
        // only send the iframe config message when the iframe asks for it, not if the message came from elsewhere
        var iframe = document.getElementById('dgk') && document.getElementById('dgk').contentWindow;
        if (!iframe || event.source !== iframe) {
            return;
        }
        const record_pk = this.state.pk;
        let keyclockToken = this.getKeycloakToken() || "";
        const record_url = SERVER_API_METADATARECORD_LABELESS_SCHEMA(record_pk);
        const elg_execution_location = serviceToolParser.getElgExecutionLocation(this.state.data);
        let elg_gui_url = serviceToolParser.getElgGuiUrl(this.state.data);
        let data = { ServiceUrl: `${elg_execution_location}`, StyleCss: " ", Authorization: `Bearer ${keyclockToken}`, ApiRecordUrl: record_url };
        iframe.postMessage(JSON.stringify(data), `${elg_gui_url}`);
    }

    reloadPage = () => {
        const number_of_failed_loads = this.state.number_of_failed_loads;
        if (number_of_failed_loads < 3) {
            this.setState({ number_of_failed_loads: number_of_failed_loads + 1 });
            this.getMetadataResourceDetail();
            return <ProgressBar number_of_failed_loads={this.state.number_of_failed_loads} />;
        } else {
            this.timeOutId = setTimeout(() => { window.location = getLogoutUrl() }, 5000)
            return <ProgressBar number_of_failed_loads={this.state.number_of_failed_loads} />;
        }
    }

    render() {
        if (this.props.keycloak && !this.props.keycloak.authenticated) {
            if (commonParser.redirectUnregisteredUsersToKeycloak(this.props.location)) {
                this.props.keycloak.login();
            }
        }

        if (this.state.loading) {
            return <ProgressBar />
        } else {
            if (this.state.data) {
            } else {
                return this.reloadPage();
            }
        }

        const { data, metadataLanguage } = this.state;
        const resourceName = serviceToolParser.getResourceName(data, metadataLanguage) ? serviceToolParser.getResourceName(data, metadataLanguage).value : "";
        const version = serviceToolParser.getVersion(data, metadataLanguage) ? serviceToolParser.getVersion(data, metadataLanguage).value : null;
        const version_label = serviceToolParser.getVersion(data, metadataLanguage) ? serviceToolParser.getVersion(data, metadataLanguage).label : "";
        const version_date = serviceToolParser.getVersionDate(data, metadataLanguage) ? serviceToolParser.getVersionDate(data, metadataLanguage).value : null;
        //const version_date_label = serviceToolParser.getVersionDate(data, metadataLanguage) ? serviceToolParser.getVersionDate(data, metadataLanguage).label : "";
        const description = serviceToolParser.getDescription(data, metadataLanguage) ? serviceToolParser.getDescription(data, metadataLanguage).value : null;
        //const description_label = serviceToolParser.getDescription(data, metadataLanguage) ? serviceToolParser.getDescription(data, metadataLanguage).label : "";
        const languages = [];//? where to get them from
        const keywords = serviceToolParser.getKeywords(data, metadataLanguage) ? serviceToolParser.getKeywords(data, metadataLanguage).value : [];
        const keywords_label = serviceToolParser.getKeywords(data, metadataLanguage) ? serviceToolParser.getKeywords(data, metadataLanguage).label : "";
        const domainKeywords = serviceToolParser.getDomainKeywords(data, metadataLanguage) ? serviceToolParser.getDomainKeywords(data, metadataLanguage).value : [];
        const domainKeywords_label = serviceToolParser.getDomainKeywords(data, metadataLanguage) ? serviceToolParser.getDomainKeywords(data, metadataLanguage).label : "";
        const subjectKeywords = serviceToolParser.getSubjectKeywords(data, metadataLanguage) ? serviceToolParser.getSubjectKeywords(data, metadataLanguage).value : [];
        const subjectKeywords_label = serviceToolParser.getSubjectKeywords(data, metadataLanguage) ? serviceToolParser.getSubjectKeywords(data, metadataLanguage).label : "";
        const intentedKeywords = serviceToolParser.getIntendedKeywords(data, metadataLanguage) ? serviceToolParser.getIntendedKeywords(data, metadataLanguage).value : [];
        const intentedKeywords_label = serviceToolParser.getIntendedKeywords(data, metadataLanguage) ? serviceToolParser.getIntendedKeywords(data, metadataLanguage).label : "";
        const lr_type = serviceToolParser.getLRType(data, metadataLanguage) ? serviceToolParser.getLRType(data, metadataLanguage).value : null;
        const citationText = serviceToolParser.getCitationText(data, metadataLanguage) ? serviceToolParser.getCitationText(data, metadataLanguage).value : "";
        const citationText_label = serviceToolParser.getCitationText(data, metadataLanguage) ? serviceToolParser.getCitationText(data, metadataLanguage).label : "Cite as";
        const citation_all_versions = serviceToolParser.getCitationAllVersions(data, metadataLanguage) ? serviceToolParser.getCitationAllVersions(data, metadataLanguage).value : "";
        const citation_all_versions_label = serviceToolParser.getCitationAllVersions(data, metadataLanguage) ? serviceToolParser.getCitationAllVersions(data, metadataLanguage).label : "Cite as"; 
        const mailingList = serviceToolParser.getMailingList(data, metadataLanguage) ? serviceToolParser.getMailingList(data, metadataLanguage).value : [];
        //const mailingList_label = serviceToolParser.getMailingList(data, metadataLanguage) ? serviceToolParser.getMailingList(data, metadataLanguage).label : "";
        const discussionUrl = serviceToolParser.getDiscussionUrl(data, metadataLanguage) ? serviceToolParser.getDiscussionUrl(data, metadataLanguage).value : [];
        //const discussionUrl_label = serviceToolParser.getDiscussionUrl(data,metadataLanguage)?serviceToolParser.getDiscussionUrl(data,metadataLanguage).label:"";
        const serviceLogo = serviceToolParser.getLogo(data);
        const additioanlInfo = serviceToolParser.getAdditionalInfo(data, metadataLanguage) ? serviceToolParser.getAdditionalInfo(data, metadataLanguage).value : [];
        const additioanlInfo_label = serviceToolParser.getAdditionalInfo(data, metadataLanguage) ? serviceToolParser.getAdditionalInfo(data, metadataLanguage).label : "";
        const resourceShortName = serviceToolParser.getResourceShortName(data, metadataLanguage);
        const elg_execution_location = serviceToolParser.getElgExecutionLocation(data)
        const elg_execution_location_sync = serviceToolParser.getElgExecutionLocationSync(data);
        let elg_gui_url = serviceToolParser.getElgGuiUrl(data);
        elg_gui_url = (elg_gui_url && elg_gui_url.endsWith("none")) ? null : elg_gui_url;
        const serviceId = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const media_type = serviceToolParser.getMediaType(data);
        const tool_type = serviceToolParser.getServiceInfoType(data);
        const keycloakToken = this.getKeycloakToken();
        const { under_construction = false } = data.management_object || {};
        const { proxied = false } = data.management_object || {};
        const { for_information_only = false } = data.management_object || {};
        const { is_active_version = false } = data.management_object || {};
        const { is_latest_version = false } = data.management_object || {};
        const all_versions = data.described_entity.field_value.all_versions ? data.described_entity.field_value.all_versions : null;
        const all_versions_label = data.described_entity.field_value.all_versions ? (data.described_entity.field_value.all_versions.field_label[metadataLanguage] || data.described_entity.field_value.all_versions.field_label["en"]) : "";

        const tabTitles = ["Overview"];
        if (!under_construction) {
            tabTitles.push("Download/Run");
        }
        if (elg_gui_url && tool_type !== RESOURCE_ACCESS) {
            tabTitles.push("Try out");
        }
        if (elg_execution_location && tool_type !== RESOURCE_ACCESS) {
            tabTitles.push("Code samples");
        }
        if (this.state.displayTabGeneral) {
            tabTitles.push("Related LRTs");
        }
        const second_column = data.described_entity.field_value.actual_use || data.described_entity.field_value.validated || false;

        return (
            <div >
                <HelmetMetaData data={data}
                    resourceName={resourceName}
                    description={description}
                    keywords={keywords}
                    domainKeywords={domainKeywords}
                    subjectKeywords={subjectKeywords}
                    intentedKeywords={intentedKeywords}
                    corpus_subclass={null}
                    languages={languages}
                    resourceShortName={resourceShortName}
                    for_information_only={for_information_only}
                    ltAreaKeywordsArray={null}
                    disciplines={null}
                    servicesOffered={null}
                    OrganizationRolesArray={null}
                    ld_subclass={null}
                    lcr_subclass={null}
                    type="tool-service"
                    pk={this.state.pk}
                />

                <div className="search-top">
                    <GoToCatalogue />
                </div>
                <Container maxWidth="xl">
                    <div className="metadata-main-card-container">
                        <ResourceLifeCyrcleInfo key={data.management_object.status} status={data.management_object.status} keycloak={this.props.keycloak} />
                        {is_latest_version === false && data.management_object.status === "p" && <section>
                            <div className="resource-progress-bar-status-message"><span className="resource-progress-bar-caption">{"There is a "}<span className="bold">{"newer version "}</span>{"of this record available"}</span></div>
                        </section>}
                        <ResourceHeader
                            key={data.management_object ? 'header' + JSON.stringify(data.management_object) + (data.service_info ? 'service_info' + JSON.stringify(data.service_info) : 'service_info') + this.state.force_update_id : 'header' + this.state.pk}
                            {...this.state}
                            logo={serviceLogo}
                            title={resourceName}
                            short_name={resourceShortName}
                            version={version}
                            version_label={version_label}
                            version_Date={version_date}
                            resource_type={lr_type}
                            proxied={proxied}
                            entity_type={null} shortnameArray={null} default_image={ToolServiceImage}
                            under_construction={under_construction}
                            for_information_only={for_information_only}
                            is_active_version={is_active_version}
                            status={data.management_object.status}
                            keycloak={this.props.keycloak} data={data} pk={this.state.pk} updateRecord={this.updateRecord} 
                            citationText={citationText} citationText_label={citationText_label} citation_all_versions={citation_all_versions}  citation_all_versions_label={citation_all_versions_label}/>

                        <div className="tab-pane-container">
                            <NavigationTabs toggleTab={this.toggleTab} activeTab={this.state.tab} tabTitles={tabTitles} />
                            <TabContent activeTab={this.state.tab}>
                                <TabPane tabId={`${tabTitles.indexOf('Overview') + 1}`}>
                                    {/*<span style={{ float: "right", marginBottom: "0px", marginTop: "0px", paddingBottom: "0px", paddingTop: "0px" }}>
                                        <SelectMetadataLanguage selectLanguage={this.selectLanguage} data={this.state.data.described_entity.description} /></span>*/}
                                    <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
                                        <Grid item xs={12} sm={8} md={8} className="search-results-main" >
                                            <DescriptionRichText description={description} />
                                            <Keywords keywordsArray={keywords}
                                                languagesArray={languages}
                                                domainKeywordsArray={domainKeywords}
                                                subjectKeywordsArray={subjectKeywords}
                                                intentedKeywordsArray={intentedKeywords}
                                                KeywordsLabel={keywords_label}
                                                DomainKeywordsLabel={domainKeywords_label}
                                                subjectKeywordsLabel={subjectKeywords_label}
                                                intentedKeywordsLabel={intentedKeywords_label}
                                            />

                                            <FunctionBox state={this.state} />

                                        </Grid>
                                        <Grid item xs={12} sm={4} className="MetadataSidebar" >
                                            <ExportMetadata pk={this.state.pk} under_construction={under_construction} for_information_only={for_information_only} keycloak={this.props.keycloak} data={this.state.data} {...this.state} />
                                            {data.management_object.status === "p" && <ShareMetadata shareUrl={`${BASE_URL}catalogue/tool-service/${this.state.pk}`} title={`ELG - ${resourceName}`} description={description} logo={serviceLogo} short_name={resourceShortName} />}
                                            <RecordStats pk={this.state.pk} />
                                            <Versions all_versions={all_versions} all_versions_label={all_versions_label} metadataLanguage={metadataLanguage} />
                                            <div className="ActionsButtonArea">
                                                {/*<Replaces data={data} metadataLanguage={metadataLanguage} />*/}
                                                {/*<IsReplacedWith data={data} metadataLanguage={metadataLanguage} />*/}                                                 
                                                <ResourceActor data={this.state.data.described_entity.field_value.resource_provider} metadataLanguage={metadataLanguage} />
                                                <FundedBy data={this.state.data} metadataLanguage={metadataLanguage} ProjectImage={ProjectImage} navigateToProjectDetail={this.navigateToProjectDetail} />
                                                <AdditionalInfo additioanlInfo={additioanlInfo} mailingList={mailingList}
                                                    discussionUrl={discussionUrl} data={this.state.data} metadataLanguage={metadataLanguage} AdditionalInfoLabel={additioanlInfo_label}
                                                    identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"}
                                                    {...commonParser.getSourceOfMetadataRecord(data, metadataLanguage)} />
                                                {/*<Contact data={this.state.data} metadataLanguage={metadataLanguage} />
                                                    <MailingList mailingListArray={mailingList} />
                                                    <DiscussionUrls discussionUrl={discussionUrl} /> */}


                                            </div>
                                        </Grid>
                                    </Grid>

                                    <Grid container spacing={4} className="MetaDataDetailsBottomContainer">
                                        <Grid item xs={12} sm={second_column ? 4 : 6} >
                                            <Documentations data={data} metadataLanguage={metadataLanguage} />
                                            <Creation data={data} metadataLanguage={metadataLanguage} />
                                            <ResourceActor data={this.state.data.described_entity.field_value.ipr_holder} metadataLanguage={metadataLanguage} />


                                        </Grid>
                                        {second_column && <Grid item xs={12} sm={4} >
                                            <ActualUse data={this.state.data} metadataLanguage={metadataLanguage} />
                                            <Validation data={this.state.data} metadataLanguage={metadataLanguage} />
                                        </Grid>}

                                        <Grid item xs={12} sm={second_column ? 4 : 6} >
                                            <TechnicalInfo data={this.state.data} metadataLanguage={metadataLanguage} />
                                            <Evaluation data={this.state.data} metadataLanguage={metadataLanguage} />
                                        </Grid>

                                    </Grid>
                                </TabPane>

                                {!under_construction && <TabPane tabId={`${tabTitles.indexOf('Download/Run') + 1}`}>
                                    <Distributions data={this.state.data} metadataLanguage={metadataLanguage} {...this.state} />
                                </TabPane>}

                                {elg_gui_url && tool_type !== RESOURCE_ACCESS && < TabPane tabId={`${tabTitles.indexOf('Try out') + 1}`}>
                                    <Grid container className="upperSpace" spacing={2}>
                                        <Grid item xs={12}>
                                            {!keycloakToken && <h3>Please log in to test this service.</h3>}
                                            {keycloakToken && <iframe id="dgk" title="iframe" frameBorder="0" scrolling="auto" className="ng-tns-c14-7 ng-star-inserted" src={`${elg_gui_url}`} style={{ height: "500px", width: "100%" }}></iframe>}
                                        </Grid>
                                    </Grid>
                                </TabPane>
                                }

                                {elg_execution_location && tool_type !== RESOURCE_ACCESS && <TabPane tabId={`${tabTitles.indexOf('Code samples') + 1}`}>
                                    <Grid container className="upperSpace" spacing={2} style={{ marginBottom: "150px" }}>
                                        <Accordion className="facets__accordion CodeSamplesAccordion" defaultExpanded={false}>
                                            <AccordionSummary
                                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                            >
                                                <h4>cURL</h4>
                                            </AccordionSummary>
                                            <AccordionDetails>
                                                <div className="Info">
                                                    {SERVICE_CODE_SAMPLE_TEXT_DESCRIPTION(resourceName)}
                                                </div>
                                                <div>
                                                    {<pre className="codeSamples"><code>{this.getCodeSapmlesText(resourceName, elg_execution_location, media_type, elg_execution_location_sync)} </code></pre>}
                                                </div>
                                                <div>
                                                    {keycloakToken && <div style={{ wordBreak: "break-word" }}>
                                                        <h3>Token: </h3>
                                                        <div className="Info">
                                                            <code style={{ color: "var(--color-highlight--teal)" }}>
                                                                {keycloakToken}
                                                            </code>
                                                        </div>
                                                    </div>}
                                                    {!keycloakToken && <div className="Info"><p><strong>Please log in to acquire a Token.</strong></p></div>}
                                                </div>
                                            </AccordionDetails>
                                        </Accordion>
                                        <Accordion className="facets__accordion CodeSamplesAccordion" defaultExpanded={false}>
                                            <AccordionSummary
                                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                            >
                                                <h4>Python</h4>
                                            </AccordionSummary>
                                            <AccordionDetails>
                                                <div className="Info">
                                                    {"If you want to call the service using Python, you can use the following code snippet:"}
                                                </div>
                                                <div>
                                                    {<pre className="codeSamples"><code>from elg import Service<br></br>service = Service.from_id({serviceId})<br></br>result = service(request_input="{(media_type === "audio") ? "ELG request object, or path to a file" : "input string, ELG request object, or path to a file"}", request_type="{media_type}")</code></pre>}
                                                </div>
                                                <div className="Info">
                                                    {"The ELG Python SDK can be installed using pip:"}
                                                </div>
                                                <div>
                                                    {<pre className="codeSamples"><code>pip install elg</code></pre>}
                                                </div>
                                                <div className="Info">
                                                    {<p>The full documentation of the Python SDK is available in <a href="https://european-language-grid.readthedocs.io/en/stable/all/A1_PythonSDK/GettingStarted.html" target="_blank" rel="noreferrer">our documentation</a> <OpenInNewIcon style={{ width: 10, height: 10 }} /></p>}
                                                </div>
                                            </AccordionDetails>
                                        </Accordion>
                                    </Grid>
                                </TabPane>
                                }

                                <TabPane tabId={`${tabTitles.indexOf('Related LRTs') + 1}`}>
                                    <Relations data={this.state.data} metadataLanguage={metadataLanguage} displayTabGeneralFunction={this.displayTabGeneralFunction} displayTabGeneral={this.state.displayTabGeneral} />
                                </TabPane>


                                {/* <TabPane tabId="4">{<OpenAPI />}</TabPane>*/}
                            </TabContent>
                        </div>
                    </div>
                </Container >
            </div >
        )

    }
}
