import React from "react";
import { ReactComponent as LaunchIcon } from "./../../assets/elg-icons/network-arrow.svg";

export default class DiscussionUrls extends React.Component {
    render() {
        const { discussionUrl } = this.props || [];
        return <div>
            {discussionUrl.length > 0 &&
                <div className="padding5">                     
                    {discussionUrl.map((discussionItem, discussionIndex) => {
                        return <div className="padding5" key={discussionIndex}><a  href={discussionItem} target="_blank" rel="noopener noreferrer" className="info_url" >
                        <span><LaunchIcon className="xsmall-icon"/> </span> <span>Discussion url</span> 
                        </a></div>
                    
                    })
                    }
                </div>
            }
        </div>
    }
}