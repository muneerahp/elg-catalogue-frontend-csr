import React from "react";
import Typography from '@material-ui/core/Typography';
export default class Description extends React.Component {
    constructor() {       
        super();
        this.state = { expanded: false };
        this.togglexpanded = this.togglexpanded.bind(this); 
    }

    togglexpanded() {
        this.setState({ expanded: !this.state.expanded })
    }
    
    render() {
        const { description } = this.props;
        const { expanded } = this.state;
        if (!description) {
            return <div></div>
        }
        return <div>
            {expanded ? (
              <div className="padding15">
             <Typography variant="body2" className="search-results__description">
                {description} <span className="ExpandButton" onClick={this.togglexpanded} > Read Less </span>
              </Typography>
              </div>
               ) : (
                <div className="padding15">
                <Typography variant="body2" className="search-results__description">
                {description.substring(0, 520)}{description.length > 520 ? <span className="ExpandButton" onClick={this.togglexpanded} > ... Read More </span> : void 0} </Typography>
                </div>
               )
          }
        </div>
    }
}