import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
//import ResourceEntityType from './ResourceEntityType';
import ResourceUnderConstruction from "./ResourceUnderConstruction";
import ResourceActions from "./actions/ResourceActions";
import Claim from "./Claim";
import HostedAtElg from "./HostedAtElg";
import ResourceForInfo from "./ResourceForInfo";
import ResourceActive from "./ResourceActive";
import CitationText from "./CitationText";

//entity_type patterns for displaying default images
//if "entity_type": "LanguageResource" then 
// "lr_type": "Corpus",
// "lr_type": "ToolService",
//  "lr_type": "LexicalConceptualResource",
//if  "entity_type": "Project" then 
// "lr_type":null
//if  "entity_type": "Organization" then
// "lr_type":null


class ResourceHeader extends React.Component {
    render() {
        const { logo, title, short_name, version, version_Date, resource_type, entity_type, shortnameArray, default_image, version_label, under_construction, for_information_only, proxied, is_active_version, keycloak, status,
            citationText, citationText_label, citation_all_versions, citation_all_versions_label } = this.props;

        return (
            <React.Fragment>
                <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start" spacing={3} className="pt1">
                    <Grid item xs={1} >
                        <div className="project-logo">
                            {/*<img width="60%" src={logo} alt="corpus logo"></img>*/}
                            <img width="80%" src={logo ? logo : default_image} ref={img => this.img = img} onError={() => this.img.src = default_image} alt="resource logo" />
                        </div>
                    </Grid>

                    <Grid item xs={7}>
                        <Typography variant="h2" id="resource-name"> {title} </Typography>
                        <Typography className="acronym"> {short_name} </Typography>

                        {version && <Typography>
                            <span className="subheading">{version_label}: </span>
                            <span className="subheading">{version}</span>
                            {version_Date && <span className="subheading">{` (${Intl.DateTimeFormat("en-GB").format(new Date(version_Date))})`}</span>}
                        </Typography>}

                        {resource_type && <HostedAtElg proxied={proxied} functional_service={this.props.data.management_object.functional_service} size={this.props.data.management_object.size} resource_type={resource_type} justifyContent={"center"} alignItems={"flex-start"} xSize={12} />}
                        {for_information_only && <ResourceForInfo for_information_only={for_information_only} xSize={12} justifyContent={"center"} alignItems={"flex-start"} />}
                        {<ResourceActive is_active_version={is_active_version} status={status} functional_service={this.props.data.management_object.functional_service} resource_type={resource_type} xSize={12} justifyContent={"center"} alignItems={"flex-start"} />}
                        
                        
                        {shortnameArray && shortnameArray.length > 0 &&
                            shortnameArray.map((keyword, index) =>
                                <Typography key={index} className="acronym"> {keyword} </Typography>
                            )}
                    </Grid>


                    <Grid item xs={4} >
                        <Claim keycloak={keycloak} data={this.props.data} pk={this.props.pk} />
                        <Grid item container xs={12} direction="row" justifyContent="space-between">
                            <Grid item xs={12} >
                                <CitationText citationText={citationText} citationText_label={citationText_label} citation_all_versions={citation_all_versions} citation_all_versions_label={citation_all_versions_label} />
                            </Grid>
                            <Grid item xs={12} >
                                <ResourceUnderConstruction under_construction={under_construction} xSize={12} justifyContent={"center"} alignItems={"flex-end"} />
                            </Grid>
                        </Grid>
                        <ResourceActions {...this.props} resource_type={resource_type} entity_type={entity_type} keycloak={keycloak} landing_page={true} data={this.props.data} pk={this.props.pk} updateRecord={this.props.updateRecord} />
                    </Grid>

                </Grid>

            </React.Fragment>




        );

    }
}

export default ResourceHeader;