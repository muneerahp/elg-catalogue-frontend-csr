import React from "react";
import axios from "axios";
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import { EXPORT_XML_METADATARECORD_ENDPOINT, IS_ADMIN, IS_CONTENT_MANAGER } from "../../config/constants";
//import CircularProgress from '@material-ui/core/CircularProgress'; 

export default class ExportMetadata extends React.Component {
    constructor(props) {
        super(props);
        this.state = { pendingRequest: false, isCurator: false, source: null };
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    handleAction = (pk) => {
        this.setState({ pendingRequest: true });
        axios.get(EXPORT_XML_METADATARECORD_ENDPOINT(pk), { responseType: 'blob', timeout: 30000, })
            .then(res => {
                if (res && res.data) {
                    this.setState({ pendingRequest: false });
                    const url = window.URL.createObjectURL(new Blob([res.data]));
                    var [, filename] = res.headers['content-disposition'].split('filename=');
                    if (!filename) {
                        try {
                            const content_disp = res.headers['content-disposition'];
                            if (content_disp.indexOf("b?") > 0 && content_disp.indexOf("?=") >= 0) {
                                var s = content_disp.indexOf("b?");
                                var e = content_disp.lastIndexOf("?=");
                                const payload = content_disp.substring(s + 2, e);
                                const decoded = Buffer.from(payload, "base64").toString("utf-8");
                                filename = decoded.substring(decoded.indexOf("filename=") + "filename=".length);
                            } else {
                                filename = this.props.data.pk || "undefined";
                                filename = filename + ".xml";
                            }
                        } catch (err) {
                            filename = this.props.data.pk || "undefined";
                            filename = filename + ".xml";
                        }
                    }
                    const link = document.createElement('a');
                    link.href = url;
                    link.setAttribute('download', filename);
                    document.body.appendChild(link);
                    link.click();
                    link.remove();
                }
            })
            .catch(err => { this.setState({ pendingRequest: false }); });
    }

    allow_export = (pk, under_construction, keycloak, data, status) => {
        if (IS_ADMIN(keycloak) || IS_CONTENT_MANAGER(keycloak)) {
            return true;
        }
        if (this.props.for_information_only === true) {
            return false;
        }
        if (!under_construction && status === "p") {
            return true;
        }
        if (this.props.isOwner) {
            return true;
        }
        return false;
    }


    render() {
        const { pk, under_construction, keycloak, data } = this.props;
        const { status } = data.management_object;
        if (!this.allow_export(pk, under_construction, keycloak, data, status)) {
            return <></>
        }
        return <div className="ActionsButtonArea" style={{ marginBottom: '1em' }}>
            <Typography variant="h3" className="title-links">Export</Typography>
            <Link disabled={this.state.pendingRequest} component="button" variant="body2" className="link-style-button" onClick={() => { this.handleAction(pk); }}> XML </Link>
        </div>
    }
}