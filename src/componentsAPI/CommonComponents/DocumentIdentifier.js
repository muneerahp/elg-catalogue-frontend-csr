import React from "react";


export default class DocumentIdentifier extends React.Component {
    render() {
        const { data, identifier_name, identifier_scheme, metadataLanguage } = this.props;

        //const filteredIdentifiers = data[identifier_name] && data[identifier_name].field_value.filter(item => item && item[identifier_scheme] && item[identifier_scheme].label && item[identifier_scheme].label["en"] !== "ELG");
        //if (filteredIdentifiers && !filteredIdentifiers.length) {
        //    return <div></div>
        //}

        if (!data) {
            return <></>
        }

        const title = (data.title.field_value[metadataLanguage] || data.title.field_value[Object.keys(data.title.field_value)[0]]) || "";

        return <>

            {data[identifier_name] && data[identifier_name].field_value.map((IdentifierItem, index) => {
                let identifier = IdentifierItem[identifier_scheme].label["en"] !== "ELG" ? IdentifierItem[identifier_scheme].label["en"] : "";//filter out elg identifiers 
                let value = IdentifierItem.value.field_value || "";

                return <div key={index}>
                    {identifier && value.includes('http') && <div>
                        <a href={value} target="_blank" rel="noopener noreferrer" className="info_url"> {title} </a> [{identifier}]</div>}
                    {identifier && !value.includes('http') && <div className="info_value">{title}. {value} [{identifier}]  </div>}
                    {!identifier && <div className="info_value">{title}</div>}
                </div>
            })
            }
        </>
    }
}




