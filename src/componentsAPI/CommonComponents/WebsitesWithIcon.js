import React from "react";
//import LaunchIcon from '@material-ui/icons/Launch';
import { ReactComponent as LaunchIcon } from "./../../assets/elg-icons/network-arrow.svg";

export default class WebsitesWithIcon extends React.Component {


    render() {
        const { websiteArray } = this.props;


        if (!websiteArray) {
            return <span className="padding5">{" "} </span>
        }
        return <>
            {websiteArray && websiteArray.map((site, siteIndex) =>
                <div key={siteIndex}><a key={siteIndex} href={site} target="_blank" rel="noopener noreferrer" className="info_url" >
                    {<div><span><LaunchIcon className="xsmall-icon" /> </span> <span>Website</span></div>}
                </a></div>

            )} </>
    }
}