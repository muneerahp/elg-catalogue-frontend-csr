import React from "react";
import BlurLinearIcon from '@material-ui/icons/BlurLinear';
import BlurCircularIcon from '@material-ui/icons/BlurCircular';
import BlurOnIcon from '@material-ui/icons/BlurOn';
//import Typography from '@material-ui/core/Typography';
import { ReactComponent as OrganizationIcon } from "./../../assets/elg-icons/buildings-modern.svg";
import { ReactComponent as ProjectIcon } from "./../../assets/elg-icons/human-resources-team-settings.svg";
import { ReactComponent as LCRIcon } from "./../../assets/elg-icons/archive-folder.svg";
import { ReactComponent as ToolServiceIcon } from "./../../assets/elg-icons/settings-user.svg";
import { ReactComponent as ResourceIcon } from "./../../assets/elg-icons/database.svg"; /*.svg";*/
import Tooltip from '@material-ui/core/Tooltip';

import { TOOL_SERVICE, CORPUS, LCR, LD, PROJECT, ORGANIZATION } from "../../config/constants";

class ResourceEntityTypeIconOnly extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: '' };
  }
  typeofresource = (type, align) => {
    switch (type) {

      case TOOL_SERVICE: return (
         <Tooltip title={TOOL_SERVICE} aria-label={TOOL_SERVICE}><ToolServiceIcon  className="small-icon ToolIcon"/></Tooltip> 
         
      );

      case CORPUS: return (
        <Tooltip title={CORPUS} aria-label={CORPUS}><ResourceIcon  className="small-icon ResourceIcon"/></Tooltip> 
        
      );

      case LCR: return (
        
          <Tooltip title={LCR} aria-label={LCR}><LCRIcon  className="small-icon LCRIcon"/></Tooltip>
          
        );

      case LD: return (
        
           <Tooltip title={LD} aria-label={LD}><BlurLinearIcon className=" small-icon LDIcon"/></Tooltip>
         
        );

      case "ToolService": return (
        <Tooltip title={TOOL_SERVICE} aria-label={TOOL_SERVICE}><ToolServiceIcon  className="small-icon ToolIcon"/> </Tooltip>
       
      );

      case "LexicalConceptualResource": return (
        
          <Tooltip title="LexicalConceptualResource" aria-label="LexicalConceptualResource"><LCRIcon className="small-icon LCRIcon"/></Tooltip>
          
        );

      case "LanguageDescription": return (
        
          <Tooltip title="LanguageDescription" aria-label="LanguageDescription"><BlurLinearIcon  className=" small-icon LDIcon"/></Tooltip>
          
        );

      case "Uncategorized Language Description": return (
        
          <Tooltip title="LanguageDescription" aria-label="LanguageDescription"><BlurLinearIcon  className=" small-icon LDIcon"/></Tooltip>
          
        );        
      case "Model": return (
        
          <Tooltip title="Model" aria-label="Model"><BlurCircularIcon  className=" small-icon LDIcon"/></Tooltip>
          
        );
      case "Grammar": return (
        
          <Tooltip title="Grammar" aria-label="Grammar"><BlurOnIcon  className=" small-icon LDIcon"/></Tooltip>
          
        );
      case "N-gram Model": return (
        
          <Tooltip title="LanguageDescription" aria-label="N-gram Model"><BlurLinearIcon  className=" small-icon LDIcon"/></Tooltip>
          
        ); 
      default: return ""
    }
  }

  typeofentity = (type, align) => {
    switch (type) {
      case PROJECT: return (
        
          <Tooltip title={PROJECT} aria-label={PROJECT}><ProjectIcon className="small-icon ProjectIcon" /></Tooltip>
           

        );
      case ORGANIZATION: return (
        
          <Tooltip title={ORGANIZATION} aria-label={ORGANIZATION}><OrganizationIcon className="small-icon OrganizationIcon" /></Tooltip>
           
        );


      default: return ""
    }
  }

  render() {
    const { resource_type, entity_type, align } = this.props;



    return (
      <React.Fragment>
        {

          !resource_type ?
            (this.typeofentity(entity_type, align)
            ) : (this.typeofresource(resource_type, align))
        }


      </React.Fragment>
    );
  }

}

export default ResourceEntityTypeIconOnly;