import React from "react";
import Typography from '@material-ui/core/Typography';
//import TwitterIcon from '@material-ui/icons/Twitter';
//import FacebookIcon from '@material-ui/icons/Facebook';
//import Grid from '@material-ui/core/Grid';
import Linkify from 'react-linkify';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import Tooltip from '@material-ui/core/Tooltip';

export default class CitationText extends React.Component {
    constructor(props) {
        super(props);

        this.state = { copySuccess: '' }
    }

    copyToClipboard = (e) => {
        this.textArea.select();
        this.textArea.setSelectionRange(0, 99999); /* For mobile devices */
        document.execCommand('copy');
        this.setState({ copySuccess: 'Copied!' });
    };

    render() {
        const { citationText, citation_all_versions } = this.props;
        //const {citationText_label,citation_all_versions_label }= this.props;
        if (!citationText) {
            return <div></div>
        }
        return <>
            {/*<Typography variant="h4" className="title-links">Share</Typography>
            <Grid container spacing={1} direction="row"   justify="flex-start" alignItems="flex-start" className="grey-background bordered share"> 
                    <Grid item sm={2}> <a href="" target="_blank" rel="noopener noreferrer"><FacebookIcon className="general-icon blue" />  </a> </Grid>
                    <Grid item sm={2}><a href="" target="_blank" rel="noopener noreferrer"><TwitterIcon className="general-icon blue" /> </a> </Grid>
                
    </Grid> */}

            {citationText &&
                <div className="padding15">
                    <Typography className="bold-p--id">Cite current version</Typography>
                    {/*<Typography className="bold-p--id">{citationText_label}</Typography>*/}
                    <Linkify>{citationText} </Linkify>
                    <span style={{ opacity: "0", width: "1px" }}>
                        <span><textarea readOnly style={{ opacity: "0", width: "1px", height: "1px" }}
                            ref={(textarea) => this.textArea = textarea}
                            defaultValue={citationText}
                        /></span>
                    </span>
                    <span><Tooltip title={this.state.copySuccess || "Copy"}><button onClick={this.copyToClipboard} className="inner-actions-link-no--border--teal"> <FileCopyIcon  className="small-icon"  /> </button></Tooltip> </span>




                </div>
            }

            {citation_all_versions &&
                <div className="padding15">
                    <Typography className="bold-p--id">Cite all versions</Typography>
                    {/*<Typography className="bold-p--id">{citation_all_versions_label}</Typography>*/}
                    <Linkify>{citation_all_versions} </Linkify>
                    <span style={{ opacity: "0", width: "1px" }}>
                        <span><textarea readOnly style={{ opacity: "0", width: "1px", height: "1px" }}
                            ref={(textarea) => this.textArea = textarea}
                            defaultValue={citation_all_versions}
                        /></span>
                    </span>
                    <span><Tooltip title={this.state.copySuccess || "Use this to cite all versions; the PID resolves to the latest version of the resource."}><button onClick={this.copyToClipboard}> <FileCopyIcon fontSize="small" className="blue" /> </button></Tooltip> </span>




                </div>
            }

        </>
    }
}