import React from "react";
import Typography from '@material-ui/core/Typography';

export default class MailingList extends React.Component {
    render() {
        const { mailingListArray } = this.props;
        return <div>
            {mailingListArray.length > 0 &&
                <div className="padding15">
                    <Typography className="bold-p--id">Mailing List </Typography>
                    {mailingListArray.map((mailingListItem, mailingListItemIndex) => {
                        return <span key={mailingListItemIndex}>{` ${mailingListItem}`}</span>
                    })}
                </div>
            }
        </div>
    }
}