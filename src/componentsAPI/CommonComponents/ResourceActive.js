import React from "react";
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';
import { TOOL_SERVICE } from "../../config/constants";

export default class ResourceActive extends React.Component {
    constructor(props) {
        super(props);
        this.isUnmount = false;
    }
  

    render() {
        const { is_active_version, functional_service, resource_type, xSize, jystify, alignItems, status } = this.props;
  
        return (
            <Grid container direction="column" justifyContent={jystify} alignItems={alignItems}>
                <Grid item xs={xSize}>                  
                    {/*(resource_type === "ToolService" || resource_type === TOOL_SERVICE) && functional_service === true && is_active_version === true && <div className="pt-1"><Tooltip title="active version"><span className="caption grey--font ui grey right ribbon label">active version</span></Tooltip></div>*/}
                    {(resource_type === "ToolService" || resource_type === TOOL_SERVICE) && (functional_service === true && is_active_version === false && status ==="p") && <div className="pt-1"><Tooltip title="older version not running in ELG"><span className="caption grey--font ui pink right ribbon label">inactive version</span></Tooltip></div>}               
                </Grid>
            </Grid>
        );

    }
}