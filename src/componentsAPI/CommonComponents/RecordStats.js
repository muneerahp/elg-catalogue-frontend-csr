import React from "react";
import axios from "axios";
import CountUp from 'react-countup';
import Grid from '@material-ui/core/Grid';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { RECORD_STATS } from "../../config/constants";
//import VisibilityIcon from '@material-ui/icons/Visibility';
//import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
//import BuildIcon from '@material-ui/icons/Build';

export default class RecordStats extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = { data: null, loading: true, source: null }
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    componentDidMount() {
        this.getStats();
    }

    getStats = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        axios.get(RECORD_STATS(this.props.pk), { cancelToken: source.token })
            .then(res => {
                this.setState({ data: res.data, source: null, loading: false });
            }).catch(err => {
                this.setState({ source: null, loading: false });
            });
    }

    render() {
        if (this.state.loading || !this.state.data) {
            return <></>
        }

        const { views = 0, downloads, service_used } = this.state.data.record || {};
        const { views: versionsViews, downloads: versionsDownloads, service_used: versionsService_used } = this.state.data.versions || {};

        return <div className="ActionsButtonArea" style={{ marginBottom: '1em' }}>
            <Grid container direction="row" justifyContent="flex-start" alignItems="center" spacing={3}>
                <Grid item sm={6} xs={12} className="stats-category">
                    <span>Views</span>
                    <span><CountUp end={views} delay={1} /></span>                    
                </Grid>
                {downloads !== undefined && <Grid item sm={6} xs={12} className="stats-category">
                    <span>Downloads</span>
                    <span><CountUp end={downloads} delay={1} /></span>                      
                </Grid>}
                {service_used !== undefined && <Grid item sm={6} xs={12} className="stats-category"> 
                    <span>Used</span>
                    <span><CountUp end={service_used} delay={1} /></span>                       
                </Grid>}
            </Grid>

             
            {this.state.data.versions && <Accordion>
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                ><Typography className="bold-p--id">All versions</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    {this.state.data.versions && <Grid container direction="row" justifyContent="flex-start" alignItems="center" spacing={3}>
                        {versionsViews !== undefined && <Grid item sm={6} xs={12} className="stats-category">
                            <span>Views</span>
                            <span><CountUp end={versionsViews} delay={1} /></span>
                        </Grid>}
                        {versionsDownloads !== undefined && <Grid item sm={6} xs={12} className="stats-category">
                            <span>Download</span>
                            <span><CountUp end={versionsDownloads} delay={1} /></span>
                        </Grid>}
                        {versionsService_used !== undefined && <Grid item sm={6} xs={12} className="stats-category">
                            <span>Used</span>
                            <span><CountUp end={versionsService_used} delay={1} /></span>
                        </Grid>}
                    </Grid>}

                </AccordionDetails>
            </Accordion> }

        </div>
    }
}