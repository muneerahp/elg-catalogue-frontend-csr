import React from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
//import { ReactComponent as IngestIcon } from "./../../../assets/elg-icons/editor/cursor-move-target-up.svg";
import { IS_ADMIN, IS_CONTENT_MANAGER } from "../../../config/constants";
import { DEACTIVATE_VERSION } from "../../../config/editorConstants";
import MenuItem from '@material-ui/core/MenuItem';
import { toast } from "react-toastify";
import messages from "../../../config/messages";
import CancelIcon from '@material-ui/icons/Cancel';
//import { getPath } from "./EditAction";

class DeactivateVersionAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [], isAuthorized: false, disable: false };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    checkIfUserIsOwner = () => {
        const { keycloak } = this.props;
        const { status = null, is_active_version = null, is_latest_version = null, functional_service = false } = this.props.data.management_object || {};
        if ((this.props.isOwner || IS_ADMIN(keycloak) || IS_CONTENT_MANAGER(keycloak)) && status === "p" && functional_service && is_active_version && !is_latest_version) {
            this.setState({ isAuthorized: true }, this.show_deactivate_version_button);
        } else {
            this.setState({ isAuthorized: false }, this.show_deactivate_version_button);
        }
    }

    send_deactivate_version_request = () => {
        this.setState({ disable: true });
        axios.patch(DEACTIVATE_VERSION(this.props.pk)).then((res) => {
            const { updated = false, error = "", inactive_version = "" } = res?.data?.filter(item => item)[0];
            if (updated) {
                toast.success(`${messages.editor_action_deactivate_vesrion_primary} completed succesfully`);
            } else {
                toast.error(`${messages.editor_action_deactivate_vesrion_primary} failed. ${error || inactive_version}`);
                this.setState({ disable: false });
            }
            this.props.updateRecord();
        }).catch((err) => {
            toast.error(`${messages.editor_action_deactivate_vesrion_primary} failed.`)
            //console.log(err);
            this.setState({ disable: false });
        });
        return "";
    }

    handleAction = (event, action, actionIndex) => {
        this.send_deactivate_version_request();
        this.props.handleClose();
    }

    show_deactivate_version_button = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        if (!this.props.showDeactivateVersionButton) {
            this.props.setVisibilityOfDeactivateVersionButton();
        }
    }

    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showDeactivateVersionButton) {
            return <></>
        }
        const actions = [{ primary: `${messages.editor_action_deactivate_vesrion_primary}`, secondary: `${messages.editor_action_deactivate_version_secondary}` }];
        return <div>
            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem>
                            <ListItem button disabled={this.state.disable} onClick={() => this.handleAction(null, action)}>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <CancelIcon className="ActionsIcon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div>
    }

}
export default withRouter(DeactivateVersionAction);