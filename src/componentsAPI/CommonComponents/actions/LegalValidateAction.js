import React from "react";
import axios from "axios";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import BallotIcon from '@material-ui/icons/Ballot';
import MenuItem from '@material-ui/core/MenuItem';
import messages from "../../../config/messages";
import { /*IS_LEGAL_VALIDATOR, IS_SUPERVISOR,*/ IS_ADMIN } from "./../../../config/constants"
import { EDITOR_LEGAL_VALIDATE } from "../../../config/editorConstants";
import { toast } from "react-toastify";
import ValidateRecord from "../../../DashboardComponents/single_actions/ValidateRecord";
import { PROJECT, ORGANIZATION } from "../../../config/constants";

export default class LegalValidateAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [], isAuthorized: false, disable: false };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    checkIfUserIsOwner = () => {
        const { keycloak } = this.props;
        const { status = null } = this.props.data.management_object || {};
        const legally_valid = this.props.data.management_object.legally_valid;
        //do not show legal validation for project & organizations. 
        //Legal validation is only available for elg-compatible & resources with data. 
        //The backend will put as a defaul value true if there is no need for legal validation.
        /*if ([PROJECT, ORGANIZATION].includes(this.getLanguageResource())) {
            this.setState({ isAuthorized: false }, this.showLegalValidateButton);
            return;
        }*/
        if (this.props.isLegalValidator && ((status === "g") && (!legally_valid))) {
            this.setState({ isAuthorized: true }, this.showLegalValidateButton);
        } else if ((IS_ADMIN(keycloak)) && ((status === "g") && (!legally_valid))) {
            this.setState({ isAuthorized: true }, this.showLegalValidateButton);
        } else {
            this.setState({ isAuthorized: false }, this.showLegalValidateButton);
        }
    }

    handleAction = (event, action, actionIndex) => {
        this.setState({ showCreateValidationDialog: true });
    }

    hideValidateRecord = () => {
        this.setState({ showCreateValidationDialog: false });
        this.props.updateRecord();
    }

    getLanguageResource = () => {
        const { data } = this.props;
        const entity_type = data.described_entity.field_value.entity_type.field_value;
        if (entity_type === "LanguageResource") {
            const lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value;
            return lr_type;
        } else {
            return entity_type;
        }
    }

    getResourceName = () => {
        const resourceType = this.getLanguageResource();
        if (resourceType === PROJECT) {
            return this.props.data.described_entity.field_value.project_name.field_value["en"];
        } else if (resourceType === ORGANIZATION) {
            return this.props.data.described_entity.field_value.organization_name.field_value["en"];
        } else {
            return this.props.data.described_entity.field_value.resource_name.field_value["en"];
        }
    }

    getValidatorNotes = () => {
        return this.props.data.management_object.validator_notes;
    }

    showLegalValidateButton = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        if (this.props.landing_page === false || this.props.landing_page === true) {
            //show action at landing page. If landing_page is undefined then we are rendering at myItems, myValidation, mySupervisors views
        } else if (this.props.tab !== undefined && this.props.tab !== 'myval') {
            return;
        }
        if (!this.props.showLegalValidateActionButton) {
            this.props.setVisibilityOfLegalValidate();
        }
    }

    validateRecord = () => {
        this.setState({ disable: true });
        axios.patch(EDITOR_LEGAL_VALIDATE(this.props.pk)).then((res) => {
            //    toast.success("Record submitted for publication successfully.", { autoClose: 3500 });
            //    this.setState({ disable: true });
            //    this.props.updateRecord();
            console.log(res)
        }).catch((err) => {
            toast.error("Legal validate action failed", { autoClose: 3500 });
            this.setState({ disable: false });
        });
        return "";
    }

    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showLegalValidateActionButton) {
            return <></>
        }

        const actions = [{ primary: `${messages.validator_action_legal}`, secondary: `${messages.validator_action_legal_secondary}` }];
        return <div>
            {
                this.state.showCreateValidationDialog && <ValidateRecord
                    resource={
                        {
                            "id": this.props.data.pk,
                            "resource_name": this.getResourceName(),//this.props.data.described_entity.field_value.resource_name.field_value["en"],
                            "resource_type": this.getLanguageResource(),
                            "validator_notes": this.getValidatorNotes(),
                        }
                    }
                    hideValidateRecord={this.hideValidateRecord}
                    type='legal' />
            }
            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem>
                            <ListItem button disabled={this.state.disable} onClick={() => this.handleAction(null, action)}>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <BallotIcon className="ActionsIcon small-icon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div>
    }
}