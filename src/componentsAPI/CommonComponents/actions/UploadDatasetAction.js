import React from "react";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import MenuItem from '@material-ui/core/MenuItem';
import messages from "../../../config/messages";
import DatasetUpload from "../../../DashboardComponents/single_actions/DatasetUpload";
//import { IS_ADMIN, IS_CONTENT_MANAGER } from "../../../config/constants";
import { IS_CONTENT_MANAGER } from "../../../config/constants";
import { keycloak } from "../../../App";

export default class UploadDatasetAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [], isAuthorized: false, showDatasetUploadDialog: false, source: null };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    checkIfUserIsOwner = () => {
        const { status = null } = this.props.data.management_object || {};
        if (this.props.isOwner && (status === "i" || status === "d") && (this.isLanguageResource())) {
            this.setState({ isAuthorized: true, source: null }, this.showUploadDataButton);
        }/* else if (IS_ADMIN(keycloak) && (this.isLanguageResource())) {
            this.setState({ isAuthorized: true, source: null }, this.showUploadDataButton);
        } */else if (IS_CONTENT_MANAGER(keycloak) && (status === "i") && (this.isLanguageResource())) {
            this.setState({ isAuthorized: true, source: null }, this.showUploadDataButton);
        } else {
            this.setState({ isAuthorized: false, source: null }, this.showUploadDataButton);
        }
    }

    isLanguageResource = () => {//we permit upload for corpora,lcr,ld
        const { data } = this.props;
        const entity_type = data.described_entity.field_value.entity_type.field_value;
        if (entity_type === "LanguageResource") {
            const lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value;
            if (lr_type === "ToolService") {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }


    handleAction = (event, action, actionIndex) => {
        this.setState({ showDatasetUploadDialog: true });
    }

    hideUploadDataset = () => {
        this.setState({ showDatasetUploadDialog: false });
        this.props.updateRecord();
    }

    showUploadDataButton = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        if (!this.props.showUploadDataActionButton) {
            this.props.setVisibilityOfUploadData();
        }
    }

    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showUploadDataActionButton) {
            return <></>
        }

        const primaryMessage = this.props.data.management_object.hasOwnProperty("size") && this.props.data.management_object.size > 0 ? "Replace content file" : messages.editor_action_upload_data_primary;
        const actions = [{ primary: `${primaryMessage}`, secondary: `${messages.editor_action_upload_data_secondary}` }];

        return <div>
            {
                this.state.showDatasetUploadDialog && <DatasetUpload
                    resource={
                        {
                            "id": this.props.data.pk,
                            "resource_name": this.props.data.described_entity.field_value.resource_name.field_value["en"],
                            "size": this.props.data.management_object.size
                        }
                    }
                    hideUploadDataset={this.hideUploadDataset} />}
            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem>
                            <ListItem button onClick={() => this.handleAction(null, action)}>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <CloudUploadIcon className="ActionsIcon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div>
    }

}