import React from "react";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import MenuItem from '@material-ui/core/MenuItem';
import messages from "../../../config/messages";
import { /*IS_METADATA_VALIDATOR, IS_SUPERVISOR, */GRAMMAR, IS_ADMIN, ML_MODEL, Uncategorized_Language_Description } from "../../../config/constants";
import ValidateRecord from "../../../DashboardComponents/single_actions/ValidateRecord";
import { PROJECT, ORGANIZATION, CORPUS, LCR, LD } from "../../../config/constants";

export default class TechnicalValidateAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [], isAuthorized: false, disable: false };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    checkIfUserIsOwner = () => {
        const { keycloak } = this.props;
        const { status = null } = this.props.data.management_object || {};
        const technically_valid = this.props.data.management_object.technically_valid;
        const is_functional_service = this.props.data.management_object.functional_service;
        const service_info = this.props.data.service_info || null;

        //-αν είναι resource με data: Κάνει ο metadata validator technical, metadata validation απο την φόρμα του technical validation// 
        //what if we have a tool/service with data?
        if ([CORPUS, LCR, LD, ML_MODEL, GRAMMAR, Uncategorized_Language_Description].includes(this.getLanguageResource()) && this.props.isMetadataValidator && ((status === "g") && (!technically_valid))) {
            this.setState({ isAuthorized: true }, this.showTechnicalValidateButton);
            return;
        } else if ([CORPUS, LCR, LD, ML_MODEL, GRAMMAR, Uncategorized_Language_Description].includes(this.getLanguageResource()) && !this.props.isMetadataValidator && ((status === "g") && (!technically_valid))) {
            this.setState({ isAuthorized: false }, this.showTechnicalValidateButton);
            return;
        }

        // If functional service && no service info, then dont show this action, because Service Registration must happen first
        if (is_functional_service && !service_info) {
            this.setState({ isAuthorized: false }, this.showTechnicalValidateButton);
        } else if (this.props.isTechnicalValidator && ((status === "g") && (!technically_valid))) {
            this.setState({ isAuthorized: true }, this.showTechnicalValidateButton);
        } else if (IS_ADMIN(keycloak) && ((status === "g") && (!technically_valid))) {
            this.setState({ isAuthorized: true }, this.showTechnicalValidateButton);
        } else {
            this.setState({ isAuthorized: false }, this.showTechnicalValidateButton);
        }
    }

    handleAction = (event, action, actionIndex) => {
        this.setState({ showCreateValidationDialog: true });
    }

    hideValidateRecord = () => {
        this.setState({ showCreateValidationDialog: false });
        this.props.updateRecord();
    }



    showTechnicalValidateButton = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        if (this.props.landing_page === false || this.props.landing_page === true) {
            //show action at landing page. If landing_page is undefined then we are rendering at myItems, myValidation, mySupervisors views
        } else if (this.props.tab !== undefined && this.props.tab !== 'myval') {
            return;
        }
        if (!this.props.showTechnicalValidateActionButton) {
            this.props.setVisibilityOfTechnicalValidate();
        }
    }

    getLanguageResource = () => {
        const { data } = this.props;
        const entity_type = data.described_entity.field_value.entity_type.field_value;
        if (entity_type === "LanguageResource") {
            const lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value;
            return lr_type;
        } else {
            return entity_type;
        }
    }

    getResourceName = () => {
        const resourceType = this.getLanguageResource();
        if (resourceType === PROJECT) {
            return this.props.data.described_entity.field_value.project_name.field_value["en"];
        } else if (resourceType === ORGANIZATION) {
            return this.props.data.described_entity.field_value.organization_name.field_value["en"];
        } else {
            return this.props.data.described_entity.field_value.resource_name.field_value["en"];
        }
    }

    getValidatorNotes = () => {
        return this.props.data.management_object.validator_notes;
    }

    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showTechnicalValidateActionButton) {
            return <></>
        }

        const actions = [{ primary: `${messages.validator_action_technical}`, secondary: `${messages.validator_action_technical_secondary}` }];
        return <div>
            {
                this.state.showCreateValidationDialog && <ValidateRecord
                    {...this.props}
                    resource={
                        {
                            "id": this.props.data.pk,
                            "resource_name": this.getResourceName(),//this.props.data.described_entity.field_value.resource_name.field_value["en"],
                            "resource_type": this.getLanguageResource(),
                            "validator_notes": this.getValidatorNotes(),
                        }
                    }
                    hideValidateRecord={this.hideValidateRecord}
                    type='technical' />
            }

            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem>
                            <ListItem button disabled={this.state.disable} onClick={() => this.handleAction(null, action)}>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <AssignmentTurnedInIcon className="ActionsIcon small-icon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div>
    }

}