import React from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import { ReactComponent as IngestIcon } from "./../../../assets/elg-icons/editor/cursor-move-target-up.svg";
//import { IS_ADMIN, IS_CONTENT_MANAGER } from "../../../config/constants";
import { EDITOR_INGEST } from "../../../config/editorConstants";
import MenuItem from '@material-ui/core/MenuItem';
import { toast } from "react-toastify";
import messages from "../../../config/messages";
import { getPath } from "./EditAction";

class IngestAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [], isAuthorized: false, disable: false };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    checkIfUserIsOwner = () => {
        //const { keycloak } = this.props;
        const { status = null } = this.props.data.management_object || {};
        if (this.props.isOwner && ((status === "i"))) {
            this.setState({ isAuthorized: true }, this.showIngestButton);
        }/* else if (IS_ADMIN(keycloak) && ((status === "i"))) {
            this.setState({ isAuthorized: true }, this.showIngestButton);
        } else if (IS_CONTENT_MANAGER(keycloak) && ((status === "i"))) {
            this.setState({ isAuthorized: true }, this.showIngestButton);
        } */else {
            this.setState({ isAuthorized: false }, this.showIngestButton);
        }

    }

    ingestRecord = () => {
        this.setState({ disable: true });
        axios.patch(EDITOR_INGEST(this.props.pk)).then((res) => {
            try {
                const { updated, error } = res.data[0];
                if (updated === true) {
                    toast.success("Record submitted for publication successfully.", { autoClose: 3500 });
                    this.setState({ disable: true });
                    this.props.updateRecord();
                } else {
                    toast.error(error, { autoClose: false });
                    this.setState({ disable: false });
                    if (error.toLowerCase().indexOf("data") >= 0) {
                        this.props.history.push(getPath(this.props));
                    } else if (error.toLowerCase().indexOf("tagged") >= 0 && error.toLowerCase().indexOf("distribution") >= 0) {
                        this.props.history.push(getPath(this.props));
                    }
                }
            } catch (err) {
                console.log("Fds");
                console.log(err);
            }
        }).catch((err) => {
            console.log(err.response.data);
            try {
                const { error } = err.response.data
                const errorMessage = this.getErrorMessage(error);
                toast.error(errorMessage, { autoClose: false });
                this.setState({ disable: false });
                if (errorMessage.toLowerCase().indexOf("data") >= 0) {
                    this.props.history.push(getPath(this.props));
                } else if (errorMessage.toLowerCase().indexOf("tagged") >= 0 && errorMessage.toLowerCase().indexOf("distribution") >= 0) {
                    this.props.history.push(getPath(this.props));
                }
            } catch (ee) {
                console.log(ee);
            }
            this.setState({ disable: false });
        });
        return "";
    }

    getErrorMessage = (error) => {
        let responseErrorMessage = '';
        if (error && typeof error === 'object' && !Array.isArray(error)) {
            const { elg_compatible_distribution = null } = error;
            responseErrorMessage = elg_compatible_distribution;
        }
        return responseErrorMessage || JSON.stringify(error);
    }

    handleAction = (event, action, actionIndex) => {
        switch (action.secondary) {
            case "Submit": //was Ingest
                return this.ingestRecord();
            default: break;
        }
        this.props.handleClose();
    }

    showIngestButton = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        if (!this.props.showIngestActionButton) {
            this.props.setVisibilityOfIngest();
        }
    }

    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showIngestActionButton) {
            return <></>
        }
        const actions = [{ primary: `${messages.editor_action_ingest_primary}`, secondary: `${messages.editor_action_ingest_secondary}` }];
        return <div>
            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem>
                            <ListItem button disabled={this.state.disable} onClick={() => this.handleAction(null, action)}>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <IngestIcon className="ActionsIcon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div>
    }

}
export default withRouter(IngestAction);