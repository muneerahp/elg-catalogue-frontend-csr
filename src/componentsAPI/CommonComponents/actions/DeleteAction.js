import React from "react";
import axios from "axios";
import { withRouter } from "react-router-dom";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { IS_ADMIN, IS_CONTENT_MANAGER } from "../../../config/constants";
import { EDITOR_BULK_DELETE } from "../../../config/editorConstants";
import MenuItem from '@material-ui/core/MenuItem';
import { toast } from "react-toastify";
import messages from "../../../config/messages";

class DeleteAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [], isAuthorized: false, disable: false };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    checkIfUserIsOwner = () => {
        const { keycloak } = this.props;
        const { status = null } = this.props.data.management_object || {};
        if (this.props.isOwner && ((status === "i") || (status === "d"))) {
            this.setState({ isAuthorized: true }, this.showDeleteButton);
        } else if (IS_ADMIN(keycloak) && (status !== "p")) {
            this.setState({ isAuthorized: true }, this.showDeleteButton);
        } else if (IS_CONTENT_MANAGER(keycloak) && (status !== "p")) {
            this.setState({ isAuthorized: true }, this.showDeleteButton);
        } else {
            this.setState({ isAuthorized: false }, this.showDeleteButton);
        }
    }

    deleteRecord = () => {
        this.setState({ disable: true });
        axios.patch(EDITOR_BULK_DELETE(this.props.pk)).then((res) => {
            toast.success("Record Deleted successfully.", { autoClose: 3500 });
            this.setState({ disable: true });
            if (this.props?.history?.location?.pathname?.includes("myitems")) {
                this.props.updateRecord();
            } else {
                this.props.history.push('/myitems');
            }
            this.props.handleClose();
        }).catch((err) => {
            toast.error("Delete action failed", { autoClose: 3500 });
            this.setState({ disable: false });
            this.props.handleClose();
        });
        return "";
    }

    handleAction = (event, action, actionIndex) => {
        return this.deleteRecord();
    }

    showDeleteButton = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        if (!this.props.showDeleteActionButton) {
            this.props.setVisibilityOfDelete();
        }
    }

    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showDeleteActionButton) {
            return <></>
        }
        const actions = [{ primary: `${messages.editor_action_landing_page_delete_primary}`, secondary: `${messages.editor_action_landing_page_delete_secondary}` }];
        return <div>
            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem>
                            <ListItem button disabled={this.state.disable} onClick={() => this.handleAction(null, action)}>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <DeleteForeverIcon className="ActionsIcon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div>
    }

}
export default withRouter(DeleteAction);