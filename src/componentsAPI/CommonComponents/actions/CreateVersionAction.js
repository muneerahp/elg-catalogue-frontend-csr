import React from "react";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import CreateNewFolderIcon from '@material-ui/icons/CreateNewFolder';
//import { TOOL_SERVICE, CORPUS, LD, LCR, PROJECT, ORGANIZATION } from "../../../config/constants";
import { PROJECT, ORGANIZATION } from "../../../config/constants";
import MenuItem from '@material-ui/core/MenuItem';
import messages from "../../../config/messages";
import CreateVersion from "../../../DashboardComponents/single_actions/CreateVersion";

export default class CreateVersionAction extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [], isAuthorized: false, showCreateVersionDialog: false, source: null };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    checkIfUserIsOwner = () => {
        const { status = null } = this.props.data.management_object || {};
        if (this.props.isOwner && (status === "p")) {
            this.setState({ isAuthorized: true, source: null }, this.showCreateVersionButton);
        } else {
            this.setState({ isAuthorized: false, source: null }, this.showCreateVersionButton);
        }
    }

    handleAction = (event, action, actionIndex) => {
        this.setState({ showCreateVersionDialog: true });
    }

    hideCreateVersion = () => {
        this.setState({ showCreateVersionDialog: false });
        this.props.updateRecord();
    }

    showCreateVersionButton = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        if (!this.props.showCreateVersionActionButton) {
            this.props.setVisibilityOfCreateVersion();
        }
    }

    getLanguageResource = () => {
        const { data } = this.props;
        const entity_type = data.described_entity.field_value.entity_type.field_value;
        if (entity_type === "LanguageResource") {
            const lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value;
            return lr_type;
        } else {
            return entity_type;
        }
    }

    getResourceName = () => {
        const resourceType = this.getLanguageResource();
        if (resourceType === PROJECT) {
            return this.props.data.described_entity.field_value.project_name.field_value["en"];
        } else if (resourceType === ORGANIZATION) {
            return this.props.data.described_entity.field_value.organization_name.field_value["en"];
        } else {
            return this.props.data.described_entity.field_value.resource_name.field_value["en"];
        }
    }

    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showCreateVersionActionButton) {
            return <></>
        }

        const actions = [{ primary: messages.editor_action_create_version_primary, secondary: messages.editor_action_create_vesrsion_secondary }];

        return <div>
            {
                this.state.showCreateVersionDialog && <CreateVersion
                    resource={
                        {
                            "id": this.props.data.pk,
                            "resource_name": this.getResourceName(),//this.props.data.described_entity.field_value.resource_name.field_value["en"],
                            "resource_type": this.getLanguageResource()
                        }
                    }
                    hideCreateVersion={this.hideCreateVersion} />
            }
            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem>
                            <ListItem button onClick={() => this.handleAction(null, action)}>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <CreateNewFolderIcon className="ActionsIcon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div>
    }

}