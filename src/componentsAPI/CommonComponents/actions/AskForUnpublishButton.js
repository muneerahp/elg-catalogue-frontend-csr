import React from "react";
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
//import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import { ReactComponent as IngestIcon } from "./../../../assets/elg-icons/editor/cursor-move-target-up.svg";
import { EDITOR_BULK_UNPUBLISH } from "../../../config/editorConstants";
import MenuItem from '@material-ui/core/MenuItem';
import { toast } from "react-toastify";
import messages from "../../../config/messages";

export default class AskForUnpublishButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            UserRoles: [], isAuthorized: false, disable: false, unpublication_requested: false,
            showDialog: false, reason: ""
        };
    }

    componentDidMount() {
        this.checkIfUserIsOwner();
    }

    checkIfUserIsOwner = () => {
        const { status = null, unpublication_requested = false } = this.props.data.management_object || {};
        if (this.props.isOwner && (status === "p") && (unpublication_requested !== true)) {
            this.setState({ isAuthorized: true }, this.showAskForUnpublishButton);
        } else {
            this.setState({ isAuthorized: false }, this.showAskForUnpublishButton);
        }
    }

    askForUnpublishRecord = () => {
        this.setState({ disable: true });
        axios.patch(EDITOR_BULK_UNPUBLISH(this.props.pk), { reason: this.state.reason }).then((res) => {
            toast.success("Request submitted successfully.", { autoClose: 3500 });
            this.setState({ disable: true, showDialog: false, reason: "" });
            this.props.updateRecord();
            this.props.handleClose();
        }).catch((err) => {
            toast.error("Request submission failed", { autoClose: 3500 });
            this.setState({ disable: false });
            //this.props.handleClose();
            this.disableDisplay();
        });
        return "";
    }

    disableDisplay = () => {
        this.setState({ showDialog: false, reason: "" });
        this.props.handleClose();
    }

    handleAction = (event, action, actionIndex) => {
        return this.askForUnpublishRecord();
    }

    showAskForUnpublishButton = () => {
        if (!this.state.isAuthorized) {
            return;
        }
        if (!this.props.showAskForUnpublishActionButton) {
            this.props.setVisibilityOfAskForUnpublish();
        }
    }

    render() {
        if (!this.state.isAuthorized) {
            return <></>
        }
        if (!this.props.showAskForUnpublishActionButton) {
            return <></>
        }
        const actions = [{ primary: `${messages.editor_action_unpublish_primary}`, secondary: `${messages.editor_action_unpublish_secondary}` }];
        return <div>
            {
                this.state.showDialog &&
                <Dialog open={this.state.showDialog} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" disableBackdropClick={true} maxWidth='sm' fullWidth>
                    <DialogTitle>{messages.editor_action_unpublish_primary}</DialogTitle>
                    <DialogContent>
                        <DialogContentText component={"div"}>
                            <TextField className="pb-3 wd-100"
                                //type="text"
                                minRows={10}
                                multiline={true}
                                required={true}
                                label={"Unpublication request reason"}
                                placeholder={"Unpublication request reason"}
                                variant="outlined"
                                helperText={
                                    <span>Please specify the reason for your unpublication request.</span>
                                }
                                value={this.state.reason}
                                onChange={(e) => { this.setState({ reason: e.target.value }) }}
                            />
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            color="primary"
                            onClick={() => this.disableDisplay()}
                            autoFocus
                        >
                            cancel
                    </Button>
                        <Button
                            color="primary"
                            disabled={!this.state.reason || this.state.disable}
                            onClick={() => this.askForUnpublishRecord()}
                        >
                            {messages.editor_action_unpublish_primary}
                        </Button>
                    </DialogActions>
                </Dialog>
            }
            {
                actions.map((action, actionIndex) => {
                    return <div key={actionIndex}>
                        <MenuItem>
                            <ListItem button disabled={this.state.disable || this.state.unpublication_requested} onClick={() => { this.setState({ showDialog: true }) }}>
                                <ListItemAvatar>
                                    <Avatar className="ActionsAvatar">
                                        <IngestIcon className="ActionsIcon" />
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText primary={action.primary} className="ActionButtonText" />
                            </ListItem>
                        </MenuItem>
                    </div>
                }
                )
            }
        </div >
    }

}