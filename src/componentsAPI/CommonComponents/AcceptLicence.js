import React from "react";
import axios from "axios";
import Button from '@material-ui/core/Button';
import GetAppIcon from '@material-ui/icons/GetApp';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
//import { OPEN_LICENCES, DOWNLOAD_RESOURCE_ENDPOINT, LICENCE_DOWNLOAD_ENDPOINT, OPEN_LICENCES_IDS } from "../../config/constants";
import { DOWNLOAD_RESOURCE_ENDPOINT, LICENCE_DOWNLOAD_ENDPOINT } from "../../config/constants";
import { ALLOWS_ACCESS_WITH_SIGNATURE, ALLOWS_DIRECT_aCCESS, REQUIRES_USER_AUTHENTICATION, ALLOWS_PROCESSING } from "../../config/constants";
//import FileDownload from "js-file-download";
import messages from "../../config/messages";

export default class AcceptLicence extends React.Component {

    constructor(props) {
        super(props);
        this.state = { open: false, userAcceptedLinces: false, s3URL: "", licencesURLS: [], pendingRequest: false, text: "" }
    }

    handleClickOpen = (distribution) => {
        //console.log(distribution);
        if (!distribution.licence_terms) {
            console.log("no distribution licences. ");
            this.setState({ open: false, s3URL: "", userAcceptedLinces: false, licencesURLS: [], pendingRequest: false, text: "" });
            return void 0;
        };
        const licencesUrls = [];
        distribution.licence_terms.field_value.forEach(licence => {
            let licencePath = "";
            let identifierObj = null;
            if (licence.licence_identifier) {
                licence.licence_identifier.field_value.forEach(identifier => {
                    if (identifier.licence_identifier_scheme.field_value === "http://w3id.org/meta-share/meta-share/SPDX") {
                        licencePath = LICENCE_DOWNLOAD_ENDPOINT(`${identifier.value.field_value}.html`);
                        identifierObj = identifier;
                    }
                });
                if (!licencePath) {//if licencePath is empty then we didn't find SPDX url. Get the ELG counterpart
                    licence.licence_identifier.field_value.forEach(identifier => {
                        if (identifier.licence_identifier_scheme.field_value === "http://w3id.org/meta-share/meta-share/elg") {
                            licencePath = LICENCE_DOWNLOAD_ENDPOINT(`${identifier.value.field_value}.pdf`);
                            identifierObj = identifier;
                        }
                    });
                }
            }
            licencesUrls.push({ licencePath: licencePath, identifier: identifierObj });
        });
        //console.log(licencesUrls);

        let { amount = 0 } = distribution.cost ? distribution.cost.field_value : "";
        amount = amount.hasOwnProperty("field_value") ? amount.field_value : amount;
        for (let licenceIndex = 0; distribution.licence_terms && licenceIndex < distribution.licence_terms.field_value.length; licenceIndex++) {
            const licence = distribution.licence_terms.field_value[licenceIndex];
            let licence_category = (licence.licence_category && licence.licence_category.field_value) || [];
            licence_category = licence_category.map(item => item.value);
            licence_category = licence_category.filter(item => item !== ALLOWS_PROCESSING);//remove processing from elg
            //console.log(licenceIndex, licence_category, licence);
            if (licence_category.includes(ALLOWS_DIRECT_aCCESS) && licence_category.length === 1) {
                this.setState({ open: false, s3URL: "", userAcceptedLinces: true, licencesURLS: licencesUrls, text: "" });
                return this.handleAcceptWithoutSignature(this.props.data.pk, distribution);
            } else if (licence_category.includes(ALLOWS_ACCESS_WITH_SIGNATURE) && licence_category.length === 1) {
                if (amount === 0) {
                    this.setState({ open: true, s3URL: "", userAcceptedLinces: false, licencesURLS: licencesUrls });
                    return;
                } else {
                    this.setState({ open: false, text: messages.distribution_download_authorized_users });
                    return;
                }
            } else if (
                licence_category.includes(REQUIRES_USER_AUTHENTICATION) &&
                licence_category.includes(ALLOWS_DIRECT_aCCESS) &&
                licence_category.length === 2) {
                if (this.props.keycloak && this.props.keycloak.authenticated) {
                    this.setState({ open: false, s3URL: "", userAcceptedLinces: true, licencesURLS: licencesUrls, text: "" });
                    return this.handleAcceptWithoutSignature(this.props.data.pk, distribution);
                }
                else {
                    this.setState({ open: false, text: messages.distribution_download_logged_in_users });
                    return;
                }
            } else if (
                licence_category.includes(REQUIRES_USER_AUTHENTICATION) &&
                licence_category.includes(ALLOWS_ACCESS_WITH_SIGNATURE) &&
                licence_category.length === 2) {
                if (this.props.keycloak && this.props.keycloak.authenticated && amount === 0) {
                    this.setState({ open: true, s3URL: "", userAcceptedLinces: false, licencesURLS: licencesUrls, text: "" });
                    return;
                } else if (this.props.keycloak && this.props.keycloak.authenticated && amount !== 0) {
                    this.setState({ open: false, text: messages.distribution_download_authorized_users });
                    return;
                } else if (!this.props.keycloak || (this.props.keycloak && !this.props.keycloak.authenticated)) {
                    this.setState({ open: false, text: messages.distribution_download_logged_in_users });
                    return;
                }
            }
        }
        //if it doesn't return inside for show this message
        this.setState({
            open: false,
            text: (!this.props.keycloak || (this.props.keycloak && !this.props.keycloak.authenticated)) ? messages.distribution_download_logged_in_users : messages.distribution_download_authorized_users
        });
    };

    handleClose = () => {
        this.setState({ open: false, s3URL: "", userAcceptedLinces: false, licencesURLS: [], pendingRequest: false, text: "" });
    };

    handleAcceptWithoutSignature(id, distribution) {
        const { dataset = "" } = distribution;
        const package_f = distribution.package;
        if (!dataset && !package_f) {
            return;
        }
        const fileName = dataset ? dataset.file : package_f.file;
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, pendingRequest: true });
        axios.post(DOWNLOAD_RESOURCE_ENDPOINT(id), null, {
            headers: {
                'Cache-Control': 'no-store, max-age=0',
                'Pragma': 'no-cache',
                'Expires': 'Wed, 21 Oct 2015 07:28:00 GMT',
                'filename': fileName,
                "elg-resource-distribution-id": distribution.pk,
                'ACCEPT-LICENCE': true,
            },
            cancelToken: source.token
        }).then((response) => {
            this.setState({ s3URL: response.data["s3-url"], source: null, pendingRequest: false });
        }).catch((errorResponse) => {
            console.log("error while downloading");
            this.setState({ source: null, pendingRequest: false });
        });

    }

    handleAccept = (id) => {
        const { dataset = "" } = this.props.distribution;
        const package_f = this.props.distribution.package;
        if (!dataset && !package_f) {
            return;
        }
        const fileName = dataset ? dataset.file : package_f.file;
        this.setState({ open: false, userAcceptedLinces: true });
        if (this.state.licencesURLS.length === 0) {
            console.log("Could not find licence text");
            this.setState({ open: false, userAcceptedLinces: true });
        } else {
            if (this.state.source) {
                this.state.source.cancel("");
            }
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            this.setState({ source: source, pendingRequest: true });
            axios.post(DOWNLOAD_RESOURCE_ENDPOINT(id), null, {
                headers: {
                    'Cache-Control': 'no-store, max-age=0',
                    'Pragma': 'no-cache',
                    'Expires': 'Wed, 21 Oct 2015 07:28:00 GMT',
                    'filename': fileName,
                    "elg-resource-distribution-id": this.props.distribution.pk,
                    'ACCEPT-LICENCE': true,
                },
                cancelToken: source.token
            }).then((response) => {
                this.setState({ open: false, userAcceptedLinces: true, s3URL: response.data["s3-url"], pendingRequest: false, source: null });
            }).catch((errorResponse) => {
                console.log("error while downloading");
                this.setState({ source: null, pendingRequest: false });
            });
        }
    }

    render() {
        const { data, distribution } = this.props;
        if (!data) {
            return <div></div>
        }
        const { dataset = "" } = distribution;
        const package_f = distribution.package;
        if (!dataset && !package_f) {
            return <></>
        }
        const { pk = -1 } = data || "";

        return (
            <div>
                {
                    <Button onClick={() => this.handleClickOpen(distribution)} classes={{ root: 'inner-link-outlined--teal' }} aria-controls="simple-menu" aria-haspopup="true" disabled={this.state.pendingRequest} endIcon={<GetAppIcon />}>
                        Download</Button>
                }

                {this.state.text && !this.state.open &&
                    <Dialog open={this.state.text && this.state.text.length > 0} onClose={this.handleClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" fullWidth={false}>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {this.state.text}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">Close</Button>
                        </DialogActions>
                    </Dialog>
                }
                {
                    this.state.open && !this.state.text &&
                    <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" maxWidth="lg" fullWidth={true}>
                        <DialogTitle id="alert-dialog-title">{"Accept the Licence Agreement"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {this.state.licencesURLS.map((url, urlIndex) =>
                                    <iframe key={urlIndex} id={urlIndex} title={`title${urlIndex}`} frameBorder="0" scrolling="auto" className="ng-tns-c14-7 ng-star-inserted" src={url.licencePath} width="100%" height="500px"></iframe>
                                )}
                                {/*Licence agrement*/}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                Disagree</Button>
                            <Button onClick={() => this.handleAccept(pk)} color="primary" autoFocus>
                                Agree</Button>
                        </DialogActions>
                    </Dialog>
                }

                {
                    (this.state.userAcceptedLinces && this.state.s3URL) && <div style={{ display: 'none' }}>
                        <iframe title="Download Reource" src={this.state.s3URL} />
                    </div>
                }
            </div >
        )
    }
}
