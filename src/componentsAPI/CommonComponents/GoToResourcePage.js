import React from "react";
import { withRouter } from 'react-router-dom'
import { ReactComponent as BackIcon } from "./../../assets/elg-icons/navigation-arrows-left-1.svg";
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import { Link } from "react-router-dom";
import messages from "./../../config/messages"; 
 
//import GoogleAnalytics from "../GoogleAnalytics";

class GoToResourcePage extends React.Component {
    

    render() {
        return <div>
            <Container maxWidth="xl">
                <Grid container direction="row" justifyContent="flex-start" alignItems="center"  >
                    <Grid item >                        
                            <span><BackIcon className="xsmall-icon mr-05" /></span> 
                            <span><Link className="font-color-white" to="/createResource">{messages.go_back_editor_message}</Link></span>                        
                    </Grid>
                </Grid>
            </Container>
            {/*<GoogleAnalytics />*/}
        </div>
    }
}

export default withRouter(GoToResourcePage);