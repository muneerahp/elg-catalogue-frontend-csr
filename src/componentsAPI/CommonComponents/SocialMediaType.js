import React from "react";
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import GitHubIcon from '@material-ui/icons/GitHub';
import YouTubeIcon from '@material-ui/icons/YouTube';
import DevicesOtherIcon from '@material-ui/icons/DevicesOther';

class SocialMediaType extends React.Component {
    constructor() {
        super();
        this.state = { data: '' };
    }
    typeofsocial = (type, value) => {
        switch (type) {
            case "facebook": return (<div style={{display: "inline-block"}}>
                <a href={value} target="_blank" rel="noopener noreferrer" >  <FacebookIcon />  </a>
            </div>);
            case "github": return (<div style={{display: "inline-block"}}>
            <a href={value} target="_blank" rel="noopener noreferrer" >  <GitHubIcon />  </a>
             </div>); 
            case "gitlab": return (<div style={{ display: "inline-block" }}>
                <a href={value} target="_blank" rel="noopener noreferrer" >  <GitHubIcon />  </a>
            </div>);
            case "other": return (<div style={{ display: "inline-block" }}>
                <a href={value} target="_blank" rel="noopener noreferrer" >  <DevicesOtherIcon />  </a>
            </div>);
            case "unspecified": return (<div style={{ display: "inline-block" }}>
                <a href={value} target="_blank" rel="noopener noreferrer" >  <DevicesOtherIcon />  </a>
            </div>);                    
            case "youtube": return (<div style={{ display: "inline-block" }}>
                <a href={value} target="_blank" rel="noopener noreferrer" >  <YouTubeIcon />  </a>
            </div>);                                      
            case "twitter": return (<div style={{display: "inline-block"}}>
                <a href={value} target="_blank" rel="noopener noreferrer" >  <TwitterIcon />  </a>
            </div>);
            case "anotherfb": return (<div style={{display: "inline-block"}}>
                <a href={value} target="_blank" rel="noopener noreferrer" >  <FacebookIcon />  </a>
            </div>);
             case "linkedIn": return (<div style={{display: "inline-block"}}>
                <a href={value} target="_blank" rel="noopener noreferrer" >  <LinkedInIcon />  </a>
            </div>);

            default: return ""
        }
    }



    render() {
        const { account_type, value } = this.props;
        return (
            <React.Fragment>
                {
                    this.typeofsocial(account_type.substring(account_type.lastIndexOf("/") + 1), value)

                }
            </React.Fragment>
        );
    }

}

export default SocialMediaType;