import React from "react";
//import Grid from '@material-ui/core/Grid';
import "../../assets/progress.css";
import messages from "../../config/messages";

import { SHOW_DASHBOARD_ROLES, AUTHENTICATED_KEYCLOAK_USER_ROLES } from "../../config/constants";

export default class ResourceLifeCyrcleInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state = { keycloak: props.keycloak, UserRoles: []};
        this.isUnmount = false;
        this.isAuthorizedToView = this.isAuthorizedToView.bind(this);
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
    }

    isAuthorizedToView(roles) {
        var isAuthorized = false;
        SHOW_DASHBOARD_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        })
        return isAuthorized;
    }

    setProgress = (status) => { 
              
        switch (status) {
            case "d":
                return <>
                <ol className="resource-progress-bar">
                    <li className="is-active"><span>{messages.draft_tag}</span></li>
                    <li><span>{messages.internal_tag}</span></li>
                    <li><span>{messages.ingested_tag}</span></li>
                    <li><span>{messages.published_tag}</span></li>                    
                </ol> 
                </>
                 
            case "i":
                return <>
                <ol className="resource-progress-bar">
                    <li className="is-complete"><span>{messages.draft_tag}</span></li>
                    <li className="is-active"><span>{messages.internal_tag}</span></li>
                    <li><span>{messages.ingested_tag}</span></li>
                    <li><span>{messages.published_tag}</span></li>                    
                </ol>
                </>
                 
            case "g":
                return <>
                <ol className="resource-progress-bar">
                    <li className="is-complete"><span>{messages.draft_tag}</span></li>
                    <li className="is-complete"><span>{messages.internal_tag}</span></li>
                    <li className="is-active"><span>{messages.ingested_tag}</span></li>
                    <li><span>{messages.published_tag}</span></li>                    
                </ol>
                </>
            case "p":
                return <>
                <ol className="resource-progress-bar">
                <li className="is-complete"><span>{messages.draft_tag}</span></li>
                <li className="is-complete"><span>{messages.internal_tag}</span></li>
                <li className="is-complete"><span>{messages.ingested_tag}</span></li>
                <li className="is-active"><span>{messages.published_tag}</span></li>                                    
            </ol>
            </>             
            default: break;
        }
         
    }

    render() {
        const { status} = this.props;
        var progress = this.setProgress(status);
        
        if (!this.state.UserRoles.length === 0) {
            return <div></div>
        }
        const isAuth = this.isAuthorizedToView(this.state.UserRoles);

        if (!isAuth || status ==="p") {
            return <div></div>
        }
        return (
            <section>
                <div className="pb1"><span className="resource-progress-bar-heading">Status</span></div>
                {status ==="p" && <div className="resource-progress-bar-status-message"><span className="resource-progress-bar-caption">{messages.status_published_message}</span></div>}
                {status ==="i" && <div className="resource-progress-bar-status-message"><span className="resource-progress-bar-caption">{messages.status_internal_message}</span></div>}
                {status ==="g" && <div className="resource-progress-bar-status-message"><span className="resource-progress-bar-caption">{messages.status_ingested_message}</span></div>}
                {status ==="d" && <div className="resource-progress-bar-status-message"><span className="resource-progress-bar-caption">{messages.status_draft_message}</span></div>}
                <div>
                 {progress}
                 </div> 
            </section>

        );

    }
}