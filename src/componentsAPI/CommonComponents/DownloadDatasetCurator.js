import React from "react";
import axios from "axios";
import Button from '@material-ui/core/Button';
import GetAppIcon from '@material-ui/icons/GetApp';
import { DOWNLOAD_RESOURCE_ENDPOINT } from "../../config/constants";

export default class DownloadDatasetCurator extends React.Component {

    constructor(props) {
        super(props);
        this.state = { s3URL: "", source: null };
    }


    handleAccept(id, distribution) {
        const { dataset = "" } = distribution;
        const package_f = distribution.package;
        if (!dataset && !package_f) {
            return;
        }
        const fileName = dataset ? dataset.file : package_f.file;
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source });
        axios.post(DOWNLOAD_RESOURCE_ENDPOINT(id), null, {
            headers: {
                'Cache-Control': 'no-store, max-age=0',
                'Pragma': 'no-cache',
                'Expires': 'Wed, 21 Oct 2015 07:28:00 GMT',
                'filename': fileName,
                "elg-resource-distribution-id": distribution.pk,
                'ACCEPT-LICENCE': true
            },
            cancelToken: source.token
        }).then((response) => {
            this.setState({ s3URL: response.data["s3-url"], source: null });
        }).catch((errorResponse) => {
            console.log("error while downloading");
            this.setState({ source: null });
        });
    }

    render() {
        const { data, distribution } = this.props;

        if (!data) {
            return <></>
        }

        const { dataset = "" } = distribution;
        const package_f = distribution.package;
        if (!dataset && !package_f) {
            return <></>
        }

        return (
            <div>
                <Button
                    onClick={() => this.handleAccept(data.pk, distribution)}
                    classes={{ root: 'inner-link-outlined--teal' }}
                    aria-controls="simple-menu"
                    aria-haspopup="true"
                    endIcon={<GetAppIcon />}
                >
                    Download
                </Button>
                {
                    (this.state.s3URL) && <div style={{ display: 'none' }}>
                        <iframe title="Download Reource" src={this.state.s3URL} />
                    </div>
                }
            </div>
        )
    }
}
