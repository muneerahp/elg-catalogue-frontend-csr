import React from "react";
import Chip from '@material-ui/core/Chip';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

export default class Keywords extends React.Component {
    render() {
        const { keywordsArray, languagesArray, domainKeywordsArray,
            subjectKeywordsArray, intentedKeywordsArray, disciplineArray,
            ltAreaKeywordsArray, servicesOfferedArray, lcr_subclass, corpus_subclass,
            //ld_subclass,
            KeywordsLabel, DisciplineKeywordsLabel, DomainKeywordsLabel, LtAreaKeywordsLabel,
            servicesOfferedKeywordsLabel, languagesLabel, subjectKeywordsLabel,
            intentedKeywordsLabel, lcr_subclassLabel, corpus_subclassLabel,
            //ld_subclassLabel, 
            model_typeArray, model_type_label, model_functionArray, model_function_label,

            grid_size } = this.props;

        return (
            <div className="padding15">
                <Grid container spacing={3} direction="row" justifyContent="flex-start" alignItems="stretch" className="classification--container">
                    {keywordsArray && keywordsArray.length > 0 &&
                        <>
                            <Grid item className="classification-item" xs={12} sm={grid_size ? grid_size : 4} md={grid_size ? grid_size : 4}>
                                <div className="padding15">
                                    <Typography variant="h3" className="classification-labels">{KeywordsLabel}</Typography>
                                </div>
                                {keywordsArray.map((keyword, index) =>
                                    <Chip key={index} size="small" label={keyword} className="ChipTagGrey" />
                                )}

                            </Grid>
                        </>
                    }
                    {languagesArray && languagesArray.length > 0 &&
                        <>
                            <Grid item className="classification-item" xs={12} sm={grid_size ? grid_size : 4} md={grid_size ? grid_size : 4}>
                                <div className="padding15">
                                    <Typography variant="h3" className="classification-labels">{languagesLabel}</Typography>
                                </div>
                                {languagesArray.map((lang, index) =>
                                    <Chip key={index} size="small" label={lang} className="ChipTagGrey" />
                                )}
                            </Grid>
                        </>
                    }

                    {domainKeywordsArray && domainKeywordsArray.length > 0 &&
                        <>
                            <Grid item className="classification-item" xs={12} sm={grid_size ? grid_size : 4} md={grid_size ? grid_size : 4}>
                                <div className="padding15">
                                    <Typography variant="h3" className="classification-labels">{DomainKeywordsLabel}</Typography>
                                </div>
                                {domainKeywordsArray.map((domainItem, domainindex) =>
                                    <Chip key={domainindex} size="small" label={domainItem} className="ChipTagGrey" />
                                )}
                            </Grid>
                        </>
                    }
                    {subjectKeywordsArray && subjectKeywordsArray.length > 0 &&
                        <>
                            <Grid item className="classification-item" xs={12} sm={grid_size ? grid_size : 4} md={grid_size ? grid_size : 4}>
                                <div className="padding15">
                                    <Typography variant="h3" className="classification-labels">{subjectKeywordsLabel}</Typography>
                                </div>
                                {subjectKeywordsArray.map((subjectItem, subjectIndex) =>
                                    <Chip key={subjectIndex} size="small" label={subjectItem} className="ChipTagGrey" />
                                )
                                }
                            </Grid>
                        </>
                    }

                    {intentedKeywordsArray && intentedKeywordsArray.length > 0 &&
                        <>
                            <Grid item className="classification-item" xs={12} sm={grid_size ? grid_size : 4} md={grid_size ? grid_size : 4}>
                                <div className="padding15">
                                    <Typography variant="h3" className="classification-labels">{intentedKeywordsLabel}</Typography>
                                </div>
                                {intentedKeywordsArray.map((applicationItem, applicationIndex) =>
                                    <Chip key={applicationIndex} size="small" label={applicationItem} className="ChipTagGrey" />
                                )
                                }
                            </Grid>
                        </>
                    }
                    {disciplineArray && disciplineArray.length > 0 &&
                        <>
                            <Grid item className="classification-item" xs={12} sm={grid_size ? grid_size : 4} md={grid_size ? grid_size : 4}>
                                <div className="padding15">
                                    <Typography variant="h3" className="classification-labels">{DisciplineKeywordsLabel}</Typography>
                                </div>
                                {
                                    disciplineArray.map((lang, index) =>
                                        <Chip key={index} size="small" label={lang} className="ChipTagGrey" />
                                    )}
                            </Grid>
                        </>
                    }
                    {ltAreaKeywordsArray && ltAreaKeywordsArray.length > 0 &&
                        <>
                            <Grid item className="classification-item" xs={12} sm={grid_size ? grid_size : 4} md={grid_size ? grid_size : 4}>
                                <div className="padding15">
                                    <Typography variant="h3" className="classification-labels">{LtAreaKeywordsLabel}</Typography>
                                </div>
                                {
                                    ltAreaKeywordsArray.map((applicationItem, applicationIndex) =>
                                        <Chip key={applicationIndex} size="small" label={applicationItem} className="ChipTagGrey" />
                                    )
                                }
                            </Grid>
                        </>
                    }
                    {servicesOfferedArray && servicesOfferedArray.length > 0 &&
                        <>
                            <Grid item className="classification-item" xs={12} sm={grid_size ? grid_size : 4} md={grid_size ? grid_size : 4}>
                                <div className="padding15">
                                    <Typography variant="h3" className="classification-labels">{servicesOfferedKeywordsLabel}</Typography>
                                </div>
                                {
                                    servicesOfferedArray.map((lang, index) =>
                                        <Chip key={index} size="small" label={lang} className="ChipTagGrey" />
                                    )}
                            </Grid>
                        </>
                    }
                    {lcr_subclass &&
                        <>
                            <Grid item className="classification-item" xs={12} sm={grid_size ? grid_size : 4} md={grid_size ? grid_size : 4}>
                                <div className="padding15">
                                    <Typography variant="h3" className="classification-labels">{lcr_subclassLabel}</Typography>
                                </div>
                                <Chip size="small" label={lcr_subclass} className="ChipTagGrey" />
                            </Grid>
                        </>
                    }
                    {corpus_subclass &&
                        <>
                            <Grid item className="classification-item" xs={12} sm={grid_size ? grid_size : 4} md={grid_size ? grid_size : 4}>
                                <div className="padding15">
                                    <Typography variant="h3" className="classification-labels">{corpus_subclassLabel}</Typography>
                                </div>
                                <Chip size="small" label={corpus_subclass} className="ChipTagGrey" />
                            </Grid>
                        </>
                    }
                    {/*ld_subclass &&
                 <>
                      <Grid item className="classification-item" xs={12} sm={grid_size? grid_size: 4 } md={grid_size? grid_size: 4}>  
                      <div className="padding15">
                        <Typography variant="h3" className="classification-labels">{ld_subclassLabel}</Typography> 
                        </div>                          
                            <Chip size="small" label={ld_subclass} className="ChipTagGrey" />                                 
                        </Grid> 
                    </>
                */}
                {model_typeArray && model_typeArray.length > 0 &&
                        <>
                            <Grid item className="classification-item" xs={12} sm={grid_size ? grid_size : 4} md={grid_size ? grid_size : 4}>
                                <div className="padding15">
                                    <Typography variant="h3" className="classification-labels">{model_type_label}</Typography>
                                </div>
                                {
                                    model_typeArray.map((modelItem, mIndex) =>
                                        <Chip key={mIndex} size="small" label={modelItem} className="ChipTagGrey" />
                                    )
                                }
                            </Grid>
                        </>
                    }
                {model_functionArray && model_functionArray.length > 0 &&
                        <>
                            <Grid item className="classification-item" xs={12} sm={grid_size ? grid_size : 4} md={grid_size ? grid_size : 4}>
                                <div className="padding15">
                                    <Typography variant="h3" className="classification-labels">{model_function_label}</Typography>
                                </div>
                                {
                                    model_functionArray.map((modelItem, mIndex) =>
                                        <Chip key={mIndex} size="small" label={modelItem} className="ChipTagGrey" />
                                    )
                                }
                            </Grid>
                        </>
                }



                </Grid>




            </div>
        );
    }
}