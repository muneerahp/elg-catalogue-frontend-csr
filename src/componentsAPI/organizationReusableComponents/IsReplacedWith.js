import React from "react";
import Typography from '@material-ui/core/Typography';
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";
import Grid from '@material-ui/core/Grid';

export default class IsReplacedWith extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
                  

        return <div>
            {data.described_entity.field_value.is_replaced_with && data.described_entity.field_value.is_replaced_with.field_value.length>0 &&  <Typography variant="h3" className="title-links"> {data.described_entity.field_value.is_replaced_with.field_label[metadataLanguage] || data.described_entity.field_value.is_replaced_with.field_label["en"] || "is_replaced_with"}</Typography>}
           
            <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start" spacing={3}>
                <Grid item sm={12} xs={12}>
                     {data.described_entity.field_value.is_replaced_with && data.described_entity.field_value.is_replaced_with.field_value.length>0 && data.described_entity.field_value.is_replaced_with.field_value.map((is_replaced_withItem, index) => {
                        let organization_name = (is_replaced_withItem.organization_name.field_value[metadataLanguage] || is_replaced_withItem.organization_name.field_value[Object.keys(is_replaced_withItem.organization_name.field_value)[0]]) || "";
                        // let version = is_replaced_withItem.version.field_value || '';
                        // const is_replaced_with_label = data.described_entity.field_value.is_replaced_with.field_label[metadataLanguage] || data.described_entity.field_value.is_replaced_with.field_label["en"] || "is_replaced_with";
                        let full_metadata_record = commonParser.getFullMetadata(is_replaced_withItem.full_metadata_record);
                        return <div key={index}>
                            {full_metadata_record ?
                                <div className="padding5 internal_url">
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        <span> {organization_name} </span>
                                    </Link>
                                </div> : 
                                organization_name && <div className="padding5 info_value">  {organization_name}  </div>}
                        </div>
                    })}

                     
                </Grid>
            </Grid>
            

        </div>
    }
}