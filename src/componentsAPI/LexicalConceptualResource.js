import React from "react";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import { TabContent, TabPane, } from "reactstrap";
import HelmetMetaData from "./CommonComponents/HelmetMetaData";
import Container from '@material-ui/core/Container';
import CorpusImage from "../assets/images/corpus.png";
import ResourceImage from "./../assets/elg-icons/archive-folder.svg";
//import { ReactComponent as BackIcon } from "./../assets/elg-icons/navigation-arrows-left-1.svg";
import GoToCatalogue from "./CommonComponents/GoToCatalogue";
import { SERVER_API_METADATARECORD, RETRIEVE_RECORD_INFO, BASE_URL, SERVER_API_METADATARECORD_LABELESS_SCHEMA, getLogoutUrl } from "../config/constants";
//common
import ExportMetadata from "./CommonComponents/ExportMetadata";
import ShareMetadata from "./CommonComponents/ShareMetadata";
import Keywords from "./CommonComponents/Keywords";
import DescriptionRichText from "./CommonComponents/DescriptionRichText";
import ResourceActor from "./CommonComponents/ResourceActor";
import ResourceHeader from "./CommonComponents/ResourceHeader";
import AdditionalInfo from "./CommonComponents/AdditionalInfo";
import ProgressBar from "./CommonComponents/ProgressBar";
import ResourceLifeCyrcleInfo from "./CommonComponents/ResourceLifeCyrcleInfo";
import Ethics from "./CommonComponents/Ethics";
//import IsReplacedWith from "./CommonComponents/IsReplacedWith";
//lRC specific
import lcrParser from "../parsers/lcrParser";
import serviceToolParser from "../parsers/ServiceToolParser";
import commonParser from "../parsers/CommonParser";
import NavigationTabs from "./lcrReusableComponents/NavigationTabs";
import LcrMediaPartType from './lcrReusableComponents/LcrMediaPartType';
//import LcrGeneral from "./lcrReusableComponents/LcrGeneral";
import LcrMediaPart from "./lcrReusableComponents/LcrMediaPart";
//common with corpus 
import FundedBy from "./corpusReusableComponents/FundedBy";
import CorpusGeneralCategories from "./corpusReusableComponents/CorpusGeneralCategories";
//import Replaces from "./CommonComponents/Replaces";
import Relations from "./CommonComponents/Relations";
import Documentations from "./corpusReusableComponents/Documentations";
import Creation from "./corpusReusableComponents/Creation";
//import IprHolder from "./CommonComponents/IprHolder";
import ActualUse from "./corpusReusableComponents/ActualUse";
import Validation from "./corpusReusableComponents/Validation";
//import PhysicalResource from "./corpusReusableComponents/PhysicalResource";
import Distributions from "./corpusReusableComponents/Distributions";
import CorpusUnspecifiedPart from "./corpusReusableComponents/CorpusUnspecifiedPart";
import Annotation from "./corpusReusableComponents/Annotation";
//import HasSubset from "./corpusReusableComponents/HasSubset";
import { RESOURCE_ACCESS } from "../config/editorConstants";
import Versions from "./CommonComponents/Versions";
import RecordStats from "./CommonComponents/RecordStats";

function select_size(first_column, second_column, third_column) {
    let grid_size = 12;

    if (first_column && second_column && third_column) {
        grid_size = 4;
    }
    if (first_column && !second_column && !third_column) {
        grid_size = 12;
    }
    if ((first_column && second_column && !third_column) || (!first_column && second_column && third_column) || (first_column && !second_column && third_column)) {
        grid_size = 6;
    }
    return grid_size;

}

export default class LexicalConceptualResource extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '', metadataLanguage: '', tab: '1', showEthics: false, showDocs: false, showCreation: false, showCategories: false,
            pk: "",
            showLcrMedia: false, showArea: false, displayTabGeneral: false,
            loading: true, number_of_failed_loads: 0,
            isOwner: false, isLegalValidator: false, isMetadataValidator: false, isTechnicalValidator: false, source: null
        };
    }

    componentDidMount() {
        this.getMetadataResourceDetail();
        window.addEventListener('message', this.respondToGuiMessage, false);
        this.setState({ number_of_failed_loads: 0 });
    }

    componentWillUnmount() {
        window.removeEventListener('message', this.respondToGuiMessage, false);
        if (this.state.source) {
            this.state.source.cancel("");
        }
        clearTimeout(this.timeOutId);
    }

    displayTabGeneralFunction = (flag) => {
        this.setState({ displayTabGeneral: flag });
    }
    showLcrMediaFunction = (flag) => {
        this.setState({ showLcrMedia: flag });
    }
    showEthicsFunction = (flag) => {
        this.setState({ showEthics: flag });
    }
    showDocsFunction = (flag) => {
        this.setState({ showDocs: flag });
    }
    showCreationFunction = (flag) => {
        this.setState({ showCreation: flag });
    }
    showAreaFunction = (flag) => {
        this.setState({ showArea: flag });
    }
    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
    }
    updateRecord = () => {
        this.getMetadataResourceDetail();
    }

    retrieveRecordInfo = (id, source) => {
        if (this.props.keycloak && this.props.keycloak.authenticated) {
            axios.get(RETRIEVE_RECORD_INFO(id), { cancelToken: source.token })
                .then(res => {
                    const { curator = false, legal_validator = false, metadata_validator = false, technical_validator = false } = res.data;
                    this.setState({ isOwner: curator, isLegalValidator: legal_validator, isMetadataValidator: metadata_validator, isTechnicalValidator: technical_validator });
                })
                .catch(err => console.log(err));
        }
    }

    getMetadataResourceDetail = () => {
        const id = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const url = SERVER_API_METADATARECORD(id);
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        this.retrieveRecordInfo(id, source);
        axios.get(url)
            .then(res => {
                this.setState(res);
                const lang = Object.keys(res.data.described_entity.field_value.resource_name.field_value).includes("en") ? "en" : Object.keys(res.data.described_entity.field_value.resource_name.field_value)[0];
                this.setState({ metadataLanguage: lang, pk: id, source: null, loading: false, number_of_failed_loads: 0 });
            }).catch(err => {
                console.log(err);
                this.setState({ source: null, loading: false });
            });
    }

    getKeycloakToken = () => {
        const { keycloak } = this.props;
        if (keycloak && keycloak.authenticated) {
            let keyclockToken = keycloak.token || "";
            return keyclockToken;
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState) {
        const thisId = this.props && this.props.match && this.props.match.params && this.props.match.params.id;
        const prevId = prevProps && prevProps.match && prevProps.match.params && prevProps.match.params.id;
        if (thisId !== prevId) {
            this.setState({ data: '', metadataLanguage: '', tab: '1', displayTabGeneral: false });
            this.getMetadataResourceDetail();
        }
    }

    respondToGuiMessage = (event) => {
        if (event.origin !== window.location.origin) {
            return;
        }
        // only send the iframe config message when the iframe asks for it, not if the message came from elsewhere
        var iframe = document.getElementById('dgk') && document.getElementById('dgk').contentWindow;
        if (!iframe || event.source !== iframe) {
            return;
        }
        const record_pk = this.state.pk;
        let keyclockToken = this.getKeycloakToken() || "";
        const record_url = SERVER_API_METADATARECORD_LABELESS_SCHEMA(record_pk);//dataset metadata record
        const is_queried_by_service_url = lcrParser.get_is_queried_by_service_url(this.state.data);//associated service metadata record
        let elg_gui_url = serviceToolParser.getElgGuiUrl(this.state.data);
        let data = { ApiRecordUrl: `${is_queried_by_service_url}`, StyleCss: "", Authorization: `Bearer ${keyclockToken}`, DatasetRecordUrl: record_url };
        iframe.postMessage(JSON.stringify(data), `${elg_gui_url}`);
    }

    reloadPage = () => {
        const number_of_failed_loads = this.state.number_of_failed_loads;
        if (number_of_failed_loads < 3) {
            this.setState({ number_of_failed_loads: number_of_failed_loads + 1 });
            this.getMetadataResourceDetail();
            return <ProgressBar number_of_failed_loads={this.state.number_of_failed_loads} />;
        } else {
            this.timeOutId = setTimeout(() => { window.location = getLogoutUrl() }, 5000)
            return <ProgressBar number_of_failed_loads={this.state.number_of_failed_loads} />;
        }
    }

    render() {
        if (this.props.keycloak && !this.props.keycloak.authenticated) {
            if (commonParser.redirectUnregisteredUsersToKeycloak(this.props.location)) {
                this.props.keycloak.login();
            }
        }

        if (this.state.loading) {
            return <ProgressBar />
        } else {
            if (this.state.data) {
            } else {
                return this.reloadPage();
            }
        }

        const { data, metadataLanguage } = this.state;
        const languages = [];// ["english", "spanish"];
        const resourceName = lcrParser.getResourceName(data, metadataLanguage) ? lcrParser.getResourceName(data, metadataLanguage).value : "";
        const logo = lcrParser.getLogo(data);
        const short_name = lcrParser.getResourceShortName(data, metadataLanguage) ? lcrParser.getResourceShortName(data, metadataLanguage).value : "";
        const version = lcrParser.getVersion(data, metadataLanguage) ? lcrParser.getVersion(data, metadataLanguage).value : "";
        const version_label = lcrParser.getVersion(data, metadataLanguage) ? lcrParser.getVersion(data, metadataLanguage).label : "";
        const version_date = lcrParser.getVersionDate(data, metadataLanguage) ? lcrParser.getVersionDate(data).value : "";
        const resource_type = lcrParser.getLRType(data, metadataLanguage) ? lcrParser.getLRType(data, metadataLanguage).value : null;
        const entity_type = lcrParser.getEntityType(data, metadataLanguage) ? lcrParser.getEntityType(data, metadataLanguage).value : null;
        const description = lcrParser.getDescription(data, metadataLanguage) ? lcrParser.getDescription(data, metadataLanguage).value : null;
        const keywords = lcrParser.getKeywords(data, metadataLanguage) ? lcrParser.getKeywords(data, metadataLanguage).value : [];
        const keywords_label = lcrParser.getKeywords(data, metadataLanguage) ? lcrParser.getKeywords(data, metadataLanguage).label : "";
        const domainKeywords = lcrParser.getDomainKeywords(data, metadataLanguage) ? lcrParser.getDomainKeywords(data, metadataLanguage).value : [];
        const domainKeywords_label = lcrParser.getDomainKeywords(data, metadataLanguage) ? lcrParser.getDomainKeywords(data, metadataLanguage).label : "";
        const subjectKeywords = lcrParser.getSubjectKeywords(data, metadataLanguage) ? lcrParser.getSubjectKeywords(data, metadataLanguage).value : [];
        const subjectKeywords_label = lcrParser.getSubjectKeywords(data, metadataLanguage) ? lcrParser.getSubjectKeywords(data, metadataLanguage).label : "";
        const intentedKeywords = lcrParser.getIntendedKeywords(data, metadataLanguage) ? lcrParser.getIntendedKeywords(data, metadataLanguage).value : [];
        const intentedKeywords_label = lcrParser.getIntendedKeywords(data, metadataLanguage) ? lcrParser.getIntendedKeywords(data, metadataLanguage).label : "";
        const citationText = lcrParser.getCitationText(data, metadataLanguage) ? lcrParser.getCitationText(data, metadataLanguage).value : "";
        const citationText_label = lcrParser.getCitationText(data, metadataLanguage) ? lcrParser.getCitationText(data, metadataLanguage).label : "Cite as";
        const citation_all_versions = lcrParser.getCitationAllVersions(data, metadataLanguage) ? lcrParser.getCitationAllVersions(data, metadataLanguage).value : "";
        const citation_all_versions_label = lcrParser.getCitationAllVersions(data, metadataLanguage) ? lcrParser.getCitationAllVersions(data, metadataLanguage).label : "Cite as"; 
        const mailingList = lcrParser.getMailingList(data, metadataLanguage) ? lcrParser.getMailingList(data, metadataLanguage).value : [];
        const discussionUrl = lcrParser.getDiscussionUrl(data, metadataLanguage) ? lcrParser.getDiscussionUrl(data, metadataLanguage).value : [];
        //const discussionUrl_label = lcrParser.getDiscussionUrl(data,metadataLanguage)?lcrParser.getDiscussionUrl(data,metadataLanguage).label:"";
        const additioanlInfo = lcrParser.getAdditionalInfo(data, metadataLanguage) ? lcrParser.getAdditionalInfo(data, metadataLanguage).value : [];
        const additioanlInfo_label = lcrParser.getAdditionalInfo(data, metadataLanguage) ? lcrParser.getAdditionalInfo(data, metadataLanguage).label : "";
        //const physical_resource = lcrParser.getPhysicalResource(data, metadataLanguage) ? lcrParser.getPhysicalResource(data, metadataLanguage).value : [];
        //const physical_resource_label = lcrParser.getPhysicalResource(data, metadataLanguage) ? lcrParser.getPhysicalResource(data, metadataLanguage).label : "";
        const personal_data_included = lcrParser.getPersonalDataIncluded(data, metadataLanguage) ? lcrParser.getPersonalDataIncluded(data, metadataLanguage).value : null;
        const personal_data_included_label = lcrParser.getPersonalDataIncluded(data, metadataLanguage) ? lcrParser.getPersonalDataIncluded(data, metadataLanguage).label : "";
        const personal_data_details = lcrParser.getPersonalDataDetails(data, metadataLanguage) ? lcrParser.getPersonalDataDetails(data, metadataLanguage).value : [];
        const personal_data_details_label = lcrParser.getPersonalDataDetails(data, metadataLanguage) ? lcrParser.getPersonalDataDetails(data, metadataLanguage).label : "";
        const sensitive_data_included = lcrParser.getSensitiveDataIncluded(data, metadataLanguage) ? lcrParser.getSensitiveDataIncluded(data, metadataLanguage).value : null;
        const sensitive_data_included_label = lcrParser.getSensitiveDataIncluded(data, metadataLanguage) ? lcrParser.getSensitiveDataIncluded(data, metadataLanguage).label : "";
        const sensitive_data_details = lcrParser.getSensitiveDataDetails(data, metadataLanguage) ? lcrParser.getSensitiveDataDetails(data, metadataLanguage).value : [];
        const sensitive_data_details_label = lcrParser.getSensitiveDataDetails(data, metadataLanguage) ? lcrParser.getSensitiveDataDetails(data, metadataLanguage).label : "";
        const anonymized = lcrParser.getAnonymized(data, metadataLanguage) ? lcrParser.getAnonymized(data, metadataLanguage).value : null;
        const anonymized_label = lcrParser.getAnonymized(data, metadataLanguage) ? lcrParser.getAnonymized(data, metadataLanguage).label : "";
        const anonymization_details = lcrParser.getAnonymizedDetails(data, metadataLanguage) ? lcrParser.getAnonymizedDetails(data, metadataLanguage).value : [];
        const anonymization_details_label = lcrParser.getAnonymizedDetails(data, metadataLanguage) ? lcrParser.getAnonymizedDetails(data, metadataLanguage).label : "";
        const lcr_subclass = lcrParser.getLcrSubclass(data, metadataLanguage) ? lcrParser.getLcrSubclass(data, metadataLanguage).value : [];
        const lcr_subclass_label = lcrParser.getLcrSubclass(data, metadataLanguage) ? lcrParser.getLcrSubclass(data, metadataLanguage).label : "";
        const lr_subclass = data.described_entity.field_value.lr_subclass || [];
        const { under_construction = false, service_compliant_dataset = false } = data.management_object || {};
        const { for_information_only = false } = data.management_object || {};
        const { is_latest_version = false } = data.management_object || {};
        const all_versions = data.described_entity.field_value.all_versions ? data.described_entity.field_value.all_versions : null;
        const all_versions_label = data.described_entity.field_value.all_versions ? (data.described_entity.field_value.all_versions.field_label[metadataLanguage] || data.described_entity.field_value.all_versions.field_label["en"]) : "";

        const { elg_gui_url, tool_type } = data.service_info || {};
        const keycloakToken = this.getKeycloakToken();
        const is_queried_by_service_url = lcrParser.get_is_queried_by_service_url(this.state.data);
        const tabTitles = ["Overview"];
        if (!under_construction) {
            tabTitles.push("Download");
        }
        if (service_compliant_dataset && elg_gui_url && is_queried_by_service_url) {
            tabTitles.push("Try out");
        }
        if (this.state.displayTabGeneral) {
            tabTitles.push("Related LRTs");
        }
        //control the number of columns to appear
        const first_column = this.state.showDocs || this.state.showCreation || data.described_entity.field_value.ipr_holder || false;
        const second_column = data.described_entity.field_value.actual_use || data.described_entity.field_value.validated || false;
        const third_column = this.state.showEthics || this.state.showArea || this.state.showLcrMedia || false;
        const grid_size = select_size(first_column, second_column, third_column);

        return <div>
            <HelmetMetaData data={data}
                resourceName={resourceName}
                description={description}
                keywords={keywords}
                domainKeywords={domainKeywords}
                subjectKeywords={subjectKeywords}
                intentedKeywords={intentedKeywords}
                corpus_subclass={null}
                languages={languages}
                resourceShortName={short_name}
                for_information_only={for_information_only}
                ltAreaKeywordsArray={null}
                disciplines={null}
                servicesOffered={null}
                OrganizationRolesArray={null}
                ld_subclass={null}
                lcr_subclass={lcr_subclass}
                type="lcr"
                pk={this.state.pk}
            />


            <div className="search-top">
                <GoToCatalogue />
            </div>
            <Container maxWidth="xl">
                <div className="metadata-main-card-container">
                    <ResourceLifeCyrcleInfo key={data.management_object.status} status={data.management_object.status} keycloak={this.props.keycloak} />
                    {is_latest_version === false && data.management_object.status === "p" && <section>
                        <div className="resource-progress-bar-status-message"><span className="resource-progress-bar-caption">{"There is a "}<span className="bold">{"newer version "}</span>{"of this record available"}</span></div>
                    </section>}
                    <ResourceHeader
                        key={data.management_object ? 'header' + JSON.stringify(data.management_object) : 'header' + this.state.pk}
                        {...this.state}
                        logo={logo}
                        title={resourceName}
                        short_name={short_name}
                        version={version}
                        version_label={version_label}
                        version_Date={version_date}
                        resource_type={resource_type}
                        proxied={null}
                        entity_type={entity_type} default_image={ResourceImage} under_construction={under_construction} for_information_only={for_information_only} keycloak={this.props.keycloak} data={data} pk={this.state.pk} updateRecord={this.updateRecord} 
                        citationText={citationText} citationText_label={citationText_label}
                            citation_all_versions={citation_all_versions}  citation_all_versions_label={citation_all_versions_label}/>
                </div>

                <div className="tab-pane-container">
                    <NavigationTabs toggleTab={this.toggleTab} activeTab={this.state.tab} tabTitles={tabTitles} />
                    <TabContent activeTab={this.state.tab}>
                        <TabPane tabId={`${tabTitles.indexOf('Overview') + 1}`}>
                            <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
                                <Grid item xs={12} sm={8} md={8} className="search-results-main" >
                                    <DescriptionRichText description={description} />
                                    <Keywords keywordsArray={keywords}
                                        languagesArray={languages}
                                        domainKeywordsArray={domainKeywords}
                                        subjectKeywordsArray={subjectKeywords}
                                        intentedKeywordsArray={intentedKeywords}
                                        lcr_subclass={lcr_subclass}
                                        lcr_subclassLabel={lcr_subclass_label}
                                        KeywordsLabel={keywords_label}
                                        DomainKeywordsLabel={domainKeywords_label}
                                        subjectKeywordsLabel={subjectKeywords_label}
                                        intentedKeywordsLabel={intentedKeywords_label} />


                                    <LcrMediaPartType lexical_conceptual_resource_media_part={lr_subclass.field_value.lexical_conceptual_resource_media_part} metadataLanguage={metadataLanguage} />
                                    <CorpusUnspecifiedPart unspecified_part={lr_subclass.field_value.unspecified_part} metadataLanguage={metadataLanguage} />


                                </Grid>
                                <Grid item xs={12} sm={4} md={4} className="MetadataSidebar" >
                                    <ExportMetadata pk={this.state.pk} under_construction={under_construction} for_information_only={for_information_only} keycloak={this.props.keycloak} data={this.state.data} {...this.state} />
                                    {data.management_object.status === "p" && <ShareMetadata shareUrl={`${BASE_URL}catalogue/lcr/${this.state.pk}`} title={resourceName} description={description} logo={logo} />}
                                    <RecordStats pk={this.state.pk} />
                                    <Versions all_versions={all_versions} all_versions_label={all_versions_label} metadataLanguage={metadataLanguage} />
                                    <div className="ActionsButtonArea">
                                        {/*<Replaces data={data} metadataLanguage={metadataLanguage} />*/}
                                        {/*<IsReplacedWith data={data} metadataLanguage={metadataLanguage} />*/}                                        
                                        <ResourceActor data={this.state.data.described_entity.field_value.resource_provider} metadataLanguage={metadataLanguage} />
                                        <FundedBy data={data} metadataLanguage={metadataLanguage} CorpusImage={CorpusImage} navigateToProjectDetail={this.navigateToProjectDetail} />

                                        <AdditionalInfo additioanlInfo={additioanlInfo}
                                            mailingList={mailingList}
                                            discussionUrl={discussionUrl}
                                            data={data}
                                            metadataLanguage={metadataLanguage} AdditionalInfoLabel={additioanlInfo_label} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"}
                                            {...commonParser.getSourceOfMetadataRecord(data, metadataLanguage)} />

                                    </div>
                                </Grid>
                            </Grid>
                            {grid_size &&
                                <Grid container spacing={2} className="MetaDataDetailsBottomContainer">
                                    {<Grid item xs={12} sm={grid_size}>
                                        <Documentations data={data} metadataLanguage={metadataLanguage} showDocsFunction={this.showDocsFunction} showDocs={this.state.showDocs} />
                                        <Creation data={data} metadataLanguage={metadataLanguage} showCreationFunction={this.showCreationFunction} showCreation={this.state.showCreation} />
                                        <ResourceActor data={data.described_entity.field_value.ipr_holder} metadataLanguage={metadataLanguage} />
                                    </Grid>}

                                    {second_column && <Grid item xs={12} sm={grid_size}>
                                        <ActualUse data={data} metadataLanguage={metadataLanguage} />
                                        <Validation data={data} metadataLanguage={metadataLanguage} />
                                    </Grid>}

                                    {<Grid item xs={12} sm={grid_size}>
                                        <LcrMediaPart data={data} metadataLanguage={metadataLanguage} showLcrMediaFunction={this.showLcrMediaFunction} showLcrMedia={this.state.showLcrMedia} />

                                        <Ethics personal_data_included={personal_data_included}
                                            personal_data_details={personal_data_details}
                                            sensitive_data_included={sensitive_data_included}
                                            sensitive_data_details={sensitive_data_details}
                                            anonymized={anonymized}
                                            anonymization_details={anonymization_details}
                                            personal_data_included_label={personal_data_included_label}
                                            personal_data_details_label={personal_data_details_label}
                                            sensitive_data_included_label={sensitive_data_included_label}
                                            sensitive_data_details_label={sensitive_data_details_label}
                                            anonymized_label={anonymized_label}
                                            anonymization_details_label={anonymization_details_label}
                                            showEthicsFunction={this.showEthicsFunction}
                                            showEthics={this.state.showEthics}
                                        />
                                        <CorpusGeneralCategories data={data} metadataLanguage={metadataLanguage} title="LCR categories" showAreaFunction={this.showAreaFunction} showArea={this.state.showArea} />

                                    </Grid>
                                    }

                                </Grid>
                            }
                        </TabPane>

                        {!under_construction && <TabPane tabId={`${tabTitles.indexOf('Download') + 1}`}>
                            {<Distributions data={data} metadataLanguage={metadataLanguage} {...this.state} />}
                        </TabPane>}

                        {elg_gui_url && tool_type === RESOURCE_ACCESS && is_queried_by_service_url && <TabPane tabId={`${tabTitles.indexOf('Try out') + 1}`}>
                            <Grid container className="upperSpace" spacing={2}>
                                <Grid item xs={12}>
                                    {!keycloakToken && <h3>Please log in to test this resource.</h3>}
                                    {keycloakToken && <iframe id="dgk" title="iframe" frameBorder="0" scrolling="auto" className="ng-tns-c14-7 ng-star-inserted" src={`${elg_gui_url}`} style={{ height: "500px", width: "100%" }}></iframe>}
                                </Grid>
                            </Grid>
                        </TabPane>
                        }

                        <TabPane tabId={`${tabTitles.indexOf('Related LRTs') + 1}`}>
                            {/*<HasSubset data={data} metadataLanguage={metadataLanguage} displayTabGeneralFunction={this.displayTabGeneralFunction} displayTabGeneral={this.state.displayTabGeneral} />*/}
                            <Annotation data={data} metadataLanguage={metadataLanguage} displayTabGeneralFunction={this.displayTabGeneralFunction} displayTabGeneral={this.state.displayTabGeneral} />
                            <Relations data={this.state.data} metadataLanguage={metadataLanguage} displayTabGeneralFunction={this.displayTabGeneralFunction} displayTabGeneral={this.state.displayTabGeneral} />
                            {/* <PhysicalResource physical_resource={physical_resource} physical_resource_label={physical_resource_label} displayTabGeneralFunction={this.displayTabGeneralFunction} displayTabGeneral={this.state.displayTabGeneral} />*/}
                            {/*<LcrGeneral
                                personal_data_included={personal_data_included}
                                personal_data_details={personal_data_details}
                                sensitive_data_included={sensitive_data_included}
                                sensitive_data_details={sensitive_data_details}
                                anonymized={anonymized}
                                anonymization_details={anonymization_details}
                                data={data}
                                metadataLanguage={metadataLanguage}
                                personal_data_included_label={personal_data_included_label}
                                personal_data_details_label={personal_data_details_label}
                                sensitive_data_included_label={sensitive_data_included_label}
                                sensitive_data_details_label={sensitive_data_details_label}
                                anonymized_label={anonymized_label}
                                anonymization_details_label={anonymization_details_label}
                                displayTabGeneralFunction={this.displayTabGeneralFunction}
                            displayTabGeneral={this.state.displayTabGeneral} />*/}
                        </TabPane>

                    </TabContent>
                </div>
            </Container>

        </div >
    }

}