import React from "react"; 
import Typography from '@material-ui/core/Typography';
 
export default class LcrDynamicElement extends React.Component {
    render() {
        const { dynamic_elementArray, metadataLanguage } = this.props;
        if (!dynamic_elementArray) {
            return <div></div>
        }
        return (
            <div className="padding15">
                { dynamic_elementArray.field_value.map(function (item, Index) {
                    //let type_of_element = item.type_of_element ? (item.type_of_element.field_value[metadataLanguage] || item.type_of_element.field_value[Object.keys(item.type_of_element.field_value)[0]]) : "";
                    let type_of_element = []; 
                        (item.type_of_element && item.type_of_element.field_value.length > 0 && item.type_of_element.field_value.map((keyword, DIndex) => {
                            type_of_element.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                            return void 0;
                    }));
                    let poses_per_subject = item.poses_per_subject ? item.poses_per_subject.field_value: "";
                    let body_partArray = item.body_part ? item.body_part.field_value.map(body => body.label[metadataLanguage]||body.label[Object.keys(body.label)[0]]) : [];
                    //let distractor = item.distractor ? (item.distractor.field_value[metadataLanguage] || item.distractor.field_value[Object.keys(item.distractor.field_value)[0]]) : "";
                    let distractor = []; 
                        (item.distractor && item.distractor.field_value.length > 0 && item.distractor.field_value.map((keyword, DIndex) => {
                            distractor.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                            return void 0;
                    }));
                    //let interactive_media = item.interactive_media ? (item.interactive_media.field_value[metadataLanguage] || item.interactive_media.field_value[Object.keys(item.interactive_media.field_value)[0]]) : "";
                    let interactive_media = []; 
                    (item.interactive_media && item.interactive_media.field_value.length > 0 && item.interactive_media.field_value.map((keyword, DIndex) => {
                        interactive_media.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                        return void 0;
                    }));
                    //let face_expression = item.face_expression ? (item.face_expression.field_value[metadataLanguage] || item.face_expression.field_value[Object.keys(item.face_expression.field_value)[0]]) : "";
                    let face_expression = []; 
                    (item.face_expression && item.face_expression.field_value.length > 0 && item.face_expression.field_value.map((keyword, DIndex) => {
                        face_expression.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                        return void 0;
                    }));
                    //let face_view = item.face_view ? (item.face_view.field_value[metadataLanguage] || item.face_view.field_value[Object.keys(item.face_view.field_value)[0]]) : "";
                    let face_view = []; 
                    (item.face_view && item.face_view.field_value.length > 0 && item.face_view.field_value.map((keyword, DIndex) => {
                        face_view.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                        return void 0;
                    }));
                    //let body_movement = item.body_movement ? (item.body_movement.field_value[metadataLanguage] || item.body_movement.field_value[Object.keys(item.body_movement.field_value)[0]]) : "";
                    let body_movement = []; 
                    (item.body_movement && item.body_movement.field_value.length > 0 && item.body_movement.field_value.map((keyword, DIndex) => {
                        body_movement.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                        return void 0;
                    }));
                    //let gesture = item.gesture ? (item.gesture.field_value[metadataLanguage] || item.gesture.field_value[Object.keys(item.gesture.field_value)[0]]) : "";
                    let gesture = []; 
                    (item.gesture && item.gesture.field_value.length > 0 && item.gesture.field_value.map((keyword, DIndex) => {
                        gesture.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                        return void 0;
                    }));
                    //let hand_arm_movement = item.hand_arm_movement ? (item.hand_arm_movement.field_value[metadataLanguage] || item.hand_arm_movement.field_value[Object.keys(item.hand_arm_movement.field_value)[0]]) : "";
                    let hand_arm_movement = []; 
                    (item.hand_arm_movement && item.hand_arm_movement.field_value.length > 0 && item.hand_arm_movement.field_value.map((keyword, DIndex) => {
                        hand_arm_movement.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                        return void 0;
                    }));
                    //let hand_manipulation = item.hand_manipulation ? (item.hand_manipulation.field_value[metadataLanguage] || item.hand_manipulation.field_value[Object.keys(item.hand_manipulation.field_value)[0]]) : "";
                    let hand_manipulation = []; 
                    (item.hand_manipulation && item.hand_manipulation.field_value.length > 0 && item.hand_manipulation.field_value.map((keyword, DIndex) => {
                        hand_manipulation.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                        return void 0;
                    }));
                    //let head_movement = item.head_movement ? (item.head_movement.field_value[metadataLanguage] || item.head_movement.field_value[Object.keys(item.head_movement.field_value)[0]]) : "";
                    let head_movement = []; 
                    (item.head_movement && item.head_movement.field_value.length > 0 && item.head_movement.field_value.map((keyword, DIndex) => {
                        head_movement.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                        return void 0;
                    }));
                    //let eye_movement = item.eye_movement ? (item.eye_movement.field_value[metadataLanguage] || item.eye_movement.field_value[Object.keys(item.eye_movement.field_value)[0]]) : "";
                    let eye_movement = []; 
                    (item.eye_movement && item.eye_movement.field_value.length > 0 && item.eye_movement.field_value.map((keyword, DIndex) => {
                        eye_movement.push(keyword[metadataLanguage] || keyword[Object.keys(keyword)[0]]);
                        return void 0;
                    }));                                        
                    let type_of_element_label= item.type_of_element ? (item.type_of_element.field_label[metadataLanguage]||item.type_of_element.field_label["en"]): "";
                    let poses_per_subject_label= item.poses_per_subject ? (item.poses_per_subject.field_label[metadataLanguage]||item.poses_per_subject.field_label["en"]): "";
                    let body_part_label= item.body_part ? (item.body_part.field_label[metadataLanguage]||item.body_part.field_label["en"]): "";
                    let distractor_label= item.distractor ? (item.distractor.field_label[metadataLanguage]||item.distractor.field_label["en"]): "";
                    let interactive_media_label= item.interactive_media ? (item.interactive_media.field_label[metadataLanguage]||item.interactive_media.field_label["en"]): "";
                    let face_view_label= item.face_view ? (item.face_view.field_label[metadataLanguage]||item.face_view.field_label["en"]): "";
                    let face_expression_label= item.face_expression ? (item.face_expression.field_label[metadataLanguage]||item.face_expression.field_label["en"]): "";
                    let body_movement_label= item.body_movement ? (item.body_movement.field_label[metadataLanguage]||item.body_movement.field_label["en"]): "";
                    let hand_arm_movement_label= item.hand_arm_movement ? (item.hand_arm_movement.field_label[metadataLanguage]||item.hand_arm_movement.field_label["en"]): "";
                    let gesture_label= item.gesture ? (item.gesture.field_label[metadataLanguage]||item.gesture.field_label["en"]): "";
                    let hand_manipulation_label= item.hand_manipulation ? (item.hand_manipulation.field_label[metadataLanguage]||item.hand_manipulation.field_label["en"]): "";
                    let head_movement_label= item.head_movement ? (item.head_movement.field_label[metadataLanguage]||item.head_movement.field_label["en"]): "";
                    let eye_movement_label= item.eye_movement ? (item.eye_movement.field_label[metadataLanguage]||item.eye_movement.field_label["en"]): "";

                    
                    return (
                    <div id={Index} key={Index} className="padding5">
                    {type_of_element.length > 0  && <div className="padding5"><Typography className="bold-p--id">{type_of_element_label}</Typography><span className="info_value">{type_of_element.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {body_partArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{body_part_label}</Typography><span className="info_value">{body_partArray.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {distractor.length > 0  && <div className="padding5"><Typography className="bold-p--id">{distractor_label}</Typography><span className="info_value">{distractor.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {face_expression.length > 0  && <div className="padding5"><Typography className="bold-p--id">{face_expression_label}</Typography><span className="info_value">{face_expression.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {interactive_media.length > 0  && <div className="padding5"><Typography className="bold-p--id">{interactive_media_label}</Typography><span className="info_value">{interactive_media.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {face_view.length > 0 &&<div className="padding5"><Typography className="bold-p--id">{face_view_label}</Typography><span className="info_value">{face_view.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {body_movement.length > 0 &&<div className="padding5"><Typography className="bold-p--id">{body_movement_label}</Typography><span className="info_value">{body_movement.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {gesture.length > 0 &&<div className="padding5"><Typography className="bold-p--id">{gesture_label}</Typography><span className="info_value">{gesture.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {hand_arm_movement.length > 0 &&<div className="padding5"><Typography className="bold-p--id">{hand_arm_movement_label}</Typography><span className="info_value">{hand_arm_movement.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {hand_manipulation.length > 0 &&<div className="padding5"><Typography className="bold-p--id">{hand_manipulation_label}</Typography><span className="info_value">{hand_manipulation.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {head_movement.length > 0 &&<div className="padding5"><Typography className="bold-p--id">{head_movement_label}</Typography><span className="info_value">{head_movement.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {eye_movement.length > 0 &&<div className="padding5"><Typography className="bold-p--id">{eye_movement_label}</Typography><span className="info_value">{eye_movement.map((item, index) => <span key={index}>{item} </span>)}</span></div>}
                    {poses_per_subject&&<div className="padding5"><Typography className="bold-p--id">{poses_per_subject_label}</Typography><span className="info_value">{poses_per_subject}</span></div>}
                    </div> 
                )

                } )
                    
                }
            </div>
        )

    }
}