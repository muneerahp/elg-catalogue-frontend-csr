import React from "react";
import Typography from '@material-ui/core/Typography';
import WebsitesWithIcon from '../CommonComponents/WebsitesWithIcon';
import { ReactComponent as OrganizationIcon } from "./../../assets/elg-icons/buildings-modern.svg";
import Grid from '@material-ui/core/Grid';
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";


class ProjectCoordinator extends React.Component {

    render() {
        const { described_entity, metadataLanguage } = this.props;
        let organization_name = (described_entity.coordinator && described_entity.coordinator.field_value.organization_name) ? (described_entity.coordinator.field_value.organization_name.field_value[metadataLanguage] || described_entity.coordinator.field_value.organization_name.field_value[Object.keys(described_entity.coordinator.field_value.organization_name.field_value)[0]]) : "";
        let website = [];
        const full_metadata_record = described_entity.coordinator ? commonParser.getFullMetadata(described_entity.coordinator.field_value.full_metadata_record) : null;
        if (described_entity.coordinator && described_entity.coordinator.field_value.website) {
            website.push(described_entity.coordinator.field_value.website.field_value);
        }

        return (
            <>
                {described_entity.coordinator &&
                    <div className="padding15">
                        <Typography variant="h3" className="title-links">{described_entity.coordinator.field_label[metadataLanguage] || described_entity.coordinator.field_label["en"]}</Typography>
                        <div className="width60">
                            <Grid container spacing={1}>
                                <Grid item container xs={10}>
                                    <Grid item xs={4} sm={2} md={2}>
                                        <OrganizationIcon className="general-icon general-icon--grey" />
                                    </Grid>
                                    <Grid item xs={8} sm={10} md={10}>
                                        {full_metadata_record ?
                                            <div className="padding5 internal_url">
                                                <Link to={full_metadata_record.internalELGUrl}>
                                                    {organization_name}
                                                </Link>
                                            </div>
                                            :
                                            <div><Typography variant="h6" >{organization_name}</Typography></div>
                                        }
                                        <WebsitesWithIcon websiteArray={website} />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                }
            </>
        );
    }

}

export default ProjectCoordinator;