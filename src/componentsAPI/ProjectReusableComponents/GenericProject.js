import React from "react";
import Typography from '@material-ui/core/Typography';
//import GeneralIdentifier from '../CommonComponents/GeneralIdentifier';
//import WebsitesWithIcon from '../CommonComponents/WebsitesWithIcon';
//import ResourceActor from "../CommonComponents/ResourceActor";
//import Chip from '@material-ui/core/Chip';
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";

export default class GenericProject extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }
        
        if (!data.field_value) {
            return <div></div>
        }
        

        return <div>
            <Typography variant="h3" className="title-links">{data.field_label[metadataLanguage] || data.field_label[Object.keys(data.field_label)[0]]}</Typography>
            {data.field_value.map((item, itemIndex) => {
                    let project_name = item.project_name.field_value[metadataLanguage] || item.project_name.field_value[Object.keys(item.project_name.field_value)[0]];
                    //let grant_number = item.grant_number.field_value[metadataLanguage] || item.grant_number.field_value[Object.keys(item.grant_number.field_value)[0]];                    
                    //let grant_number_label = item.grant_number.field_label[metadataLanguage] || item.grant_number.field_label[Object.keys(item.grant_number.field_value)[0]];
                    //let funding_type_label = item.funding_type.field_label[metadataLanguage] || item.funding_type.field_label[Object.keys(item.funding_type.field_value)[0]];  
                    //let funding_typeArray = item.funding_type ? (item.funding_type.field_value.map(item => (item.label[metadataLanguage] || item.label[Object.keys(item.label)[0]]))) : [];                
                    //let website = item.website && item.website.field_value.length > 0 && (item.website.field_value.map(website => website) || []);
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return (
                        <div className="padding5" key={itemIndex}>
                            {full_metadata_record ?
                                    <div className="internal_url">
                                        <span><NavIcon className="xsmall-icon mr-05" /></span>
                                        <Link to={full_metadata_record.internalELGUrl}>
                                            <span className="info_value">{project_name}</span>
                                        </Link>
                                    </div> :
                                    <span className="info_value">{project_name}</span>
                                }
                            {/*<GeneralIdentifier data={data} identifier_name={"project_identifier"} identifier_scheme={"project_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            <div className="padding5"><WebsitesWithIcon websiteArray={website} /></div>
                            <div className="padding5">
                            <Typography className="bold-p--id">{grant_number_label}: {grant_number}</Typography>
                           </div>
                            <ResourceActor data={item.funder} metadataLanguage={metadataLanguage} />*/}

                            {/*funding_typeArray.length > 0 && <div><Typography className="bold-p--id">{funding_type_label}</Typography><span className="info_value">{funding_typeArray.map((item, index) => <Chip key={index} size="small" label={item} className="ChipTagGrey" />)}</span></div>*/}
                          

                            

                        </div>
                    )
                 
            })}
        </div>
    }
}