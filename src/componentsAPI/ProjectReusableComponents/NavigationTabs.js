import React from "react";
import classnames from 'classnames';
import { Nav, NavItem, NavLink } from "reactstrap";
export default class NavigationTabs extends React.Component {
    render() {
        const { activeTab } = this.props;
        const { reverse_relations } = this.props;
        const tabTitles = ["Overview"];
        if (reverse_relations) {
            tabTitles.push("Related entities");
        }
        return <div>
            <Nav tabs>
                {tabTitles.map((tabTitle, index) => <NavItem key={index}>
                    <NavLink className={classnames({ active: activeTab === `${(index + 1)}` })} onClick={() => this.props.toggleTab(`${(index + 1)}`)}>{tabTitle}</NavLink>
                </NavItem>)}
            </Nav>
        </div>
    }
}