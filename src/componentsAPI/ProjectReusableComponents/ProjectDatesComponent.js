import React from "react";
//import CustomProgressBar from './CustomProgressBar';
import Typography from '@material-ui/core/Typography';
//import Grid from '@material-ui/core/Grid';

export default class DatesComponent extends React.Component {
    render() {
        const { startDate, endDate, startDateLabel, endDateLabel } = this.props;
        return (
            <React.Fragment>

                {/*startDate && endDate &&
                    <div className="padding15">
                        <Grid container direction="row" justifyContent="space-between" alignItems="flex-start">
                            <Grid item xs={12} sm={6}>
                                <Typography className="bold-p--id padding5">{startDateLabel}</Typography>
                                <Typography className="date">{startDate}</Typography>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <Typography className="bold-p--id padding5" style={{ float: "right", clear: "both" }}>{endDateLabel} </Typography>
                                <Typography className="date" style={{ float: "right", clear: "both" }}> {endDate} </Typography>
                            </Grid>

                            <Grid item xs={12}><CustomProgressBar startDate={startDate} endDate={endDate}/> </Grid>
                        </Grid>
                    </div>

                */}

                {startDate && <div className="padding15"><Typography className="bold-p--id">{startDateLabel}</Typography>
                    <span className="info_value">{Intl.DateTimeFormat("en-GB").format(new Date(startDate))}</span></div>}

                {endDate && <div className="padding15"> <Typography className="bold-p--id">{endDateLabel} </Typography>
                    <span className="info_value"> {Intl.DateTimeFormat("en-GB").format(new Date(endDate))} </span> </div>}




            </React.Fragment>
        )
    }
}
