import React from "react";
import Typography from '@material-ui/core/Typography';
import ResourceActor from "../CommonComponents/ResourceActor";

class ProjectFunding extends React.Component { 


    render() {
        const { described_entity,metadataLanguage } = this.props;



        return (
            <React.Fragment>
                {described_entity.funder && described_entity.funder.field_value.length>0 &&
                <div className="padding15">
               {/*} <Typography variant="h2" className="padding15">{described_entity.funder.field_label[metadataLanguage] || 
                described_entity.funder.field_label["en"] }</Typography>*/}
                
                <ResourceActor data={described_entity.funder} metadataLanguage={metadataLanguage} />

                    </div>
                }

                {described_entity.funding_scheme_category && described_entity.funding_scheme_category.field_value &&
                    <div className="padding15">
                        <Typography variant="h3" className="title-links">{described_entity.funding_scheme_category.field_label[metadataLanguage] || 
                        described_entity.funding_scheme_category.field_label["en"]}</Typography>
                        <span className="info_value">{described_entity.funding_scheme_category.field_value}</span>
                    </div>
                }

                {described_entity.funding_country && described_entity.funding_country.field_value &&
                    <div className="padding15">
                        <Typography variant="h3" className="title-links">{described_entity.funding_country.field_label[metadataLanguage] || 
                            described_entity.funding_country.field_label["en"] }</Typography>
                        {described_entity.funding_country.field_value.map((fundcountry, fundcountryIndex) =>
                            <span key={fundcountryIndex} className="info_value">{fundcountry.label[metadataLanguage] || fundcountry.label[Object.keys(fundcountry.label)[0]]} &nbsp;</span>)
                        }
                    </div>
                }
                {described_entity.funding_type && described_entity.funding_type.field_value &&
                    <div className="padding15">
                        <Typography variant="h3" className="title-links">{described_entity.funding_type.field_label[metadataLanguage] || 
                            described_entity.funding_type.field_label["en"] }</Typography>
                        {described_entity.funding_type.field_value.map((fund, fundIndex) =>
                            <span key={fundIndex} className="info_value">{fund.label[metadataLanguage]||fund.label[Object.keys(fund.label)[0]]} &nbsp;</span>)
                        }
                    </div>
                } 

            </React.Fragment>
        );
    }

}

export default ProjectFunding;