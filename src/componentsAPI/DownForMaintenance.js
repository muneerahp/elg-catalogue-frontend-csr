import React from "react";
import { Helmet } from "react-helmet";
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { ReactComponent as Maintenance } from "./../assets/images/undraw_server_cluster_jwwq.svg";
import Grid from '@material-ui/core/Grid';

export default class  DownForMaintenance extends React.Component {

     

    render() {

        return  <>
            <Helmet>
                <title>ELG</title>
            </Helmet>
            <div className="fullWhiteBackground mb-2 mt-2 centered-text">
                <Container maxWidth="lg">
                    <Grid container spacing={2} direction="row" justifyContent="center" alignItems="center">
                    <Grid item xs={12} sm={12} className="content-inner mb-2" >
                        <Typography variant="h3" className="pt-1"> ELG is down for scheduled maintance.</Typography>
                        <Typography variant="body1">Our team is working hard and we expect to be back soon! Thank you for your patience.</Typography>
                        </Grid>
                        <Grid item xs={12} sm={12} className="content-inner" >
                        <Maintenance className="xlarge-icon"/>
                        </Grid>
                    </Grid>
                </Container >
                </div>
        </>
     }
}

