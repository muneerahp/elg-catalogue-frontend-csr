import React from "react";
import { FormattedMessage } from 'react-intl';
//import {FormattedHTMLMessage} from 'react-intl';
import Link from '@material-ui/core/Link';
//import "./FacetsComponent.css";
import { Button, TextField } from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import RemoveIcon from '@material-ui/icons/Remove';
import AddIcon from '@material-ui/icons/Add';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
//import { languages2Move } from "../data/MoveFacetEuropeanLanguages";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "../config/messages";
import Typography from '@material-ui/core/Typography';
import SelectAndOr from "./CommonComponents/SelectAndOr";
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const truncated_number_of_items_to_show = 5;
const filters = [
    'resource_type', 'function', 'licence',
    'condition_of_use', 'entity_type', 'language_eu',
    'language_eu_other', 'language_rest', 'intended_application',
    'linguality_type', "multilinguality_type", "media_type",
    "language_dependent", "elg_integrated_services_and_data",
    "source"
];

const SELECTION_MODE = { and: "AND", or: "OR" };

class FacetsComponentAccordion2 extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            euLanguage_searchKeyword: "", //eu_langauge_displayTextField: false,
            eu_other_Language_searchKeyword: "", //eu_other_Language_displayTextField: false,
            other_language_searchKeyword: "", //other_language_displayTextField: false,
            function_searchKeyword: "", //function_displayTextField: false,
            licence_searchKeyword: "",
            intended_application_searchKeyword: "", //intended_application_displayTextField: false,
        };
        for (let i = 0; i < filters.length; i++) {
            this.state = { ...this.state, ["items_to_show_" + filters[i]]: truncated_number_of_items_to_show };
        }
        //this.state = { ...this.state, condition_of_use_selection_mode: SELECTION_MODE.or, global_selection_mode: SELECTION_MODE.and }
        this.state = { ...this.state, condition_of_use_selection_mode: this.props.condition_of_use_mode, global_selection_mode: this.props.facet_mode }
    }


    toggleFacet = (filter_name, bucket_length) => {
        this.setState(
            {
                ['items_to_show_' + filter_name]: this.state[`items_to_show_${filter_name}`] === truncated_number_of_items_to_show ? bucket_length : truncated_number_of_items_to_show
            }
        )
    }

    handleUnselected = (index, keyword) => {
        this.props.handleUnselectedFacet(index);
        this.props.unselectFacet(keyword);
    }

    handleFacetSearch = (keyword, value, selectedFacets) => {
        const index = selectedFacets.indexOf(value);
        if (index === -1) {
            this.props.markFacetAsSelected(value);
            this.props.onSelectKeyword(keyword);
        } else {//facet is selected, now it should be unselected
            this.handleUnselected(index, keyword);
        }
    }

    clearAllFilters = () => {
        this.setState({
            euLanguage_searchKeyword: "", //eu_langauge_displayTextField: false,
            eu_other_Language_searchKeyword: "", //eu_other_Language_displayTextField: false,
            other_language_searchKeyword: "", //other_language_displayTextField: false,
            function_searchKeyword: "", //function_displayTextField: false,
            licence_searchKeyword: "",
        });
        for (let i = 0; i < filters.length; i++) {
            this.setState({ ["items_to_show_" + filters[i]]: truncated_number_of_items_to_show });
        }
        this.props.onClearAllSearchFilters();
    }

    handleSelectionModeChoice = (field, value, selectedFacets) => {
        this.setState({ [field]: value });
    }

    render() {
        let { facets, all_facet_values } = this.props;

        const selectedFacetsUriEncoded = this.props.selectedFacets || [];
        const selectedFacets = [];
        selectedFacetsUriEncoded.forEach(element => {
            selectedFacets.push(decodeURIComponent(element));
        });

        const act_as_end = this.state.global_selection_mode === SELECTION_MODE.and;

        if (!act_as_end && all_facet_values && all_facet_values.facets) {
            facets = JSON.parse(JSON.stringify(all_facet_values.facets));
        }

        let service_function_filter_selected = false;
        if (act_as_end) {
            service_function_filter_selected = selectedFacets.filter(item => item !== true && item !== false && item.indexOf("function__term::__::") >= 0).length > 0;
        } else {
            service_function_filter_selected = selectedFacets.filter(item => item !== true && item !== false && item.indexOf("function__::__::") >= 0).length > 0;
        }
        const tool_service_filter_value_selected = selectedFacets.includes("Tool/Service");

        if ((selectedFacets.includes('true') || selectedFacets.includes('false'))) {
            if (!tool_service_filter_value_selected && !service_function_filter_selected) {
                const value = selectedFacets.includes('true') ? 'true' : 'false';
                if (act_as_end) {
                    this.handleFacetSearch("language_dependent__term=" + value, value, selectedFacets)
                } else {
                    this.handleFacetSearch("language_dependent=" + value, value, selectedFacets)
                }
                return <></>;
            }
        }

        return (
            <div>
                <div>
                    {selectedFacets.length > 0 && <Button classes={{ root: 'inner-link-outlined--teal' }} endIcon={<HighlightOffIcon />} onClick={() => this.clearAllFilters()}>
                        <FormattedMessage id="facetsComponent.clearAllFilters" defaultMessage="Clear all filters" >
                        </FormattedMessage>
                    </Button>}
                </div>

                <div style={{background:"#fff", borderRadius:"4px", marginBottom:"1em"}}>
                    <SelectAndOr selectionMode={SELECTION_MODE} handleSelectionModeChoice={this.handleSelectionModeChoice} selectedFacets={selectedFacets || []} facet_values={[]} initialValue={this.state.global_selection_mode} field="global_selection_mode" />
                </div>


                {facets._filter_resource_type.resource_type.buckets.length > 0 &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_resource_type.resource_type.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 || false}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-resource_type"
                                id="panel1a-header-resource_type"
                            >
                                <h4><FormattedMessage id="facetsComponent.resourceType" defaultMessage="Language Resources & Technologies" >
                                </FormattedMessage></h4>
                            </AccordionSummary>
                            <AccordionDetails>

                                <ul className="facets-list">
                                    {facets._filter_resource_type.resource_type.buckets.slice(0, this.state['items_to_show_resource_type']).map((resource_type, index) =>
                                        <li className="FacetButton" button="true" key={resource_type.key} onClick={(e) => this.handleFacetSearch(act_as_end ? ("resource_type__term=" + resource_type.key) : ("resource_type=" + resource_type.key), resource_type.key, selectedFacets)} >
                                            {selectedFacets.indexOf(resource_type.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {resource_type.key} </span>
                                            <span className="count" > ({resource_type.doc_count}) </span>

                                        </li>)
                                    }
                                    {facets._filter_resource_type.resource_type.buckets.length > truncated_number_of_items_to_show &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={(e) => this.toggleFacet('resource_type', facets._filter_resource_type.resource_type.buckets.length)} >
                                                {this.state["items_to_show_resource_type"] > truncated_number_of_items_to_show ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }


                {facets._filter_function.function.buckets.length > 0 &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_function.function.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 || false}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-function"
                                id="panel1a-header-function"
                            >
                                <h4><FormattedMessage id="facetsComponent.Functions" defaultMessage="Service functions" >
                                </FormattedMessage></h4>
                            </AccordionSummary>
                            <AccordionDetails>
                                <div className="id--small" >
                                    {/*(!this.state.function_displayTextField && !this.state.function_searchKeyword) && <span onMouseEnter={(e) => { this.setState({ function_displayTextField: true }) }}><SearchIcon /></span>*/}
                                    {//(this.state.function_displayTextField || this.state.function_searchKeyword) && 
                                        <TextField
                                            helperText="Type to narrow down service functions"
                                            //placeholder="Type to narrow down service functions"
                                            //label="Type to narrow down service functions"
                                            variant="outlined"
                                            style={{ "width": "100%", marginLeft: "1px", paddingBottom: "20px" }}
                                            size="small"
                                            value={this.state.function_searchKeyword} onChange={(e) => { this.setState({ function_searchKeyword: e.target.value }) }}
                                            InputProps={{
                                                endAdornment: (
                                                    <SearchIcon />
                                                )
                                            }}
                                        //onMouseOut={(e) => { this.setState({ function_displayTextField: false }) }}
                                        />}
                                </div>

                                <ul className="facets-list">
                                    {facets._filter_function.function.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.function_searchKeyword.toLocaleLowerCase()) >= 0).slice(0, this.state["items_to_show_function"]).map((functionService, index) =>
                                        <li className="filterItem" button="true" key={functionService.key} onClick={() => this.handleFacetSearch(act_as_end ? ("function__term=" + functionService.key) : ("function=" + functionService.key), act_as_end ? ("function__term::__::" + functionService.key) : ("function__::__::" + functionService.key), selectedFacets)}>
                                            {/*selectedFacets.indexOf(functionService.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>*/}
                                            {(selectedFacets.indexOf("function__term::__::" + functionService.key) !== -1 || (selectedFacets.indexOf("function__::__::" + functionService.key) !== -1)) ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {functionService.key} </span>
                                            <span className="count" > ({functionService.doc_count}) </span>
                                        </li>)
                                    }
                                    {facets._filter_function.function.buckets.length > truncated_number_of_items_to_show &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={(e) => this.toggleFacet('function', facets._filter_function.function.buckets.length)} >
                                                {this.state["items_to_show_function"] > truncated_number_of_items_to_show ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>}
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }

                {facets._filter_intended_application.intended_application.buckets.length > 0 &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_intended_application.intended_application.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 || false}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-intended_application"
                                id="panel1a-header-intended_application"
                            >
                                <Tooltip title={messages.intended_application_hover_text}><h4><FormattedMessage id="facetsComponent.intended_application" defaultMessage={`${messages.intended_application_hover_text}`} >
                                </FormattedMessage></h4></Tooltip>
                            </AccordionSummary>
                            <AccordionDetails>
                                {false && <Typography variant="caption">{messages.intended_application_hover_text}</Typography>}
                                <div className="id--small" >
                                    {/*(!this.state.intended_application_displayTextField && !this.state.intended_application_searchKeyword) && <span onMouseEnter={(e) => { this.setState({ intended_application_displayTextField: true }) }}><SearchIcon /></span>*/}
                                    {//(this.state.intended_application_displayTextField || this.state.intended_application_searchKeyword) && 
                                        <TextField
                                            helperText="Type to narrow down intended application"
                                            //placeholder="Type to narrow down intended application"
                                            //label="Type to narrow down intended application"
                                            variant="outlined"
                                            style={{ "width": "100%", marginLeft: "1px", paddingBottom: "20px" }}
                                            size="small"
                                            value={this.state.intended_application_searchKeyword} onChange={(e) => { this.setState({ intended_application_searchKeyword: e.target.value }) }}
                                            InputProps={{
                                                endAdornment: (
                                                    <SearchIcon />
                                                )
                                            }}
                                        //onMouseOut={(e) => { this.setState({ intended_application_displayTextField: false }) }}
                                        />}
                                </div>
                                <ul className="facets-list">
                                    {facets._filter_intended_application.intended_application.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.intended_application_searchKeyword.toLocaleLowerCase()) >= 0).slice(0, this.state["items_to_show_intended_application"]).map((intended_application, index) =>
                                        <li className="FacetButton" button="true" key={intended_application.key} onClick={(e) => this.handleFacetSearch(act_as_end ? ("intended_application__term=" + intended_application.key) : ("intended_application=" + intended_application.key), intended_application.key, selectedFacets)} >
                                            {selectedFacets.indexOf(intended_application.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {intended_application.key} </span>
                                            <span className="count" > ({intended_application.doc_count}) </span>
                                        </li>)
                                    }
                                    {facets._filter_intended_application.intended_application.buckets.length > truncated_number_of_items_to_show &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={(e) => this.toggleFacet('intended_application', facets._filter_intended_application.intended_application.buckets.length)} >
                                                {this.state['items_to_show_intended_application'] > truncated_number_of_items_to_show ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }

                <div style={{ marginBottom: "1em" }}>
                    <Accordion className="facets__accordion languages"
                        defaultExpanded={
                            facets._filter_language_eu.language_eu.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 ||
                            //facets._filter_language_eu_other.language_eu_other.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 ||
                            facets._filter_language_rest.language_rest.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 ||
                            facets?._filter_language_dependent?.language_dependent?.buckets?.filter(item => selectedFacets.indexOf(item.key_as_string) !== -1).length > 0
                        }>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon className="grey--font" />}
                            aria-controls="panel1a-content-languages"
                            id="panel1a-header-languages"
                        >
                            <h4>Languages</h4>
                        </AccordionSummary>
                        <AccordionDetails>
                            {(tool_service_filter_value_selected || service_function_filter_selected) && facets._filter_language_dependent.language_dependent.buckets.length > 0 && <div>
                                {<Typography variant="caption">{messages.language_dependent_hover_text}</Typography>}
                                <ul className="facets-list">
                                    {facets._filter_language_dependent.language_dependent.buckets.slice(0, this.state["items_to_show_language_dependent"]).map((language_dependent, index) =>
                                        <li className="FacetButton" button="true" key={language_dependent.key} onClick={(e) => this.handleFacetSearch(act_as_end ? ("language_dependent__term=" + language_dependent.key_as_string) : "language_dependent=" + language_dependent.key_as_string, language_dependent.key_as_string, selectedFacets)} >
                                            {selectedFacets.indexOf(language_dependent.key_as_string) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {language_dependent.key_as_string === "true" ? "language dependent" : "language independent"} </span>
                                            <span className="count" > ({language_dependent.doc_count}) </span>
                                        </li>)
                                    }
                                    {facets._filter_language_dependent.language_dependent.buckets.length > truncated_number_of_items_to_show &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={(e) => this.toggleFacet('language_dependent', facets._filter_language_dependent.language_dependent.buckets.length)} >
                                                {this.state['items_to_show_language_dependent'] > truncated_number_of_items_to_show ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </div>}
                            {facets._filter_language_eu.language_eu.buckets.length > 0 &&
                                <div style={{ marginBottom: "1em" }}>
                                    <div className="id--small" >
                                        {/*<span className="id--small">Official EU languages</span>*/}
                                        {/*(!this.state.eu_langauge_displayTextField && !this.state.euLanguage_searchKeyword) && <span onMouseEnter={(e) => { this.setState({ eu_langauge_displayTextField: true }) }}><SearchIcon /></span>*/}
                                        {//(this.state.eu_langauge_displayTextField || this.state.euLanguage_searchKeyword) && 
                                            <TextField
                                                style={{ "width": "100%", marginLeft: "1px", paddingBottom: "20px" }}
                                                helperText="Type to narrow down Official EU languages"
                                                placeholder="Official EU languages"
                                                size="small"
                                                value={this.state.euLanguage_searchKeyword} onChange={(e) => { this.setState({ euLanguage_searchKeyword: e.target.value }) }}
                                                InputProps={{
                                                    endAdornment: (
                                                        <SearchIcon />
                                                    )
                                                }}
                                            //onMouseOut={(e) => { this.setState({ eu_langauge_displayTextField: false }) }}
                                            />}
                                    </div>

                                    <ul className={(this.state['items_to_show_language_eu'] && facets._filter_language_eu.language_eu.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.euLanguage_searchKeyword.toLowerCase()) >= 0).slice(0, this.state["items_to_show_language_eu"]).length > truncated_number_of_items_to_show) ? "facets-list scroll-facet" : "facets-list"}>
                                        {facets._filter_language_eu.language_eu.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.euLanguage_searchKeyword.toLowerCase()) >= 0).slice(0, this.state["items_to_show_language_eu"]).map((language, index) =>

                                            <li className="FacetButton" button="true" key={language.key}
                                                onClick={() => {
                                                    this.setState({ euLanguage_searchKeyword: "", eu_other_Language_searchKeyword: "", other_language_searchKeyword: "" });
                                                    this.handleFacetSearch(act_as_end ? ("language__term=" + language.key) : ("language=" + language.key), language.key, selectedFacets)
                                                    //this.handleFacetSearch("language__term=" + language.key, language.key, selectedFacets)
                                                }}>
                                                {selectedFacets.indexOf(language.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                                <span className="FacetText" > {language.key} </span>
                                                <span className="count" > ({language.doc_count}) </span>
                                            </li>)
                                        }
                                        {facets._filter_language_eu.language_eu.buckets.length > truncated_number_of_items_to_show &&
                                            <div className="ShowMore">
                                                <Link component="button" onClick={() => this.toggleFacet("language_eu", facets._filter_language_eu.language_eu.buckets.length)} >
                                                    {this.state["items_to_show_language_eu"] > truncated_number_of_items_to_show ? (<span>
                                                        <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                        </FormattedMessage>
                                                    </span>) : (<span>
                                                        <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                        </FormattedMessage>
                                                    </span>)}
                                                </Link>
                                            </div>
                                        }
                                    </ul>
                                </div>
                            }

                            {facets._filter_language_rest.language_rest.buckets.length > 0 &&
                                <div style={{ marginBottom: "1em" }}>
                                    <div className="id--small" >
                                        {/*<span className="id--small">Other languages</span>*/}
                                        {/*(!this.state.other_language_displayTextField && !this.state.other_language_searchKeyword) && <span onMouseEnter={(e) => { this.setState({ other_language_displayTextField: true }) }}><SearchIcon /></span>*/}
                                        {//(this.state.other_language_displayTextField || this.state.other_language_searchKeyword) && 
                                            <TextField
                                                style={{ "width": "100%", marginLeft: "1px", paddingBottom: "20px" }}
                                                helperText="Type to narrow down Other languages"
                                                placeholder="Other languages"
                                                size="small"
                                                value={this.state.other_language_searchKeyword} onChange={(e) => { this.setState({ other_language_searchKeyword: e.target.value }) }}
                                                InputProps={{
                                                    endAdornment: (
                                                        <SearchIcon />
                                                    )
                                                }}
                                            //onMouseOut={(e) => { this.setState({ other_language_displayTextField: false }) }}
                                            />}
                                    </div>
                                    <ul className={(this.state["items_to_show_language_rest"] && facets._filter_language_rest.language_rest.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.other_language_searchKeyword.toLowerCase()) >= 0).slice(0, this.state["items_to_show_language_rest"]).length > truncated_number_of_items_to_show) ? "facets-list scroll-facet" : "facets-list"}>
                                        {facets._filter_language_rest.language_rest.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.other_language_searchKeyword.toLowerCase()) >= 0).slice(0, this.state["items_to_show_language_rest"]).map((language_rest, index) =>
                                            <li className="FacetButton" button="true" key={language_rest.key}
                                                onClick={() => {
                                                    this.setState({ euLanguage_searchKeyword: "", eu_other_Language_searchKeyword: "", other_language_searchKeyword: "" });
                                                    this.handleFacetSearch(act_as_end ? ("language__term=" + language_rest.key) : ("language=" + language_rest.key), language_rest.key, selectedFacets)
                                                }}>
                                                {selectedFacets.indexOf(language_rest.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                                <span className="FacetText" > {language_rest.key} </span>
                                                <span className="count" > ({language_rest.doc_count}) </span>
                                            </li>)
                                        }
                                        {facets._filter_language_rest.language_rest.buckets.length > truncated_number_of_items_to_show &&
                                            <div className="ShowMore">
                                                <Link component="button" onClick={() => this.toggleFacet('language_rest', facets._filter_language_rest.language_rest.buckets.length)} >
                                                    {this.state["items_to_show_language_rest"] > truncated_number_of_items_to_show ? (<span>
                                                        <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                        </FormattedMessage>
                                                    </span>) : (<span>
                                                        <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                        </FormattedMessage>
                                                    </span>)}
                                                </Link>
                                            </div>
                                        }
                                    </ul>
                                </div>
                            }

                        </AccordionDetails>
                    </Accordion>
                </div>

                {
                    facets._filter_media_type.media_type.buckets.length > 0 &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_media_type.media_type.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 || false}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-media_type"
                                id="panel1a-header-media_type"
                            >
                                <Tooltip title={messages.media_type_hover_text}><h4><FormattedMessage id="facetsComponent.media_type_hover_text" defaultMessage={`${messages.media_type_hover_text}`} >
                                </FormattedMessage></h4></Tooltip>
                            </AccordionSummary>
                            <AccordionDetails>
                                {false && <Typography variant="caption">{messages.media_type_hover_text}</Typography>}
                                <ul className="facets-list">
                                    {facets._filter_media_type.media_type.buckets.slice(0, this.state["items_to_show_media_type"]).map((media_type, index) =>
                                        <li className="FacetButton" button="true" key={media_type.key} onClick={(e) => this.handleFacetSearch(act_as_end ? ("media_type__term=" + media_type.key) : ("media_type=" + media_type.key), media_type.key, selectedFacets)} >
                                            {selectedFacets.indexOf(media_type.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {media_type.key} </span>
                                            <span className="count" > ({media_type.doc_count}) </span>
                                        </li>)
                                    }
                                    {facets._filter_media_type.media_type.buckets.length > truncated_number_of_items_to_show &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={(e) => this.toggleFacet('media_type', facets._filter_media_type.media_type.buckets.length)} >
                                                {this.state['items_to_show_media_type'] > truncated_number_of_items_to_show ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }



                {
                    facets._filter_licence.licence.buckets.length > 0 &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_licence.licence.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 || false}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-licence"
                                id="panel1a-header-licence"
                            >
                                <h4><FormattedMessage id="facetsComponent.licences" defaultMessage="Licences" >
                                </FormattedMessage></h4>
                            </AccordionSummary>
                            <AccordionDetails>
                                <div className="id--small" >
                                    {/*(!this.state.licence_displayTextField && !this.state.licence_searchKeyword) && <div  className="mb05" onMouseEnter={(e) => { this.setState({ licence_displayTextField: true }) }}><SearchIcon/></div>*/}
                                    {//(this.state.licence_displayTextField || this.state.licence_searchKeyword) && 
                                        <TextField
                                            helperText="Type to narrow down service licences"
                                            //placeholder="Type to narrow down service licences"
                                            //label="Type to narrow down service licences"
                                            variant="outlined"
                                            style={{ "width": "100%", marginLeft: "1px", paddingBottom: "20px" }}
                                            size="small"
                                            value={this.state.licence_searchKeyword} onChange={(e) => { this.setState({ licence_searchKeyword: e.target.value }) }}
                                            InputProps={{
                                                endAdornment: (
                                                    <SearchIcon />
                                                )
                                            }}
                                        //onMouseOut={(e) => { this.setState({ licence_displayTextField: false }) }}
                                        />}
                                </div>
                                <ul className="facets-list">
                                    {facets._filter_licence.licence.buckets.filter(item => item.key.toLowerCase().indexOf(this.state.licence_searchKeyword.toLocaleLowerCase()) >= 0).slice(0, this.state["items_to_show_licence"]).map((license, index) =>
                                        <li className="FacetButton" button="true" key={license.key} onClick={() => this.handleFacetSearch(act_as_end ? ("licence__term=" + license.key) : ("licence=" + license.key), license.key, selectedFacets)} >
                                            {selectedFacets.indexOf(license.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {license.key} </span>
                                            <span className="count" > ({license.doc_count}) </span>
                                        </li>)
                                    }
                                    {facets._filter_licence.licence.buckets.length > truncated_number_of_items_to_show &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={(e) => this.toggleFacet('licence', facets._filter_licence.licence.buckets.length)} >
                                                {this.state["items_to_show_licence"] > truncated_number_of_items_to_show ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }

                {
                    facets._filter_condition_of_use.condition_of_use.buckets.length > 0 &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_condition_of_use.condition_of_use.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 || false}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-condition_of_use"
                                id="panel1a-header-condition_of_use"
                            >
                                <Tooltip title={messages.condition_of_use_hover_text}><h4><FormattedMessage id="facetsComponent.conditionΟfUse" defaultMessage="Conditions of use for data" >
                                </FormattedMessage></h4></Tooltip>
                            </AccordionSummary>
                            <AccordionDetails>
                                {<Typography variant="caption">{messages.condition_of_use_hover_text}</Typography>}
                                {/*<SelectAndOr selectionMode={SELECTION_MODE} handleSelectionModeChoice={this.handleSelectionModeChoice} selectedFacets={selectedFacets || []} facet_values={facets?._filter_condition_of_use?.condition_of_use?.buckets || []} initialValue={this.state.condition_of_use_selection_mode} field="condition_of_use_selection_mode" />*/}
                                <ul className="facets-list">
                                    {facets._filter_condition_of_use.condition_of_use.buckets.slice(0, this.state["items_to_show_condition_of_use"]).map((condition_of_use, index) =>
                                        <li className="FacetButton" button="true" key={condition_of_use.key}>
                                            {/*selectedFacets.indexOf(condition_of_use.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>*/}
                                            <span className="FacetText" >
                                                <FormControlLabel
                                                    control={<Checkbox
                                                        checked={selectedFacets.indexOf(condition_of_use.key) !== -1}
                                                        color="primary"
                                                        inputProps={{ 'aria-label': `condition-of-use-${index}` }}
                                                        //onChange={(e) => this.handleFacetSearch(`${this.state.condition_of_use_selection_mode === SELECTION_MODE.and ? 'condition_of_use__term' : 'condition_of_use'}=` + condition_of_use.key, condition_of_use.key, selectedFacets)}
                                                        onChange={(e) => this.handleFacetSearch(`${this.state.global_selection_mode === SELECTION_MODE.and ? 'condition_of_use__term' : 'condition_of_use'}=` + condition_of_use.key, condition_of_use.key, selectedFacets)}
                                                    />}
                                                    label={condition_of_use.key}
                                                />
                                            </span>
                                            <span className="count" > ({condition_of_use.doc_count}) </span>
                                        </li>)
                                    }
                                    {facets._filter_condition_of_use.condition_of_use.buckets.length > truncated_number_of_items_to_show &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={(e) => this.toggleFacet('condition_of_use', facets._filter_condition_of_use.condition_of_use.buckets.length)} >
                                                {this.state['items_to_show_condition_of_use'] > truncated_number_of_items_to_show ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }


                {
                    facets._filter_entity_type.entity_type.buckets.length > 0 && !(facets._filter_entity_type.entity_type.buckets.length === 1 && facets._filter_entity_type.entity_type.buckets[0].key === "LanguageResource") &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_entity_type.entity_type.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 || false}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-entity_type"
                                id="panel1a-header-entity_type"
                            >
                                <h4><FormattedMessage id="facetsComponent.entityType" defaultMessage="Related Enitites" >
                                </FormattedMessage></h4>
                            </AccordionSummary>
                            <AccordionDetails>
                                <ul className="facets-list">
                                    {facets._filter_entity_type.entity_type.buckets.slice(0, this.state["items_to_show_entity_type"]).map((entity_type, index) =>
                                        entity_type.key !== "LanguageResource" && <li className="FacetButton" button="true" key={entity_type.key} onClick={() => this.handleFacetSearch(act_as_end ? ("entity_type__term=" + entity_type.key) : ("entity_type=" + entity_type.key), entity_type.key, selectedFacets)} >
                                            {selectedFacets.indexOf(entity_type.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {entity_type.key}</span>
                                            <span className="count" > ({entity_type.doc_count}) </span>
                                        </li>)
                                    }
                                    {facets._filter_entity_type.entity_type.buckets.length > truncated_number_of_items_to_show &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={(e) => this.toggleFacet("entity_type", facets._filter_entity_type.entity_type.buckets.length)} >
                                                {this.state['items_to_show_entity_type'] > truncated_number_of_items_to_show ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }

                {
                    facets._filter_elg_integrated_services_and_data.elg_integrated_services_and_data.buckets.length > 0 &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_elg_integrated_services_and_data.elg_integrated_services_and_data.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 || false}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-elg_hosted_data"
                                id="panel1a-header-elg_hosted_data"
                            >
                                <Tooltip title={messages.elg_integrated_services_and_data_hover_text}>
                                    <h4>
                                        <FormattedMessage id="facetsComponent.elg_integrated_services_and_data_hover_text" defaultMessage={messages.elg_integrated_services_and_data_hover_text} ></FormattedMessage>
                                    </h4>
                                </Tooltip>
                            </AccordionSummary>
                            <AccordionDetails>
                                <ul className="facets-list">
                                    {facets._filter_elg_integrated_services_and_data.elg_integrated_services_and_data.buckets.slice(0, this.state["items_to_show_elg_integrated_services_and_data"]).map((elg_integrated_services_and_data, index) =>
                                        <li className="FacetButton" button="true" key={elg_integrated_services_and_data.key} onClick={() => this.handleFacetSearch(act_as_end ? ("elg_integrated_services_and_data__term=" + elg_integrated_services_and_data.key) : ("elg_integrated_services_and_data=" + elg_integrated_services_and_data.key), elg_integrated_services_and_data.key, selectedFacets)} >
                                            {selectedFacets.indexOf(elg_integrated_services_and_data.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {elg_integrated_services_and_data.key}</span>
                                            <span className="count" > ({elg_integrated_services_and_data.doc_count}) </span>
                                        </li>)
                                    }
                                    {facets._filter_elg_integrated_services_and_data.elg_integrated_services_and_data.buckets.length > truncated_number_of_items_to_show &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={(e) => this.toggleFacet("elg_integrated_services_and_data", facets._filter_elg_integrated_services_and_data.elg_integrated_services_and_data.buckets.length)} >
                                                {this.state['items_to_show_elg_integrated_services_and_data'] > truncated_number_of_items_to_show ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }

                {
                    facets._filter_source.source.buckets.length > 0 &&
                    <div style={{ marginBottom: "1em" }}>
                        <Accordion className="facets__accordion" defaultExpanded={facets._filter_source.source.buckets.filter(item => selectedFacets.indexOf(item.key) !== -1).length > 0 || false}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon className="grey--font" />}
                                aria-controls="panel1a-content-source"
                                id="panel1a-header-source"
                            >
                                <Tooltip title={messages.source_hover_text}>
                                    <h4>
                                        <FormattedMessage id="facetsComponent.source_facet_text" defaultMessage={messages.source_facet_text} ></FormattedMessage>
                                    </h4>
                                </Tooltip>
                            </AccordionSummary>
                            <AccordionDetails>

                                <ul className="facets-list">
                                    {facets._filter_source.source.buckets.slice(0, this.state['items_to_show_source']).map((source, index) =>
                                        <li className="FacetButton" button="true" key={source.key} onClick={(e) => this.handleFacetSearch(act_as_end ? ("source__term=" + source.key) : ("source=" + source.key), source.key, selectedFacets)} >
                                            {selectedFacets.indexOf(source.key) !== -1 ? <span className="FacetIcon" > <RemoveIcon /></span> : <span className="FacetIcon" > <AddIcon /></span>}
                                            <span className="FacetText" > {source.key} </span>
                                            <span className="count" > ({source.doc_count}) </span>

                                        </li>)
                                    }
                                    {facets._filter_source.source.buckets.length > truncated_number_of_items_to_show &&
                                        <div className="ShowMore">
                                            <Link component="button" onClick={(e) => this.toggleFacet('source', facets._filter_source.source.buckets.length)} >
                                                {this.state["items_to_show_source"] > truncated_number_of_items_to_show ? (<span>
                                                    <FormattedMessage id="facetsComponent.showLess" defaultMessage="Show less" >
                                                    </FormattedMessage>
                                                </span>) : (<span>
                                                    <FormattedMessage id="facetsComponent.showMore" defaultMessage="Show more" >
                                                    </FormattedMessage>
                                                </span>)}
                                            </Link>
                                        </div>
                                    }
                                </ul>
                            </AccordionDetails>
                        </Accordion>
                    </div>
                }

                {false && <div style={{ marginBottom: "1em" }}>
                    <Accordion className="facets__accordion" defaultExpanded={false}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon className="grey--font" />}
                            aria-controls="panel1a-content-source"
                            id="panel1a-header-select-and-or"
                        >
                            <h4>Selection behaviour</h4>
                        </AccordionSummary>
                        <AccordionDetails>
                            <ul className="facets-list">
                                <li>
                                    <SelectAndOr selectionMode={SELECTION_MODE} handleSelectionModeChoice={this.handleSelectionModeChoice} selectedFacets={selectedFacets || []} facet_values={[]} initialValue={this.state.global_selection_mode} field="global_selection_mode" />
                                </li>
                            </ul>
                        </AccordionDetails>
                    </Accordion>
                </div>
                }

            </div >
        )
    }
}

export default FacetsComponentAccordion2;