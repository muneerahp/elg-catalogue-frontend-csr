
import React from 'react';
import { Redirect, withRouter } from "react-router-dom";
import axios from "axios";
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Helmet } from "react-helmet";
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
//import { ReactComponent as BackIcon } from "./../assets/elg-icons/navigation-arrows-left-1.svg";
import { UPLOAD_XML_PERMITTED_ROLES, UPLOAD_XML_METADATARECORD_ENDPOINT, BATCH_UPLOAD_XML_METADATARECORD_ENDPOINT, AUTHENTICATED_KEYCLOAK_USER_ROLES, getAuthorizationHeader } from "../config/constants";
import { PROXY_2_S3 } from "../config/constants";
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
import { toast } from "react-toastify";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SimpleTabPanel from "./CustomVerticalTabs/SimpleTabPanel";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { Alert, AlertTitle } from '@material-ui/lab';
import CircularProgress from '@material-ui/core/CircularProgress';
import Box from '@material-ui/core/Box';
import DashboardAppBar from "../DashboardComponents/DashboardAppBar";
import messages from "../config/messages";

registerPlugin(FilePondPluginImagePreview, FilePondPluginFileValidateType);



function CircularProgressWithLabel(props) {
  return (
    <Box position="relative" display="inline-flex">
      <CircularProgress variant="static" {...props} />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Typography variant="caption" component="div" color="textSecondary">{`${Math.round(
          props.value,
        )}%`}</Typography>
      </Box>
    </Box>
  );
}

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const xml = ["text/xml, application/xml"];
const zipTypes = ["application/gzip", "application/tar", "application/tar+gzip", "application/zip", "application/octet-stream", "application/x-zip-compressed", "multipart/x-zip"];

function getSteps() {
  //return ['Upload metadata file'];
  return ['Upload metadata file', 'Upload Data File'];
}

function getStepContent(step) {
  switch (step) {
    case 0:
      return `Upload an XML file compatible with the ELG metadata schema`;
    case 1:
      return 'You can now upload a Data File in .zip format.';
    default:
      return 'Unknown step';
  }
}

class UploadWithSteps extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activeStep: 0, UserRoles: [], xmlResponseData: "", error: "", proxyError: "", tab: 0, under_construction: false, functional_service: false, zipFiles: [], bePatientMessage: false, dummyProgressValue: 1 };
    this.intervalId = 1;
  }

  componentDidMount() {
    const { keycloak } = this.props;
    this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
  }

  toggleTab = (tabIndex) => {
    this.setState({ tab: tabIndex, activeStep: 0, xmlResponseData: "", error: "", proxyError: "", under_construction: false, functional_service: false, bePatientMessage: false, dummyProgressValue: 1 });
  }

  handleNext = () => {
    this.setState({ activeStep: this.state.activeStep + 1 });
  }

  handleBack = () => {
    this.setState({ activeStep: this.state.activeStep - 1 });
  }

  handleReset = () => {
    this.setState({ activeStep: 0, xmlResponseData: "", error: "", proxyError: "", under_construction: false, functional_service: false, bePatientMessage: false, dummyProgressValue: 1 });
  }

  isAuthorizedToView = (roles) => {
    var isAuthorized = false;
    UPLOAD_XML_PERMITTED_ROLES.forEach(item => {
      if (roles.includes(item)) {
        isAuthorized = true;
      }
    })
    return isAuthorized;
  }

  handle_Simple_and_batcch_XMLError = (errorResponse) => {
    clearInterval(this.intervalId);
    this.setState({ bePatientMessage: false, dummyProgressValue: 1 });
    let obj = {};
    try {
      obj = JSON.parse(errorResponse);
    } catch (err) {
      obj.message = "Internal server error";
    }
    const format = JSON.stringify(obj, null, 2);
    this.setState({ activeStep: 0, xmlResponseData: "", error: format, proxyError: "" });
    toast.error("Upload failed.", { autoClose: 3500 });
  }

  handleSimpleXMLResponse = (response) => {
    const xmlResponse = JSON.parse(response);
    this.setState({ xmlResponseData: xmlResponse, activeStep: this.state.activeStep + 1, error: "", proxyError: "", under_construction: false, functional_service: false });
    toast.success("File has been successfully uploaded.", { autoClose: 3500 });
  }

  batch_handleXMLResponse = (response) => {
    clearInterval(this.intervalId);
    this.setState({ bePatientMessage: false, dummyProgressValue: 1 });
    const jsonResponse = JSON.parse(response);
    const errorMessages = [];
    for (let i = 0; i < jsonResponse.length; i++) {
      const recordName = jsonResponse[i].record;
      const isSuccessfull = jsonResponse[i].metadata.pk;
      if (!isSuccessfull) {
        const obj = { "file": recordName, errors: jsonResponse[i].metadata };
        errorMessages.push(obj);
      }
    }
    if (errorMessages.length === 0) {
      //increase active step by 2 for batch xml, because we don't want to display an option to upload a dataset
      this.setState({ xmlResponseData: jsonResponse, activeStep: this.state.activeStep + 2, error: "", proxyError: "", under_construction: false, functional_service: false });
      toast.success("All files have been successfully uploaded.", { autoClose: 3500 });
    } else {
      const format = JSON.stringify(errorMessages, null, 2);
      //increase active step by 2 for batch xml, because we don't want to display an option to upload a dataset
      this.setState({ error: format, proxyError: "", activeStep: this.state.activeStep + 2, });
      toast.error(`Found ${errorMessages.length}/${jsonResponse.length} files with errors`, { autoClose: 3500 });
    }
  }

  handleS3Proxy = (response) => {
    toast.success("File has been successfully uploaded.", { autoClose: 3500 });
    this.setState({ activeStep: this.state.activeStep + 1, xmlResponseData: "", error: "", proxyError: "" });
  }

  handleS3ProxyError = (responseError) => {
    toast.error("Upload to proxy failed.", { autoClose: 3500 });
    try {
      if (responseError && responseError.indexOf("html") >= 0) {//render html response page as error report
        this.setState({ proxyError: responseError });
      } else {
        this.setState({ error: responseError });//render json as error report
      }
      console.log(JSON.stringify(responseError));
    } catch (err) {
    }
  }

  handleUnderConstruction = (e) => {
    this.setState({ under_construction: e.target.checked });
  }

  hanldeFunctionalService = (e) => {
    this.setState({ functional_service: e.target.checked });
  }

  ShowErrorMessage(error) {
    return <div>
      {error && <div style={{ marginBottom: "100px", paddingBottom: "100px" }}>
        <h3>Error</h3>
        <div className=" boxed">
          <Paper elevation={13} >
            <code>
              <pre id="special">
                {error}
              </pre>
            </code>
          </Paper>
        </div>
      </div>
      }
    </div>
  }

  notAuthorized = () => {
    return <div>
      {this.PageHeader()}
      <div>You do not have sufficient rights.</div>
    </div>
  }

  PageHeader = () => {
    return <div>
      <Helmet>
        <title>ELG - Upload </title>
      </Helmet>
      <DashboardAppBar {...this.props} />
      <div className="editor-container-white">
        <Container maxWidth="lg">
          <div className="empty"></div>
          <Typography className="dashboard-title-box pb-3">
            Upload records
                </Typography>

        </Container>
      </div>
    </div>

  }

  upload_process_zip = (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
    const formData = new FormData();
    formData.append("file", file, file.name);
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    //this.can = source;//to cancel the request since abort doesn't work
    axios.put(PROXY_2_S3, file, {
      headers: {
        'Content-Type': 'application/octet-stream',
        'Authorization': getAuthorizationHeader(this.props.keycloak),
        'ELG-RESOURCE-ID': this.state.xmlResponseData.pk,//1009
        'scope': "private",
        'filetype': "und",
        'filename': "und"
      },
      cancelToken: source.token,
      onUploadProgress: (e) => {
        progress(e.lengthComputable, e.loaded, e.total);
      }
    }).then(response => {
      load(response);
      this.setState({ zipFiles: [] });
      this.handleS3Proxy(response);
    }).catch(err => {
      console.log("error, ", err);
      this.setState({ zipFiles: [] });
      error(err);
      if (axios.isCancel(err)) {
        console.log('Request canceled', err.message);
      } else {
      } this.handleS3ProxyError(err)
    });
    return {
      abort: () => {
        source.cancel('Operation canceled by the user.');
        abort();
      }
    }
  }

  render() {
    if (!this.props.keycloak || !this.props.keycloak.authenticated) {
      return <Redirect to="/" />
    }
    const { activeStep } = this.state;
    const steps = getSteps();
    if (!this.isAuthorizedToView(this.state.UserRoles)) {
      return this.notAuthorized();
    }
    return (
      <>
        {this.PageHeader()}

        <Container maxWidth="lg" className="upload botomMargin boxed mt2">

          <Tabs value={this.state.tab} onChange={this.toggleTab} aria-label="simple tabs example" className="simple-tabs-forms">
            <Tab label="UPLOAD SINGLE ITEM" {...a11yProps(0)} onClick={() => { this.toggleTab(0); }} />
            <Tab label="UPLOAD MULTIPLE ITEMS" {...a11yProps(1)} onClick={() => { this.toggleTab(1); }} />
          </Tabs>

          <SimpleTabPanel value={this.state.tab} index={0} className="upload--outer">
            <Stepper activeStep={activeStep} alternativeLabel>
              {steps.map((label, index) => {
                return (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                );
              })}
            </Stepper>

            {this.state.activeStep === 0 &&
              <div>
                <Grid container direction="row" justifyContent="center" alignItems="center" spacing={2}>
                  <Grid item container xs={9} className="mb2" spacing={1}>
                    <Grid item xs={4}>
                      <FormControlLabel className="upload-checkboxes p-05 wd-100"
                        control={<Checkbox
                          checked={this.state.under_construction === true ? true : false}
                          color="primary"
                          inputProps={{ 'aria-label': 'under construction checkbox' }}
                          onChange={e => { this.handleUnderConstruction(e) }}
                        />}
                        label="Under construction"
                      />
                    </Grid>

                    <Grid item xs={8}>
                      <Typography variant="subtitle1"> If the metadata record is for a resource that you plan to deliver later, please check the "under construction" box.</Typography>
                    </Grid>

                    <Grid item xs={4}>
                      <FormControlLabel className="upload-checkboxes p-05 wd-100"
                        control={<Checkbox
                          checked={this.state.functional_service === true ? true : false}
                          color="primary"
                          inputProps={{ 'aria-label': 'functional service checkbox' }}
                          onChange={e => { this.hanldeFunctionalService(e) }}
                        />}
                        label="Functional service" />
                    </Grid>

                    <Grid item xs={8}>
                      <Typography variant="subtitle1">If the metadata record is for a service to be integrated in ELG <span><a href={messages.dialog_functional_service_infoLink_message} target="_blank" rel="noopener noreferrer">({messages.dialog_functional_service_infoLink_message})</a></span>, please check the box "functional service". </Typography>
                    </Grid>
                  </Grid>
                </Grid>

                <FilePond
                  credits={false}
                  allowMultiple={false}
                  name="file"
                  instantUpload={false}
                  acceptedFileTypes={xml}
                  labelIdle={'Drag & Drop your file or <span class="filepond--label-action"> Browse </span>'}
                  server={
                    {
                      url: UPLOAD_XML_METADATARECORD_ENDPOINT,
                      process: {
                        method: 'POST',
                        headers: {
                          'Authorization': getAuthorizationHeader(this.props.keycloak),
                          'UNDER-CONSTRUCTION': this.state.under_construction === true ? true : false,
                          'FUNCTIONAL-SERVICE': this.state.functional_service === true ? true : false
                        },
                        onload: (response) => this.handleSimpleXMLResponse(response),
                        onerror: (response) => this.handle_Simple_and_batcch_XMLError(response),
                      },
                    }
                  }
                  onremovefile={(file) => { this.setState({ error: "" }); xml[0] = "text/xml"; xml[1] = "application/xml" }}
                />
              </div>
            }

            {this.state.activeStep === 1 && this.state.xmlResponseData && this.state.xmlResponseData.hasOwnProperty("pk") &&// deactivate second step
              <FilePond
                credits={false}
                allowMultiple={false}
                name="file"
                instantUpload={false}
                acceptedFileTypes={zipTypes}
                ref={ref => (this.pond = ref)}
                files={this.state.zipFiles}
                onupdatefiles={(fileItems) => {
                  this.setState({
                    zipFiles: fileItems.map(fileItem => fileItem.file)
                  });
                }}
                server={
                  {
                    url: PROXY_2_S3,
                    process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => { this.upload_process_zip(fieldName, file, metadata, load, error, progress, abort, transfer, options) },
                    revert: null,
                    load: null,
                    restore: null,
                    fetch: null,
                    onload: (response) => this.handleS3Proxy(response),
                    onerror: (response) => this.handleS3ProxyError(response),
                  }
                }
              />
            }

            <div className="upload-container">
              {activeStep === steps.length ? (
                <div className="padding15">

                  <Typography className="padding15">All steps completed</Typography>
                  <Button classes={{ root: 'inner-link-outlined--teal' }} onClick={this.handleReset}>New upload </Button>

                  <Paper><hr /></Paper>

                  <Typography>
                    Please go to my items to edit the metadata record and submit it for publication
                  </Typography>
                  <Button classes={{ root: 'inner-link-outlined--teal' }} onClick={() => { this.props.history.push("/myItems") }}>Go to my items </Button>

                </div>
              ) : (
                <div className="padding15" style={{ textAlign: "center" }}>
                  <Typography variant="h4" className="mb2">{getStepContent(activeStep)}</Typography>
                  {this.state.activeStep !== 0 &&
                    <span className="pr10 mt2">
                      <Button disabled={activeStep === 0} classes={{ root: 'inner-link-outlined--teal' }} onClick={this.handleBack} > Back</Button>
                    </span>
                  }
                  {this.state.activeStep !== 0 &&
                    <span className="pr10 mt2">
                      <Button classes={{ root: 'inner-link-outlined--teal' }} onClick={this.handleNext}> {activeStep === steps.length - 1 ? 'Finish' : 'Next'} </Button>
                    </span>
                  }
                </div>
              )}
            </div>

            {this.state.error && this.ShowErrorMessage(this.state.error)}
            {this.state.proxyError && <div dangerouslySetInnerHTML={{ __html: this.state.proxyError }} />}

          </SimpleTabPanel>

          <SimpleTabPanel value={this.state.tab} index={1} className="upload--outer">
            <div>
              <div className="centered-text mb2"><Typography variant="subtitle1">You can upload one <span className="bold-titles">zip</span> file each time (with multiple XML files).</Typography></div>
              <Grid container direction="row" justifyContent="center" alignItems="center" spacing={2}>
                <Grid item container xs={9} className="mb2" spacing={1}>
                  <Grid item xs={4}>
                    <FormControlLabel className="upload-checkboxes p-05 wd-100"
                      control={<Checkbox
                        checked={this.state.under_construction === true ? true : false}
                        color="primary"
                        inputProps={{ 'aria-label': 'under construction checkbox' }}
                        onChange={e => { this.handleUnderConstruction(e) }}
                      />}
                      label="Under construction"
                    />
                  </Grid>
                  <Grid item xs={8}>
                    <Typography variant="subtitle1">If the metadata records are for resources that you plan to deliver later, please check the "under construction" box.</Typography>
                  </Grid>
                  <Grid item xs={4}>
                    <FormControlLabel className="upload-checkboxes p-05 wd-100"
                      control={<Checkbox
                        checked={this.state.functional_service === true ? true : false}
                        color="primary"
                        inputProps={{ 'aria-label': 'functional service checkbox' }}
                        onChange={e => { this.hanldeFunctionalService(e) }}
                      />}
                      label="Functional service"
                    />
                  </Grid>
                  <Grid item xs={8}>
                    <Typography variant="subtitle1">If the metadata records are for services to be integrated in ELG <span><a href={messages.dialog_functional_service_infoLink_message} target="_blank" rel="noopener noreferrer">({messages.dialog_functional_service_infoLink_message})</a></span>, please check the box "functional service". </Typography>
                  </Grid>
                </Grid>
              </Grid>

              {
                this.state.activeStep === 0 && <FilePond
                  credits={false}
                  allowMultiple={false}
                  onprocessfileabort={() => {
                    clearInterval(this.intervalId);
                    this.setState({ bePatientMessage: false, dummyProgressValue: 1 });
                  }}
                  name="file"
                  instantUpload={false}
                  labelIdle={'Drag & Drop your file or <span class="filepond--label-action"> Browse </span>'}
                  server={
                    {
                      url: BATCH_UPLOAD_XML_METADATARECORD_ENDPOINT,
                      process: {
                        method: 'POST',
                        //timeout:18000
                        headers: {
                          'Authorization': getAuthorizationHeader(this.props.keycloak),
                          'UNDER-CONSTRUCTION': this.state.under_construction === true ? true : false,
                          'FUNCTIONAL-SERVICE': this.state.functional_service === true ? true : false
                        },
                        onload: (response) => this.batch_handleXMLResponse(response),
                        onerror: (response) => this.handle_Simple_and_batcch_XMLError(response),
                        ondata: (formData) => {
                          this.intervalId = setInterval(() => { this.setState({ dummyProgressValue: this.state.dummyProgressValue + 1 }) }, 5000);
                          this.setState({ bePatientMessage: true })
                          this.setState({ error: "" });
                          return formData;
                        },
                      },
                    }
                  }
                />
              }

              <div className="upload-container">
                {
                  activeStep === steps.length &&
                  <div className="padding15">
                    <Typography className="padding15"> The upload of your file has been completed and it will now be processed by the system. When the processing is completed, you will receive an email informing you of their successful import or of any validation errors. Following actions are described in the email.</Typography>
                    <Button classes={{ root: 'inner-link-outlined--teal' }} onClick={this.handleReset}>New upload </Button>

                    {/*<Paper><hr /></Paper>

                    <Typography>
                      Please go to my items to edit the metadata record and submit it for publication
                  </Typography>
                    <Button classes={{ root: 'inner-link-outlined--teal' }} onClick={() => { this.props.history.push("/myItems") }}>Go to my items </Button>*/}
                  </div>
                }
                {this.state.bePatientMessage && <Alert severity="info">
                  <AlertTitle><CircularProgressWithLabel value={this.state.dummyProgressValue} /></AlertTitle>
                  <div>Depending on the number of files that you are submitting this may take a while. Please be patient.
                  </div>
                </Alert>}
              </div>

              {this.state.error && this.ShowErrorMessage(this.state.error)}
            </div>
          </SimpleTabPanel>

        </Container>
      </>
    );
  }
}

export default withRouter(UploadWithSteps);