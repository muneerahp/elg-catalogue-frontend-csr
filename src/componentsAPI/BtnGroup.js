import React from "react";
import PropTypes from 'prop-types';
import ListIcon from '@material-ui/icons/List';
import BorderAllIcon from '@material-ui/icons/BorderAll';

const BtnGroup = (props) => {
    return (
        <div className="btn-group" >
            <span onClick={props.handleList} id="list" className="btn btn-default btn-xs" style={{ cursor: props.listView ? "not-allowed" : '' }}>
                <ListIcon color={props.listView ? "disabled" : 'inherit'}></ListIcon>
            </span>
            <span onClick={props.handleGrid} id="grid" className="btn btn-default btn-xs" style={{ cursor: !props.listView ? "not-allowed" : '' }}>
                <BorderAllIcon color={!props.listView ? "disabled" : 'inherit'}></BorderAllIcon>
            </span>
        </div>
    );
};

BtnGroup.propTypes = {
    handleList: PropTypes.func,
    handleGrid: PropTypes.func,
};

export default BtnGroup;