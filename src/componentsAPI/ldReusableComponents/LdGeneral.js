import React from "react";
import Typography from '@material-ui/core/Typography';
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";

export default class LdGeneral extends React.Component {
    componentDidMount() {
        const { personal_data_included, personal_data_details, sensitive_data_included, sensitive_data_details, anonymized, anonymization_details, data } = this.props;
        const { is_analysed_by, is_edited_by, is_elicited_by, is_converted_version_of, time_coverage, geographic_coverage, register } = data.described_entity.field_value.lr_subclass.field_value || [];

        if (!data) {
            return false;
        }
        const { displayTabGeneral } = this.props;
        if ((personal_data_included !== undefined && personal_data_included) ||
            (personal_data_details) ||
            (sensitive_data_included !== undefined && sensitive_data_included) ||
            (sensitive_data_details) ||
            (anonymized !== undefined && anonymized) ||
            (anonymization_details) ||
            (is_analysed_by && is_analysed_by.field_value.length > 0) ||
            (is_edited_by && is_edited_by.field_value.length > 0) ||
            (is_elicited_by && is_elicited_by.field_value.length > 0) ||
            (is_converted_version_of && is_converted_version_of.field_value.length > 0) ||
            (time_coverage && time_coverage.field_value.length > 0) ||
            (geographic_coverage && geographic_coverage.field_value.length > 0) ||
            (register && register.field_value.length > 0)
        ) {
            displayTabGeneral ? void 0 : this.props.displayTabGeneralFunction(true);
            //console.log(personal_data_included , sensitive_data_included)
        }
    }

    render() {
        const { personal_data_included, personal_data_details, sensitive_data_included, sensitive_data_details, anonymized, anonymization_details, data, metadataLanguage } = this.props;
        const { peresonal_data_included_label, peresonal_data_details_label, sensitive_data_included_label, sensitive_data_details_label, anonymized_label, anonymization_details_label } = this.props;
        const { is_analysed_by, is_edited_by, is_elicited_by, is_converted_version_of, time_coverage, geographic_coverage } = data.described_entity.field_value.lr_subclass.field_value || [];

        if (!data) {
            return <div></div>
        }
        return <div>


            {personal_data_included !== undefined && personal_data_included && <div className="padding5"><Typography className="bold-p--id">{peresonal_data_included_label}</Typography><span className="info_value">{`${personal_data_included}`}</span></div>}

            {personal_data_details && <div className="padding5"><Typography className="bold-p--id">{peresonal_data_details_label}</Typography><span className="info_value">{personal_data_details}</span></div>}

            {sensitive_data_included !== undefined && sensitive_data_included && <div className="padding5"><Typography className="bold-p--id">{sensitive_data_included_label}</Typography><span className="info_value">{`${sensitive_data_included}`}</span></div>}

            {sensitive_data_details && <div className="padding5"><Typography className="bold-p--id">{sensitive_data_details_label}</Typography> <span className="info_value">{sensitive_data_details}</span></div>}

            {anonymized !== undefined && anonymized !== null && <div className="padding5"><Typography className="bold-p--id">{anonymized_label}</Typography><span className="info_value">{`${anonymized}`}</span></div>}

            {anonymization_details && <div className="padding5"><Typography className="bold-p--id">{anonymization_details_label}</Typography><span className="info_value">{anonymization_details}</span> </div>}



            {is_analysed_by && is_analysed_by.field_value.length > 0 && <div className="padding5"> <Typography className="bold-p--id">{is_analysed_by.field_label[metadataLanguage] || is_analysed_by.field_label["en"]}</Typography>
                {is_analysed_by && is_analysed_by.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    let version = item.version ? item.version.field_value : "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="padding5 internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name}, {version}</span>
                                </Link>
                            </div> :
                            <span className="info_value">{resource_name}, {version}</span>
                        }
                    </div>
                })}</div>}
            {is_edited_by && is_edited_by.field_value.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_edited_by.field_label[metadataLanguage] || is_edited_by.field_label["en"]}</Typography>
                {is_edited_by && is_edited_by.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    let version = item.version ? item.version.field_value : "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="padding5 internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name}, {version}</span>
                                </Link>
                            </div> :
                            <span className="info_value">{resource_name}, {version}</span>
                        }
                    </div>
                })}</div>}
            {is_elicited_by && is_elicited_by.field_value.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_elicited_by.field_label[metadataLanguage] || is_elicited_by.field_label["en"]}</Typography>
                {is_elicited_by && is_elicited_by.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    let version = item.version ? item.version.field_value : "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="padding5 internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name}, {version}</span>
                                </Link>
                            </div> :
                            <span className="info_value">{resource_name}, {version}</span>
                        }
                    </div>
                })}</div>}
            {is_converted_version_of && is_converted_version_of.field_value.length > 0 && <div className="padding5"><Typography className="bold-p--id">{is_converted_version_of.field_label[metadataLanguage] || is_converted_version_of.field_label["en"]}</Typography>
                {is_converted_version_of && is_converted_version_of.field_value.map((item, index) => {
                    let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]) : "";
                    let version = item.version ? item.version.field_value : "";
                    let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                    return <div key={index}>
                        {full_metadata_record ?
                            <div className="padding5 internal_url">
                                <span><NavIcon className="xsmall-icon mr-05" /></span>
                                <Link to={full_metadata_record.internalELGUrl}>
                                    <span className="info_value">{resource_name}, {version}</span>
                                </Link>
                            </div> :
                            <span className="info_value">{resource_name}, {version}</span>
                        }
                    </div>
                })}</div>}
            {time_coverage && time_coverage.field_value.length > 0 && <div className="padding5"> <Typography className="bold-p--id">{time_coverage.field_label[metadataLanguage] || time_coverage.field_label["en"]}</Typography>
                {time_coverage && time_coverage.field_value.map((item, index) => {
                    let cov = item[metadataLanguage] || item[Object.keys(item)[0]];
                    return <div key={index}>
                        <span className="info_value">{cov}</span>
                    </div>
                })}</div>}
            {geographic_coverage && geographic_coverage.field_value.length > 0 && <div className="padding5"> <Typography className="bold-p--id">{geographic_coverage.field_label[metadataLanguage] || geographic_coverage.field_label["en"]}</Typography>
                {geographic_coverage && geographic_coverage.field_value.map((item, index) => {
                    let cov = item[metadataLanguage] || item[Object.keys(item)[0]];
                    return <div key={index}>
                        <span className="info_value">{cov}</span>
                    </div>
                })}</div>}
        </div>
    }
}