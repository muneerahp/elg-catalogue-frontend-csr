import React from "react";
import Typography from '@material-ui/core/Typography';
import ldLrSubclassParser from '../../parsers/ldLrSubclassParser';
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";
import commonParser from "../../parsers/CommonParser";
import { Link } from "react-router-dom";

export default class LdMediaPart extends React.Component {

    render() {
        const { data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }

        const lr_subclass = data.described_entity.field_value.lr_subclass ? data.described_entity.field_value.lr_subclass.field_value : null;
        if (!lr_subclass) {
            return <div></div>
        }
        //console.log(lr_subclass)
        //const lr_type = ldLrSubclassParser.getLrtype(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getLrtype(lr_subclass, metadataLanguage).value : "";
        //const lr_type_label = ldLrSubclassParser.getLrtype(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getLrtype(lr_subclass, metadataLanguage).label : "";
        const encodinglevelArray = ldLrSubclassParser.getEncodingLevel(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getEncodingLevel(lr_subclass, metadataLanguage).value : [];
        const encodinglevelArrayLabel = ldLrSubclassParser.getEncodingLevel(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getEncodingLevel(lr_subclass, metadataLanguage).label : "";
        const theoretic_model = ldLrSubclassParser.getTheoreticModel(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getTheoreticModel(lr_subclass, metadataLanguage).value : [];
        const theoretic_model_label = ldLrSubclassParser.getTheoreticModel(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getTheoreticModel(lr_subclass, metadataLanguage).label : "";
        const baseitemArray = ldLrSubclassParser.getBaseItem(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getBaseItem(lr_subclass, metadataLanguage).value : [];
        const baseitemArrayLabel = ldLrSubclassParser.getBaseItem(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getBaseItem(lr_subclass, metadataLanguage).label : "";
        const perplexity = ldLrSubclassParser.getPerplexity(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getPerplexity(lr_subclass, metadataLanguage).value : "";
        const perplexityLabel = ldLrSubclassParser.getPerplexity(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getPerplexity(lr_subclass, metadataLanguage).label : "";
        const order = ldLrSubclassParser.getOrder(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getOrder(lr_subclass, metadataLanguage).value : "";
        const orderLabel = ldLrSubclassParser.getOrder(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getOrder(lr_subclass, metadataLanguage).label : "";
        const is_factored = ldLrSubclassParser.getIsFactored(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getIsFactored(lr_subclass, metadataLanguage).value : "";
        const is_factoredLabel = ldLrSubclassParser.getIsFactored(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getIsFactored(lr_subclass, metadataLanguage).label : "";
        const smoothing = ldLrSubclassParser.getSmoothing(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getSmoothing(lr_subclass, metadataLanguage).value : "";
        const smoothingLabel = ldLrSubclassParser.getSmoothing(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getSmoothing(lr_subclass, metadataLanguage).label : "";
        const interpolated = ldLrSubclassParser.getInterpolated(lr_subclass) ? ldLrSubclassParser.getInterpolated(lr_subclass).value : "";
        const interpolatedLabel = ldLrSubclassParser.getInterpolated(lr_subclass) ? ldLrSubclassParser.getInterpolated(lr_subclass).label : "";
        const factorArray = ldLrSubclassParser.getFactor(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getFactor(lr_subclass, metadataLanguage).value : [];
        const factorArrayLabel = ldLrSubclassParser.getFactor(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getFactor(lr_subclass, metadataLanguage).label : "";
        const running_environment_details = ldLrSubclassParser.getrunning_environment_details(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getrunning_environment_details(lr_subclass, metadataLanguage).value : "";
        const running_environment_detailsLabel = ldLrSubclassParser.getrunning_environment_details(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getrunning_environment_details(lr_subclass, metadataLanguage).label : "";
        const required_hardwareArray = ldLrSubclassParser.getRequiredHardware(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getRequiredHardware(lr_subclass, metadataLanguage).value : [];
        const required_hardwareArrayLabel = ldLrSubclassParser.getRequiredHardware(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getRequiredHardware(lr_subclass, metadataLanguage).label : "";
        const requires_softwareArray = ldLrSubclassParser.getRequiresSoftware(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getRequiresSoftware(lr_subclass, metadataLanguage).value : [];
        const requires_softwareArrayLabel = ldLrSubclassParser.getRequiresSoftware(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getRequiresSoftware(lr_subclass, metadataLanguage).label : "";
        const formalism = ldLrSubclassParser.getFormalism(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getFormalism(lr_subclass, metadataLanguage).value : "";
        const formalismLabel = ldLrSubclassParser.getFormalism(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getFormalism(lr_subclass, metadataLanguage).label : "";
        const grammatical_phenomena_coverageArray = ldLrSubclassParser.getgrammatical_phenomena_coverage(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getgrammatical_phenomena_coverage(lr_subclass, metadataLanguage).value : [];
        const grammatical_phenomena_coverageArrayLabel = ldLrSubclassParser.getgrammatical_phenomena_coverage(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getgrammatical_phenomena_coverage(lr_subclass, metadataLanguage).label : "";
        const ld_taskArray = ldLrSubclassParser.getld_task(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getld_task(lr_subclass, metadataLanguage).value : [];
        const ld_taskArrayLabel = ldLrSubclassParser.getld_task(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getld_task(lr_subclass, metadataLanguage).label : "";
        const weighted_grammar = ldLrSubclassParser.getweighted_grammar(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getweighted_grammar(lr_subclass, metadataLanguage).value : "";
        const weighted_grammarLabel = ldLrSubclassParser.getweighted_grammar(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getweighted_grammar(lr_subclass, metadataLanguage).label : "";
        const model_variant = ldLrSubclassParser.getmodel_variant(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getmodel_variant(lr_subclass, metadataLanguage).value : "";
        const model_variantLabel = ldLrSubclassParser.getmodel_variant(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getmodel_variant(lr_subclass, metadataLanguage).label : "";
        //const ml_framework = ldLrSubclassParser.getml_framework(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getml_framework(lr_subclass, metadataLanguage).value : "";
        //const ml_frameworkLabel = ldLrSubclassParser.getml_framework(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getml_framework(lr_subclass, metadataLanguage).label : "";
        const development_framework = ldLrSubclassParser.getdevelopment_framework(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getdevelopment_framework(lr_subclass, metadataLanguage).value : "";
        const development_frameworkLabel = ldLrSubclassParser.getdevelopment_framework(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getdevelopment_framework(lr_subclass, metadataLanguage).label : "";
        const method = ldLrSubclassParser.getmethod(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getmethod(lr_subclass, metadataLanguage).value : "";
        const methodLabel = ldLrSubclassParser.getmethod(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getmethod(lr_subclass, metadataLanguage).label : "";
        const algorithm_details = ldLrSubclassParser.getalgorithm_details(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getalgorithm_details(lr_subclass, metadataLanguage).value : "";
        const algorithm_detailsLabel = ldLrSubclassParser.getalgorithm_details(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getalgorithm_details(lr_subclass, metadataLanguage).label : "";
        const algorithm = ldLrSubclassParser.getalgorithm(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getalgorithm(lr_subclass, metadataLanguage).value : "";
        const algorithmLabel = ldLrSubclassParser.getalgorithm(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getalgorithm(lr_subclass, metadataLanguage).label : "";
        const training_corpus_details = ldLrSubclassParser.gettraining_corpus_details(lr_subclass, metadataLanguage) ? ldLrSubclassParser.gettraining_corpus_details(lr_subclass, metadataLanguage).value : "";
        const training_corpus_detailsLabel = ldLrSubclassParser.gettraining_corpus_details(lr_subclass, metadataLanguage) ? ldLrSubclassParser.gettraining_corpus_details(lr_subclass, metadataLanguage).label : "";
        const training_process_details = ldLrSubclassParser.gettraining_process_details(lr_subclass, metadataLanguage) ? ldLrSubclassParser.gettraining_process_details(lr_subclass, metadataLanguage).value : "";
        const training_process_detailsLabel = ldLrSubclassParser.gettraining_process_details(lr_subclass, metadataLanguage) ? ldLrSubclassParser.gettraining_process_details(lr_subclass, metadataLanguage).label : "";
        const bias_details = ldLrSubclassParser.getbias_details(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getbias_details(lr_subclass, metadataLanguage).value : "";
        const bias_detailsLabel = ldLrSubclassParser.getbias_details(lr_subclass, metadataLanguage) ? ldLrSubclassParser.getbias_details(lr_subclass, metadataLanguage).label : "";
        const {has_original_source} = lr_subclass.language_description_subclass ? lr_subclass.language_description_subclass.field_value : "" ; 
        

        if (!lr_subclass) {
            return <div></div>
        }

        /*const showArea = model_variant  || method || algorithm_details || algorithm || training_corpus_details || (development_framework && development_framework.length>0) ||
            (formalism || weighted_grammar || (encodinglevelArray && encodinglevelArray.length > 0) ||
                (grammatical_phenomena_coverageArray && grammatical_phenomena_coverageArray.length > 0)) || (order || is_factored || smoothing || interpolated || (baseitemArray && baseitemArray.length > 0) || (perplexity) ||
                    (factorArray && factorArray.length > 0)) || (ld_taskArray && ld_taskArray.length > 0) || (complieswithArray && complieswithArray.length > 0) || (theoretic_model && theoretic_model.length > 0) ||
            running_environment_details || (requires_softwareArray && requires_softwareArray.length > 0) || (required_hardwareArray && required_hardwareArray.length > 0) ||
            (lr_subclass.language_description_subclass && lr_subclass.language_description_subclass.field_value.typesystem && lr_subclass.language_description_subclass.field_value.typesystem.field_value.length > 0) ||
            (lr_subclass.language_description_subclass && lr_subclass.language_description_subclass.field_value.annotation_schema && lr_subclass.language_description_subclass.field_value.annotation_schema.field_value.length > 0) ||
            (lr_subclass.language_description_subclass && lr_subclass.language_description_subclass.field_value.tagset && lr_subclass.language_description_subclass.field_value.tagset.field_value.length > 0);
        */
       
        return <> {true ?
            <div className="padding15">
                {(model_variant || method || algorithm_details || algorithm || training_corpus_details || training_process_details || bias_details) && <Typography variant="h3" className="title-links">ML model details</Typography>}
                {/* {lr_type && <div className="padding5"><Typography className="bold-p--id">{lr_type_label}</Typography><span className="info_value">{lr_type}</span></div>}*/}
                {model_variant && <div className="padding5"><Typography className="bold-p--id">{model_variantLabel}</Typography><span className="info_value">{model_variant}</span></div>}
                {development_framework && development_framework.length > 0 && <div className="padding5"><Typography className="bold-p--id">{development_frameworkLabel}</Typography>
                    {development_framework.map((item, frIndex) => {
                        return <div key={frIndex}>
                            <span className="info_value">{item}</span>
                        </div>
                    })}</div>
                }
                {method && <div className="padding5"><Typography className="bold-p--id">{methodLabel}</Typography><span className="info_value">{method}</span></div>}
                {algorithm_details && <div className="padding5"><Typography className="bold-p--id">{algorithm_detailsLabel}</Typography><span className="info_value">{algorithm_details}</span></div>}
                {algorithm && <div className="padding5"><Typography className="bold-p--id">{algorithmLabel}</Typography><span className="info_value">{algorithm}</span></div>}
                {training_corpus_details && <div className="padding5"><Typography className="bold-p--id">{training_corpus_detailsLabel}</Typography><span className="info_value">{training_corpus_details}</span></div>}
                {training_process_details && <div className="padding5"><Typography className="bold-p--id">{training_process_detailsLabel}</Typography><span className="info_value">{training_process_details}</span></div>}
                {bias_details && <div className="padding5"><Typography className="bold-p--id">{bias_detailsLabel}</Typography><span className="info_value">{bias_details}</span></div>}

                {(formalism ||
                    weighted_grammar ||
                    (encodinglevelArray && encodinglevelArray.length > 0) ||
                    (grammatical_phenomena_coverageArray && grammatical_phenomena_coverageArray.length > 0)) &&
                    <Typography variant="h3" className="title-links">Grammar details</Typography>}
                {formalism && <div className="padding5"><Typography className="bold-p--id">{formalismLabel}</Typography><span className="info_value">{formalism}</span></div>}
                {weighted_grammar && <div className="padding5"><Typography className="bold-p--id">{weighted_grammarLabel}</Typography><span className="info_value">{`${weighted_grammar}`}</span></div>}
                {encodinglevelArray && encodinglevelArray.length > 0 &&
                    <div className="padding5"><Typography className="bold-p--id">{encodinglevelArrayLabel}</Typography>
                        {encodinglevelArray.map((item, encodingIndex) => {
                            return <div key={encodingIndex}>
                                <span className="info_value">{item}</span>
                            </div>
                        })} </div>}

                {grammatical_phenomena_coverageArray && grammatical_phenomena_coverageArray.length > 0 &&
                    <div className="padding5"><Typography className="bold-p--id">{grammatical_phenomena_coverageArrayLabel}</Typography>
                        {grammatical_phenomena_coverageArray.map((item, gIndex) => {
                            return <div key={gIndex}>
                                <span className="info_value">{item}</span>
                            </div>
                        })} </div>}

                {(order ||
                    is_factored ||
                    smoothing ||
                    interpolated ||
                    (baseitemArray && baseitemArray.length > 0) ||
                    (perplexity) ||
                    (factorArray && factorArray.length > 0)) &&
                    <Typography variant="h3" className="title-links">N-gram model details</Typography>}
                {order !== undefined && <div className="padding5"><Typography className="bold-p--id">{orderLabel}</Typography><span className="info_value">{order}</span></div>}
                {is_factored && <div className="padding5"><Typography className="bold-p--id">{is_factoredLabel}</Typography><span className="info_value">{`${is_factored}`}</span></div>}
                {smoothing && <div className="padding5"><Typography className="bold-p--id">{smoothingLabel}</Typography><span className="info_value">{smoothing}</span></div>}
                {interpolated && <div className="padding5"><Typography className="bold-p--id">{interpolatedLabel}</Typography><span className="info_value">{`${interpolated}`}</span></div>}

                {baseitemArray && baseitemArray.length > 0 &&
                    <div className="padding5"><Typography className="bold-p--id">{baseitemArrayLabel}</Typography>
                        {baseitemArray.map((item, baseIndex) => {
                            return <div key={baseIndex}>
                                <span className="info_value">{item}</span>
                            </div>
                        })} </div>}

                {perplexity &&
                    <div className="padding5"><Typography className="bold-p--id">{perplexityLabel}</Typography>
                        <span className="info_value">{perplexity}</span>
                    </div>}

                {factorArray && factorArray.length > 0 &&
                    <div className="padding5"><Typography className="bold-p--id">{factorArrayLabel}</Typography>
                        {factorArray.map((item, factorIndex) => {
                            return <div key={factorIndex}>
                                <span className="info_value">{item}</span>
                            </div>
                        })} </div>}

                {ld_taskArray && ld_taskArray.length > 0 &&
                    <div className="padding5"><Typography className="bold-p--id">{ld_taskArrayLabel}</Typography>
                        {ld_taskArray.map((item, ldIndex) => {
                            return <div key={ldIndex}>
                                <span className="info_value">{item}</span>
                            </div>
                        })} </div>}

                {theoretic_model && theoretic_model.length > 0 &&
                    <div className="padding5"><Typography className="bold-p--id">{theoretic_model_label}</Typography>
                        {theoretic_model.map((item, tIndex) => {
                            return <div key={tIndex}>
                                <span className="info_value">{item}</span>
                            </div>
                        })} </div>}

                {running_environment_details && <div className="padding5"><Typography className="bold-p--id">{running_environment_detailsLabel}</Typography><span className="info_value">{running_environment_details}</span></div>}

                {requires_softwareArray && requires_softwareArray.length > 0 &&
                    <div className="padding5"><Typography className="bold-p--id">{requires_softwareArrayLabel}</Typography>
                        {requires_softwareArray.map((item, softIndex) => {
                            return <div key={softIndex}>
                                <span className="info_value">{item}</span>
                            </div>
                        })} </div>}

                {required_hardwareArray && required_hardwareArray.length > 0 &&
                    <div className="padding5"><Typography className="bold-p--id">{required_hardwareArrayLabel}</Typography>
                        {required_hardwareArray.map((item, hardIndex) => {
                            return <div key={hardIndex}>
                                <span className="info_value">{item}</span>
                            </div>
                        })} </div>}

                {lr_subclass.language_description_subclass && lr_subclass.language_description_subclass.field_value.typesystem && lr_subclass.language_description_subclass.field_value.typesystem.field_value.length > 0 &&
                    <div className="padding5"><Typography className="bold-p--id">{lr_subclass.language_description_subclass.field_value.typesystem.field_label[metadataLanguage] || lr_subclass.language_description_subclass.field_value.typesystem.field_label["en"]}</Typography>
                        {lr_subclass.language_description_subclass.field_value.typesystem.field_value.map((item, Index) => {
                            let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                            if (full_metadata_record) {
                                return <div className="padding5 internal_url" key={Index}>
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        <span>
                                            {item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]} - {item.version?.field_value || ""}
                                        </span>
                                    </Link>
                                </div>
                            }
                            return <div key={Index}>
                                <Typography className="bold-p--id">{item.resource_name.field_label[metadataLanguage] || item.resource_name.field_label["en"]}</Typography>
                                <span className="info_value">{item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]}</span>
                                <Typography className="bold-p--id">{item.version.field_label[metadataLanguage] || item.version.field_label["en"]}</Typography>
                                <span className="info_value">{item.version?.field_value || ""}</span>
                                <Typography className="bold-p--id">{item.lr_identifier.field_label[metadataLanguage] || item.lr_identifier.field_label["en"]}</Typography>
                                {item.lr_identifier && item.lr_identifier.field_value.length > 0 && item.lr_identifier.field_value.map((lridentifier, lrIndex) => {
                                    if (lridentifier.lr_identifier_scheme.label["en"] !== "ELG") {
                                        return <div key={lrIndex}>
                                            <a href={lridentifier.lr_identifier_scheme.field_value} target="blank">{lridentifier.value.field_value}</a>
                                        </div>
                                    }
                                    return void 0;
                                })}
                            </div>
                        })}</div>}

                {lr_subclass.language_description_subclass && lr_subclass.language_description_subclass.field_value.annotation_schema && lr_subclass.language_description_subclass.field_value.annotation_schema.field_value.length > 0 &&
                    <div className="padding5"><Typography className="bold-p--id">{lr_subclass.language_description_subclass.field_value.annotation_schema.field_label[metadataLanguage] || lr_subclass.language_description_subclass.field_value.annotation_schema.field_label["en"]}</Typography>
                        {lr_subclass.language_description_subclass.field_value.annotation_schema.field_value.map((item, Index) => {
                            let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                            if (full_metadata_record) {
                                return <div className="padding5 internal_url" key={Index}>
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        <span>
                                            {item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]} - {item.version?.field_value || ""}
                                        </span>
                                    </Link>
                                </div>
                            }
                            return <div key={Index}>
                                <Typography className="bold-p--id">{item.resource_name.field_label[metadataLanguage] || item.resource_name.field_label["en"]}</Typography>
                                <span className="info_value">{item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]}</span>
                                <Typography className="bold-p--id">{item.version.field_label[metadataLanguage] || item.version.field_label["en"]}</Typography>
                                <span className="info_value">{item.version?.field_value || ""}</span>
                                <Typography className="bold-p--id">{item.lr_identifier.field_label[metadataLanguage] || item.lr_identifier.field_label["en"]}</Typography>
                                {item.lr_identifier && item.lr_identifier.field_value.length > 0 && item.lr_identifier.field_value.map((lridentifier, lrIndex) => {
                                    if (lridentifier.lr_identifier_scheme.label["en"] !== "ELG") {
                                        return <div key={lrIndex}>
                                            <a href={lridentifier.lr_identifier_scheme.field_value} target="blank">{lridentifier.value.field_value}</a>
                                        </div>
                                    }
                                    return void 0;
                                })}
                            </div>
                        })}</div>}

                {lr_subclass.language_description_subclass && lr_subclass.language_description_subclass.field_value.annotation_resource && lr_subclass.language_description_subclass.field_value.annotation_resource.field_value.length > 0 &&
                    <div className="padding5"><Typography className="bold-p--id">{lr_subclass.language_description_subclass.field_value.annotation_resource.field_label[metadataLanguage] || lr_subclass.language_description_subclass.field_value.annotation_resource.field_label["en"]}</Typography>
                        {lr_subclass.language_description_subclass.field_value.annotation_resource.field_value.map((item, Index) => {
                            let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                            if (full_metadata_record) {
                                return <div className="padding5 internal_url" key={Index}>
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        <span>
                                            {item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]} - {item.version?.field_value || ""}
                                        </span>
                                    </Link>
                                </div>
                            }
                            return <div key={Index}>
                                <Typography className="bold-p--id">{item.resource_name.field_label[metadataLanguage] || item.resource_name.field_label["en"]}</Typography>
                                <span className="info_value">{item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]}</span>
                                <Typography className="bold-p--id">{item.version.field_label[metadataLanguage] || item.version.field_label["en"]}</Typography>
                                <span className="info_value">{item.version?.field_value || ""}</span>
                                <Typography className="bold-p--id">{item.lr_identifier.field_label[metadataLanguage] || item.lr_identifier.field_label["en"]}</Typography>
                                {item.lr_identifier && item.lr_identifier.field_value.length > 0 && item.lr_identifier.field_value.map((lridentifier, lrIndex) => {
                                    if (lridentifier.lr_identifier_scheme.label["en"] !== "ELG") {
                                        return <div key={lrIndex}>
                                            <a href={lridentifier.lr_identifier_scheme.field_value} target="blank">{lridentifier.value.field_value}</a>
                                        </div>
                                    }
                                    return void 0;
                                })}
                            </div>
                        })}</div>}

                {lr_subclass.language_description_subclass && lr_subclass.language_description_subclass.field_value.tagset && lr_subclass.language_description_subclass.field_value.tagset.field_value.length > 0 &&
                    <div className="padding5"><Typography className="bold-p--id">{lr_subclass.language_description_subclass.field_value.tagset.field_label[metadataLanguage] || lr_subclass.language_description_subclass.field_value.tagset.field_label["en"]}</Typography>
                        {lr_subclass.language_description_subclass.field_value.tagset.field_value.map((item, Index) => {
                            let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                            if (full_metadata_record) {
                                return <div className="padding5 internal_url" key={Index}>
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        <span>
                                            {item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]} - {item.version?.field_value || ""}
                                        </span>
                                    </Link>
                                </div>
                            }
                            return <div key={Index}>
                                <Typography className="bold-p--id">{item.resource_name.field_label[metadataLanguage] || item.resource_name.field_label["en"]}</Typography>
                                <span className="info_value">{item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]]}</span>
                                <Typography className="bold-p--id">{item.version.field_label[metadataLanguage] || item.version.field_label["en"]}</Typography>
                                <span className="info_value">{item.version?.field_value || ""}</span>
                                <Typography className="bold-p--id">{item.lr_identifier.field_label[metadataLanguage] || item.lr_identifier.field_label["en"]}</Typography>
                                {item.lr_identifier && item.lr_identifier.field_value.length > 0 && item.lr_identifier.field_value.map((lridentifier, lrIndex) => {
                                    if (lridentifier.lr_identifier_scheme.label["en"] !== "ELG") {
                                        return <div key={lrIndex}>
                                            <a href={lridentifier.lr_identifier_scheme.field_value} target="blank">{lridentifier.value.field_value}</a>
                                        </div>
                                    }
                                    return void 0;
                                })}
                            </div>
                        })}</div>}

                {has_original_source && has_original_source.field_value.length > 0 && <div className="padding15">
                    <Typography className="bold-p--id">{has_original_source.field_label[metadataLanguage] || has_original_source.field_label["en"]}</Typography>
                    {has_original_source.field_value.map((item, index) => {
                        let resource_name = item.resource_name ? (item.resource_name.field_value[metadataLanguage] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]] || "") : "";
                        let version = item.version ? item.version.field_value : "";
                        let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
                        return <div key={index}>
                            {full_metadata_record ?
                                (<div className="padding5 internal_url">
                                    <span><NavIcon className="xsmall-icon mr-05" /></span>
                                    <Link to={full_metadata_record.internalELGUrl}>
                                        {resource_name} ({version})
                          </Link>
                                </div>)
                                :
                                (<div className="info_value" key={index}>{resource_name} ({version}) </div>)
                            }
                        </div>
                    })
                    }
                </div>
                }

            </div> : <span></span>}
        </>

    }
}

