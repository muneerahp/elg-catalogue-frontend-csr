import React from "react";
import { withRouter } from "react-router-dom";
//import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {/* EDITOR_PERMITTED_ROLES,*/ AUTHENTICATED_KEYCLOAK_USER_ROLES } from "../../config/constants";
import { ReactComponent as CreateIcon } from "../../assets/elg-icons/editor/pencil-write.svg";

class MyConsumerHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isOpen: false, UserRoles: [] };
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
    }

    isAuthorizedToView = (roles) => {
        var isAuthorized = false;
        /*EDITOR_PERMITTED_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        });*/
        isAuthorized = this.props.keycloak && this.props.keycloak.authenticated;
        return isAuthorized;
    }


    render() {
        if (!this.isAuthorizedToView(this.state.UserRoles)) {
            return <></>
        }
        const routeToShow = this.props.route || {};

        return <Button className="app--bar--default" onClick={(event, newValue) => { this.props.history.push(routeToShow.url); }} startIcon={<CreateIcon className="xsmall-icon" />}> <Typography variant="h6" className=" pl-1">{routeToShow.human_name}</Typography> </Button>
    }
}

export default withRouter(MyConsumerHeader);