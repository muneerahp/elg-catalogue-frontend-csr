import React from "react";
import { withRouter } from "react-router-dom";
//import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { EDITOR_PERMITTED_ROLES, AUTHENTICATED_KEYCLOAK_USER_ROLES } from "../../config/constants";
import { ReactComponent as ItemsIcon } from "../../assets/elg-icons/editor/common-file-stack.svg";

class MyItemsHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isOpen: false, UserRoles: [] };
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
    }

    isAuthorizedToView = (roles) => {
        var isAuthorized = false;
        EDITOR_PERMITTED_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        });
        return isAuthorized;
    }


    render() {
        if (!this.isAuthorizedToView(this.state.UserRoles)) {
            return <></>
        }
        return <Button className="app--bar--default" value={this.state.value} onClick={(event, newValue) => { this.props.history.push("/myitems"); }} startIcon={<ItemsIcon className="xsmall-icon" />}> <Typography variant="h6" className=" pl-1">My items</Typography> </Button>
    }
}

export default withRouter(MyItemsHeader);