class CommonParser {
    constructor() {
        this.getReverseRelations = this.getReverseRelations.bind(this);
        this.getFullMetadata = this.getFullMetadata.bind(this);
        this.getURL = this.getURL.bind(this);
        this.switchStatement = this.switchStatement.bind(this);
        this.getSourceOfMetadataRecord = this.getSourceOfMetadataRecord.bind(this);
    }

    getSourceOfMetadataRecord(data, lang) {
        if (data.source_of_metadata_record) {
            let SourceOfMetadata_label = data.source_of_metadata_record.field_label[lang] || data.source_of_metadata_record.field_label["en"];
            let repository_name_label = data.source_of_metadata_record.field_value.repository_name.field_label[lang] || data.source_of_metadata_record.field_value.repository_name.field_label["en"];
            let repository_name_value = data.source_of_metadata_record.field_value.repository_name.field_value[lang] || data.source_of_metadata_record.field_value.repository_name.field_value["en"];
            //let repository_url_label = data.source_of_metadata_record.field_value.repository_url.field_label[lang] || data.source_of_metadata_record.field_value.repository_url.field_label["en"];
            let repository_url_value = data.source_of_metadata_record.field_value.repository_url ? data.source_of_metadata_record.field_value.repository_url.field_value : "";
            let repository_institution = data.source_of_metadata_record.field_value.repository_institution ? data.source_of_metadata_record.field_value.repository_institution.field_value : "";
            return { SourceOfMetadata_label, repository_name_label, repository_name_value, repository_url_value, repository_institution };
        }
        return void 0;
    }

    getReverseRelations(reverseRelations, lang) {
        const reletionsArray = [];
        if (reverseRelations && reverseRelations.field_value.length > 0) {
            reverseRelations.field_value.map(relation => {
                const relation_label = relation.field_label[lang] || relation.field_label["en"];
                const relation_valuesArray = [];
                relation.field_value.map(item => {
                    if (item.project_name) {
                        const resource_name = item.project_name.field_value[lang] || item.project_name.field_value[Object.keys(item.project_name.field_value)[0]];
                        const websiteArray = item.website ? item.website.field_value : [];
                        const rEntity_type = item.full_metadata_record ? (item.full_metadata_record.field_value.entity_type && item.full_metadata_record.field_value.entity_type.field_value) : "";
                        const rLr_type = item.full_metadata_record ? (item.full_metadata_record.field_value.lr_type && item.full_metadata_record.field_value.lr_type.field_value) : "";
                        const rLink = item.full_metadata_record ? (item.full_metadata_record.field_value.link && item.full_metadata_record.field_value.link.field_value) : "";
                        relation_valuesArray.push({ relationLabel: relation_label, resourceName: resource_name, website: websiteArray, entity_type: rEntity_type, lr_type: rLr_type, link: rLink });
                    } else if (item.resource_name) {
                        const resource_name = item.resource_name.field_value[lang] || item.resource_name.field_value[Object.keys(item.resource_name.field_value)[0]];
                        const rVersion = item.version ? item.version.field_value : "";
                        const rEntity_type = item.full_metadata_record ? (item.full_metadata_record.field_value.entity_type && item.full_metadata_record.field_value.entity_type.field_value) : "";
                        const rLr_type = item.full_metadata_record ? (item.full_metadata_record.field_value.lr_type && item.full_metadata_record.field_value.lr_type.field_value) : "";
                        const rLink = item.full_metadata_record ? (item.full_metadata_record.field_value.link && item.full_metadata_record.field_value.link.field_value) : "";
                        relation_valuesArray.push({ relationLabel: relation_label, resourceName: resource_name, version: rVersion, entity_type: rEntity_type, lr_type: rLr_type, link: rLink });
                    }
                    return void 0;
                });
                reletionsArray.push({ relationLabel: relation_label, relationsValuesArray: relation_valuesArray });
                return void 0;
            });
        }
        return reletionsArray;
    }

    getFullMetadata(full_metadata_record) {
        if (full_metadata_record) {
            const rEntity_type = full_metadata_record.field_value.entity_type && full_metadata_record.field_value.entity_type.field_value;
            const rLr_type = full_metadata_record.field_value.lr_type && full_metadata_record.field_value.lr_type.field_value;
            const rLink = full_metadata_record.field_value.link && full_metadata_record.field_value.link.field_value;
            const relation = { entity_type: rEntity_type, lr_type: rLr_type, link: rLink };
            const internalELGUrl = this.getURL(relation);
            relation.internalELGUrl = internalELGUrl;
            return relation;
        }
        return null;
    }

    getURL(relation) {
        const { link, entity_type, lr_type } = relation;
        let id = link.substring(0, link.lastIndexOf("/"))
        id = id.substring(id.lastIndexOf("/") + 1);
        let redirectURL = "";
        if (lr_type) {
            redirectURL = this.switchStatement(lr_type, id);
        } else if (entity_type) {
            redirectURL = this.switchStatement(entity_type, id);
        }
        return redirectURL;
    }

    switchStatement(type, id) {
        let redirectURL = "";
        switch (type) {
            case "LanguageDescription": redirectURL = `/ld/${id}`; break;
            case "LexicalConceptualResource": redirectURL = `/lcr/${id}`; break;
            case "ToolService": redirectURL = `/tool-service/${id}`; break;
            case "Corpus": redirectURL = `/corpus/${id}`; break;
            case "Project": redirectURL = `/project/${id}`; break;
            case "Organization": redirectURL = `/organization/${id}`; break;
            default: void 0;
        }
        return redirectURL;
    }

    redirectUnregisteredUsersToKeycloak = (location) => {
        let searchParamsFromURL = location ? location.search : "";
        if (searchParamsFromURL && searchParamsFromURL.indexOf("?") >= 0) {
            searchParamsFromURL = searchParamsFromURL.substring(searchParamsFromURL.indexOf("?") + 1);
            if (searchParamsFromURL.indexOf("&") >= 0) {
                const paramsArray = searchParamsFromURL.split("&");
                for (let index = 0; index < paramsArray.length; index++) {
                    const param = paramsArray[index];
                    if (param.indexOf("=") >= 0 && param.indexOf("auth") >= 0) {
                        const auth = param.split("=")[1];
                        if (auth === "true") {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            } else {
                if (searchParamsFromURL.indexOf("=") >= 0 && searchParamsFromURL.indexOf("auth") >= 0) {
                    const auth = searchParamsFromURL.split("=")[1];
                    if (auth === "true") {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return false;
    }



}
const commonParser = new CommonParser();

export default commonParser;