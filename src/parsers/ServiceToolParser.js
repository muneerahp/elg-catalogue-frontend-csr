class ServiceToolParser {
    constructor() {
        this.getResourceName = this.getResourceName.bind(this);
        this.getVersion = this.getVersion.bind(this);
        this.getVersionDate = this.getVersionDate.bind(this);
        this.getUpdateFrequency = this.getUpdateFrequency.bind(this);
        this.getDescription = this.getDescription.bind(this);
        this.getKeywords = this.getKeywords.bind(this);
        this.getDomainKeywords = this.getDomainKeywords.bind(this);
        this.getSubjectKeywords = this.getSubjectKeywords.bind(this);
        this.getIntendedKeywords = this.getIntendedKeywords.bind(this);
        this.getRevision = this.getRevision.bind(this);
        this.getCitationText = this.getCitationText.bind(this);
        this.getCitationAllVersions = this.getCitationAllVersions.bind(this);
        this.getMailingList = this.getMailingList.bind(this);
        this.getLogo = this.getLogo.bind(this);
        this.getFundingProjects = this.getFundingProjects.bind(this);
        this.getAdditionalInfo = this.getAdditionalInfo.bind(this);
        this.getResourceShortName = this.getResourceShortName.bind(this);
        this.getDiscussionUrl = this.getDiscussionUrl.bind(this);
        this.getElgExecutionLocation = this.getElgExecutionLocation.bind(this);
        this.getElgExecutionLocationSync = this.getElgExecutionLocationSync.bind(this);
        this.getElgGuiUrl = this.getElgGuiUrl.bind(this);
        this.getMediaType = this.getMediaType.bind(this);
        this.getLRType = this.getLRType.bind(this);
    }

    getLRType(data, lang) {
        let lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type;
        if (lr_type) {
            lr_type = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value;
            const lr_type_label = data.described_entity.field_value.lr_subclass.field_value.lr_type.field_label[lang] || data.described_entity.field_value.lr_subclass.field_value.lr_type.field_label["en"];
            return { "label": lr_type_label, "value": lr_type };
        } else { lr_type = null }
        return lr_type;
    }

    getResourceName(data, lang) {
        let resourceName = data.described_entity.field_value.resource_name;
        if (resourceName) {
            resourceName = data.described_entity.field_value.resource_name.field_value[lang] || data.described_entity.field_value.resource_name.field_value[Object.keys(data.described_entity.field_value.resource_name.field_value)[0]];
            const resourceName_label = data.described_entity.field_value.resource_name.field_label[lang] || data.described_entity.field_value.resource_name.field_label["en"];
            return { "label": resourceName_label, "value": resourceName };
        }
        return resourceName || "";
    }

    getVersion(data, lang) {
        let version = data.described_entity.field_value.version;
        if (version) {
            version = data.described_entity.field_value.version.field_value;
            const version_label = data.described_entity.field_value.version.field_label[lang] || data.described_entity.field_value.version.field_label["en"];
            return { "label": version_label, "value": version };
        } else { version = null; }
        return version;
    }

    getVersionDate(data, lang) {
        let versionDate = data.described_entity.field_value.version_date;
        if (versionDate) {
            versionDate = data.described_entity.field_value.version_date.field_value;
            const versionDate_label = data.described_entity.field_value.version_date.field_label[lang] || data.described_entity.field_value.version_date.field_label["en"];
            return { "label": versionDate_label, "value": versionDate };
        } else { versionDate = null }
        return versionDate;
    }

    getUpdateFrequency(data, lang) {
        let updateFrequency = data.described_entity.field_value.update_frequency;
        if (updateFrequency) {
            updateFrequency = data.described_entity.field_value.update_frequency.field_value[lang] || data.described_entity.field_value.update_frequency.field_value[Object.keys(data.described_entity.field_value.update_frequency.field_value)[0]];
            const updateFrequency_label = data.described_entity.field_value.update_frequency.field_label[lang] || data.described_entity.field_value.update_frequency.field_label["en"];
            return { "label": updateFrequency_label, "value": updateFrequency };
        }
        return updateFrequency || "";
    }

    getDescription(data, lang) {
        let description = data.described_entity.field_value.description;
        if (description) {
            description = data.described_entity.field_value.description.field_value[lang] || data.described_entity.field_value.description.field_value[Object.keys(data.described_entity.field_value.description.field_value)[0]];
            const description_label = data.described_entity.field_value.description.field_label[lang] || data.described_entity.field_value.description.field_label["en"];
            return { "label": description_label, "value": description };
        }
        return description || "";
    }

    getKeywords(data, lang) {
        let keywords = [];
        if (data.described_entity.field_value.keyword && data.described_entity.field_value.keyword.field_value.length > 0) {
            data.described_entity.field_value.keyword.field_value.forEach((keyword, index) => {
                keywords.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
            const keyword_label = data.described_entity.field_value.keyword.field_label[lang] || data.described_entity.field_value.keyword.field_label["en"];
            return { "label": keyword_label, "value": keywords };
        }
        return null;
    }

    getDomainKeywords(data, lang) {
        let domainKeywords = [];
        if (data.described_entity.field_value.domain && data.described_entity.field_value.domain.field_value.length > 0) {
            data.described_entity.field_value.domain.field_value.forEach((keyword, index) => {
                domainKeywords.push(keyword.category_label.field_value[lang] || keyword.category_label.field_value[Object.keys(keyword.category_label.field_value)[0]])
            });
            const domain_label = data.described_entity.field_value.domain.field_label[lang] || data.described_entity.field_value.domain.field_label["en"];
            return { "label": domain_label, "value": domainKeywords };
        }
        return null;
    }

    getSubjectKeywords(data, lang) {
        let subject = [];
        if (data.described_entity.field_value.subject && data.described_entity.field_value.subject.field_value.length > 0) {
            data.described_entity.field_value.subject.field_value.forEach((keyword, index) => {
                subject.push(keyword.category_label.field_value[lang] || keyword.category_label.field_value[Object.keys(keyword.category_label.field_value)[0]])
            });
            const subject_label = data.described_entity.field_value.subject.field_label[lang] || data.described_entity.field_value.subject.field_label["en"];
            return { "label": subject_label, "value": subject };
        }
        return null;
    }

    getIntendedKeywords(data, lang) {
        let intendedKeywords = [];
        if (data.described_entity.field_value.intended_application && data.described_entity.field_value.intended_application.field_value.length > 0) {
            data.described_entity.field_value.intended_application.field_value.forEach((keyword, index) => {
                keyword.label ? (intendedKeywords.push(keyword.label[lang] || keyword.label[Object.keys(keyword.label)[0]])) : intendedKeywords.push(keyword.value)
            });
            const intended_application_label = data.described_entity.field_value.intended_application.field_label[lang] || data.described_entity.field_value.intended_application.field_label["en"];
            return { "label": intended_application_label, "value": intendedKeywords };
        }
        return null;
    }

    getRevision(data, lang) {
        let revision = data.described_entity.field_value.revision;
        if (revision) {
            revision = data.described_entity.field_value.revision.field_value[lang] || data.described_entity.field_value.revision.field_value[Object.keys(data.described_entity.field_value.revision.field_value)[0]];
            const revisionlabel = data.described_entity.field_value.revision.field_label[lang] || data.described_entity.field_value.revision.field_label["en"]
            return { label: revisionlabel, value: revision };
        }
        return revision || "";
    }

    getCitationText(data, lang) {
        let citationText = data.described_entity.field_value.citation_text;
        if (citationText) {
            citationText = data.described_entity.field_value.citation_text.field_value[lang] || data.described_entity.field_value.citation_text.field_value[Object.keys(data.described_entity.field_value.citation_text.field_value)[0]];
            const citetionText_label = data.described_entity.field_value.citation_text.field_label[lang] || data.described_entity.field_value.citation_text.field_label["en"];
            return { "label": citetionText_label, "value": citationText };
        }
        return citationText || "";
    }

    getCitationAllVersions(data, lang) {
        let citation_all_versions = data.described_entity.field_value.citation_all_versions;
        if (citation_all_versions) {
            citation_all_versions = data.described_entity.field_value.citation_all_versions.field_value[lang] || data.described_entity.field_value.citation_all_versions.field_value[Object.keys(data.described_entity.field_value.citation_all_versions.field_value)[0]];
            const citation_all_versions_label = data.described_entity.field_value.citation_all_versions.field_label[lang] || data.described_entity.field_value.citation_all_versions.field_label["en"];
            return { "label": citation_all_versions_label, "value": citation_all_versions };
        }
        return citation_all_versions || "";
    }

    getMailingList(data, lang) {
        let mailingList = [];
        if (data.described_entity.field_value.mailing_list_name && data.described_entity.field_value.mailing_list_name.field_value.length > 0) {
            data.described_entity.field_value.mailing_list_name.field_value.forEach((item, index) => mailingList.push(item))
            const mailingList_label = data.described_entity.field_value.mailing_list_name.field_label[lang] || data.described_entity.field_value.mailing_list_name.field_label["en"];
            return { "label": mailingList_label, "value": mailingList };
        }
        return null;
    }

    getDiscussionUrl(data, lang) {
        let discussionUrl = [];
        if (data.described_entity.field_value.discussion_url && data.described_entity.field_value.discussion_url.field_value.length > 0) {
            data.described_entity.field_value.discussion_url.field_value.forEach((item, index) => discussionUrl.push(item));
            const discussionUrl_label = data.described_entity.field_value.discussion_url.field_label[lang] || data.described_entity.field_value.discussion_url.field_label["en"];
            return { "label": discussionUrl_label, "value": discussionUrl };
        }
        return null;
    }

    getLogo(data) {
        let logo = data.described_entity.field_value.logo;
        if (logo) {
            logo = data.described_entity.field_value.logo.field_value;
        } else { logo = null };
        return logo;
    }

    getFundingProjects(data, lang) {
        let fundingProjects = [];
        if (data.described_entity.field_value.funding_project && data.described_entity.field_value.funding_project.field_value && data.described_entity.field_value.funding_project.field_value.length > 0) {
            data.described_entity.field_value.funding_project.field_value.forEach((fundingProject, fundingProjectIndex) => {
                //let pk = fundingProject.pk;
                let project_name = fundingProject.project_name ? (fundingProject.project_name.field_value[lang] || fundingProject.project_name.field_value[Object.keys(fundingProject.project_name.field_value)[0]]) : "";
                if (project_name.includes('Unspecified Project Name')) {project_name=""}
                let website = fundingProject.website ? fundingProject.website.field_value : "";
                let website_label = fundingProject.website ? (fundingProject.website.field_label[lang] || fundingProject.website.field_label["en"]) : "";
                let project_identifier = [...fundingProject.project_identifier.field_value];
                let funding_type_label = fundingProject.funding_type ? (fundingProject.funding_type.field_label[lang] || fundingProject.funding_type.field_label["en"]) : "";
                let funding_type = fundingProject.funding_type ? [...fundingProject.funding_type.field_value] : [];    
                fundingProjects.push({ project_name, website, website_label, project_identifier, funding_type_label, funding_type });
            }
            )
        }
        return fundingProjects;
    }

    getAdditionalInfo(data, lang) {
        let additionalInfo = [];
        if (data.described_entity.field_value.additional_info) {
            data.described_entity.field_value.additional_info.field_value.forEach(additional_infoItem => additionalInfo.push(additional_infoItem));
        }
        let additionalInfolabel = data.described_entity.field_value.additional_info ? data.described_entity.field_value.additional_info.field_label[lang] || data.described_entity.field_value.additional_info["en"] : "";

        return { "label": additionalInfolabel, "value": additionalInfo };
    }

    getResourceShortName(data, lang) {
        let resourceShortName = null;
        if (data.described_entity.field_value.resource_short_name) {
            resourceShortName = data.described_entity.field_value.resource_short_name.field_value[lang] || data.described_entity.field_value.resource_short_name.field_value[Object.keys(data.described_entity.field_value.resource_short_name.field_value)[0]];
        }
        return resourceShortName;
    }

    getElgExecutionLocation(data) {
        let elg_execution_location = "";
        if (data.service_info) {
            elg_execution_location = data.service_info.elg_execution_location;
        }
        return elg_execution_location;
    }

    getElgExecutionLocationSync(data) {
        let elg_execution_location_sync = "";
        if (data.service_info) {
            elg_execution_location_sync = data.service_info.elg_execution_location_sync;
        }
        return elg_execution_location_sync;
    }

    getElgGuiUrl(data) {
        let elg_gui_url = "";
        if (data.service_info) {
            elg_gui_url = data.service_info.elg_gui_url;
        }
        return elg_gui_url;
    }

    getMediaType(data) {
        let media_type = "text";
        if (data.service_info) {
            if (data.service_info.tool_type) {
                if (['Machine Translation', 'Information Extraction', 'Text-to-Speech Synthesis', 'Text categorization'].includes(data.service_info.tool_type)) {
                    media_type = "text";
                } else if (['Speech Recognition'].includes(data.service_info.tool_type)) {
                    media_type = "audio";
                }
            }
        }
        return media_type;
    }

    getServiceInfoType = (data) => {
        let tool_type = null;
        if (data.service_info) {
            tool_type = data.service_info.tool_type;
        }
        return tool_type;
    }
}


const serviceToolParser = new ServiceToolParser();

export default serviceToolParser;