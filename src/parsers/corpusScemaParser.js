class CorpusScemaParser {
    constructor() {
        this.getResourceName = this.getResourceName.bind(this);
        this.getVersion = this.getVersion.bind(this);
        this.getVersionDate = this.getVersionDate.bind(this);
        this.getLogo = this.getLogo.bind(this);
        this.getResourceShortName = this.getResourceShortName.bind(this);
        this.getDescription = this.getDescription.bind(this);
        this.getUpdateFrequency = this.getUpdateFrequency.bind(this);
        this.getRevision = this.getRevision.bind(this);
        this.getMailingList = this.getMailingList.bind(this);
        this.getDiscussionUrl = this.getDiscussionUrl.bind(this);
        this.getCitationText = this.getCitationText.bind(this);
        this.getKeywords = this.getKeywords.bind(this);
        this.getDomainKeywords = this.getDomainKeywords.bind(this);
        this.getSubjectKeywords = this.getSubjectKeywords.bind(this);
        this.getIntendedKeywords = this.getIntendedKeywords.bind(this);
        this.getAdditionalInfo = this.getAdditionalInfo.bind(this);
        this.getFundingProjects = this.getFundingProjects.bind(this);
        this.getPhysicalResource = this.getPhysicalResource.bind(this);
        this.getCorpusSubclass = this.getCorpusSubclass.bind(this);
    }

    typeoffield = (type) => {
        switch (type) {
    
          case "field": return ("text")
          case "string": return ("text")
          case "list":  return ("list")
          case "choice" : return ("choice")
          case "boolean" : return ("boolean")
          case "nested object":return ("nested object")

          default: return ""
        }
    }

    getResourceName(data) {
        let { help_text, label, required, type } = data.resource_name;
        type = this.typeoffield(type); 
        return { "help_text": help_text, "label": label , "required": required , "type":type };        
    }

 
    getVersion(data) {
        let { help_text, label, required, type , max_length } = data.version;
        type = this.typeoffield(type);
        return { "help_text": help_text, "label": label , "required": required , "type":type, "max_length":max_length };   
    }

    getVersionDate(data) {
        let { help_text, label, required, type } = data.version_date;
        type = this.typeoffield(type);
        return { "help_text": help_text, "label": label , "required": required , "type":type}; 
    }

    getLogo(data) {
        let { help_text, label, required, type , max_length} = data.logo;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type , "max_length":max_length};
    } 

    getResourceShortName(data) {
        let { help_text, label, required, type } = data.resource_short_name;
        type = this.typeoffield(type);
        return { "help_text": help_text, "label": label , "required": required , "type":type };    
    }

    getDescription(data) {
        let { help_text, label, required, type } = data.description;
        type = this.typeoffield(type);
        return { "help_text": help_text, "label": label, "required": required, "type": type };
    }

    getUpdateFrequency(data) {
        let { help_text, label, required, type } = data.update_frequency;
        type = this.typeoffield(type);
        return { "help_text": help_text, "label": label, "required": required, "type": type };
    }
    getRevision(data) {
        let { help_text, label, required, type } = data.revision;
        type = this.typeoffield(type);
        return { "help_text": help_text, "label": label, "required": required, "type": type };
    }

    getMailingList(data) {
        let { help_text, label, required, type } = data.mailing_list_name;
        type = this.typeoffield(type);
        return { "help_text": help_text, "label": label, "required": required, "type": type };
    }

    getDiscussionUrl(data) {
        let { help_text, label, required, type } = data.discussion_url;
        type = this.typeoffield(type);
        return { "help_text": help_text, "label": label, "required": required, "type": type };
    }

    getCitationText(data) {
        let citationText = data.described_entity.citation_text;
        let { help_text, label, required, type } = data.citation_text;
        type = this.typeoffield(type);
        return { "help_text": help_text, "label": label, "required": required, "type": type };
    }

    getKeywords(data) {
        let { help_text, label, required, type } = data.keyword;
        type = this.typeoffield(type); 
        return { "help_text": help_text, "label": label , "required": required , "type":type };        
    }

    getDomainKeywords(data) {
        let { help_text, label, required, type } = data.domain;

        let { required: pk_domain_required, read_only: pk_domain_read_only, label: pk_domain_label, type: pk_domain_type } = data.domain.child.children.pk;
        let { help_text: category_help_text, label: category_label, required: category_required, type: category_type, read_only: category_read_only } = data.domain.child.children.category_label;

        let { required: pk_identifier_required, read_only: pk_identifier_read_only, label: pk_identifier_label, type: pk_identifier_type } = data.domain.child.children.domain_identifier.children.pk;
        let { choices: domain_choices, help_text: domain_help_text, label: domain_label, read_only: domain_read_only, required: domain_required } = data.domain.child.children.domain_identifier.children.domain_classification_scheme;
        let { label: value_label, required: value_required, max_length: value_max_length } = data.domain.child.children.domain_identifier.children.value;
        type = this.typeoffield(type);
        //return { "help_text": help_text, "label": label, "required": required, "type": type, "choices": choices };
        return {
            help_text, label, required, type,
            pk_domain_required, pk_domain_read_only, pk_domain_label, pk_domain_type,
            category_help_text, category_label, category_required, category_type, category_read_only,
            pk_identifier_required, pk_identifier_read_only, pk_identifier_label, pk_identifier_type,
            domain_choices, domain_help_text, domain_label, domain_read_only, domain_required,
            value_label, value_required, value_max_length
        }
    }

     getSubjectKeywords(data) {
        let { help_text, label, required, type } = data.subject;
        let {choices} = data.subject.child.children.subject_identifier.children.subject_classification_scheme;
        type = this.typeoffield(type); 
        return { "help_text": help_text, "label": label , "required": required , "type":type, "choices": choices };        
    } 

    getIntendedKeywords(data) {        
        let { help_text, label, required, type } = data.intended_application;
        type = this.typeoffield(type); 
        return { "help_text": help_text, "label": label , "required": required , "type":type };    
    }

    getAdditionalInfo(data) {
        let { help_text, label, required, type } = data.additional_info;
        type = this.typeoffield(type); 
        let { required: landing_page_required, label: landing_page_label, type: landing_page_type, max_length: landing_page_max_length } = data.additional_info.child.children.landing_page;
        let { required: email_required, label: email_label, type: email_type, max_length: email_max_length } = data.additional_info.child.children.email;

        return {
            help_text, label, required, type,landing_page_required,landing_page_label,landing_page_type,landing_page_max_length,
            email_required,email_label,email_type,email_max_length}
    }

    getFundingProjects(data ) {
        let { help_text, label, required, type } = data.funding_project;
    
        let { required: pk_funding_project_required, read_only: pk_funding_project_read_only, label: pk_funding_project_label, type: pk_funding_project_type } = data.funding_project.child.children.pk;
        let { help_text: project_help_text, label: project_label, required: project_required, type: project_type, read_only: project_read_only } = data.funding_project.child.children.project_name;
    
        let { required: pk_identifier_required, read_only: pk_identifier_read_only, label: pk_identifier_label, type: pk_identifier_type } = data.funding_project.child.children.project_identifier.children.pk;
        let { choices: funding_project_choices, help_text: funding_project_help_text, label: funding_project_label, read_only: funding_project_read_only, required: funding_project_required } = data.funding_project.child.children.project_identifier.children.project_identifier_scheme;
        let { label: value_label, required: value_required, max_length: value_max_length } = data.funding_project.child.children.project_identifier.children.value;
        type = this.typeoffield(type);
        //return { "help_text": help_text, "label": label, "required": required, "type": type, "choices": choices };
        return {
            help_text, label, required, type,
            pk_funding_project_required, pk_funding_project_read_only, pk_funding_project_label, pk_funding_project_type,
            project_help_text, project_label, project_required, project_type, project_read_only,
            pk_identifier_required, pk_identifier_read_only, pk_identifier_label, pk_identifier_type,
            funding_project_choices, funding_project_help_text, funding_project_label, funding_project_read_only, funding_project_required,
            value_label, value_required, value_max_length
        }
    }

    getPhysicalResource(data) {
        let { help_text, label, required, type } = data.physical_resource;
        type = this.typeoffield(type); 
        return { "help_text": help_text, "label": label , "required": required , "type":type };  
    }

    getCorpusSubclass(data) {
        let { help_text, label, required, type } = data.lr_subclass;
        type = this.typeoffield(type); 
        return { "help_text": help_text, "label": label , "required": required , "type":type }; 
    }

    

 

}


const corpusScemaParser = new CorpusScemaParser();

export default corpusScemaParser;