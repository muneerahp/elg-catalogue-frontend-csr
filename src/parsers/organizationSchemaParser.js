class OrganizationScemaParser {
    constructor() {
        this.getOrganizationName = this.getOrganizationName.bind(this);
        this.getOrganizationShortName = this.getOrganizationShortName.bind(this);
        this.getOrganizationAltName = this.getOrganizationAltName.bind(this);
        this.getLogo = this.getLogo.bind(this);
        this.getOrganizationRole = this.getOrganizationRole.bind(this);
        this.getOrganizationLegalStatus = this.getOrganizationLegalStatus.bind(this);
        this.getStartup = this.getStartup.bind(this);
        this.getCountryOfRegistration = this.getCountryOfRegistration.bind(this);
        this.getOrganizationBio = this.getOrganizationBio.bind(this);
        this.getKeywords = this.getKeywords.bind(this);
        this.getDomainKeywords = this.getDomainKeywords.bind(this);
        this.getSocialMedia = this.getSocialMedia.bind(this);
        this.getLtAreaKeywords = this.getLtAreaKeywords.bind(this);
        this.getDisciplineKeywords = this.getDisciplineKeywords.bind(this);
        this.getservicesOfferedKeywords = this.getservicesOfferedKeywords.bind(this);
        this.getOrganizationWebsites = this.getOrganizationWebsites.bind(this);
        this.getOrganizationEmails = this.getOrganizationEmails.bind(this);
        this.getTelephoneNumbers = this.getTelephoneNumbers.bind(this);
        this.getFaxNumbers = this.getFaxNumbers.bind(this);
        this.GetOtherOfficeAddress = this.GetOtherOfficeAddress.bind(this);
        this.GetHeadOfficeAddress = this.GetHeadOfficeAddress.bind(this);
        this.getHasDivision = this.getHasDivision.bind(this);
        this.get_member_of_association = this.get_member_of_association.bind(this);
        this.getOrganizationIdentifier = this.getOrganizationIdentifier.bind(this);
        this.getHasDivisionName = this.getHasDivisionName.bind(this);
        this.getHasDivisionShortName = this.getHasDivisionShortName.bind(this);
        this.getHasDivisionAltName = this.getHasDivisionAltName.bind(this);

    }
    typeoffield = (type) => {
        switch (type) {

            case "field": return ("text")
            case "list": return ("list")
            case "choice": return ("choice")
            case "boolean": return ("boolean")

            default: return ""
        }
    }


    getOrganizationIdentifier(data) {
        let { help_text, label, required, type, read_only } = data.organization_identifier;

        let { required: pk_organization_identifier_required, read_only: pk_organization_identifier_read_only, label: pk_organization_identifier_label, type: pk_organization_identifier_type } = data.organization_identifier.child.children.pk;
        let { choices: organization_identifier_scheme_choices, help_text: organization_identifier_scheme_help_text, label: organization_identifier_scheme_label, read_only: organization_identifier_scheme_read_only, required: organization_identifier_scheme_required, type: organization_identifier_scheme_type } = data.organization_identifier.child.children.organization_identifier_scheme;
        let { label: organization_identifier_value_label, required: organization_identifier_value_required, max_length: organization_identifier_value_max_length, type: organization_identifier_value_type, read_only: organization_identifier_value_read_only, placeholder: organization_identifier_value_placeholder, help_text: organization_identifier_value_help_text } = data.organization_identifier.child.children.value;
        type = this.typeoffield(type);
        pk_organization_identifier_type = this.typeoffield(pk_organization_identifier_type);
        organization_identifier_scheme_type = this.typeoffield(organization_identifier_scheme_type);
        organization_identifier_value_type = this.typeoffield(organization_identifier_value_type);


        return {
            help_text, label, required, type, read_only,
            pk_organization_identifier_required, pk_organization_identifier_read_only, pk_organization_identifier_label, pk_organization_identifier_type,
            organization_identifier_scheme_choices, organization_identifier_scheme_help_text, organization_identifier_scheme_label, organization_identifier_scheme_read_only, organization_identifier_scheme_required, organization_identifier_scheme_type,
            organization_identifier_value_label, organization_identifier_value_required, organization_identifier_value_max_length, organization_identifier_value_type, organization_identifier_value_read_only, organization_identifier_value_placeholder, organization_identifier_value_help_text
        }
    }


    getOrganizationName(data) {
        let { help_text, label, required, type, placeholder } = data.organization_name;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "placeholder": placeholder };
    }

    getLogo(data) {
        let { help_text, label, required, type, placeholder } = data.logo;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "placeholder": placeholder };
    }

    getOrganizationShortName(data) {
        let { help_text = "Organization short name", label = "Organization short name", required = false, type = "list", placeholder = "" } = data.organization_short_name;
        //let { help_text, label, required, type, placeholder } = data.organization_short_name;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "placeholder": placeholder };
    }

    getOrganizationBio(data) {
        let { help_text, label, required, type } = data.organization_bio;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type };
    }

    getOrganizationAltName(data) {
        let { help_text, label, required, type } = data.organization_alternative_name;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type };
    }

    getOrganizationRole(data) {
        let { help_text, label, required, type, placeholder } = data.organization_role;
        let { choices } = data.organization_role.child;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "choices": choices, "placeholder": placeholder };
    }

    getOrganizationLegalStatus(data) {
        let { help_text, label, required, type, choices, placeholder } = data.organization_legal_status;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "choices": choices, "placeholder": placeholder };
    }

    getCountryOfRegistration(data) {
        let { help_text, label, required, type } = data.country_of_registration;
        let { choices } = data.country_of_registration.child;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "choices": choices };
    }


    getStartup(data) {
        let { help_text, label, required, type } = data.startup;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type };
    }

    getOrganizationWebsites(data) {
        let { help_text, label, required, type, placeholder } = data.website;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "placeholder": placeholder };
    }

    getOrganizationEmails(data) {
        let { help_text, label, required, type } = data.email;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type };
    }

    getTelephoneNumbers(data) {
        let { help_text, label, required, type, placeholder } = data.telephone_number;
        type = "number";

        return { "help_text": help_text, "label": label, "required": required, "type": type, "placeholder": placeholder };
    }

    getFaxNumbers(data) {
        let { help_text, label, required, type, placeholder } = data.fax_number;
        type = "number";

        return { "help_text": help_text, "label": label, "required": required, "type": type, "placeholder": placeholder };
    }

    getKeywords(data) {
        let { help_text, label, required, type, placeholder } = data.keyword;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "placeholder": placeholder };
    }

    getLtAreaKeywords(data) {
        let { help_text, label, required, type, placeholder } = data.lt_area;
        type = this.typeoffield(type);
        const { recommended_choices, max_length } = data.lt_area.child;

        return {
            "help_text": help_text, "label": label, "required": required, "type": type,
            "recommended_choices": recommended_choices, "max_length": max_length, "placeholder": placeholder
        };
    }

    getDomainKeywords(data) {
        let { help_text, label, required, type } = data.domain;

        let { required: pk_domain_required, read_only: pk_domain_read_only, label: pk_domain_label, type: pk_domain_type } = data.domain.child.children.pk;
        let { help_text: category_help_text, label: category_label, required: category_required, type: category_type, read_only: category_read_only, placeholder: category_placeholder } = data.domain.child.children.category_label;

        let { required: pk_identifier_required, read_only: pk_identifier_read_only, label: pk_identifier_label, type: pk_identifier_type } = data.domain.child.children.domain_identifier.children.pk;
        let { choices: domain_choices, help_text: domain_help_text, label: domain_label, read_only: domain_read_only, required: domain_required } = data.domain.child.children.domain_identifier.children.domain_classification_scheme;
        let { label: value_label, required: value_required, max_length: value_max_length, placeholder: value_placeholder, help_text: value_help_text } = data.domain.child.children.domain_identifier.children.value;
        let { label: domain_identifier_label, help_text: domain_identifier_help_text } = data.domain.child.children.domain_identifier;
        type = this.typeoffield(type);

        return {
            help_text, label, required, type,
            pk_domain_required, pk_domain_read_only, pk_domain_label, pk_domain_type,
            category_help_text, category_label, category_required, category_type, category_read_only, category_placeholder,
            pk_identifier_required, pk_identifier_read_only, pk_identifier_label, pk_identifier_type,
            domain_choices, domain_help_text, domain_label, domain_read_only, domain_required,
            value_label, value_required, value_max_length, value_placeholder, value_help_text,
            domain_identifier_label, domain_identifier_help_text
        }
    }
    getservicesOfferedKeywords(data) {
        let { help_text, label, required, type, placeholder } = data.service_offered;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "placeholder": placeholder };
    }

    getDisciplineKeywords(data) {
        let { help_text, label, required, type, placeholder } = data.discipline;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "placeholder": placeholder };
    }

    getSocialMedia(data) {
        let { help_text, label, required, type } = data.social_media_occupational_account;
        type = this.typeoffield(type);
        let { required: social_media_required, label: social_media_label, type: social_media_type, help_text: social_media_help_text, choices: social_media_choices } = data.social_media_occupational_account.child.children.social_media_occupational_account_type;
        let { required: value_required, label: value_label, type: value_type, max_length: value_max_length, placeholder: value_placeholder, help_text: value_help_text } = data.social_media_occupational_account.child.children.value;


        return {
            help_text, label, required, type,
            social_media_required, social_media_label, social_media_type, social_media_choices, social_media_help_text,
            value_label, value_required, value_max_length, value_type, value_placeholder, value_help_text
        }
    }

    get_member_of_association(data) {
        let { help_text, label, required, type, read_only } = data.member_of_association;

        let { required: pk_member_of_association_required, read_only: pk_member_of_association_read_only, label: pk_member_of_association_label, type: pk_member_of_association_type } = data.member_of_association.child.children.pk;
        let { help_text: member_of_association_actor_type_help_text, label: member_of_association_actor_type_label, required: member_of_association_actor_type_required, type: member_of_association_actor_type_type, read_only: member_of_association_actor_type_read_only, max_length: member_of_association_actor_type_max_length } = data.member_of_association.child.children.actor_type;
        let { help_text: member_of_association_organization_name_help_text, label: member_of_association_organization_name_label, required: member_of_association_organization_name_required, type: member_of_association_organization_name_type, read_only: member_of_association_organization_name_read_only, placeholder: member_of_association_organization_name_placeholder } = data.member_of_association.child.children.organization_name;

        let { required: pk_organization_identifier_required, read_only: pk_organization_identifier_read_only, label: pk_organization_identifier_label, type: pk_organization_identifier_type } = data.member_of_association.child.children.organization_identifier.child.children.pk;
        let { choices: organization_identifier_scheme_choices, help_text: organization_identifier_scheme_help_text, label: organization_identifier_scheme_label, read_only: organization_identifier_scheme_read_only, required: organization_identifier_scheme_required, type: organization_identifier_scheme_type } = data.member_of_association.child.children.organization_identifier.child.children.organization_identifier_scheme;
        let { label: organization_identifier_value_label, required: organization_identifier_value_required, max_length: organization_identifier_value_max_length, type: organization_identifier_value_type, read_only: organization_identifier_value_read_only, placeholder: organization_identifier_value_placeholder, help_text: organization_identifier_value_help_text } = data.member_of_association.child.children.organization_identifier.child.children.value;
        let { help_text: website_help_text, label: website_name_label, required: website_required, type: website_type, read_only: website_read_only, placeholder: website_placeholder } = data.member_of_association.child.children.website;

        let { help_text: organization_identifier_help_text, label: organization_identifier_label, required: organization_identifier_required, type: organization_identifier_type, read_only: organization_identifier_read_only } = data.member_of_association.child.children.organization_identifier;


        type = this.typeoffield(type);
        pk_member_of_association_type = this.typeoffield(pk_member_of_association_type);
        member_of_association_actor_type_type = this.typeoffield(member_of_association_actor_type_type);
        member_of_association_organization_name_type = this.typeoffield(member_of_association_organization_name_type);
        pk_organization_identifier_type = this.typeoffield(pk_organization_identifier_type);
        organization_identifier_scheme_type = this.typeoffield(organization_identifier_scheme_type);
        organization_identifier_value_type = this.typeoffield(organization_identifier_value_type);
        website_type = this.typeoffield(website_type);
        organization_identifier_type = this.typeoffield(organization_identifier_type);

        return {
            help_text, label, required, type, read_only,
            pk_member_of_association_required, pk_member_of_association_read_only, pk_member_of_association_label, pk_member_of_association_type,
            member_of_association_actor_type_help_text, member_of_association_actor_type_label, member_of_association_actor_type_required, member_of_association_actor_type_type, member_of_association_actor_type_read_only, member_of_association_actor_type_max_length,
            member_of_association_organization_name_help_text, member_of_association_organization_name_label, member_of_association_organization_name_required, member_of_association_organization_name_type, member_of_association_organization_name_read_only, member_of_association_organization_name_placeholder,
            pk_organization_identifier_required, pk_organization_identifier_read_only, pk_organization_identifier_label, pk_organization_identifier_type,
            organization_identifier_scheme_choices, organization_identifier_scheme_help_text, organization_identifier_scheme_label, organization_identifier_scheme_read_only, organization_identifier_scheme_required, organization_identifier_scheme_type,
            organization_identifier_value_label, organization_identifier_value_required, organization_identifier_value_max_length, organization_identifier_value_type, organization_identifier_value_read_only, organization_identifier_value_placeholder, organization_identifier_value_help_text,
            website_help_text, website_name_label, website_required, website_type, website_read_only, website_placeholder,
            organization_identifier_help_text, organization_identifier_label, organization_identifier_required, organization_identifier_type, organization_identifier_read_only
        }
    }

    GetOtherOfficeAddress(data) {
        let { help_text, label, required, type, read_only } = data.other_office_address;
        let { required: pk_other_office_address_required, read_only: pk_other_office_address_read_only, label: pk_other_office_address_label, type: pk_other_office_address_type } = data.other_office_address.child.children.pk;
        let { help_text: address_help_text, label: address_label, required: address_required, type: address_type, read_only: address_read_only } = data.other_office_address.child.children.address;
        let { help_text: region_help_text, label: region_label, required: region_required, type: region_type, read_only: region_read_only } = data.other_office_address.child.children.region;
        let { help_text: zip_code_help_text, label: zip_code_label, required: zip_code_required, type: zip_code_type, read_only: zip_code_read_only, max_length: zip_code_max_length } = data.other_office_address.child.children.zip_code;
        let { help_text: city_help_text, label: city_label, required: city_required, type: city_type, read_only: city_read_only } = data.other_office_address.child.children.city;
        let { help_text: country_help_text, label: country_label, required: country_required, type: country_type, read_only: country_read_only, choices: country_choices } = data.other_office_address.child.children.country;

        type = this.typeoffield(type);
        address_type = this.typeoffield(address_type);
        region_type = this.typeoffield(region_type);
        zip_code_type = this.typeoffield(zip_code_type);
        city_type = this.typeoffield(city_type);
        country_type = this.typeoffield(country_type);

        return {
            help_text, label, required, type, read_only,
            pk_other_office_address_required, pk_other_office_address_read_only, pk_other_office_address_label, pk_other_office_address_type,
            address_help_text, address_label, address_required, address_type, address_read_only,
            region_help_text, region_label, region_required, region_type, region_read_only,
            zip_code_help_text, zip_code_label, zip_code_required, zip_code_type, zip_code_read_only, zip_code_max_length,
            city_help_text, city_label, city_required, city_type, city_read_only,
            country_help_text, country_label, country_required, country_type, country_read_only, country_choices
        }

    }
    GetHeadOfficeAddress(data) {
        let { help_text, label, required, type, read_only } = data.head_office_address;
        let { required: pk_head_office_address_required, read_only: pk_head_office_address_read_only, label: pk_head_office_address_label, type: pk_head_office_address_type } = data.head_office_address.children.pk;
        let { help_text: address_help_text, label: address_label, required: address_required, type: address_type, read_only: address_read_only } = data.head_office_address.children.address;
        let { help_text: region_help_text, label: region_label, required: region_required, type: region_type, read_only: region_read_only } = data.head_office_address.children.region;
        let { help_text: zip_code_help_text, label: zip_code_label, required: zip_code_required, type: zip_code_type, read_only: zip_code_read_only, max_length: zip_code_max_length } = data.head_office_address.children.zip_code;
        let { help_text: city_help_text, label: city_label, required: city_required, type: city_type, read_only: city_read_only } = data.head_office_address.children.city;
        let { help_text: country_help_text, label: country_label, required: country_required, type: country_type, read_only: country_read_only, choices: country_choices } = data.head_office_address.children.country;

        type = this.typeoffield(type);
        address_type = this.typeoffield(address_type);
        region_type = this.typeoffield(region_type);
        zip_code_type = this.typeoffield(zip_code_type);
        city_type = this.typeoffield(city_type);
        country_type = this.typeoffield(country_type);

        return {
            help_text, label, required, type, read_only,
            pk_head_office_address_required, pk_head_office_address_read_only, pk_head_office_address_label, pk_head_office_address_type,
            address_help_text, address_label, address_required, address_type, address_read_only,
            region_help_text, region_label, region_required, region_type, region_read_only,
            zip_code_help_text, zip_code_label, zip_code_required, zip_code_type, zip_code_read_only, zip_code_max_length,
            city_help_text, city_label, city_required, city_type, city_read_only,
            country_help_text, country_label, country_required, country_type, country_read_only, country_choices
        }

    }

    getHasDivision(data) {
        let { help_text: has_division_help_text, label: has_division_label, required: has_division_required, type: has_division_type, read_only: has_division_read_only } = data.has_division;    //reserve simple label for domain    
        let { help_text: organization_bio_help_text, label: organization_bio_label, required: organization_bio_required, type: organization_bio_type, read_only: organization_bio_read_only } = data.has_division.child.children.organization_bio;
        //let { help_text: country_of_registration_help_text, label: country_of_registration_label, required: country_of_registration_required, type: country_of_registration_type, read_only: country_of_registration_read_only } = data.has_division.child.children.country_of_registration;
        //let { choices: country_of_registration_choices } = data.has_division.child.children.country_of_registration.child;
        let { help_text: division_category_help_text, label: division_category_label, required: division_category_required, type: division_category_type, read_only: division_category_read_only, choices: division_category_choices } = data.has_division.child.children.division_category;
        let { help_text: logo_help_text, label: logo_label, required: logo_required, type: logo_type, read_only: logo_read_only, placeholder: logo_placeholder } = data.has_division.child.children.logo;
        let { help_text: service_offered_help_text, label: service_offered_label, required: service_offered_required, type: service_offered_type, read_only: service_offered_read_only, placeholder: service_offered_placeholder } = data.has_division.child.children.service_offered;
        let { help_text: lt_area_help_text, label: lt_area_label, required: lt_area_required, type: lt_area_type, read_only: lt_area_read_only } = data.has_division.child.children.lt_area;
        const { recommended_choices: lt_area_recomended_choices, max_length: lt_area_max_length } = data.has_division.child.children.lt_area.child;
        let { help_text: discipline_help_text, label: discipline_label, required: discipline_required, type: discipline_type, read_only: discipline_read_only, placeholder: discipline_placeholder } = data.has_division.child.children.discipline;
        let { help_text: keyword_help_text, label: keyword_label, required: keyword_required, type: keyword_type, placeholder: keyword_placeholder } = data.has_division.child.children.keyword;

        has_division_type = this.typeoffield(has_division_type);
        organization_bio_type = this.typeoffield(organization_bio_type);
        //country_of_registration_type = this.typeoffield(country_of_registration_type);
        division_category_type = this.typeoffield(division_category_type);
        lt_area_type = this.typeoffield(lt_area_type);
        discipline_type = this.typeoffield(discipline_type);
        logo_type = this.typeoffield(logo_type);
        service_offered_type = this.typeoffield(service_offered_type);
        keyword_type = this.typeoffield(keyword_type);

        let domain = this.getDomainKeywords(data.has_division.child.children);
        let head_office_address = this.GetHeadOfficeAddress(data.has_division.child.children);
        let other_office_address = this.GetOtherOfficeAddress(data.has_division.child.children);
        let member_of_association = this.get_member_of_association(data.has_division.child.children);
        let social_media = this.getSocialMedia(data.has_division.child.children);
        let website = this.getOrganizationWebsites(data.has_division.child.children);
        let email = this.getOrganizationEmails(data.has_division.child.children);
        let telephone_number = this.getTelephoneNumbers(data.has_division.child.children);
        let fax_number = this.getFaxNumbers(data.has_division.child.children);


        return {
            has_division_help_text, has_division_label, has_division_required, has_division_type, has_division_read_only,
            organization_bio_help_text, organization_bio_label, organization_bio_required, organization_bio_type, organization_bio_read_only,
            //country_of_registration_help_text, country_of_registration_label, country_of_registration_required, country_of_registration_type, country_of_registration_read_only, country_of_registration_choices,
            division_category_help_text, division_category_label, division_category_required, division_category_type, division_category_read_only, division_category_choices,
            logo_help_text, logo_label, logo_required, logo_type, logo_read_only, logo_placeholder,
            service_offered_help_text, service_offered_label, service_offered_required, service_offered_type, service_offered_read_only, service_offered_placeholder,
            lt_area_help_text, lt_area_label, lt_area_required, lt_area_type, lt_area_read_only, lt_area_recomended_choices, lt_area_max_length,
            discipline_help_text, discipline_label, discipline_required, discipline_type, discipline_read_only, discipline_placeholder, domain, other_office_address, head_office_address, member_of_association, social_media,
            keyword_type, keyword_label, keyword_help_text, keyword_required, keyword_placeholder, website, email, telephone_number, fax_number
        }
    }

    getHasDivisionName(data) {
        let { help_text, label, required, type, placeholder } = data.has_division.child.children.division_name;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "placeholder": placeholder };
    }

    getHasDivisionShortName(data) {
        let { help_text, label, required, type, placeholder } = data.has_division.child.children.division_short_name;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "placeholder": placeholder };
    }

    getHasDivisionAltName(data) {
        let { help_text, label, required, type, placeholder } = data.has_division.child.children.division_alternative_name;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "placeholder": placeholder };
    }




}


const organizationSchemaParser = new OrganizationScemaParser();

export default organizationSchemaParser;