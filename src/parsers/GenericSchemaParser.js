class GenericSchemaParser {
    constructor() {
        this.getFormElement = this.getFormElement.bind(this);
        this.readNestedObject = this.readNestedObject.bind(this);
    }

    getFormElement = (elementName, elementSchema) => {
        //console.log(elementName,elementSchema)
        if (elementSchema.type === "field" || elementSchema.type === "url") {
            let max_length = elementSchema.max_length ? elementSchema.max_length : "";
            let recommended = elementSchema.recommended ? elementSchema.recommended : false;
            let placeholder = elementSchema.placeholder ? elementSchema.placeholder : false;
            let { help_text, label, required, read_only, type } = elementSchema;

            if (elementSchema.child) {
                if (elementSchema.child.type === "nested object") {
                    let formElements = this.readNestedObject(elementSchema.child.children);
                    //console.log(formElements)
                    return { help_text, label, required, read_only, type, max_length, recommended, formElements, placeholder };
                }
            }
            else {
                return { help_text, label, required, read_only, type, max_length, recommended, placeholder };
            }
        }

        //////////
        if (elementSchema.type === "date") {
            let { help_text, label, required, read_only, type } = elementSchema;
            let recommended = elementSchema.recommended ? elementSchema.recommended : false;
            let placeholder = elementSchema.placeholder ? elementSchema.placeholder : false;
            return { help_text, label, required, read_only, type, recommended, placeholder }
        }
        //////////
        if (elementSchema.type === "float") {
            let { help_text, label, required, read_only, type } = elementSchema;
            let recommended = elementSchema.recommended ? elementSchema.recommended : false;
            let placeholder = elementSchema.placeholder ? elementSchema.placeholder : false;
            return { help_text, label, required, read_only, type, recommended, placeholder }
        }
        //////////
        if (elementSchema.type === "email") {
            let { help_text, label, required, read_only, type } = elementSchema;
            let recommended = elementSchema.recommended ? elementSchema.recommended : false;
            let placeholder = elementSchema.placeholder ? elementSchema.placeholder : false;
            let max_length = elementSchema.max_length ? elementSchema.max_length : "";
            return { help_text, label, required, read_only, type, max_length, recommended, placeholder }
        }

        //////////
        if (elementSchema.type === "integer") {
            let { label, required, read_only, type } = elementSchema;
            return { label, required, read_only, type }
        }

        //////////
        if (elementSchema.type === "string") {
            let { label, required, read_only, type } = elementSchema;
            let help_text = elementSchema.help_text ? elementSchema.help_text : "";
            let placeholder = elementSchema.placeholder ? elementSchema.placeholder : false;
            let recommended_choices = elementSchema.recommended_choices ? elementSchema.recommended_choices : [];
            return { help_text, label, required, read_only, type, placeholder, "choices": recommended_choices }
        }

        ////////
        if (elementSchema.type === "choice") {
            let { help_text, label, required, read_only, type, choices } = elementSchema;
            let recommended = elementSchema.recommended ? elementSchema.recommended : false;
            let placeholder = elementSchema.placeholder ? elementSchema.placeholder : false;
            return { help_text, label, required, read_only, type, choices, recommended, placeholder }
        }

        ////////
        if (elementSchema.type === "nested object") {
            let { help_text, label, required, read_only, type } = elementSchema;
            let recommended = elementSchema.recommended ? elementSchema.recommended : false;
            let placeholder = elementSchema.placeholder ? elementSchema.placeholder : false;
            let formElements = this.readNestedObject(elementSchema.children);
            return { help_text, label, required, read_only, type, recommended, formElements, placeholder };
        }

        /////////
        if (elementSchema.type === "list") {
            let { help_text, label, required, read_only, type } = elementSchema;
            let recommended = elementSchema.recommended ? elementSchema.recommended : false;
            let placeholder = elementSchema.placeholder ? elementSchema.placeholder : false;
            if (elementSchema.child.type === "choice") {
                let choices = elementSchema.child.choices;
                return { help_text, label, required, read_only, type, choices, recommended, placeholder };
            }
            if (elementSchema.child.type === "field") {
                let max_length = elementSchema.child.max_length ? elementSchema.child.max_length : "";
                return { help_text, label, required, type, read_only, max_length, recommended, placeholder };
            }
            if (elementSchema.child.type === "string") {
                let child_required = elementSchema.child.required;
                let max_length = elementSchema.child.max_length ? elementSchema.child.max_length : "";
                let child_recommended = elementSchema.child.recommended ? elementSchema.child.recommended : false;
                let placeholder = elementSchema.placeholder ? elementSchema.placeholder : false;
                let recommended_choices = elementSchema.child.recommended_choices ? elementSchema.child.recommended_choices : [];
                return { help_text, label, required, read_only, type, child_required, max_length, child_recommended, "choices": recommended_choices, placeholder };
                //return { help_text, label, required, read_only, type, child_required, max_length, child_recommended, recommended_choices };
            }

            if (elementSchema.child.type === "email" || elementSchema.child.type === "url" || elementSchema.child.type === "website") {
                let max_length = elementSchema.child.max_length ? elementSchema.child.max_length : "";
                let child_required = elementSchema.child.required;
                let child_recommended = elementSchema.child.recommended ? elementSchema.child.recommended : false;
                return { help_text, label, required, read_only, type, child_required, max_length, child_recommended, placeholder };
            }

        }

        if (elementSchema.type === "boolean") {
            let { help_text, label, required, read_only, type } = elementSchema;
            let recommended = elementSchema.recommended ? elementSchema.recommended : false;
            let placeholder = elementSchema.placeholder ? elementSchema.placeholder : false;
            return { help_text, label, required, read_only, type, recommended, placeholder }
        }

    }

    //the idea is to return the help_text, required, choices, etc for each element in a nested object. 
    readNestedObject = (object) => {
        const formElements = {};
        Object.keys(object).map((key, ind) => {
            formElements[key] = (this.getFormElement(key, object[key]));
            //console.log(formElements)
            return void 0;
        })

        return formElements;

    }



}


const genericToolSchemaParser = new GenericSchemaParser();

export default genericToolSchemaParser;