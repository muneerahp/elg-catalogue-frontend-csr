class OrganizationHasDivisionParser {
    constructor() {
        this.getDomainKeywords = this.getDomainKeywords.bind(this);
        this.getLtAreaKeywords = this.getLtAreaKeywords.bind(this);
        this.getSocialMedia = this.getSocialMedia.bind(this);
        this.getDivisionWebsites = this.getDivisionWebsites.bind(this);
        this.getDivisionEmails = this.getDivisionEmails.bind(this);
        this.getTelephoneNumbers = this.getTelephoneNumbers.bind(this);
        this.getFaxNumbers = this.getFaxNumbers.bind(this);
        this.getmemberOfAssociationName = this.getmemberOfAssociationName.bind(this);
        this.getDiscipline = this.getDiscipline.bind(this);
        this.getServiceOffered = this.getServiceOffered.bind(this);
        //labels
        this.getDisciplineLabel = this.getDisciplineLabel.bind(this);
        this.getDomainKeywordsLabel = this.getDomainKeywordsLabel.bind(this);
        this.getLtAreaKeywordsLabel = this.getLtAreaKeywordsLabel.bind(this);
        this.getSocialMediaLabel = this.getSocialMediaLabel.bind(this);
        this.getDivisionWebsitesLabel = this.getDivisionWebsitesLabel.bind(this);
        this.getDivisionEmailsLabel = this.getDivisionEmailsLabel.bind(this);
        this.getTelephoneNumbersLabel = this.getTelephoneNumbersLabel.bind(this);
        this.getFaxNumbersLabel = this.getFaxNumbersLabel.bind(this);
        this.getmemberOfAssociationNameLabel = this.getmemberOfAssociationNameLabel.bind(this);
        this.getDivisionCategoryLabel = this.getDivisionCategoryLabel.bind(this);
        this.getOrganizationBioLabel = this.getOrganizationBioLabel.bind(this);
        this.getOrganizationBio = this.getOrganizationBio.bind(this);
        this.getCountryOfRegistrationLabel = this.getCountryOfRegistrationLabel.bind(this);
        this.getServiceOfferedLabel = this.getServiceOfferedLabel.bind(this);
        this.getKeywordLabel = this.getKeywordLabel.bind(this);

    }

    getServiceOffered(data, lang) {
        let servicesOffered = [];
        if (data.service_offered && data.service_offered.field_value && data.service_offered.field_value.length > 0) {
            data.service_offered.field_value.forEach((keyword, index) => {
                servicesOffered.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
        }
        return servicesOffered;
    }

    getDiscipline(data, lang) {
        let disciplines = [];
        if (data.discipline && data.discipline.field_value && data.discipline.field_value.length > 0) {
            data.discipline.field_value.forEach((keyword, index) => {
                disciplines.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
        }
        return disciplines;
    }

    getDisciplineLabel(data, lang) {
        let disciplinelabel = "";
        if (data.discipline && data.discipline.field_label) {
            disciplinelabel = data.discipline.field_label[lang] ||
                data.discipline.field_label["en"];
        }
        return disciplinelabel;
    }

    getKeywords(data, lang) {
        let keywords = [];
        if (data.keyword && data.keyword.field_value && data.keyword.field_value.length > 0) {
            data.keyword.field_value.forEach((keyword, index) => {
                keywords.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
        }
        return keywords;
    }

    getKeywordLabel(data, lang) {
        let keywordlabel = "";
        if (data.keyword && data.keyword.field_label) {
            keywordlabel = data.keyword.field_label[lang] ||
                data.keyword.field_label["en"];
        }
        return keywordlabel;
    }


    getOrganizationBio(data, lang) {
        let organization_bio = "";
        if (data.organization_bio && data.organization_bio.field_value) {
            organization_bio = data.organization_bio.field_value[lang] ||
                data.organization_bio.field_value[Object.keys(data.organization_bio.field_value)[0]];
        }
        return organization_bio;
    }

    getOrganizationBioLabel(data, lang) {
        let organization_biolabel = "";
        if (data.organization_bio && data.organization_bio.field_label) {
            organization_biolabel = data.organization_bio.field_label[lang] ||
                data.organization_bio.field_label["en"];
        }
        return organization_biolabel;
    }

    getCountryOfRegistrationLabel(data, lang) {
        let country_of_registrationlabel = "";
        if (data.country_of_registration && data.country_of_registration.field_label) {
            country_of_registrationlabel = data.country_of_registration.field_label[lang] ||
                data.country_of_registration.field_label["en"];
        }
        return country_of_registrationlabel;
    }
    getServiceOfferedLabel(data, lang) {
        let service_offeredlabel = "";
        if (data.service_offered && data.service_offered.field_label) {
            service_offeredlabel = data.service_offered.field_label[lang] ||
                data.service_offered.field_label["en"];
        }
        return service_offeredlabel;
    }




    getmemberOfAssociationName(data, lang) {
        let memberOfAssociations = [];
        if (data.member_of_association && data.member_of_association.field_value && data.member_of_association.field_value.length > 0) {
            data.member_of_association.field_value.forEach((memberOfAssociation, memberOfAssociationIndex) => {
                //let pk = memberOfAssociation.pk;
                let organization_name = memberOfAssociation.organization_name.field_value[lang] || memberOfAssociation.organization_name.field_value[Object.keys(memberOfAssociation.organization_name.field_value)[0]];
                let website = memberOfAssociation.website ? memberOfAssociation.website.field_value : "";
                let organization_identifier = memberOfAssociation.organization_identifier ? [...memberOfAssociation.organization_identifier.field_value] : [];

                //let organization_identifier_name=organization_identifier.label[lang] || organization_identifier.label[Object.keys(organization_identifier.label[0])];

                memberOfAssociations.push({ organization_name, website, organization_identifier });
            }
            )
        }
        return memberOfAssociations;
    }

    getmemberOfAssociationNameLabel(data, lang) {
        let memberOfAssociationslabel = "";
        if (data.member_of_association && data.member_of_association.field_label) {
            memberOfAssociationslabel = data.member_of_association.field_label[lang] ||
                data.member_of_association.field_label["en"];
        }
        return memberOfAssociationslabel;
    }

    getDomainKeywords(data, lang) {
        let domainKeywords = [];
        if (data.domain && data.domain.field_value && data.domain.field_value.length > 0) {
            data.domain.field_value.forEach((keyword, index) => {
                domainKeywords.push(keyword.category_label.field_value[lang] || keyword.category_label.field_value[Object.keys(keyword.category_label.field_value)[0]])
            });
        }
        return domainKeywords;
    }

    getDomainKeywordsLabel(data, lang) {
        let domainKeywordslabel = "";
        if (data.domain && data.domain.field_label) {
            domainKeywordslabel = data.domain.field_label[lang] ||
                data.domain.field_label["en"];

        }
        return domainKeywordslabel;
    }

    getLtAreaKeywords(data, lang) {
        let LtAreaKeywordsArray = data.lt_area ?
            data.lt_area.field_value.map(keyword =>
                (keyword.label ?
                    (keyword.label[lang] ||
                        keyword.label[Object.keys(keyword.label)[0]]) :
                    keyword.value)) : [];
        return LtAreaKeywordsArray;
    }



    getLtAreaKeywordsLabel(data, lang) {
        let LtAreaKeywordslabel = "";
        if (data.lt_area && data.lt_area.field_label) {
            LtAreaKeywordslabel = data.lt_area.field_label[lang] ||
                data.lt_area.field_label["en"];

        }
        return LtAreaKeywordslabel;
    }

    get_head_office_AddressSet = (data) => {
        let headOfficeAddress = [];
        if (data.head_office_address && data.head_office_address.field_value) {
            headOfficeAddress.push(data.head_office_address.field_value);
        }
        return headOfficeAddress;
    }

    get_head_office_address_label = (data, lang) => {
        let head_office_address_label = "";
        if (data.head_office_address && data.head_office_address.field_label) {
            head_office_address_label = data.head_office_address.field_label[lang] ||
                data.head_office_address.field_label["en"];
        }
        return head_office_address_label;
    }

    get_other_office_address_label = (data, lang) => {
        let other_office_address_label = "";
        if (data.other_office_address && data.other_office_address.field_label) {
            other_office_address_label = data.other_office_address.field_label[lang] ||
                data.other_office_address.field_label["en"];
        }
        return other_office_address_label;
    }

    get_other_office_AddressSet = (data) => {
        let otherOfficeAddress = [];
        if (data.other_office_address && data.other_office_address.field_value && data.other_office_address.field_value.length > 0) {
            otherOfficeAddress = data.other_office_address.field_value;
        }
        return otherOfficeAddress;
    }

    getTelephoneNumbers(data) {
        let telephonenumbers = [];
        if (data.telephone_number && data.telephone_number.field_value && data.telephone_number.field_value.length > 0) {
            data.telephone_number.field_value.forEach((keyword, index) => {
                telephonenumbers.push(keyword)
            });
        }
        return telephonenumbers;
    }

    getTelephoneNumbersLabel(data, lang) {
        let telephonenumberslabel = "";
        if (data.telephone_number && data.telephone_number.field_label) {
            telephonenumberslabel = data.telephone_number.field_label[lang] ||
                data.telephone_number.field_label["en"];
        }
        return telephonenumberslabel;
    }


    getFaxNumbers(data) {
        let faxnumbers = [];
        if (data.fax_number && data.fax_number.field_value && data.fax_number.field_value.length > 0) {
            data.fax_number.field_value.forEach((keyword, index) => {
                faxnumbers.push(keyword)
            });
        }
        return faxnumbers;
    }

    getFaxNumbersLabel(data, lang) {
        let faxnumberslabel = "";
        if (data.fax_number && data.fax_number.field_label) {
            faxnumberslabel = data.fax_number.field_label[lang] ||
                data.fax_number.field_label["en"];
        }
        return faxnumberslabel;
    }


    getSocialMedia(data) {
        let socialMedia = {};
        if (data.social_media_occupational_account && data.social_media_occupational_account.field_value && data.social_media_occupational_account.field_value.length > 0) {
            socialMedia = data.social_media_occupational_account.field_value;
        }
        return socialMedia;
    }

    getSocialMediaLabel(data, lang) {
        let socialMedialabel = "";
        if (data.social_media_occupational_account && data.social_media_occupational_account.field_label) {
            socialMedialabel = data.social_media_occupational_account.field_label[lang] ||
                data.social_media_occupational_account.field_label["en"];
        }
        return socialMedialabel;
    }

    getDivisionEmails(data) {
        let emails = [];
        if (data.email && data.email.field_value && data.email.field_value.length > 0) {
            data.email.field_value.forEach((keyword, index) => {
                emails.push(keyword)
            });
        }
        return emails;
    }

    getDivisionEmailsLabel(data, lang) {
        let emailslabel = "";
        if (data.email && data.email.field_label) {
            emailslabel = data.email.field_label[lang] ||
                data.email.field_label["en"];
        }
        return emailslabel;
    }

    getDivisionWebsites(data) {
        let websites = [];
        if (data.website && data.website.field_value && data.website.field_value.length > 0) {
            data.website.field_value.forEach((keyword, index) => {
                websites.push(keyword)
            });
        }
        return websites;
    }

    getDivisionWebsitesLabel(data, lang) {
        let websiteslabel = "";
        if (data.website && data.website.field_label) {
            websiteslabel = data.website.field_label[lang] ||
                data.website.field_label["en"];
        }
        return websiteslabel;
    }

    getDivisionCategoryLabel(data, lang) {
        let divisioncategorylabel = "";
        if (data.division_category && data.division_category.field_label) {
            divisioncategorylabel = data.division_category.field_label[lang] ||
                data.division_category.field_label["en"];
        }
        return divisioncategorylabel;
    }


    getDivisionAltName = (data, lang) => {
        let divisionAltName = [];
        if (data.division_alternative_name && data.division_alternative_name.field_value && data.division_alternative_name.field_value.length > 0) {
            data.division_alternative_name.field_value.forEach((keyword, index) => {
                divisionAltName.push(keyword[lang] || keyword[Object.keys(keyword)[0]])
            });
        }
        return divisionAltName;
    }

}


const organizationHasDivisionParser = new OrganizationHasDivisionParser();

export default organizationHasDivisionParser;