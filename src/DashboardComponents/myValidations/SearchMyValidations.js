import React from "react";
import axios from "axios";
import { FormattedMessage } from 'react-intl';
import MyValidationsList from "./MyValidationsList";
import DashboardFacetsComponent from "../DashboardFacetsComponent";
import { Input } from "reactstrap";
//import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ReorderIcon from '@material-ui/icons/Reorder';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import CircularProgress from '@material-ui/core/CircularProgress';
//import DashboardAppBar from "./DashboardAppBar";
import {
    SERVER_API_MANAGEMENT_VALIDATION_URL, searchManagementFacetValidationUrl, searchManagmentKeywordCombineWithFacetValidationUrl,
    //    searchManagmentKeywordCombineWithFacetUrl
} from "../../config/constants";
import messages from "../../config/messages";
import { Helmet } from "react-helmet";
import BulkActions from "../bulk_actions/BulkActions";
import SortComponentValidations from "../SortComponentValidations";
import queryString from 'query-string';

class SearchMyValidations extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '', width: window.innerWidth, searchKeyword: this.getInitialSearchKeyword(),
            facetSearchKeyword: '', selectedFacets: [], keycloak: props.keycloak, source: null, loading: false,
            checkedList: [], sortBy: "",
        };
        this.isUnmound = false;
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
        if (this.props.match.params && this.props.match.params.term) {
            let keywordSearch = this.props.match.params.term;
            this.getKeywordBasedSearchResults(keywordSearch);
        } else {
            this.getAllResults();
        }
    }

    componentWillUnmount() {
        this.isUnmound = false;
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }

    componentDidUpdate(prevProps, prevState) {
        let keyword1 = this.props && this.props.match && this.props.match.params && this.props.match.params.term;
        let keyword2 = prevProps.match && prevProps.match.params && prevProps.match.params.term;
        //let search1 = this.props && this.props.location && this.props.location.search;
        //let search2 = prevProps && prevProps.location && prevProps.location.search;
        if (keyword1 !== keyword2) {//|| search1 !== search2 //messes up sorted by parameters
            if (keyword1) {
                this.getKeywordBasedSearchResults(keyword1);
            } else {
                this.getAllResults();
            }
        }
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };

    getInitialSearchKeyword = () => {
        if (this.props.match.params && this.props.match.params.term) {
            return this.props.match.params.term;
        } else {
            return '';
        }
    }

    getAllResults = () => {
        this.getKeywordBasedSearchResults(null);
    }

    getKeywordBasedSearchResults = (keyword) => {
        try {
            let facetSearchFromURL = this.props.location ? this.props.location.search : "";
            const parsed = queryString.parse(facetSearchFromURL);
            if (facetSearchFromURL) {
                facetSearchFromURL = facetSearchFromURL.substring(facetSearchFromURL.indexOf("?") + 1);
            }
            if (Object.keys(parsed).length > 0) {
                const arrayWithFacetValues = [];
                for (const [key, value] of Object.entries(parsed)) {
                    if (key === "page") {
                        if (!Number.isInteger(Number(value)) && Number(value) <= 0) {
                            this.props.history.push("/");
                            return;
                        }
                        continue;
                    }
                    if (Array.isArray(value)) {
                        for (let valueIndex = 0; valueIndex < value.length; valueIndex++) {
                            arrayWithFacetValues.push(decodeURIComponent(value[valueIndex]));
                        }
                    } else {
                        if (key === "username") {
                            arrayWithFacetValues.push("username");
                        } else if (key === "has_data__term") {
                            arrayWithFacetValues.push("has_data__term" + value);
                        } else if (key === "metadata_valid__term") {
                            arrayWithFacetValues.push("metadata_valid__term" + value);
                        } else if (key === "legally_valid__term") {
                            arrayWithFacetValues.push("legally_valid__term" + value);
                        } else if (key === "functional_service__term") {
                            arrayWithFacetValues.push("functional_service__term" + value);
                        } else {
                            arrayWithFacetValues.push(decodeURIComponent(value));
                        }
                    }
                }
                this.setState({ facetSearchKeyword: facetSearchFromURL, selectedFacets: arrayWithFacetValues });
            } else {
                this.setState({ searchKeyword: keyword ? keyword : "", facetSearchKeyword: "", selectedFacets: [] });
            }
            if (this.state.source) {
                this.state.source.cancel("");
            }
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            this.setState({ source: source, loading: true });
            this.clearSortedBy();
            if (facetSearchFromURL) {//make complex query with facets included
                axios.get(searchManagmentKeywordCombineWithFacetValidationUrl(keyword || "", facetSearchFromURL))
                    .then(res => {

                        this.setState({ data: res.data, searchKeyword: keyword || "", facetSearchKeyword: facetSearchFromURL, source: null, loading: false });

                    }).catch(err => { this.setState({ source: null, loading: false }); });;
            } else {
                //console.log(searchManagmentKeywordCombineWithFacetValidationUrl(keyword || "",""))
                axios.get(searchManagmentKeywordCombineWithFacetValidationUrl(keyword || "", ""))
                    .then(res => {
                        this.setState({ data: res.data, searchKeyword: keyword || "", facetSearchKeyword: "", selectedFacets: [], source: null, loading: false });
                    }).catch(err => { this.setState({ source: null, loading: false }); });
            }
        } catch (err) {
            console.log(err);
        }
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    getNextPage = () => {
        this.isUnmound = true;
        const nextPageUrl = this.state.data.next;
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        axios.get(nextPageUrl)
            .then(res => {
                if (this.isUnmound) {
                    this.setState(res);
                    this.setState({ source: null, loading: false });
                }
            }).catch(err => { this.setState({ source: null, loading: false }); });
    }

    getPreviousPage = () => {
        this.isUnmound = true;
        const previousPageUrl = this.state.data.previous;
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        axios.get(previousPageUrl)
            .then(res => {
                if (this.isUnmound) {
                    this.setState(res);
                    this.setState({ source: null, loading: false });
                }
            }).catch(err => { this.setState({ source: null, loading: false }); });
    }

    search = (event) => {
        event && event.preventDefault();
        if (!this.state.searchKeyword) {
            this.props.history.push({ pathname: '/myValidations', state: { tab: 'myval' } });
            //this.getAllResults();
            this.clearAllSearchFilters();
            return false;
        }
        if (this.state.searchKeyword && this.state.searchKeyword.length === 0) {
            this.props.history.push({ pathname: '/myValidations', state: { tab: 'myval' } });
            this.getAllResults();
            return false;
        }
        let previousfacetSearchKeyword = this.state.facetSearchKeyword;
        if (this.state.facetSearchKeyword) {
            this.props.history.push(`/myValidations/${encodeURIComponent(this.state.searchKeyword)}?${previousfacetSearchKeyword}`);
        } else {
            this.props.history.push(`/myValidations/${encodeURIComponent(this.state.searchKeyword)}`);
        }
        //this.getKeywordBasedSearchResults(this.state.searchKeyword)
        return false;
    }

    handleChange = (event) => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const searchTerm = event.target.value;
        this.setState({ searchKeyword: searchTerm });
        if (searchTerm.length === 0) {
            this.clearAllSearchFilters();
            return false;
        }
    }

    handleFacetChange = (value) => {
        if (value.length === 0) {
            this.getAllResults();
            return false;
        }
        value = value.split("=")[0] + "=" + encodeURIComponent(value.split("=")[1]);
        let previousfacetSearchKeyword = this.state.facetSearchKeyword;
        if (previousfacetSearchKeyword.length > 1 && previousfacetSearchKeyword.indexOf(value) === -1) {
            value = `${previousfacetSearchKeyword}&${value}`;
            this.setState({ facetSearchKeyword: value });
        } else if (previousfacetSearchKeyword.length > 1 && previousfacetSearchKeyword.indexOf(value) !== -1) {
            value = `${previousfacetSearchKeyword}`;//value is already in url, do not append it and do not clear the previous facets
            this.setState({ facetSearchKeyword: value });
        } else if (previousfacetSearchKeyword === "") {
            this.setState({ facetSearchKeyword: value });
        }
        this.props.history.push(`?${value}`);
        this.isUnmound = true;
        this.setState({ loading: true })
        if (!this.state.searchKeyword) {//if there is no search keyword just make a generic facet search
            axios.get(searchManagementFacetValidationUrl(value))
                .then(res => {
                    if (this.isUnmound) {
                        this.setState({ data: res.data, searchKeyword: "", loading: false });
                    }
                }).catch(err => { this.setState({ loading: false }) });;
        } else {
            axios.get(searchManagmentKeywordCombineWithFacetValidationUrl(this.state.searchKeyword || "", value))
                .then(res => {
                    if (this.isUnmound) {
                        this.setState({ data: res.data, searchKeyword: this.state.searchKeyword || "", facetSearchKeyword: value, loading: false });
                    }
                }).catch(err => { this.setState({ loading: false }) });
        }
        return false;
    }

    unselectFacet = (value2Unselect) => {
        let previousfacetSearchKeyword = this.state.facetSearchKeyword;
        const parsed = queryString.parse(previousfacetSearchKeyword);
        const [key, value] = value2Unselect.split("=");
        if (key === "username") {
            if (value.trim()) {
                parsed[key] = value;//update username with new username value
            } else {
                delete parsed[key];//delete the username filter when it is empty
            }
        } else {
            if (parsed[key]) {
                if (Array.isArray(parsed[key])) {
                    const index = parsed[key].indexOf(value);
                    parsed[key].splice(index, 1);
                    if (parsed[key].length === 0) {
                        delete parsed[key]
                    }
                } else {
                    delete parsed[key]
                }
            }
        }
        let newSearchValue = '';
        if (Object.keys(parsed).length > 0) {
            for (const [key, value] of Object.entries(parsed)) {
                if (key === "page") {
                    continue;
                }
                if (Array.isArray(value)) {
                    for (let valueIndex = 0; valueIndex < value.length; valueIndex++) {
                        newSearchValue += key + "=" + encodeURIComponent(value[valueIndex]) + "&"
                    }
                } else {
                    newSearchValue += key + "=" + encodeURIComponent(value) + "&"
                }
            }
            if (newSearchValue.endsWith("&")) {
                newSearchValue = newSearchValue.substring(0, newSearchValue.lastIndexOf("&"));
            }
        }
        const newSelectedFacets = [];
        for (const [key, value] of Object.entries(parsed)) {
            if (key === "username") {
                newSelectedFacets.push("username");
            } else if (key === "has_data__term") {
                newSelectedFacets.push("has_data__term" + value);
            } else if (key === "metadata_valid__term") {
                newSelectedFacets.push("metadata_valid__term" + value);
            } else if (key === "legally_valid__term") {
                newSelectedFacets.push("legally_valid__term" + value);
            } else if (key === "functional_service__term") {
                newSelectedFacets.push("functional_service__term" + value);
            } else {
                newSelectedFacets.push(value);
            }
        }
        this.setState({ selectedFacets: newSelectedFacets });
        this.props.history.push(`?${newSearchValue}`);
        axios.get(searchManagmentKeywordCombineWithFacetValidationUrl(this.state.searchKeyword || "", newSearchValue))
            .then(res => {
                this.setState({ data: res.data, searchKeyword: this.state.searchKeyword || "", facetSearchKeyword: newSearchValue, loading: false });
            }).catch(err => { this.setState({ loading: false }) });
        return false;
    }

    clearAllSearchFilters = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source });
        this.setState({ searchKeyword: "", facetSearchKeyword: "", selectedFacets: [] });
        this.setState({ loading: true })
        this.props.history.push({ pathname: '/myValidations', state: { tab: 'myval' } });
        this.isUnmound = true;
        this.clearChekcedList();
        this.clearSortedBy();
        axios.get(SERVER_API_MANAGEMENT_VALIDATION_URL)
            .then(res => {
                if (this.isUnmound) {
                    this.setState(res);
                    this.setState({ searchKeyword: "", selectedFacets: [], facetSearchKeyword: '', loading: false });
                }
            }).catch(err => { this.setState({ loading: false }) });
        return false;
    }

    markFacetAsSelected = (facetName) => {
        if (this.state.selectedFacets.indexOf(facetName) === -1) {//checks if i have allready selected the facet, if not add it to array
            this.setState(prevState => ({ selectedFacets: [...prevState.selectedFacets, facetName] }));
        }
        this.clearChekcedList();
    }

    handleUnselectedFacet = (index) => {
        const selectedArray = [...this.state.selectedFacets];
        selectedArray.splice(index, 1);
        this.setState({ selectedFacets: selectedArray });
        this.clearChekcedList();
    }

    addCheckedResource2List = (resource) => {
        const { checkedList } = this.state;
        checkedList.push(resource);
        this.setState({ checkedList });
    }

    removeResourceFromCheckedList = (resource) => {
        let { checkedList } = this.state;
        checkedList = checkedList.filter(item => item.id !== resource.id);
        this.setState({ checkedList });
    }

    clearChekcedList = () => {
        this.setState({ checkedList: [] });
        this.clearSortedBy();
    }

    updateRecords = (resArray) => {
        this.setState({ checkedList: [] });
        this.clearAllSearchFilters();
    }

    clearSortedBy = () => {
        this.setState({ sortBy: "" });
    }

    sortBy = (value) => {
        if (!value) {
            return;
        }
        let newOrderingQuery = value.value;
        const oldOrderingQuery = this.state.sortBy;
        this.setState({ sortBy: value });
        if (newOrderingQuery !== oldOrderingQuery) {
            const facetsAndOrdering = this.state.facetSearchKeyword ? `${this.state.facetSearchKeyword}&${newOrderingQuery}` : `${newOrderingQuery}`;
            if (this.state.source) {
                this.state.source.cancel("");
            }
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            this.setState({ source: source, loading: true });
            this.props.history.push(`?${facetsAndOrdering}`);
            if (!this.state.searchKeyword) {//if there is no search keyword just make a generic facet search
                axios.get(searchManagementFacetValidationUrl(facetsAndOrdering))
                    .then(res => {
                        if (this.isUnmound) {
                            this.setState({ data: res.data, source: null, loading: false });
                        }
                    }).catch(err => { this.setState({ source: null, loading: false }); });
            } else {
                axios.get(searchManagmentKeywordCombineWithFacetValidationUrl(this.state.searchKeyword || "", facetsAndOrdering))
                    .then(res => {
                        if (this.isUnmound) {
                            this.setState({ data: res.data, source: null, loading: false });
                        }
                    }).catch(err => { this.setState({ source: null, loading: false }) });
            }
        }
    }

    render() {
        //console.log(this.state.data.results)
        return (<div>
            <Helmet>
                <title>ELG {messages.dashboard_my_validations}</title>
            </Helmet>


            <section className="wd-100" style={{ cursor: this.state.loading ? "progress" : "" }}>
                <div className="empty"></div>

                <Paper elevation={3} className="dashboard-content">

                    <Grid container direction="row" justifyContent="space-between" alignItems="center">
                        <Grid item container xs={12} spacing={3} className="dashboard_searchClass" direction="row" justifyContent="space-between" alignItems="center">
                            <form onSubmit={this.search} className="dashboard_searchClass_form">
                                <Grid item xs={12} container spacing={3} direction="row" justifyContent="space-between" alignItems="center">
                                    <Grid item xs={9} sm={9} >
                                        <FormattedMessage id="searchCatalogue.searchPlaceholder" defaultMessage={messages.dashboard_search_box} >
                                            {placeholder =>
                                                <Input type="search" className="search-box__input" value={decodeURIComponent(this.state.searchKeyword)} onChange={this.handleChange} placeholder={placeholder} />
                                            }
                                        </FormattedMessage>
                                    </Grid>
                                    <Grid item xs={3} sm={3}>
                                        <Button onClick={this.search} className="inner-link-outlined--teal wd-100">
                                            <FormattedMessage id="searchCatalogue.searchButton" defaultMessage={messages.search} ></FormattedMessage>
                                        </Button>
                                    </Grid>
                                </Grid>
                            </form>
                        </Grid>

                        <Grid item container alignItems="flex-start" xs={12} spacing={3} sm={12} className="dashboard-results-area">

                            <Grid item container xs={3} className="search-results-container">
                                <Hidden only={['lg', 'md', 'sm', 'xl']}>
                                    <Grid item xs={12} className="left-facets-area">
                                        <Accordion>
                                            <AccordionSummary expandIcon={<ReorderIcon />} aria-controls="panel1a-content" id="panel1a-header" >
                                                <Typography> Filters </Typography>
                                            </AccordionSummary>
                                            <AccordionDetails>
                                                <div>{this.state.data.facets && <DashboardFacetsComponent facets={this.state.data.facets} onSelectKeyword={this.handleFacetChange} onClearAllSearchFilters={this.clearAllSearchFilters} unselectFacet={this.unselectFacet} selectedFacets={this.state.selectedFacets} markFacetAsSelected={this.markFacetAsSelected} handleUnselectedFacet={this.handleUnselectedFacet} tab='myval' />}</div>
                                            </AccordionDetails>
                                        </Accordion>
                                    </Grid>
                                </Hidden>
                                <Hidden only='xs'>
                                    <Grid item xs={12} className="left-facets-area">
                                        {this.state.data.facets && <DashboardFacetsComponent facets={this.state.data.facets} onSelectKeyword={this.handleFacetChange} onClearAllSearchFilters={this.clearAllSearchFilters} unselectFacet={this.unselectFacet} selectedFacets={this.state.selectedFacets} markFacetAsSelected={this.markFacetAsSelected} handleUnselectedFacet={this.handleUnselectedFacet} tab='myval' />}
                                    </Grid>
                                </Hidden>
                            </Grid>

                            <Grid item container xs={9} sm={9} alignItems="center" >
                                <Grid item container xs={12} justifyContent="flex-end" alignItems="center" spacing={2} className="DashboardResultsCount">
                                    <Grid item xs={8} sm={8}>
                                        <BulkActions checkedList={this.state.checkedList} updateRecords={this.updateRecords} />
                                    </Grid>

                                    <Grid item xs={4} sm={4}>
                                        <span className="text-small" style={{ float: "right" }}>
                                            {this.state.data.count}&nbsp;search results {`${(this.props?.match?.params?.term && this.props?.match?.params?.term?.length >= 3) ? `for ${decodeURIComponent(this.props?.match?.params?.term)}` : ""}`}{this.state.loading ? <CircularProgress size={20} /> : void 0}
                                        </span>
                                    </Grid>
                                </Grid>

                                {
                                    <Grid container className="CardContainer" alignItems="stretch" direction="row" justifyContent="center">
                                        <Grid item xs={12} container spacing={3} alignItems="center" justifyContent="flex-start" className="ResourceListItem--header">
                                            <Grid item xs={1}>
                                                <SortComponentValidations sortBy={this.sortBy} selectedValue={this.state.sortBy} />
                                            </Grid>
                                            <Grid item xs={9}>
                                                <Typography variant="h6">Resource name</Typography>
                                            </Grid>
                                            {/*<Grid item xs={2} className="container-align-center">
                                                    <Typography variant="h6">Actions</Typography>
                                                </Grid>*/}
                                            <Grid item xs={2} className="container-align-center">
                                                <Typography variant="h6">Status</Typography>
                                            </Grid>
                                        </Grid>

                                        {this.state.data.results &&
                                            this.state.data.results.map(
                                                (resource, index) => {
                                                    return <MyValidationsList
                                                        key={JSON.stringify(resource)}
                                                        resource={resource}
                                                        keycloak={this.state.keycloak}
                                                        addCheckedResource2List={this.addCheckedResource2List}
                                                        removeResourceFromCheckedList={this.removeResourceFromCheckedList}
                                                        checkedList={this.state.checkedList}
                                                        updateRecords={this.updateRecords}
                                                    />
                                                }
                                            )}
                                    </Grid>

                                }
                            </Grid>

                        </Grid>

                        <Grid item container xs={12} className="botomMargin pt-3" justifyContent="center" alignItems="center">
                            <Grid item xs={3} sm={2}> {this.state.data.previous && <Button disabled={this.state.loading} onClick={this.getPreviousPage} classes={{ root: 'inner-link-outlined--teal' }}>&#8592;
                                <FormattedMessage id="searchCatalogue.previousPage" defaultMessage={messages.previous} >
                                </FormattedMessage> </Button>}</Grid>
                            <Grid item xs={6} sm={8}>{null && this.state.data.next}<div>{null && this.state.data.previous}</div></Grid>
                            <Grid item xs={3} sm={2} style={{ textAlign: "right" }}> {this.state.data.next && <Button disabled={this.state.loading} onClick={this.getNextPage} classes={{ root: 'inner-link-outlined--teal' }}>
                                <FormattedMessage id="searchCatalogue.nextPage" defaultMessage={messages.next} >
                                </FormattedMessage> &#8594;</Button>}</Grid>
                        </Grid>

                    </Grid>

                </Paper>
            </section>



        </div >
        )
    }

}


export default SearchMyValidations;