
import React from "react";
import { Redirect, withRouter } from "react-router-dom";
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import { ReactComponent as HelpImage } from "./../assets/images/undraw_questions_re_1fy7.svg";

class DashboardHomeNeedHelpArea extends React.Component {


  render() {
    if (!this.props.keycloak || !this.props.keycloak.authenticated) {
      return <Redirect to="/" />
    }

    return (<>

      {/* a new grid row*/}

      <Grid item container direction="row" justifyContent="flex-start" alignItems="stretch" spacing={2} sm={12} xs={12}>
        <Grid item sm={12} xs={12} style={{ display: 'flex' }}>
          <Card style={{ width: '100%', display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }}>
            <CardContent>
              <Grid container direction="row" justifyContent="space-between" spacing={1}>
                <Grid item sm={3}>
                  <HelpImage className="big-icon" />
                </Grid>

                <Grid item sm={9}>
                  <Typography variant="h3" className="teal--font pb-3"> Need Help ?  </Typography>
                  <Typography variant="body1" className="pb-3"> Find more information on the use of the platform in the ELG User Manual  </Typography>

                  <div className="pt-2">
                    <a className="inner-link-outlined--teal" href={"https://european-language-grid.readthedocs.io/"} target={"_blank"} rel="noreferrer" style={{ float: "left", fontWeight: "bolder", fontSize: "0.875rem", lineHeight: "1.75" }}>View manual</a>
                  </div>
                </Grid>


              </Grid>

            </CardContent>
          </Card>
        </Grid>

      </Grid>




    </>
    );
  }
}

export default withRouter(DashboardHomeNeedHelpArea);
