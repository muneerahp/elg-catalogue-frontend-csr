import React from "react";
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import messages from "../../config/messages";
import IngestBulk from "./IngestBulk";
import RequestUnpublishBulk from "./RequestUnpublishBulk";
import DeleteBulk from "./DeleteBulk";
import ExportBatch from "./ExportBatch";
import DeactivateVersionBatch from "./DeactivateVersionBatch";

export default class BulkActions extends React.Component {

    constructor(props) {
        super(props);
        this.state = { selectedAction: "" }
    }

    clearSelect = (value) => {
        this.setState({ selectedAction: value });
    }

    getRecordsOfStatus = (array, status) => {
        let status_items = this.props.checkedList?.filter(item => item.status === status) || []
        if (status_items.length) {
            switch (status) {
                case "internal":
                    array.push(messages.editor_action_ingest_primary);
                    array.includes(messages.editor_action_delete_primary) ? void 0 : array.push(messages.editor_action_delete_primary);
                    array.includes(messages.editor_action_export_data) ? void 0 : array.push(messages.editor_action_export_data);
                    break;
                case "published":
                    status_items = status_items.filter(item => item.unpublication_requested !== true);
                    status_items.length > 0 && array.push(messages.editor_action_unpublish_primary);
                    array.includes(messages.editor_action_export_data) ? void 0 : array.push(messages.editor_action_export_data);
                    const deactivate_version_items = status_items.filter(item => item.elg_compatible_service && item.is_active_version && !item.is_latest_version) || [];
                    deactivate_version_items.length > 0 ? array.push(messages.editor_action_deactivate_vesrion_primary) : void 0;
                    break;
                case "draft":
                    !array.includes(messages.editor_action_delete_primary) && array.push(messages.editor_action_delete_primary);
                    break;
                case "ingested":
                    array.includes(messages.editor_action_export_data) ? void 0 : array.push(messages.editor_action_export_data);
                    break;
                default: break;
            }
        }
        return status_items;
    }

    render() {
        const choices = []
        const internal_items = this.getRecordsOfStatus(choices, "internal");
        const published_items = this.getRecordsOfStatus(choices, "published");
        const draft_items = this.getRecordsOfStatus(choices, "draft");
        const ingested_items = this.getRecordsOfStatus(choices, "ingested");
        const unpublication_requested_items = this.props.checkedList?.filter(item => item.status === "published").filter(item => item.unpublication_requested === true) || [];
        const deactivate_version_items = this.props.checkedList?.filter(item => item.status === "published").filter(item => item.elg_compatible_service && item.is_active_version && !item.is_latest_version)

        switch (this.state.selectedAction) {
            case messages.editor_action_ingest_primary:
                return <IngestBulk {...this.props} internal_items={internal_items} updateRecords={this.props.updateRecords} clearSelect={this.clearSelect} />
            case messages.editor_action_unpublish_primary:
                return <RequestUnpublishBulk {...this.props} published_items={published_items} updateRecords={this.props.updateRecords} clearSelect={this.clearSelect} />
            case messages.editor_action_delete_primary:
                return <DeleteBulk {...this.props} internal_and_draft_items={internal_items.concat(draft_items)} updateRecords={this.props.updateRecords} clearSelect={this.clearSelect} />
            case messages.editor_action_export_data:
                return <ExportBatch {...this.props} internal_and_ingested_and_published_items={internal_items.concat(ingested_items, published_items, unpublication_requested_items)} updateRecords={this.props.updateRecords} clearSelect={this.clearSelect} />
            case messages.editor_action_deactivate_vesrion_primary:
                return <DeactivateVersionBatch {...this.props} deactivate_version_items={deactivate_version_items} updateRecords={this.props.updateRecords} clearSelect={this.clearSelect} />
            default: break;
        }
        if (choices.length === 0) {
            return <></>;
        }
        return <>

            <FormControl variant="outlined" className="wd-100">
                <InputLabel htmlFor="outlined-bulk-action">Action</InputLabel>
                <Select
                    variant="outlined"
                    native
                    value={this.state.selectedAction}
                    onChange={(e) => { this.setState({ selectedAction: e.target.value }); }}
                    label="Action"
                    inputProps={{
                        name: 'bulk-action',
                        id: 'bulk-action',
                    }}
                >
                    <option aria-label="None" value="" />
                    {choices.map((item, index) => <option key={index} value={item}>{item}</option>)}
                </Select>
                <FormHelperText>Select an action to perform on your selected items</FormHelperText>
            </FormControl>

        </>
    }
}