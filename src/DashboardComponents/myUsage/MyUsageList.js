import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withRouter, Link } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import {
    TOOL_SERVICE, CORPUS, LD, ML_MODEL, GRAMMAR, PROJECT, ORGANIZATION, LCR,
    //AUTHENTICATED_KEYCLOAK_USER_USERNAME, 
    getUrlToLandingPageFromCatalogue, Uncategorized_Language_Description, OTHER,
    //HIDE_ENTITIES_FROM_MY_ITEMS
    //    IS_CONTENT_MANAGER
} from "../../config/constants";
import Chip from '@material-ui/core/Chip';

class MyUsageList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            resourceData: props.resource, keycloak: props.keycloak, anchorEl: false, loading: false,
        };
    }

    handleClick = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleClose = () => {
        this.setState({ anchorEl: false });
    }

    navigateToResourceDetail(resource) {
        if (resource.status === "draft") {
            return;
        }
        const url2LandingPage = getUrlToLandingPageFromCatalogue(resource);
        if (url2LandingPage) {
            return url2LandingPage;
        }
    }

    handleCheck = (e) => {
        if (e.target.checked) {
            this.props.addCheckedResource2List(this.state.resourceData);
        } else {
            this.props.removeResourceFromCheckedList(this.state.resourceData);
        }
    }

    convertData = (resource) => {
        let data = {
            pk: resource.id,
            management_object: {
                unpublication_requested: resource.unpublication_requested,
                size: resource.size,
                status: "",
                is_active_version: "",
                is_latest_version: "",
                functional_service: ""
            },
            described_entity: {
                field_value: {
                    entity_type: {
                        field_value: { "en": "" }
                    },
                    project_name: {
                        field_value: { "en": "" }
                    },
                    organization_name: {
                        field_value: { "en": "" }
                    },
                    resource_name: {
                        field_value: { "en": "" }
                    },
                    lr_subclass: {
                        field_value: {
                            lr_type: {
                                field_value: ""
                            }
                        }
                    }
                }
            }
        };
        if (resource.status === "internal") {
            data.management_object.status = "i";
        } else if (resource.status === "draft") {
            data.management_object.status = "d";
        } else if (resource.status === "published") {
            data.management_object.status = "p";
        } else if (resource.status === "ingested") {
            data.management_object.status = "g";
        } else {
            console.log(resource.status);
        }
        if (resource.entity_type === PROJECT) {
            data.described_entity.field_value.entity_type.field_value = PROJECT;
            data.described_entity.field_value.project_name.field_value = { "en": resource.resource_name }
        } else if (resource.resource_type === TOOL_SERVICE || resource.resource_type === "ToolService") {
            data.described_entity.field_value.entity_type.field_value = "LanguageResource";
            data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value = "ToolService";
            data.described_entity.field_value.resource_name.field_value = { "en": resource.resource_name }
            data.management_object.is_active_version = resource.is_active_version;
            data.management_object.is_latest_version = resource.is_latest_version;
            data.management_object.functional_service = resource.elg_compatible_service;
        } else if (resource.resource_type === CORPUS) {
            data.described_entity.field_value.entity_type.field_value = "LanguageResource";
            data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value = CORPUS;
            data.described_entity.field_value.resource_name.field_value = { "en": resource.resource_name }
        } else if (resource.resource_type === LCR || resource.resource_type === "LexicalConceptualResource") {
            data.described_entity.field_value.entity_type.field_value = "LanguageResource";
            data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value = "LexicalConceptualResource";
            data.described_entity.field_value.resource_name.field_value = { "en": resource.resource_name }
        } else if (resource.resource_type === LD || resource.resource_type === ML_MODEL || resource.resource_type === GRAMMAR || resource.resource_type === OTHER || resource.resource_type === Uncategorized_Language_Description) {
            data.described_entity.field_value.entity_type.field_value = "LanguageResource";
            data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value = "LanguageDescription";
            data.described_entity.field_value.resource_name.field_value = { "en": resource.resource_name }
        } else if (resource.entity_type === ORGANIZATION) {
            data.described_entity.field_value.entity_type.field_value = ORGANIZATION;
            data.described_entity.field_value.organization_name.field_value = { "en": resource.resource_name }
        }
        return data;
    }

    render() {
        const resource = this.state.resourceData;
        //let hide_actions = false;
        //if (HIDE_ENTITIES_FROM_MY_ITEMS.includes(resource.resource_type) || HIDE_ENTITIES_FROM_MY_ITEMS.includes(resource.entity_type)) {
        //    hide_actions = true;
        //}
        //const data = this.convertData(resource);
        const resourceLandingPage = this.navigateToResourceDetail(resource);
        return (
            <React.Fragment>
                <Paper className="DashboardListItem">
                    <Grid item xs={12} container spacing={1} justifyContent="space-between" alignItems="center" className="ResourceListItem--inner">
                        {/*<Grid item xs={1}>
                            {resource.status !== "" && !hide_actions &&//published
                                <Checkbox
                                    checked={(this.props.checkedList.find(item => item.id === resource.id) && this.props.checkedList.find(item => item.id === resource.id).id >= 0) ? true : false}
                                    size="small"
                                    onChange={e => { this.handleCheck(e) }}
                                />}
                        </Grid>*/}
                        <Grid item sm={8}>
                            {resourceLandingPage && <Typography variant="h5" className="ResourceListTitle pt-05" ><Link to={resourceLandingPage}>{resource.resource_name}</Link></Typography>}
                            <Grid item container sm={12}>
                                <Grid item sm={6}>                                    
                                    <Grid item container spacing={1} className="pb-05 pt-05">
                                        <Grid item><Typography variant="caption" className="grey--font">{resource.version && resource.version !== "undefined" && resource.version} </Typography></Grid>
                                        <Grid item>{resource.is_active_version === true && <div><span className="badge bg-teal-light mr-05">active version</span></div>}</Grid>
                                        <Grid item>{resource.is_latest_version === true && <div><span className="badge bg-orange-light mr-05">latest version</span></div>}</Grid>
                                    </Grid>

                                    {resource.execution_start ? <Grid item container direction="column" justifyContent="flex-start" alignItems="flex-start" >
                                        {resource.execution_start && <Grid item><Typography variant="caption">execution started at:&nbsp;
                                            {new Intl.DateTimeFormat("en-GB", {
                                                year: "numeric",
                                                month: "long",
                                                day: "2-digit"
                                            }).format(new Date(resource.execution_start))} </Typography></Grid>
                                        }
                                    </Grid> : void 0}
                                    <Grid item sm={12} container direction="row" justifyContent="flex-start" alignItems="flex-start">
                                        {resource.duration && <Typography variant="caption" className="pt-05" >duration: {resource.duration} (secs)</Typography>}                                        
                                    </Grid>
                                    <Grid item sm={12} container direction="row" justifyContent="flex-start" alignItems="flex-start">                                        
                                        {resource.bytes && <Typography variant="caption" className="pt-05" >bytes: {resource.bytes}</Typography>}
                                    </Grid>
                                </Grid>

                            </Grid>

                        </Grid>
                        <Grid item sm={4} className="centered-text">
                            {resource.status === "succeeded" ?
                                <Chip size="small" label={"succeeded"} className="ChipTagGreen" />
                                :
                                <Chip size="small" label={resource.status} className="ChipTagLightPink" />}
                        </Grid>

                    </Grid>



                </Paper >
            </React.Fragment >
        );

    }
}
export default withRouter(MyUsageList);
