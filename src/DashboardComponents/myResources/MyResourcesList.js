import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withRouter } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import ResourceStatus from "../ResourceStatus";
import {
    TOOL_SERVICE, CORPUS, LD, ML_MODEL, GRAMMAR, PROJECT, ORGANIZATION, LCR, AUTHENTICATED_KEYCLOAK_USER_USERNAME, getUrlToLandingPageFromCatalogue, Uncategorized_Language_Description, OTHER,
    HIDE_ENTITIES_FROM_MY_ITEMS
    //    IS_CONTENT_MANAGER
} from "../../config/constants";
import { ReactComponent as CreatedIcon } from "./../../assets/elg-icons/editor/calendar-clock.svg";
import { ReactComponent as ModifiedIcon } from "./../../assets/elg-icons/editor/calendar-refresh.svg";
import CircularProgress from '@material-ui/core/CircularProgress';
import Checkbox from '@material-ui/core/Checkbox';
import ResourceActions from "../../componentsAPI/CommonComponents/actions/ResourceActions";

class MyResourcesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            resourceData: props.resource, expanded: false, expandedLang: false,
            expandedLicence: false, keycloak: props.keycloak, anchorEl: false, loading: false,
        };
    }

    handleClick = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleClose = () => {
        this.setState({ anchorEl: false });
    }

    navigateToResourceDetail(resource) {
        if (resource.status === "draft") {
            return;
        }
        const url2LandingPage = getUrlToLandingPageFromCatalogue(resource);
        if (url2LandingPage) {
            this.props.history.push({ pathname: url2LandingPage });
        }
    }

    handleCheck = (e) => {
        if (e.target.checked) {
            this.props.addCheckedResource2List(this.state.resourceData);
        } else {
            this.props.removeResourceFromCheckedList(this.state.resourceData);
        }
    }

    convertData = (resource) => {
        let data = {
            pk: resource.id,
            management_object: {
                unpublication_requested: resource.unpublication_requested,
                size: resource.size,
                status: "",
                is_active_version: "",
                is_latest_version: "",
                functional_service: ""
            },
            described_entity: {
                field_value: {
                    entity_type: {
                        field_value: { "en": "" }
                    },
                    project_name: {
                        field_value: { "en": "" }
                    },
                    organization_name: {
                        field_value: { "en": "" }
                    },
                    resource_name: {
                        field_value: { "en": "" }
                    },
                    lr_subclass: {
                        field_value: {
                            lr_type: {
                                field_value: ""
                            }
                        }
                    }
                }
            }
        };
        if (resource.status === "internal") {
            data.management_object.status = "i";
        } else if (resource.status === "draft") {
            data.management_object.status = "d";
        } else if (resource.status === "published") {
            data.management_object.status = "p";
        } else if (resource.status === "ingested") {
            data.management_object.status = "g";
        } else {
            console.log(resource.status);
        }
        if (resource.entity_type === PROJECT) {
            data.described_entity.field_value.entity_type.field_value = PROJECT;
            data.described_entity.field_value.project_name.field_value = { "en": resource.resource_name }
        } else if (resource.resource_type === TOOL_SERVICE || resource.resource_type === "ToolService") {
            data.described_entity.field_value.entity_type.field_value = "LanguageResource";
            data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value = "ToolService";
            data.described_entity.field_value.resource_name.field_value = { "en": resource.resource_name }
            data.management_object.is_active_version = resource.is_active_version;
            data.management_object.is_latest_version = resource.is_latest_version;
            data.management_object.functional_service = resource.elg_compatible_service;
        } else if (resource.resource_type === CORPUS) {
            data.described_entity.field_value.entity_type.field_value = "LanguageResource";
            data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value = CORPUS;
            data.described_entity.field_value.resource_name.field_value = { "en": resource.resource_name }
        } else if (resource.resource_type === LCR || resource.resource_type === "LexicalConceptualResource") {
            data.described_entity.field_value.entity_type.field_value = "LanguageResource";
            data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value = "LexicalConceptualResource";
            data.described_entity.field_value.resource_name.field_value = { "en": resource.resource_name }
        } else if (resource.resource_type === LD || resource.resource_type === ML_MODEL || resource.resource_type === GRAMMAR || resource.resource_type === OTHER || resource.resource_type === Uncategorized_Language_Description) {
            data.described_entity.field_value.entity_type.field_value = "LanguageResource";
            data.described_entity.field_value.lr_subclass.field_value.lr_type.field_value = "LanguageDescription";
            data.described_entity.field_value.resource_name.field_value = { "en": resource.resource_name }
        } else if (resource.entity_type === ORGANIZATION) {
            data.described_entity.field_value.entity_type.field_value = ORGANIZATION;
            data.described_entity.field_value.organization_name.field_value = { "en": resource.resource_name }
        }
        return data;
    }

    render() {
        const resource = this.state.resourceData;
        const isOwner = resource.curator === AUTHENTICATED_KEYCLOAK_USER_USERNAME(this.state.keycloak);
        //const isContentManager = IS_CONTENT_MANAGER(this.state.keycloak);
        const isLegalValidator = resource.legal_validator === AUTHENTICATED_KEYCLOAK_USER_USERNAME(this.state.keycloak);
        const isMetadataValidator = resource.metadata_validator === AUTHENTICATED_KEYCLOAK_USER_USERNAME(this.state.keycloak);
        const isTechnicalValidator = resource.technical_validator === AUTHENTICATED_KEYCLOAK_USER_USERNAME(this.state.keycloak);
        let hide_actions = false;
        if (HIDE_ENTITIES_FROM_MY_ITEMS.includes(resource.resource_type) || HIDE_ENTITIES_FROM_MY_ITEMS.includes(resource.entity_type)) {
            hide_actions = true;
        }
        const data = this.convertData(resource);
        return (
            <React.Fragment>
                <Paper className="DashboardListItem">
                    <Grid item xs={12} container spacing={1} justifyContent="space-between" alignItems="center" className="ResourceListItem--inner">
                        <Grid item xs={1}>
                            {resource.status !== "" && !hide_actions &&//published
                                <Checkbox
                                    checked={(this.props.checkedList.find(item => item.id === resource.id) && this.props.checkedList.find(item => item.id === resource.id).id >= 0) ? true : false}
                                    size="small"
                                    onChange={e => { this.handleCheck(e) }}
                                />}
                        </Grid>
                        <Grid item xs={7}>
                            <Typography variant="h5" className={(resource.status === "draft" || hide_actions) ? "" : "ResourceListTitle"} onClick={() => hide_actions ? () => { } : this.navigateToResourceDetail(resource)}>{resource.resource_name}</Typography>

                            {resource.version && resource.version !== "undefined" && <span><Typography variant="caption" className="grey--font">{resource.version}</Typography></span>}
                            <Grid item direction="column" container justifyContent="flex-start" alignItems="flex-start" className="pt-05 pb-05">
                                <Grid item><CreatedIcon className="xsmall-icon mr-02" /><Typography variant="caption">Created:&nbsp;
                                    {new Intl.DateTimeFormat("en-GB", {
                                        year: "numeric",
                                        month: "long",
                                        day: "2-digit"
                                    }).format(new Date(resource.creation_date))} </Typography></Grid>
                                <Grid item><ModifiedIcon className="xsmall-icon mr-02" /><Typography variant="caption">Updated:&nbsp;
                                    {new Intl.DateTimeFormat("en-GB", {
                                        year: "numeric",
                                        month: "long",
                                        day: "2-digit"
                                    }).format(new Date(resource.last_date_updated))}</Typography></Grid>
                            </Grid>
                            <Typography variant="caption" className="grey--font">{resource.entity_type !== "LanguageResource" ? resource.entity_type : resource.resource_type}</Typography>
                        </Grid>

                        <Grid item xs={2} className="container-align-center">
                            {
                                this.state.loading ?
                                    <CircularProgress /> : (
                                        hide_actions ? <></> :
                                            <ResourceActions
                                                keycloak={this.state.keycloak}
                                                isOwner={isOwner}
                                                isLegalValidator={isLegalValidator}
                                                isMetadataValidator={isMetadataValidator}
                                                isTechnicalValidator={isTechnicalValidator}
                                                pk={data.pk}
                                                data={data}
                                                className="inner-actions-link-no--border--teal"
                                                updateRecord={this.props.updateRecords}
                                            />
                                    )
                            }
                        </Grid>

                        <Grid item xs={2} style={{ marginTop: " 1em" }}>
                            <ResourceStatus status={resource.status} unpublication_requested={resource.unpublication_requested} xSize={12} keycloak={this.state.keycloak} />
                        </Grid>
                    </Grid>
                </Paper >
            </React.Fragment >
        );

    }
}
export default withRouter(MyResourcesList);
