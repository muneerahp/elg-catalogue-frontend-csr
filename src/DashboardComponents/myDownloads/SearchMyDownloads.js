import React from "react";
import axios from "axios";
import { FormattedMessage } from 'react-intl';
import MyDownloadsList from "./MyDownloadsList";
import DashboardFacetsComponent from "../DashboardFacetsComponent";
import { Input } from "reactstrap";
//import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ReorderIcon from '@material-ui/icons/Reorder';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import CircularProgress from '@material-ui/core/CircularProgress';
//import DashboardAppBar from "../DashboardAppBar";
import { SERVER_API_CONSUMER_DOWNLOADS, searchDownloadsFacetUrl, searchDownloadsKeywordCombineWithFacetUrl } from "../../config/constants";
import messages from "./../../config/messages";
import { Helmet } from "react-helmet";
import BulkActions from "../bulk_actions/BulkActions";
import SortComponent from "../SortComponent";
//import Checkbox from '@material-ui/core/Checkbox';


class SearchMyDownloads extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '', width: window.innerWidth, searchKeyword: this.getInitialSearchKeyword(),
            facetSearchKeyword: '', selectedFacets: [], keycloak: props.keycloak, source: null, loading: false,
            checkedList: [], sortBy: "", isCheckAll: false,
        };
        this.isUnmound = false;
        this.input = React.createRef();
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleWindowSizeChange);
        if (this.props.match.params && this.props.match.params.term) {
            let keywordSearch = this.props.match.params.term;
            this.getKeywordBasedSearchResults(keywordSearch);
        } else {
            this.getAllResults();
        }
    }

    handleWindowSizeChange = () => {
        this.setState({ width: window.innerWidth });
    };

    componentWillUnmount() {
        this.isUnmound = false;
        window.removeEventListener('resize', this.handleWindowSizeChange);
    }

    getInitialSearchKeyword = () => {
        if (this.props.match.params && this.props.match.params.term) {
            return this.props.match.params.term;
        } else {
            return '';
        }
    }

    getAllResults = () => {
        this.getKeywordBasedSearchResults(null);
    }

    getKeywordBasedSearchResults = (keyword) => {
        let facetSearchFromURL = this.props.location ? this.props.location.search : "";
        if (facetSearchFromURL) {
            facetSearchFromURL = facetSearchFromURL.substring(facetSearchFromURL.indexOf("?") + 1);
        }
        if (facetSearchFromURL) {
            const arrayWithFacetValues = [];
            const facetSearchFromURLParts = facetSearchFromURL.split("&") || [];
            for (var index = 0; index < facetSearchFromURLParts.length; index++) {
                const facetURLPart = facetSearchFromURLParts[index].split("=") || [];
                if (facetURLPart.length === 2 && arrayWithFacetValues.indexOf(facetURLPart[1]) === -1) {
                    arrayWithFacetValues.push(facetURLPart[1]);
                }
            }
            this.setState({ facetSearchKeyword: facetSearchFromURL, selectedFacets: arrayWithFacetValues });
        } else {
            this.setState({ searchKeyword: keyword ? keyword : "", facetSearchKeyword: "", selectedFacets: [] });
        }
        this.isUnmound = true;
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        this.clearSortedBy();
        if (facetSearchFromURL) {//make complex query with facets included
            axios.get(searchDownloadsKeywordCombineWithFacetUrl(keyword || "", facetSearchFromURL), { cancelToken: source.token })
                .then(res => {
                    if (this.isUnmound) {
                        this.setState({ data: res.data, searchKeyword: keyword || "", facetSearchKeyword: facetSearchFromURL, source: null, loading: false });
                    }
                }).catch(err => { this.setState({ source: null, loading: false }); });;
        } else {
            axios.get(searchDownloadsKeywordCombineWithFacetUrl(keyword || "", ""))
                .then(res => {
                    if (this.isUnmound) {
                        this.setState({ data: res.data, searchKeyword: keyword || "", facetSearchKeyword: "", selectedFacets: [], source: null, loading: false });
                    }
                }).catch(err => { this.setState({ source: null, loading: false }); });
        }
    }

    getNextPage = () => {
        this.isUnmound = true;
        this.setState({ isCheckAll: false });
        const nextPageUrl = this.state.data.next;
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        axios.get(nextPageUrl, { cancelToken: source.token })
            .then(res => {
                if (this.isUnmound) {
                    this.setState(res);
                    this.setState({ source: null, loading: false });
                }
            }).catch(err => { this.setState({ source: null, loading: false }); });
    }

    getPreviousPage = () => {
        this.isUnmound = true;
        this.setState({ isCheckAll: false });
        const previousPageUrl = this.state.data.previous;
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        axios.get(previousPageUrl, { cancelToken: source.token })
            .then(res => {
                if (this.isUnmound) {
                    this.setState(res);
                    this.setState({ source: null, loading: false });
                }
            }).catch(err => { this.setState({ source: null, loading: false }); });
    }


    search = (event) => {
        event && event.preventDefault();
        const searchKeyword = this.input.current.value
        this.setState({ searchKeyword });
        if (!searchKeyword) {
            this.props.history.push('/mydownloads');
            //this.getAllResults();
            this.clearAllSearchFilters();
            return false;
        }
        if (searchKeyword && searchKeyword.length === 0) {
            this.props.history.push('/mydownloads');
            this.getAllResults();
            return false;
        }
        let previousfacetSearchKeyword = this.state.facetSearchKeyword;
        if (this.state.facetSearchKeyword) {
            this.props.history.push(`/mydownloads/${encodeURIComponent(searchKeyword)}?${previousfacetSearchKeyword}`);
        } else {
            this.props.history.push(`/mydownloads/${encodeURIComponent(searchKeyword)}`);
        }
        //this.getKeywordBasedSearchResults(this.state.searchKeyword)
        return false;
    }

    handleFacetChange = (value) => {
        if (value.length === 0) {
            this.getAllResults();
            return false;
        }
        const previousfacetSearchKeyword = this.state.facetSearchKeyword;
        if (previousfacetSearchKeyword.length > 1 && previousfacetSearchKeyword.indexOf(value) === -1) {
            value = `${this.state.facetSearchKeyword}&${value}`;
            this.setState({ facetSearchKeyword: value });
        } else if (previousfacetSearchKeyword.length > 1 && previousfacetSearchKeyword.indexOf(value) !== -1) {
            value = `${this.state.facetSearchKeyword}`;//value is already in url, do not append it and do not clear the previous facets
            this.setState({ facetSearchKeyword: value });
        } else if (previousfacetSearchKeyword === "") {
            this.setState({ facetSearchKeyword: value });
        }
        //this.props.history.push(`/`);
        this.props.history.push(`?${value}`);
        this.isUnmound = true;
        this.setState({ loading: true })
        if (!this.state.searchKeyword) {//if there is no search keyword just make a generic facet search
            axios.get(searchDownloadsFacetUrl(value))
                .then(res => {
                    if (this.isUnmound) {
                        this.setState({ data: res.data, searchKeyword: "", loading: false });
                    }
                }).catch(err => { this.setState({ loading: false }) });;
        } else {
            axios.get(searchDownloadsKeywordCombineWithFacetUrl(this.state.searchKeyword || "", value))
                .then(res => {
                    if (this.isUnmound) {
                        this.setState({ data: res.data, searchKeyword: this.state.searchKeyword || "", facetSearchKeyword: value, loading: false });
                    }
                }).catch(err => { this.setState({ loading: false }) });
        }
        return false;
    }

    unselectFacet = (value) => {
        const previousfacetSearchKeyword = this.state.facetSearchKeyword;
        const valueParts = value.split("=");
        let decodedValue = `${valueParts[0]}=${encodeURIComponent(valueParts[1])}`;
        decodedValue = decodedValue.replace("%2F", "/");///// ugly!!!!!!!!!!!!!!!!!!!!!!
        if (previousfacetSearchKeyword.indexOf(decodedValue) !== -1) {
            value = decodedValue;
        }
        if (previousfacetSearchKeyword.indexOf(value) !== -1) {
            var newSearchValue = previousfacetSearchKeyword.replace(`&${value}`, '');
            newSearchValue = previousfacetSearchKeyword.replace(`?${value}`, '');
            newSearchValue = previousfacetSearchKeyword.replace(`${value}`, '');
            if (newSearchValue.startsWith("&")) {
                newSearchValue = newSearchValue.substring(1);
            }
            if (newSearchValue.indexOf("&&") !== -1) {
                newSearchValue = newSearchValue.replace("&&", "&");
            }
            if (newSearchValue.endsWith("&")) {
                newSearchValue = newSearchValue.substring(0, newSearchValue.lastIndexOf("&"));
            }
            this.setState({ facetSearchKeyword: newSearchValue, loading: true });
            this.props.history.push(`?${newSearchValue}`);
            this.isUnmound = true;
            axios.get(searchDownloadsKeywordCombineWithFacetUrl(this.state.searchKeyword || "", newSearchValue))
                .then(res => {
                    if (this.isUnmound) {
                        this.setState({ data: res.data, searchKeyword: this.state.searchKeyword || "", facetSearchKeyword: newSearchValue, loading: false });
                    }
                }).catch(err => { this.setState({ loading: false }) });
            return false;
        }
    }

    clearAllSearchFilters = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source });
        this.setState({ searchKeyword: "", facetSearchKeyword: "", selectedFacets: [] });
        this.setState({ loading: true })
        this.props.history.push({ pathname: '/mydownloads', state: { tab: 'myres' } });
        this.isUnmound = true;
        this.clearChekcedList();
        this.clearSortedBy();
        axios.get(SERVER_API_CONSUMER_DOWNLOADS, { cancelToken: source.token })
            .then(res => {
                if (this.isUnmound) {
                    this.setState(res);
                    this.setState({ searchKeyword: "", selectedFacets: [], facetSearchKeyword: '', loading: false });
                }
            }).catch(err => { this.setState({ loading: false }) });
        return false;
    }

    componentDidUpdate(prevProps, prevState) {
        let keyword1 = this.props && this.props.match && this.props.match.params && this.props.match.params.term;
        let keyword2 = prevProps.match && prevProps.match.params && prevProps.match.params.term;
        if (keyword1 !== keyword2) {
            if (keyword1) {
                this.getKeywordBasedSearchResults(keyword1);
            } else {
                this.getAllResults();
            }
        }
    }

    markFacetAsSelected = (facetName) => {
        if (this.state.selectedFacets.indexOf(facetName) === -1) {//checks if i have allready selected the facet, if not add it to array
            this.setState(prevState => ({ selectedFacets: [...prevState.selectedFacets, facetName] }));
        }
        this.clearChekcedList();
    }

    handleUnselectedFacet = (index) => {
        const selectedArray = [...this.state.selectedFacets];
        selectedArray.splice(index, 1);
        this.setState({ selectedFacets: selectedArray });
        this.clearChekcedList();
    }

    addCheckedResource2List = (resource) => {
        const { checkedList } = this.state;
        checkedList.push(resource);
        this.setState({ checkedList });
    }

    addAllCheckedResource2List = () => {
        this.setState({ isCheckAll: !this.state.isCheckAll });
        let items = this.state.data.results;
        (items.map(item => this.addCheckedResource2List(item)));

    }

    removeAllResourceFromCheckedList = () => {
        this.setState({ isCheckAll: false });
        this.setState({ checkedList: [] });
    }

    handleCheckAll = (e) => {
        if (e.target.checked) {
            this.addAllCheckedResource2List(this.state.resourceData);
        } else {
            this.removeAllResourceFromCheckedList(this.state.resourceData);
        }
    }

    removeResourceFromCheckedList = (resource) => {
        let { checkedList } = this.state;
        checkedList = checkedList.filter(item => item.id !== resource.id);
        this.setState({ checkedList });
    }

    clearChekcedList = () => {
        this.setState({ checkedList: [] });
        this.setState({ isCheckAll: false });
        this.clearSortedBy();
    }

    updateRecords = (resArray) => {
        this.setState({ checkedList: [] });
        this.setState({ isCheckAll: false });
        this.clearAllSearchFilters();
    }

    clearSortedBy = () => {
        this.setState({ sortBy: "" });
    }

    sortBy = (value) => {
        if (!value) {
            return;
        }
        let newOrderingQuery = value.value;
        const oldOrderingQuery = this.state.sortBy;
        this.setState({ sortBy: value });
        if (newOrderingQuery !== oldOrderingQuery) {
            const facetsAndOrdering = this.state.facetSearchKeyword ? `${this.state.facetSearchKeyword}&${newOrderingQuery}` : `${newOrderingQuery}`;
            if (this.state.source) {
                this.state.source.cancel("");
            }
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            this.setState({ source: source, loading: true });
            this.props.history.push(`?${facetsAndOrdering}`);
            if (!this.state.searchKeyword) {//if there is no search keyword just make a generic facet search
                axios.get(searchDownloadsFacetUrl(facetsAndOrdering), { cancelToken: source.token })
                    .then(res => {
                        if (this.isUnmound) {
                            this.setState({ data: res.data, source: null, loading: false });
                        }
                    }).catch(err => { this.setState({ source: null, loading: false }); });
            } else {
                axios.get(searchDownloadsKeywordCombineWithFacetUrl(this.state.searchKeyword || "", facetsAndOrdering), { cancelToken: source.token })
                    .then(res => {
                        if (this.isUnmound) {
                            this.setState({ data: res.data, source: null, loading: false });
                        }
                    }).catch(err => { this.setState({ source: null, loading: false }) });
            }
        }
    }

    render() {
        //console.log(this.state.data.results)       
        return (<div>
            <Helmet>
                <title>European Language Grid My Usage</title>
            </Helmet>

            <section className="wd-100" style={{ cursor: this.state.loading ? "progress" : "" }}>

                <div className="empty"></div>

                <Paper elevation={3} className="dashboard-content">

                    <Grid container direction="row" justifyContent="space-between" alignItems="center">
                        <Grid item container xs={12} spacing={3} className="dashboard_searchClass" direction="row" justifyContent="space-between" alignItems="center">
                            <form onSubmit={this.search} className="dashboard_searchClass_form">
                                <Grid item xs={12} container spacing={3} direction="row" justifyContent="space-between" alignItems="center">
                                    <Grid item xs={9} >
                                        <FormattedMessage id="searchCatalogue.searchPlaceholder" defaultMessage={messages.dashboard_search_box} >
                                            {placeholder =>
                                                <Input type="search" className="search-box__input" innerRef={this.input} defaultValue={decodeURIComponent(this.state.searchKeyword)} /*onChange={this.handleChange}*/ placeholder={placeholder} onChange={() => { this.input.current.value?.length ? void 0 : this.search() }} />
                                            }
                                        </FormattedMessage>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <Button onClick={this.search} className="inner-link-outlined--teal wd-100">
                                            <FormattedMessage id="searchCatalogue.searchButton" defaultMessage={messages.search} ></FormattedMessage>
                                        </Button>
                                    </Grid>
                                </Grid>
                            </form>
                        </Grid>

                        <Grid item container xs={12} spacing={3} sm={12} className="dashboard-results-area">

                            <Grid item container xs={3} className="search-results-container">
                                <Hidden only={['lg', 'md', 'sm', 'xl']}>
                                    <Grid item xs={12} className="left-facets-area">
                                        <Accordion>
                                            <AccordionSummary expandIcon={<ReorderIcon />} aria-controls="panel1a-content" id="panel1a-header" >
                                                <Typography> Filters </Typography>
                                            </AccordionSummary>
                                            <AccordionDetails>
                                                <div>{this.state.data.facets && <DashboardFacetsComponent facets={this.state.data.facets} onSelectKeyword={this.handleFacetChange} onClearAllSearchFilters={this.clearAllSearchFilters} unselectFacet={this.unselectFacet} selectedFacets={this.state.selectedFacets} markFacetAsSelected={this.markFacetAsSelected} handleUnselectedFacet={this.handleUnselectedFacet} />}</div>
                                            </AccordionDetails>
                                        </Accordion>
                                    </Grid>
                                </Hidden>
                                <Hidden only='xs'>
                                    <Grid item xs={12} className="left-facets-area">
                                        {this.state.data.facets && <DashboardFacetsComponent facets={this.state.data.facets} onSelectKeyword={this.handleFacetChange} onClearAllSearchFilters={this.clearAllSearchFilters} unselectFacet={this.unselectFacet} selectedFacets={this.state.selectedFacets} markFacetAsSelected={this.markFacetAsSelected} handleUnselectedFacet={this.handleUnselectedFacet} />}
                                    </Grid>
                                </Hidden>
                            </Grid>

                            <Grid item container xs={9} alignItems="center" >
                                <Grid item container xs={12} justifyContent="flex-end" alignItems="center" spacing={2} className="DashboardResultsCount">
                                    <Grid item xs={8}>
                                        <BulkActions checkedList={this.state.checkedList} updateRecords={this.updateRecords} />
                                    </Grid>

                                    <Grid item xs={4}>
                                        <span className="text-small" style={{ float: "right" }}>
                                            {this.state.data.count}&nbsp;search results {`${(this.props?.match?.params?.term && this.props?.match?.params?.term?.length >= 3) ? `for ${decodeURIComponent(this.props?.match?.params?.term)}` : ""}`}{this.state.loading ? <CircularProgress size={20} /> : void 0}
                                        </span>
                                    </Grid>
                                </Grid>

                                {
                                    <Grid container className="CardContainer" alignItems="stretch" direction="row" justifyContent="center">
                                        <Grid item xs={12} container spacing={3} alignItems="center" justifyContent="flex-start" className="ResourceListItem--header">
                                            <Grid item xs={1}>
                                                <SortComponent sortBy={this.sortBy} selectedValue={this.state.sortBy} />
                                            </Grid>
                                            <Grid item xs={7}>
                                                <Typography variant="h5">Resource name</Typography>
                                            </Grid>
                                            {/*<Grid item xs={2} className="container-align-center">
                                                <Typography variant="h5">Actions</Typography>
                                            </Grid>*/}
                                            <Grid item xs={4} className="centered-text">
                                                <Typography variant="h5">Licence(s)</Typography>
                                            </Grid>
                                        </Grid>
                                        {/*<Grid item container xs={12} spacing={3} alignItems="center" justifyContent="flex-start" className="pt2 pb-15">
                                            <Grid item xs={1}><Checkbox
                                                checked={this.state.isCheckAll}
                                                size="small"
                                                onChange={e => { this.handleCheckAll(e) }}
                                            /></Grid>
                                            <Grid item xs={11}>  <Typography variant="h4" className="grey--font">Select All</Typography> </Grid>
                                        </Grid>*/}

                                        {this.state.data.results &&
                                            this.state.data.results.map(
                                                (resource, index) => {
                                                    return <MyDownloadsList
                                                        key={JSON.stringify(resource)}
                                                        resource={resource}
                                                        keycloak={this.state.keycloak}
                                                        //addCheckedResource2List={this.addCheckedResource2List}
                                                        //removeResourceFromCheckedList={this.removeResourceFromCheckedList}
                                                        //checkedList={this.state.checkedList}
                                                        //updateRecords={this.updateRecords}
                                                    />
                                                }
                                            )}
                                    </Grid>

                                }
                            </Grid>

                        </Grid>

                        <Grid item container xs={12} className="botomMargin pt-3" justifyContent="center" alignItems="center">
                            <Grid item xs={3} sm={2}> {this.state.data.previous && <Button disabled={this.state.loading} onClick={this.getPreviousPage} classes={{ root: 'inner-link-outlined--teal' }}>&#8592;
                                <FormattedMessage id="searchCatalogue.previousPage" defaultMessage={messages.previous} >
                                </FormattedMessage> </Button>}</Grid>
                            <Grid item xs={6} sm={8}>{null && this.state.data.next}<div>{null && this.state.data.previous}</div></Grid>
                            <Grid item xs={3} sm={2} style={{ textAlign: "right" }}> {this.state.data.next && <Button disabled={this.state.loading} onClick={this.getNextPage} classes={{ root: 'inner-link-outlined--teal' }}>
                                <FormattedMessage id="searchCatalogue.nextPage" defaultMessage={messages.next} >
                                </FormattedMessage> &#8594;</Button>}</Grid>
                        </Grid>

                    </Grid>

                </Paper>


            </section>
        </div >
        )
    }

}


export default SearchMyDownloads;