import React from "react";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import GetAppIcon from '@material-ui/icons/GetApp';
import { withRouter, Link } from "react-router-dom";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import Paper from '@material-ui/core/Paper';
import { getUrlToLandingPageFromCatalogue, DOWNLOAD_RESOURCE_ENDPOINT } from "../../config/constants";
//import Chip from '@material-ui/core/Chip';

class MyDownloadsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            resourceData: props.resource,
            expandedLicence: false, keycloak: props.keycloak, anchorEl: false, loading: false, source: null, s3URL: null
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    togglexpandedLicence = () => {
        this.setState({ expandedLicence: !this.state.expandedLicence })
    }

    navigateToResourceDetail(resource) {
        if (resource.status === "draft") {
            return;
        }
        const url2LandingPage = getUrlToLandingPageFromCatalogue(resource);
        if (url2LandingPage) {
            return url2LandingPage;
        }
    }

    handleAccept(record_id, distribution_id, fileName) {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        axios.post(DOWNLOAD_RESOURCE_ENDPOINT(record_id), null, {
            headers: {
                'Cache-Control': 'no-store, max-age=0',
                'Pragma': 'no-cache',
                'Expires': 'Wed, 21 Oct 2015 07:28:00 GMT',
                'filename': fileName,
                "elg-resource-distribution-id": distribution_id,
                'ACCEPT-LICENCE': true
            },
            cancelToken: source.token
        }).then((response) => {
            this.setState({ s3URL: response.data["s3-url"], source: null, loading: false });
        }).catch((errorResponse) => {
            console.log("error while downloading");
            this.setState({ source: null, loading: false, "s3-url": null });
        });
    }

    render() {
        const resource = this.state.resourceData;
        delete Object.assign(resource, { "id": resource["record_id"] });//put record_id to id property
        const { expandedLicence } = this.state;
        const has_distribution_id = resource.distribution_id;
        const resourceLandingPage = this.navigateToResourceDetail(resource);

        return (
            <React.Fragment>
                <Paper className="DashboardListItem">
                    <Grid item xs={12} container spacing={1} justifyContent="space-between" alignItems="center" className="ResourceListItem--inner">
                        <Grid item sm={6}>
                            {resourceLandingPage && <Typography variant="h5" className="ResourceListTitle pt-05" ><Link to={resourceLandingPage}>{resource.resource_name}</Link></Typography>}
                            <Grid item container sm={12}>
                                <Grid item sm={6}>
                                    <Grid item container spacing={1} className="pb-05 pt-05">
                                        <Grid item><Typography variant="caption" className="grey--font">{resource.version && resource.version !== "undefined" && resource.version} </Typography></Grid>
                                        <Grid item>{resource.is_latest_version === true && <div><span className="badge bg-orange-light mr-05">latest version</span></div>}</Grid>
                                    </Grid>

                                    {resource.download_date ? <Grid item container direction="column" justifyContent="flex-start" alignItems="flex-start" >
                                        {resource.download_date && <Grid item><Typography variant="caption">downloaded at:&nbsp;
                                            {new Intl.DateTimeFormat("en-GB", {
                                                year: "numeric",
                                                month: "long",
                                                day: "2-digit"
                                            }).format(new Date(resource.download_date))} </Typography></Grid>
                                        }
                                    </Grid> : void 0}

                                </Grid>

                            </Grid>

                        </Grid>

                        <Grid item sm={2}>
                            {has_distribution_id && resource.distribution_id && resource.file_name && <Button
                                onClick={() => this.handleAccept(resource.record_id, resource.distribution_id, resource.file_name)}
                                classes={{ root: 'inner-link-outlined--teal' }}
                                aria-controls="simple-menu"
                                aria-haspopup="true"
                                disabled={this.state.loading}
                                endIcon={<GetAppIcon />}>Download</Button>}
                        </Grid>

                        <Grid item sm={4} className="centered-text">
                            {resource.licences && resource.licences.length > 0 ?
                                (<div style={{ display: "flex", alignItems: "baseline" }}>
                                    {expandedLicence ? (
                                        <div>
                                            {resource.licences.map((license, index) => <Typography variant="caption" key={index}>{license}</Typography>)}
                                            <span className="ExpandButton grey--font" onClick={this.togglexpandedLicence} > <ExpandLessIcon className="grey--font" /> </span>
                                        </div>
                                    ) : (
                                        <div>
                                            {resource.licences.slice(0, 2).map((license, index) => <Typography variant="caption" key={index}>{license}</Typography>)}
                                            {resource.licences.length > 2 ?
                                                <span className="ExpandButton grey--font" onClick={this.togglexpandedLicence} > <ExpandMoreIcon className="grey--font" /> </span>
                                                : void 0}
                                        </div>
                                    )
                                    }

                                </div>) : void 0
                            }
                        </Grid>


                    </Grid>



                </Paper >
                {
                    (this.state.s3URL) && <div style={{ display: 'none' }}>
                        <iframe title="Download Reource" src={this.state.s3URL} />
                    </div>
                }
            </React.Fragment >
        );

    }
}
export default withRouter(MyDownloadsList);
