
import React from "react";
import { Redirect, withRouter } from "react-router-dom";
import { Helmet } from "react-helmet";
import Container from '@material-ui/core/Container';
import DashboardAppBar from "./DashboardAppBar";
import Grid from '@material-ui/core/Grid';
import DashboardHomeConsumerArea from "./DashboardHomeConsumerArea";
import DashboardHomeProviderArea from "./DashboardHomeProviderArea";
import DashboardHomeNeedHelpArea from "./DashboardHomeNeedHelpArea";
import { AUTHENTICATED_KEYCLOAK_USER_ROLES, IS_ADMIN, IS_CONTENT_MANAGER } from "../config/constants";

class DashboardHome extends React.Component {

  render() {
    if (!this.props.keycloak || !this.props.keycloak.authenticated) {
      return <Redirect to="/" />
    }

    const show_provider_area = (AUTHENTICATED_KEYCLOAK_USER_ROLES(this.props.keycloak).includes("provider") || IS_ADMIN(this.props.keycloak) || IS_CONTENT_MANAGER(this.props.keycloak))

    return (<>
      <Helmet>
        <title>European Language Grid Dashboard</title>
      </Helmet>


      <DashboardAppBar {...this.props} />


      <div className="editor-container pt-50 pb-150">
        <Container maxWidth="xl">
          <Grid container direction="row" alignItems="stretch" justifyContent="center" spacing={2} className="padding15">
            <DashboardHomeConsumerArea {...this.props} />
          </Grid>

          {/* a new grid row*/}
          {show_provider_area && <Grid container direction="row" alignItems="stretch" justifyContent="center" spacing={2} className="padding15">
            <DashboardHomeProviderArea {...this.props} />
          </Grid>}

          {/* a new grid row*/}
          <Grid container direction="row" alignItems="stretch" justifyContent="center" spacing={2} className="padding15">
            <DashboardHomeNeedHelpArea {...this.props} />
          </Grid>
          <div>
          </div>
        </Container>
      </div>

    </>
    );
  }
}

export default withRouter(DashboardHome);
