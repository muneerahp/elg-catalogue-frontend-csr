import React from "react";
import { withRouter } from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Draggable from 'react-draggable';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
//import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
//import CircularProgress from '@material-ui/core/CircularProgress';
import { toast } from "react-toastify";
import { EDITOR_METADATA_VALIDATE, EDITOR_LEGAL_VALIDATE, EDITOR_TECHNICAL_VALIDATE } from "../../config/editorConstants";
import { TOOL_SERVICE, CORPUS, LD, ML_MODEL, GRAMMAR, LCR, PROJECT, ORGANIZATION, OTHER, Uncategorized_Language_Description } from "../../config/constants";
//import messages from "../../config/messages";

function PaperComponent(props) {
    return (
        <Draggable
            handle={'#draggable-dialog-title'}
            cancel={'[class*="MuiDialogContent-root"]'}
        //handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}
        >
            <Paper {...props} style={{ margin: 0, maxHeight: '100%' }} />
        </Draggable>
    );
}

class ValidateRecord extends React.Component {
    constructor(props) {
        super(props);
        this.state = { displayDialog: true, loading: false, source: null, error: null, selectedOption: "", technicalSelectedOption: "", reasons: "", validator_notes: "" };
        this.onValueChange = this.onValueChange.bind(this);
        this.onTechnicalValueChange = this.onTechnicalValueChange.bind(this);
    }

    componentWillUnmount = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    updateDate = (field, newDate) => {
        this.setState({ date: newDate });
    }

    disableDisplay = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        this.setState({ displayDialog: true, loading: false, source: null, error: null, selectedOption: "", technicalSelectedOption: "", reasons: "", validator_notes: "" });
        this.props.hideValidateRecord(this.props.action);
    }

    handleSubmit = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });

        if (this.props.type === 'legal') {
            if (this.state.selectedOption === "Reject") {
                axios.patch(EDITOR_LEGAL_VALIDATE(this.props.resource.id), { "legally_approve": false, "review_comments": this.state.reasons, "validator_notes": this.state.validator_notes })
                    .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
                    .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err) })
            }
            else if (this.state.selectedOption === "Approve") {
                axios.patch(EDITOR_LEGAL_VALIDATE(this.props.resource.id), { "legally_approve": true, "validator_notes": this.state.validator_notes })
                    .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
                    .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err.toString()) })
            }
            else {
                // Validator notes only
                axios.patch(EDITOR_LEGAL_VALIDATE(this.props.resource.id), { "validator_notes": this.state.validator_notes })
                    .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
                    .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err.toString()) })
            }
        }

        if (this.props.type === 'metadata') {
            if (this.state.selectedOption === "Reject") {
                axios.patch(EDITOR_METADATA_VALIDATE(this.props.resource.id), { "metadata_approve": false, "review_comments": this.state.reasons, "validator_notes": this.state.validator_notes })
                    .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
                    .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err) })
            }
            else if (this.state.selectedOption === "Approve") {
                axios.patch(EDITOR_METADATA_VALIDATE(this.props.resource.id), { "metadata_approve": true, "validator_notes": this.state.validator_notes })
                    .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
                    .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err) })
            }
            else {
                // Validator notes only
                axios.patch(EDITOR_METADATA_VALIDATE(this.props.resource.id), { "validator_notes": this.state.validator_notes })
                    .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
                    .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err) })
            }
        }

        if (this.props.type === 'technical') {
            if (this.state.selectedOption === "Reject" || this.state.technicalSelectedOption === "Reject") {
                axios.patch(EDITOR_TECHNICAL_VALIDATE(this.props.resource.id), {
                    "metadata_approve": this.state.selectedOption === "Approve",
                    "technically_approve": this.state.technicalSelectedOption === "Approve",
                    "review_comments": this.state.reasons,
                    "validator_notes": this.state.validator_notes
                })
                    .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
                    .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err) })
            }
            else if (this.state.selectedOption === "Approve" && this.state.technicalSelectedOption === "Approve") {
                axios.patch(EDITOR_TECHNICAL_VALIDATE(this.props.resource.id), {
                    "metadata_approve": true,
                    "technically_approve": true,
                    "validator_notes": this.state.validator_notes
                })
                    .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
                    .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err) })
            }
            else {
                // Validator notes only
                axios.patch(EDITOR_TECHNICAL_VALIDATE(this.props.resource.id), { "validator_notes": this.state.validator_notes })
                    .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
                    .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err) })
            }
        }
    }

    successResponse = (response) => {
        toast.success("Success", { autoClose: 3500 });
        this.setState({ displayDialog: false })
        this.props.hideValidateRecord(this.props.action);
        this.props.history.push("/myValidations");
    }

    errorResponse = (responseError) => {

        toast.error("Your request failed.", { autoClose: 3500 });
        let errData = '';
        try {
            errData = responseError.response.data;
        } catch (err) {
            errData = 'The request has failed. Please contact the ELG team.'
        }
        this.setState({ error: errData });//render json as error report
    }

    onValueChange(event) {
        //this.state.reasons = ""; // Clear Reasons text field so that its not submitted with approve || Do not mutate state directly. Use setState()
        this.setState({
            reasons: "",
            selectedOption: event.target.value
        });
    }

    onTechnicalValueChange(event) {
        //this.state.reasons = ""; // Clear Reasons text field so that its not submitted with approve || Do not mutate state directly. Use setState()
        this.setState({
            reasons: "",
            technicalSelectedOption: event.target.value
        });
    }

    ShowErrorMessage(error) {
        return <div>
            {error && <div >
                <h3>Error</h3>
                <div className="boxed">
                    <Paper elevation={13} >
                        <code>
                            <pre id="special">
                                {error}{/*JSON.stringify(error)*/}
                            </pre>
                        </code>
                    </Paper>
                </div>
            </div>
            }
        </div>
    }

    getLRDialogue = (options) => {
        return <>

            <Dialog
                open={this.state.displayDialog}
                onClose={this.disableDisplay}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                //disableEnforceFocus // Allows other things to take focus
                //hideBackdrop  // Hides the shaded backdrop
                //disableBackdropClick  // Prevents backdrop clicks
                PaperComponent={PaperComponent}
            >
                <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
                    <Typography className="pb-3" align="center" color="primary" variant="body1"> Please fill in the following fields in order to validate  <small className="teal--font">{this.props.resource.resource_name}</small></Typography>
                </DialogTitle>
                <DialogContent>
                    <DialogContentText component={"div"}>
                        {this.props.type === 'technical' && <Typography variant='h4'>Metadata validation: </Typography>}
                        <RadioGroup
                            aria-label="validate"
                            name="validate"
                            value={this.state.selectedOption}
                            onChange={this.onValueChange}
                        >
                            {options.map((option) => (
                                <FormControlLabel value={option} key={option} control={<Radio />} label={option} />
                            ))}
                        </RadioGroup>

                        {this.props.type === 'technical' && <Typography variant='h4'>Technical validation: </Typography>}
                        {this.props.type === 'technical' && <RadioGroup
                            aria-label="validate"
                            name="validate"
                            value={this.state.technicalSelectedOption}
                            onChange={this.onTechnicalValueChange}
                        >
                            {options.map((option) => (
                                <FormControlLabel value={option} key={option} control={<Radio />} label={option} />
                            ))}
                        </RadioGroup>}

                    </DialogContentText>
                    <DialogContentText component={"div"}>

                        {this.state.selectedOption === "Reject" || this.state.technicalSelectedOption === "Reject" ?
                            <Grid>
                                <Grid item>
                                    <TextField className="pb-3 wd-100"
                                        multiline
                                        minRows={8}
                                        required={this.state.selectedOption === "Reject" || this.state.technicalSelectedOption === "Reject" ? true : false}
                                        label={"Reasons"}
                                        placeholder={"Reason for rejection"}
                                        variant="outlined"
                                        helperText={
                                            <span>
                                                Please state the reasons for rejecting this record (to be sent out to the curator)
                                            </span>
                                        }
                                        value={this.state.reasons}
                                        onChange={(e) => { this.setState({ reasons: e.target.value }) }}
                                    />
                                </Grid>
                            </Grid>
                            : <></>
                        }
                        <div>
                            <Grid>
                                <Grid item>
                                    <TextField className="pb-3 wd-100"
                                        multiline
                                        minRows={8}
                                        required={false}
                                        label={"Validator notes"}
                                        placeholder={"Validator notes"}
                                        variant="outlined"
                                        helperText={
                                            <span>
                                                Internal notes (visible only to other validators)
                                            </span>
                                        }
                                        value={this.state.validator_notes}
                                        onChange={(e) => { this.setState({ validator_notes: e.target.value }) }}
                                    />
                                </Grid>
                            </Grid>
                        </div>

                        {/*this.props.resource.validator_notes && this.props.resource.validator_notes.length > 0 && <div className="review-card mt-1"><Typography variant="body2" className="Text-fontSize--12 bold">Previous validator notes</Typography>
                            <Typography variant="caption" className="pt-05" >{this.props.resource.validator_notes}</Typography></div>*/}

                        {this.props.resource.validator_notes && this.props.resource.validator_notes.length > 0 && <div className="review-card mt-1"><Typography variant="body2" className="Text-fontSize--12 bold">Previous validator notes</Typography>
                            {this.props.resource.validator_notes.split(' | ').map((note, index) => (<div key={index}><Typography variant="caption" className="pt-05" >{note}</Typography></div>))}</div>}
                    </DialogContentText>
                    <DialogContentText component={"div"}>
                        {this.state.error && this.ShowErrorMessage(this.state.error)}
                    </DialogContentText>
                </DialogContent>

                <DialogActions>
                    <Button
                        color="primary"
                        disabled={this.state.loading ||
                            (this.state.selectedOption === "Reject" && !this.state.reasons) ||
                            (this.state.technicalSelectedOption === "Reject" && !this.state.reasons) ||
                            ((!this.state.selectedOption && !this.state.validator_notes) ||
                                (this.props.type === 'technical' && !this.state.technicalSelectedOption && !this.state.validator_notes))}
                        onClick={() => this.handleSubmit()}
                    >
                        Submit
                    </Button>
                    <span>
                        <Button
                            color="primary"
                            disabled={this.state.loading}
                            onClick={() => this.disableDisplay()}
                        >
                            Cancel
                        </Button>
                    </span>


                </DialogActions>
            </Dialog>
        </>
    }




    render() {
        const { resource_type } = this.props.resource;
        const options = ["Approve", "Reject"];

        if ((resource_type === ORGANIZATION || resource_type === PROJECT || resource_type === CORPUS || resource_type === LCR || resource_type === LD || resource_type === ML_MODEL || resource_type === GRAMMAR || resource_type === OTHER || resource_type === Uncategorized_Language_Description || resource_type === TOOL_SERVICE || resource_type === "ToolService" || resource_type === "LexicalConceptualResource" || resource_type === "LanguageDescription")) {
            return this.getLRDialogue(options);
        } else {
            return <></>
        }

    }
}

export default withRouter(ValidateRecord)