import React from "react";
//import FormHelperText from '@material-ui/core/FormHelperText';
//import FormControl from '@material-ui/core/FormControl';
//import Select from '@material-ui/core/Select';
//import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
//import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
//import messages from "../../config/messages";
import { ReactComponent as SortIcon } from "./../assets/elg-icons/editor/filter-text.svg";
export default class SortComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = { anchorEl: null, openSelect: true, selectedValue: props.selectedValue || "" }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            selectedValue: nextProps.selectedValue || ""
        };
    }

    changeValue = (value) => {
        this.handleClose();
        this.props.sortBy(value);
    }

    handleClose = () => {
        this.setState({ anchorEl: null })
    }

    render() {
        //const choices = ["Sort by", "Resource Name ↓↑", "Updated Date", "Resource ID"];
        //const choices = ["Sort by"];
        //, "Resource Name", "Updated Date", "Resource ID"
        const choices = [
            { display_value: "Sort by", value: "" },
            { display_value: "item name ↑", value: "ordering=resource_name" },
            { display_value: "item name ↓", value: "ordering=-resource_name" },
            { display_value: "update date ↑", value: "ordering=last_date_updated" },
            { display_value: "update date ↓", value: "ordering=-last_date_updated" },
            //{ display_value: "resource id ↑", value: "ordering=id" },
            //{ display_value: "resource id ↓", value: "ordering=-id" },
        ]
        return <>
            <div onMouseOver={(e) => { this.setState({ anchorEl: e.currentTarget }) }}>
                <SortIcon className="small-icon yellow--font" />
            </div>
            <Menu
                id="simple-menu"
                anchorEl={this.state.anchorEl}
                keepMounted
                open={Boolean(this.state.anchorEl)}
                onClose={() => this.handleClose()}
            >
                {choices.map((item, index) => {
                    if (index === 0) {
                        return <MenuItem key={index} value={item.value} disabled={index === 0} onClick={() => { this.handleClose() }}>
                            <small>{item.display_value}</small>
                        </MenuItem>
                    }
                    if (item.value === this.state.selectedValue.value) {
                        return <MenuItem key={index} value={item.value} onClick={() => { this.changeValue(item) }} style={{ backgroundColor: "lightblue" }}>
                            <span>{item.display_value}</span>
                        </MenuItem>
                    }
                    return <MenuItem key={index} value={item.value} onClick={() => { this.changeValue(item) }}>
                        <span>{item.display_value}</span>
                    </MenuItem>
                })}
            </Menu>

        </>
    }
}