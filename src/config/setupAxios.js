import axios from 'axios';
import { toast } from "react-toastify";
import { GoBackToCatalogue } from "./constants";

let toastID = null;

const setupAxiosInterceptors = Keycloak => {
    const onRequestSuccess = config => {
        if (Keycloak && Keycloak.authenticated) {
            let keyclockToken = Keycloak.token || "";
            config.headers.Authorization = `Bearer ${keyclockToken}`;
        }
        return config;
    };
    axios.interceptors.request.use(onRequestSuccess);
    axios.interceptors.response.use((response) => response, (error) => {
        if (error && error.message) {
            let errorMessage = error.message;
            if (errorMessage.indexOf("404") > -1) {//not found
                errorMessage = `${errorMessage}, Not Found`;
                //redirectToCatalogue();
                GoBackToCatalogue();
            } else if (errorMessage.indexOf("403") > -1) {//Insufficient Rights
                errorMessage = `${errorMessage}, Insufficient Rights`;
                //redirectToCatalogue();
                GoBackToCatalogue();
            } else if (error && error.config && error.config.method === "patch") {
                return Promise.reject(error);
                //e.g. when create vesrion is failed do not show more than one toasts.
            } else if (error && error.config && error.config.url && error.config.url.indexOf("/catalogue_backend/api/registry/metadatarecord/") >= 0 && (error.config.method === "post" || error.config.method === "put") && (errorMessage.indexOf("400") > -1)) {
                //disable toast error for duplicate record name at the editor
                return Promise.reject(error);
            } //else if (error && error.config && error.config.url && error.config.url.indexOf("https://dev.european-language-grid.eu/cms/api/") >= 0) {
                //return;
            //}
            if (!toast.isActive(toastID)) {
                toastID = toast.info(errorMessage);
            }
            return Promise.reject(error)
        }
    });
};

export default setupAxiosInterceptors;
