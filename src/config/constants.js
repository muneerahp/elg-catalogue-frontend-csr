//export const BASE_URL = process.env.REACT_APP_BASEURL;
//export const BASE_URL = "https://192.168.188.194/";
export const BASE_URL = "https://dev.european-language-grid.eu/";
//export const BASE_URL = "https://live.european-language-grid.eu/";

export const PRODUCTION = BASE_URL === "https://live.european-language-grid.eu/";

export const KEYCLOAK_URL = PRODUCTION ? "https://live.european-language-grid.eu/auth/" : "https://dev.european-language-grid.eu/auth/";

//export const GRAPH_QL_ENDPOINT = `${BASE_URL}catalogue_backend/graphql/`;
export const DJANGO_ADMIN_PAGE = `${BASE_URL}catalogue_backend/admin/`;
export const DJANGO_ADMIN_PAGE_LOGIN = `${BASE_URL}catalogue_backend/api/accounts/login/`;
export const DJANGO_ADMIN_PAGE_LOGOUT = `${BASE_URL}catalogue_backend/api/accounts/logout/`;

export const UPLOAD_XML_METADATARECORD_ENDPOINT = `${BASE_URL}catalogue_backend/api/registry/xmlmetadatarecord/`;
export const EXPORT_XML_METADATARECORD_ENDPOINT = (id) => `${BASE_URL}catalogue_backend/api/registry/xmlmetadatarecord/${id}/`;
export const BATCH_XML_EXPORT_ENDPOINT = (ids) => `${BASE_URL}catalogue_backend/api/registry/batch_xml_retrieve/${ids}/`;//catalogue_backend/api/registry/batch_xml/${ids}/`
export const BATCH_UPLOAD_XML_METADATARECORD_ENDPOINT = `${BASE_URL}catalogue_backend/api/registry/batch_create/`;
//export const VALIDATE_XML_ENDPOINT = `${BASE_URL}catalogue_backend/api/registry/xml-tools/validate-xml/`;
export const VALIDATE_XML_ENDPOINT = `${BASE_URL}xml-tools/validate-xml/`;

export const S3_TO_GET_URL_ENDPOINT = (id) => `${BASE_URL}catalogue_backend/api/registry/metadatarecord/${id}/actions/upload/`;
export const PROXY_2_S3 = PRODUCTION ? `https://live.european-language-grid.eu/s3/` : `https://dev.european-language-grid.eu/s3/`;
export const LIST_DATA_OF_RECORD = id => `${BASE_URL}catalogue_backend/api/management/content-files/${id}/`;

export const DELETE_DATA_ENDPOINT = (id) => `${BASE_URL}catalogue_backend/api/management/upload/${id}/`;

//export const DOWNLOAD_RESOURCE_ENDPOINT = (id) => `${BASE_URL}catalogue_backend/api/registry/metadatarecord/${id}/actions/download/`;//before management_object to delete
export const DOWNLOAD_RESOURCE_ENDPOINT = (id) => `${BASE_URL}catalogue_backend/api/management/download/${id}/`;//after management_object

export const LICENCE_DOWNLOAD_ENDPOINT = (name) => `${BASE_URL}catalogue_backend/static/project/licences/${name}`;

export const SERVER_API_URL = `${BASE_URL}catalogue_backend/api/registry/search/`;
export const SERVER_API_MANAGEMENT_URL = `${BASE_URL}catalogue_backend/api/management/dashboard/my_items/`;
export const SERVER_API_MANAGEMENT_VALIDATION_URL = `${BASE_URL}catalogue_backend/api/management/dashboard/my_validations/`;
export const SERVER_API_METADATARECORD_LABELESS_SCHEMA = id => `${BASE_URL}catalogue_backend/api/registry/metadatarecord/${id}/`;
//export const SERVER_API_METADATARECORD = id => `${BASE_URL}catalogue_backend/api/registry/metadatarecord/${id}/?stat`;//WITH_STATISTICS
export const SERVER_API_METADATARECORD = id => `${BASE_URL}catalogue_backend/api/registry/metadatarecord/${id}/?display&stat`;//WITH_LABELS & stats

export const SERVER_API_CONSUMER_USAGE = `${BASE_URL}catalogue_backend/api/analytics/consumer_grid/my_usage/`;
export const SERVER_API_CONSUMER_DOWNLOADS = `${BASE_URL}catalogue_backend/api/analytics/consumer_grid/my_downloads/`;
export const SERVER_API_CONSUMER_STATS = `${BASE_URL}catalogue_backend/api/accounts/user-index/`;

export const TILDE_MAIN_MENU_API = PRODUCTION ? "https://live.european-language-grid.eu/cms/api/menu_items/main?_format=hal_json" : "https://dev.european-language-grid.eu/cms/api/menu_items/main?_format=hal_json";
export const TILDE_FOOTER_MENU_API = PRODUCTION ? "https://live.european-language-grid.eu/cms/api/menu_items/footer?_format=hal_json" : "https://dev.european-language-grid.eu/cms/api/menu_items/footer?_format=hal_json";
export const TILDE_FEEDBACK_MENU_API = PRODUCTION ? "https://live.european-language-grid.eu/cms/api/menu_items/feedback?_format=hal_json" : "https://dev.european-language-grid.eu/cms/api/menu_items/feedback?_format=hal_json";

export const RECORD_STATS = id => `${BASE_URL}catalogue_backend/api/analytics/get_record_stats/${id}/`
export const RETRIEVE_RECORD_INFO = id => `${BASE_URL}catalogue_backend/api/management/record-info/${id}/`;
export const HEADER_BASE_URL = PRODUCTION ? "https://live.european-language-grid.eu/" : "https://dev.european-language-grid.eu/";
export const FOOTER_BASE_URL = PRODUCTION ? "https://live.european-language-grid.eu/" : "https://dev.european-language-grid.eu/";

export const OPEN_API_URL = PRODUCTION ? "https://live.european-language-grid.eu/assets/swagger/openapi_ie.json" : "https://dev.european-language-grid.eu/assets/swagger/openapi_ie.json";

export const COOKIE_CONSENT_POLICY_URL = PRODUCTION ? "https://live.european-language-grid.eu/page/terms-of-use" : "https://dev.european-language-grid.eu/page/terms-of-use";

export const ADMIN_PERMITTED_ROLES = ["admin"];
export const UPLOAD_XML_PERMITTED_ROLES = ["admin", "content_manager", "provider"];
export const EDITOR_PERMITTED_ROLES = ["admin", "content_manager", "provider"];
export const VALIDATOR_PERMITTED_ROLES = ["metadata_validator", "legal_validator", "technical_validator"];
export const CLAIM_PERMITTED_ROLES = ["consumer"];

export const SHOW_DASHBOARD_ROLES = ["admin", "content_manager", "provider", "legal_validator", "metadata_validator", "technical_validator"];
//export const SHOW_ADMINISTRATOR_ROLES = ["admin", "content_manager", "legal_validator", "metadata_validator", "technical_validator"];
export const SHOW_ADMINISTRATOR_ROLES = ["admin", "content_manager"];
export const SHOW_MY_ITEMS_ROLES = ["admin", "content_manager", "provider"];
export const SHOW_CONSUMER_GRID_ROLES = ["consumer"];
export const SHOW_MY_VALIDATION_ROLES = ["admin", "content_manager", "metadata_validator", "legal_validator", "technical_validator"];

//export const searchUrl = (searchKeyword) => `${SERVER_API_URL}?search=${searchKeyword}`;
export const searchUrl = (searchKeyword) => `${SERVER_API_URL}?search_lucene=${searchKeyword}`;//lucene based
export const searchFacetUrl = (searchKeyword) => `${SERVER_API_URL}?${searchKeyword}`;
//export const searchKeywordCombineWithFacetUrl = (searchKeyword, facetSearch) => searchKeyword ? (`${SERVER_API_URL}?search=${searchKeyword}&${facetSearch}`) : (`${SERVER_API_URL}?${facetSearch}`);
export const searchKeywordCombineWithFacetUrl = (searchKeyword, facetSearch) => searchKeyword ? (`${SERVER_API_URL}?search_lucene=${searchKeyword}&${facetSearch}`) : (`${SERVER_API_URL}?${facetSearch}`);//lucene based


//export const searchManagementUrl = (searchKeyword) => `${SERVER_API_MANAGEMENT_URL}?search=${searchKeyword}`;
export const searchManagementUrl = (searchKeyword) => `${SERVER_API_MANAGEMENT_URL}?search_lucene=${searchKeyword}`;//lucene based
export const searchManagementFacetUrl = (searchKeyword) => `${SERVER_API_MANAGEMENT_URL}?${searchKeyword}`;
//export const searchManagmentKeywordCombineWithFacetUrl = (searchKeyword, facetSearch) => searchKeyword ? (`${SERVER_API_MANAGEMENT_URL}?search=${searchKeyword}&${facetSearch}`) : (`${SERVER_API_MANAGEMENT_URL}?${facetSearch}`);
export const searchManagmentKeywordCombineWithFacetUrl = (searchKeyword, facetSearch) => searchKeyword ? (`${SERVER_API_MANAGEMENT_URL}?search_lucene=${searchKeyword}&${facetSearch}`) : (`${SERVER_API_MANAGEMENT_URL}?${facetSearch}`);//lucene based

export const searchManagementFacetValidationUrl = (searchKeyword) => `${SERVER_API_MANAGEMENT_VALIDATION_URL}?${searchKeyword}`;
export const searchManagmentKeywordCombineWithFacetValidationUrl = (searchKeyword, facetSearch) => searchKeyword ? (`${SERVER_API_MANAGEMENT_VALIDATION_URL}?search=${searchKeyword}&${facetSearch}`) : (`${SERVER_API_MANAGEMENT_VALIDATION_URL}?${facetSearch}`);

export const searchConsumerFacetUrl = (searchKeyword) => `${SERVER_API_CONSUMER_USAGE}?${searchKeyword}`;
export const searchConsumerKeywordCombineWithFacetUrl = (searchKeyword, facetSearch) => searchKeyword ? (`${SERVER_API_CONSUMER_USAGE}?search=${searchKeyword}&${facetSearch}`) : (`${SERVER_API_CONSUMER_USAGE}?${facetSearch}`);

export const searchDownloadsFacetUrl = (searchKeyword) => `${SERVER_API_CONSUMER_DOWNLOADS}?${searchKeyword}`;
export const searchDownloadsKeywordCombineWithFacetUrl = (searchKeyword, facetSearch) => searchKeyword ? (`${SERVER_API_CONSUMER_DOWNLOADS}?search=${searchKeyword}&${facetSearch}`) : (`${SERVER_API_CONSUMER_DOWNLOADS}?${facetSearch}`);


export const SERVICE_CODE_SAMPLE_TEXT_DESCRIPTION = (SERVICE_NAME) => `If you want to send a processing request from your command line to ${SERVICE_NAME ? SERVICE_NAME : "this service"} you can use the following code sample/template`;
export const SERVICE_CODE_SAMPLE = (TOKEN, url) => `curl -H "Authorization: Bearer ${TOKEN ? TOKEN : "$TOKEN"}" -H "Content-Type: text/plain" --data-binary "@./path/somefile" ${url ? url : "execution_url"}`;
export const SERVICE_CODE_SAMPLE_AUDIO = (TOKEN, url) => `curl -H "Authorization: Bearer ${TOKEN ? TOKEN : "$TOKEN"}" -H "Content-Type: audio/x-wav" --data-binary "@./path/somefile" ${url ? url : " execution_url"}`;

export const ACADEMIC = "http://w3id.org/meta-share/meta-share/academic1";
export const ALLOWS_ACCESS_WITH_SIGNATURE = "http://w3id.org/meta-share/meta-share/allowsAccessWithSignature";
export const ALLOWS_DIRECT_aCCESS = "http://w3id.org/meta-share/meta-share/allowsDirectAccess";
export const ALLOWS_PROCESSING = "http://w3id.org/meta-share/meta-share/allowsProcessing";
export const PUBLIC = "http://w3id.org/meta-share/meta-share/public";
export const REQUIRES_USER_AUTHENTICATION = "http://w3id.org/meta-share/meta-share/requiresUserAuthentication";
export const REQUIRES_USER_AUTHORIZATION = "http://w3id.org/meta-share/meta-share/requiresUserAuthorization";
export const RESTRICTED = "http://w3id.org/meta-share/meta-share/restricted";

export const GoBackToCatalogue = (timeout = 5000) => setTimeout(() => { window.location = getLogoutUrl() }, timeout)

export const TOOL_SERVICE = "Tool/Service";
export const CORPUS = "Corpus";
export const LCR = "Lexical/Conceptual resource";
export const LD = "Language description";
export const PROJECT = "Project";
export const ORGANIZATION = "Organization";
export const ML_MODEL = "Model";
export const GRAMMAR = "Grammar";
export const N_GRAM_MODEL = "N-gram Model";
export const OTHER = "Other";
export const Uncategorized_Language_Description = "Uncategorized Language Description";

export const HIDE_ENTITIES_FROM_MY_ITEMS = ["Person", "Group", "Document", "LicenceTerms", "Repository"];

export const ELG_CATALOGUE_PREV_PAGE = "ELG_CATALOGUE_PREV_PAGE";

export const addUnloadListener = () => window.onbeforeunload = function () {
    window.sessionStorage.removeItem(ELG_CATALOGUE_PREV_PAGE);
};

export const GoogleAnalyticsCallback = (Cookies, ReactGA) => {
    const cookieconsent_status = Cookies.get("cookieconsent_status");
    if (cookieconsent_status && cookieconsent_status === "allow" && PRODUCTION) {
        try {
            const url = window.location.href;
            const pagePath = url.substring(url.indexOf("/catalogue"));
            const search = window.location.search;
            ReactGA.set({ page: pagePath + search }); // Update the user's current page
            ReactGA.pageview(pagePath + search); // Record a pageview for the given page
            //console.log(pagePath+search);
            //console.log(url);
            //console.log(window.location.pathname + window.location.search);
        } catch (err) {
            console.log("ga error: ", err);
        }
    }
}

export const getAuthorizationHeader = (keycloak) => {
    if (keycloak && keycloak.authenticated) {
        let keyclockToken = keycloak.token || "";
        return `Bearer ${keyclockToken}`;
    }
    return "";
}

export const AUTHENTICATED_KEYCLOAK_USER_ROLES = (keycloak) => {
    try {
        if (keycloak && keycloak.authenticated) {
            const { resource_access, roles } = keycloak.tokenParsed || "";
            if (roles) {
                return roles;
            }
            if (resource_access && resource_access["react-client"]) {
                const { roles = [] } = resource_access["react-client"] || [];
                return roles;
            } else {
                if (roles) {
                    return roles;
                }
            }
        }
    } catch (err) {
        console.log("user roles error.");
    }
    return [];
}

export const AUTHENTICATED_KEYCLOAK_USER_USERNAME = (keycloak) => {
    try {
        if (keycloak && keycloak.authenticated) {
            const { preferred_username } = keycloak.tokenParsed || "";
            return preferred_username;
        }
    } catch (err) {
        console.log("username error.");
    }
    return "";
}

export const IS_ADMIN = (keycloak) => {
    try {
        return AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak).includes("admin");
    } catch (err) {
        console.log("error 1.");
    }
    return false;
}

export const IS_CONTENT_MANAGER = (keycloak) => {
    try {
        return AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak).includes("content_manager");
    } catch (err) {
        console.log("error 2.");
    }
    return false;
}
//validations
export const IS_LEGAL_VALIDATOR = (keycloak) => {
    try {
        return AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak).includes("legal_validator");
    } catch (err) {
        console.log("error 3.");
    }
    return false;
}

export const IS_METADATA_VALIDATOR = (keycloak) => {
    try {
        return AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak).includes("metadata_validator");
    } catch (err) {
        console.log("error 3.");
    }
    return false;
}

export const IS_TECHNICAL_VALIDATOR = (keycloak) => {
    try {
        return AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak).includes("technical_validator");
    } catch (err) {
        console.log("error 3.");
    }
    return false;
}

export const IS_CUSTOMER = (keycloak) => {
    try {
        return AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak).includes("customer");
    } catch (err) {
        console.log("error 4.");
    }
    return false;
}

export const loginFunction = (keycloak, Cookies) => {
    if (clear_SAML_COOKIES(Cookies)) {
        keycloak && keycloak.login();
    }
}

export const logoutFunction = (keycloak, Cookies) => {
    if (clear_SAML_COOKIES(Cookies)) {
        const logoutOptionsObject = { 'redirectUri': getLogoutUrl() }
        keycloak && keycloak.logout(logoutOptionsObject);
    }
}

export const getLogoutUrl = () => {
    const origin = window.location.origin;
    if (BASE_URL === "https://live.european-language-grid.eu/") {
        return "https://live.european-language-grid.eu/catalogue/";
    } else if (BASE_URL === "https://dev.european-language-grid.eu/") {
        return "https://dev.european-language-grid.eu/catalogue/";
    } else if (BASE_URL === "https://192.168.188.194/") {
        return "https://192.168.188.194/catalogue/";
    } else if (BASE_URL === "http://192.168.188.194/" && origin === "http://elg-front.ilsp.gr:3000") {
        return "http://elg-front.ilsp.gr:3000/";
    } else if (BASE_URL === "http://192.168.188.194/" && origin === "http://localhost:3000") {
        return "http://localhost:3000/";
    } else {
        return "https://live.european-language-grid.eu/catalogue/";
    }
}

export const clear_SAML_COOKIES = (Cookies) => {
    try {
        document.cookie = "SimpleSAMLAuthToken=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        document.cookie = "SimpleSAML=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        Cookies.remove("SimpleSAML");
        Cookies.remove("SimpleSAMLAuthToken");
    } catch (err) {
        console.log(err);
    }
    return true;
}

export const get_landing_page_url = (metadataRecord) => {
    let url = "/";
    try {
        if (!metadataRecord || !metadataRecord.described_entity) {
            return url;
        }
        const pk = metadataRecord.pk;
        if (!pk) {
            return url;
        }
        if (metadataRecord.described_entity.entity_type === PROJECT) {
            url = `/project/${pk}`;
        } else if (metadataRecord.described_entity.entity_type === ORGANIZATION) {
            url = `/organization/${pk}`
        } else if (metadataRecord.described_entity.lr_subclass.lr_type === TOOL_SERVICE || metadataRecord.described_entity.lr_subclass.lr_type === "ToolService") {
            url = `/tool-service/${pk}`
        } else if (metadataRecord.described_entity.lr_subclass.lr_type === CORPUS) {
            url = `/corpus/${pk}`;
        } else if (metadataRecord.described_entity.lr_subclass.lr_type === LCR || metadataRecord.described_entity.lr_subclass.lr_type === "LexicalConceptualResource") {
            url = `/lcr/${pk}`
        } else if ([LD, "LanguageDescription", ML_MODEL, GRAMMAR, Uncategorized_Language_Description, OTHER].includes(metadataRecord.described_entity.lr_subclass.lr_type)) {
            url = `/ld/${pk}`
        } else {
            console.log('Unknown metadata record type. ', JSON.stringify(metadataRecord));
        }
    } catch (error) {
        console.log(error);
    }
    return url;
}

export const get_landing_page_url_Editor = (pk, type) => {
    let url = "/";
    try {
        if (!pk || !type) {
            return url;
        }
        if (type === PROJECT) {
            url = `/project/${pk}`;
        } else if (type === ORGANIZATION) {
            url = `/organization/${pk}`
        } else if (type === TOOL_SERVICE || type === "ToolService") {
            url = `/tool-service/${pk}`
        } else if (type === CORPUS) {
            url = `/corpus/${pk}`;
        } else if (type === LCR || type === "LexicalConceptualResource") {
            url = `/lcr/${pk}`
        } else if ([LD, "LanguageDescription", ML_MODEL, GRAMMAR, Uncategorized_Language_Description, OTHER].includes(type)) {
            url = `/ld/${pk}`
        } else {
            console.log('Unknown metadata record type. ', type, pk);
        }
    } catch (error) {
        console.log(error);
    }
    return url;
}

export const getUrlToLandingPageFromCatalogue = (resource) => {
    let id = '';
    if (resource.detail) {
        var n = resource.detail.indexOf("/metadatarecord/");
        id = resource.detail.substring(n + 16, resource.detail.length - 1);
    } else if (resource.id) {
        id = resource.id;
    }
    let url = "/"
    if (resource.resource_type === TOOL_SERVICE || resource.resource_type === "ToolService") {
        url = `/tool-service/${id}`
    } else if (resource.resource_type === CORPUS) {
        url = `/corpus/${id}`
    } else if (resource.resource_type === LCR || resource.resource_type === "LexicalConceptualResource") {
        url = `/lcr/${id}`
    } else if ([LD, "LanguageDescription", ML_MODEL, GRAMMAR, N_GRAM_MODEL, Uncategorized_Language_Description, OTHER].includes(resource.resource_type)) {
        url = `/ld/${id}`
    } else if (resource.entity_type === ORGANIZATION) {
        url = `/organization/${id}`
    } else if (resource.entity_type === PROJECT) {
        url = `/project/${id}`
    } else {
        console.log("resource/entity type unspecified");
    }
    return url;
}


export const getEditorPath_labeless_Schema = (data) => {
    const { pk } = data;
    const entity_type = data.described_entity.entity_type;
    let editor_url = "";
    if (entity_type === "LanguageResource") {
        const lr_type = data.described_entity.lr_subclass.lr_type;
        switch (lr_type) {
            case "ToolService":
                editor_url = `/create/Service/${pk}/`;
                break;
            case TOOL_SERVICE:
                editor_url = `/create/Service/${pk}/`;
                break;
            case "Corpus":
                editor_url = `/create/Corpus/${pk}/`;
                break;
            case CORPUS:
                editor_url = `/create/Corpus/${pk}/`;
                break;
            case "LexicalConceptualResource":
                editor_url = `/create/lexical_conceptual_resource/${pk}/`;
                break;
            case LCR:
                editor_url = `/create/lexical_conceptual_resource/${pk}/`;
                break;
            case "LanguageDescription":
                editor_url = `/create/language_description/${pk}/`;
                break;
            case LD:
                editor_url = `/create/language_description/${pk}/`;
                break;
            case ML_MODEL:
                editor_url = `/create/language_description/${pk}/`;
                break;
            case GRAMMAR:
                editor_url = `/create/language_description/${pk}/`;
                break;
            case OTHER:
                editor_url = `/create/language_description/${pk}/`;
                break;
            case Uncategorized_Language_Description:
                editor_url = `/create/language_description/${pk}/`;
                break;
            default: break;
        }
    } else {
        switch (entity_type) {
            case "Project":
                editor_url = `/create/Project/${pk}/`;
                break;
            case "Organization":
                editor_url = `/create/Organization/${pk}/`;
                break;
            default: break;
        }
    }
    return editor_url;
}

export const SHOW_BILLING_CORPUS_PK = PRODUCTION ? "" : 684;
export const SHOW_BILLING_SERVICE_PK = PRODUCTION ? "" : 479;
//export const SYS_ADMIM_ID = 3; //this is the id for the elg-system